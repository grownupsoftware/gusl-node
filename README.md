![Alt text](https://www.googleapis.com/download/storage/v1/b/gusl-images/o/logo.png?generation=1588764693002370&alt=media)
# Node

Node is a project which is common to node web applications.

## Requirements
1. Java 11
2. Gradle 7

## Getting Started

```groovy

dependencies {
    implementation "co.gusl:gusl-core:${guslVersion}"
    implementation "co.gusl:gusl-node:${guslVersion}"
}

war {

    with copySpec {
        (configurations.runtimeClasspath).collect {
            if (it.toString().indexOf("gusl-node") > 0) {
                from(zipTree(it)) {
                    include 'WEB-APP/**'
                    includeEmptyDirs = false
                    eachFile { fcd -> fcd.relativePath = new RelativePath(true, fcd.relativePath.segments.drop(1))
                    }
                }
            }
        }
    }
}
```



