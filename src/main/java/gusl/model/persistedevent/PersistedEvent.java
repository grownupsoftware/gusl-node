package gusl.model.persistedevent;

import gusl.model.Auditable;

/**
 * @author grantwallace
 */
public interface PersistedEvent extends Auditable {

    public Long getPersistedEventId();

    public void setPersistedEventId(Long persistedEventId);

}
