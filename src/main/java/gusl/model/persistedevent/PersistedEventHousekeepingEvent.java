package gusl.model.persistedevent;

/**
 *
 * @author grantwallace
 */
public class PersistedEventHousekeepingEvent {

    private long timestamp;

    public PersistedEventHousekeepingEvent() {
        timestamp = System.currentTimeMillis();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
