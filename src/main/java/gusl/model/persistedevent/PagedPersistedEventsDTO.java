/* Copyright lottomart */
package gusl.model.persistedevent;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;

import java.util.List;

/**
 *
 * @author grant
 */
public class PagedPersistedEventsDTO { //extends AbstractPagedDTO {

    @ToStringCount
    private List<PersistedEventDO> content;

    public List<PersistedEventDO> getContent() {
        return content;
    }

    public void setContent(List<PersistedEventDO> content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
