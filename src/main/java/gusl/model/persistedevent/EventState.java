/* Copyright lottomart */
package gusl.model.persistedevent;

/**
 *
 * @author grantwallace
 */
public enum EventState {
    PENDING("PENDING"),
    FAILURE("FAILURE"),
    PROCESSED("PROCESSED");

    private final String name;

    EventState(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
