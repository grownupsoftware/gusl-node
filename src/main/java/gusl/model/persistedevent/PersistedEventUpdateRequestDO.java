package gusl.model.persistedevent;

/**
 *
 * @author grantwallace
 */
public class PersistedEventUpdateRequestDO {

    private Long id;

    private Integer processedByNodeId;

    private EventState state;

    private String errorMessage;

    public PersistedEventUpdateRequestDO() {
    }

    public PersistedEventUpdateRequestDO(Long id, Integer processedByNodeId, EventState state, String errorMessage) {
        this.id = id;
        this.processedByNodeId = processedByNodeId;
        this.state = state;
        this.errorMessage = errorMessage;
    }

    public PersistedEventUpdateRequestDO(Long id, Integer processedByNodeId, EventState state) {
        this.id = id;
        this.processedByNodeId = processedByNodeId;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProcessedByNodeId() {
        return processedByNodeId;
    }

    public void setProcessedByNodeId(Integer processedByNodeId) {
        this.processedByNodeId = processedByNodeId;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
