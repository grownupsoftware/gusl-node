package gusl.model.persistedevent;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.model.AuditableDO;
import gusl.model.nodeconfig.NodeType;

/**
 * @author grantwallace
 */
@DocClass(description = "Persisted event")
public class PersistedEventDO extends AuditableDO {

    @DocField(description = "Unique Id")
    private Long id;

    @DocField(description = "Publisher node type")
    private NodeType publisherNodeType;

    @DocField(description = "Event created by node")
    private Integer publisherNodeId;

    @DocField(description = "Class name of event")
    private String eventClassName;

    @DocField(description = "mapper output of message")
    private String eventBody;

    @DocField(description = "Requires subscriber of node type")
    private NodeType subscriberNodeType;

    @DocField(description = "processed by node")
    private Integer processedByNodeId;

    @DocField(description = "Current state of event")
    private EventState state;

    @DocField(description = "List of messages - possible failer messages")
    private PersistedEventMessagesDO infoMessages;

    public PersistedEventDO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPublisherNodeId() {
        return publisherNodeId;
    }

    public void setPublisherNodeId(Integer publisherNodeId) {
        this.publisherNodeId = publisherNodeId;
    }

    public String getEventClassName() {
        return eventClassName;
    }

    public void setEventClassName(String eventClassName) {
        this.eventClassName = eventClassName;
    }

    public String getEventBody() {
        return eventBody;
    }

    public void setEventBody(String eventBody) {
        this.eventBody = eventBody;
    }

    public Integer getProcessedByNodeId() {
        return processedByNodeId;
    }

    public void setProcessedByNodeId(Integer processedByNodeId) {
        this.processedByNodeId = processedByNodeId;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public PersistedEventMessagesDO getInfoMessages() {
        return infoMessages;
    }

    public void setInfoMessages(PersistedEventMessagesDO infoMessages) {
        this.infoMessages = infoMessages;
    }

    public NodeType getSubscriberNodeType() {
        return subscriberNodeType;
    }

    public void setSubscriberNodeType(NodeType subscriberNodeType) {
        this.subscriberNodeType = subscriberNodeType;
    }

    public NodeType getPublisherNodeType() {
        return publisherNodeType;
    }

    public void setPublisherNodeType(NodeType publisherNodeType) {
        this.publisherNodeType = publisherNodeType;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
