package gusl.model.persistedevent;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author grantwallace
 */
public class PersistedEventMessagesDO {

    private List<String> messages = new ArrayList<>();

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

}
