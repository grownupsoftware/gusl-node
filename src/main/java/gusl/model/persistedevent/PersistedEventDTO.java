/* Copyright lottomart */
package gusl.model.persistedevent;

import gusl.core.annotations.DocClass;
import gusl.core.tostring.ToString;

@DocClass(description = "")
public class PersistedEventDTO {

//    @UiKey
//    @DocField(description = "Unique Id")
//    @UiField(type = FieldType.number, displayInTable = true)
//    private Long id;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.date_time, label = "Created", displayInTable = true, dateFormat = UiDateFormat.FULL_DATE_AND_TIME)
//    private Date dateCreated;
//
//    @DocField(description = "Publisher node type")
//    @UiField(type = FieldType.option, displayInTable = true)
//    private NodeType publisherNodeType;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.number, displayInTable = true)
//    private Integer publisherNodeId;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.text, displayInTable = true)
//    private String eventClassName;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.properties)
//    private String eventBody;
//
//    @DocField(description = "Requires subscriber of node type")
//    @UiField(type = FieldType.option, displayInTable = true)
//    private NodeType subscriberNodeType;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.number, displayInTable = true)
//    private Integer processedByNodeId;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.date_time, label = "Changed", displayInTable = true, dateFormat = UiDateFormat.FULL_DATE_AND_TIME)
//    private Date dateChanged;
//
//    @DocField(description = "")
//    @UiField(type = FieldType.option, displayInTable = true)
//    private EventState state;



    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
