package gusl.model.persistedevent;

import gusl.core.annotations.DocField;

/**
 *
 * @author grantwallace
 */
public class AbstractBasePersistedEventDO {

    @DocField(description = "Unique Id")
    private Long persistedEventId;

    public Long getPersistedEventId() {
        return persistedEventId;
    }

    public void setPersistedEventId(Long persistedEventId) {
        this.persistedEventId = persistedEventId;
    }

}
