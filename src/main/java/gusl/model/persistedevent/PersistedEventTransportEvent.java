package gusl.model.persistedevent;

/**
 *
 * @author grantwallace
 */
public class PersistedEventTransportEvent {

    private PersistedEventDO persistedEvent;

    public PersistedEventTransportEvent() {
    }

    public PersistedEventTransportEvent(PersistedEventDO persistedEvent) {
        this.persistedEvent = persistedEvent;
    }

    public PersistedEventDO getPersistedEvent() {
        return persistedEvent;
    }

    public void setPersistedEvent(PersistedEventDO persistedEvent) {
        this.persistedEvent = persistedEvent;
    }

}
