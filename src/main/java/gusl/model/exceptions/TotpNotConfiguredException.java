/* Copyright lottomart */
package gusl.model.exceptions;

import gusl.core.errors.ErrorDO;

import java.util.ArrayList;
import java.util.List;

public class TotpNotConfiguredException extends RuntimeException {

    private List<ErrorDO> errors = new ArrayList<>(1);

    public TotpNotConfiguredException(String message, Throwable ex) {
        super(message, ex);
    }

    public TotpNotConfiguredException(String message) {
        super(message);
    }

    public TotpNotConfiguredException(ErrorDO error) {
        super(error.toString());
        this.errors.add(error);
    }

    public TotpNotConfiguredException(ErrorDO error, Throwable cause) {
        super(error.toString(), cause);
        this.errors.add(error);
    }

    public ErrorDO getError() {
        if (!errors.isEmpty()) {
            return errors.get(0);
        } else {
            return new ErrorDO(null, getMessage(), "internal.error");
        }
    }

}
