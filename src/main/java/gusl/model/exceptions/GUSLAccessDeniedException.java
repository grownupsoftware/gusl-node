/* Copyright lottomart */
package gusl.model.exceptions;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.errors.ErrorDO;

/**
 * non runtime exception
 *
 * @author grant
 */
public class GUSLAccessDeniedException extends GUSLErrorException {

    private static final long serialVersionUID = 1L;

    public GUSLAccessDeniedException() {
    }

    public GUSLAccessDeniedException(ErrorDO error) {
        super(error);
    }
}
