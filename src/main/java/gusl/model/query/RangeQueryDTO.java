package gusl.model.query;

import gusl.core.tostring.ToString;
import gusl.query.RangeQueryType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 21/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class RangeQueryDTO {

    private RangeQueryType type;
    private String formatHint;
    private boolean exclusive;
    private String field;
    private String from;
    private String to;
    private String gt;
    private String lt;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
