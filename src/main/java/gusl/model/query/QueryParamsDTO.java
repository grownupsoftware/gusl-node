package gusl.model.query;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

/**
 * @author dhudson
 * @since 21/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class QueryParamsDTO {
    // -1 is as many as the Database will allow, 10K for Elastic
    // 0 is a valid limit for elastic
    private int limit = -1;
    private Set<OrderByDTO> orderBys;
    private int skip;

    private Set<RangeQueryDTO> rangeQueries;
    private Set<MatchQueryDTO> musts;
    private Set<MatchQueryDTO> mustNots;
    private Set<MatchQueryDTO> should;
    private Set<InQueryDTO> ins;
    private Set<String> distinct;
    private boolean firstAndLast;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
