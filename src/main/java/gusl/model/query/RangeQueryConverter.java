package gusl.model.query;

import gusl.core.dates.DateMathParser;
import gusl.core.dates.GUSLDateException;
import gusl.core.exceptions.GUSLErrorException;
import gusl.query.DateRangeQuery;
import gusl.query.NumberRangeQuery;
import gusl.query.RangeQuery;
import gusl.query.RangeQueryType;
import lombok.CustomLog;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author dhudson
 * @since 08/08/2023
 */
@CustomLog
public class RangeQueryConverter extends CustomConverter<RangeQueryDTO, RangeQuery> {

    @Override
    public RangeQuery convert(RangeQueryDTO rangeQueryDTO, Type<? extends RangeQuery> type, MappingContext mappingContext) {
        if (rangeQueryDTO.getType() == null || rangeQueryDTO.getType() == RangeQueryType.DATE) {
            return createDateRangeQuery(rangeQueryDTO);
        }
        return createNumberRangeQuery(rangeQueryDTO);
    }

    private DateRangeQuery createDateRangeQuery(RangeQueryDTO from) {
        DateRangeQuery rangeQuery = new DateRangeQuery(from.getField());
        rangeQuery.setFormatHint(from.getFormatHint());
        try {
            rangeQuery.setFrom(convertFromDateString(from.getFrom()));
            rangeQuery.setTo(convertFromDateString(from.getTo()));
            rangeQuery.setGreaterThan(convertFromDateString(from.getGt()));
            rangeQuery.setLessThan(convertFromDateString(from.getLt()));
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to create Date Range Query {}", from);
        }
        return rangeQuery;
    }

    private NumberRangeQuery createNumberRangeQuery(RangeQueryDTO from) {
        NumberRangeQuery rangeQuery = new NumberRangeQuery(from.getField());
        rangeQuery.setFormatHint(from.getFormatHint());
        try {
            rangeQuery.setFrom(convertFromNumberString(from.getFrom()));
            rangeQuery.setTo(convertFromNumberString(from.getTo()));

            rangeQuery.setGreaterThan(convertFromNumberString(from.getGt()));
            rangeQuery.setLessThan(convertFromNumberString(from.getLt()));
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to create Number Range Query {}", from);
        }
        return rangeQuery;
    }

    private Number convertFromNumberString(String value) {
        if (value != null) {
            try {
                return NumberFormat.getInstance().parse(value);
            } catch (ParseException ex) {
                logger.warn("Unable to parse number {}", value);
            }
        }
        return null;
    }

    private Object convertFromDateString(String value) {
        if (value != null) {
            try {
                return LocalDateTime.parse(value, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            } catch (DateTimeException ignore) {
                try {
                    return DateMathParser.parse(value);
                } catch (GUSLDateException ex) {
                    logger.warn("Unable to parse date {}", value);
                }
            }
        }
        return null;
    }
}
