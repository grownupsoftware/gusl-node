package gusl.model.query;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 21/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class MatchQueryDTO {

    private String field;
    private String value;
    private boolean fuzzy;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
