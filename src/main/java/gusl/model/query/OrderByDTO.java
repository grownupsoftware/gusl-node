package gusl.model.query;

import gusl.core.tostring.ToString;
import gusl.query.QueryOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 21/02/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class OrderByDTO {

    private String field;
    private QueryOrder order;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
