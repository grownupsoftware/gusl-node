package gusl.model.query;

import gusl.node.converter.ModelConverterConfigurator;
import gusl.query.InQuery;
import gusl.query.MatchQuery;
import gusl.query.OrderBy;
import gusl.query.QueryParams;
import lombok.CustomLog;
import ma.glasnost.orika.MapperFactory;
import org.jvnet.hk2.annotations.Service;

/**
 * @author dhudson
 * @since 21/02/2022
 */
@Service
@CustomLog
public class QueryModelMapper implements ModelConverterConfigurator {

    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.getConverterFactory().registerConverter(new RangeQueryConverter());
        mapperFactory.classMap(MatchQueryDTO.class, MatchQuery.class).byDefault().register();
        mapperFactory.classMap(OrderByDTO.class, OrderBy.class).byDefault().register();
        mapperFactory.classMap(InQueryDTO.class, InQuery.class).byDefault().register();
        mapperFactory.classMap(QueryParamsDTO.class, QueryParams.class).byDefault().register();
    }

}
