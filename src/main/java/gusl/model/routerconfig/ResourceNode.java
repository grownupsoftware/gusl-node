package gusl.model.routerconfig;

import gusl.core.tostring.ToString;
import gusl.core.utils.Utils;
import gusl.model.cache.CacheGroup;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Represents a Node within a Casanova Construct.
 *
 * @author dhudson
 */
public class ResourceNode {

    public static final Comparator<ResourceNode> IDSORT = Comparator.comparingInt(ResourceNode::getNodeId);

    private String host;
    private int port;
    private NodeType type;
    private String applicationUrl;
    private String contextUrl;
    private String baseUrl;
    private int nodeId;
    private NodeStatus status;
    private long lastUpdated;
    private boolean master;
    private String version;
    private List<CacheGroup> cacheGroups;

    public ResourceNode() {
    }

    public ResourceNode(String host, int port, NodeType type, int nodeId) {
        this.host = host;
        this.port = port;
        this.type = type;
        this.nodeId = nodeId;
    }

    public void setStatus(NodeStatus status) {
        touch();
        this.status = status;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public NodeType getType() {
        return type;
    }

    public int getNodeId() {
        return nodeId;
    }

    public NodeStatus getStatus() {
        return status;
    }

    public void touch() {
        lastUpdated = System.currentTimeMillis();
    }

    public void setLastUpdated(long time) {
        lastUpdated = time;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public String getLastUpdatedTime() {
        return Utils.formatDateTime(lastUpdated);
    }

    public boolean getMaster() {
        return master;
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public List<CacheGroup> getCacheGroups() {
        return cacheGroups;
    }

    public void setCacheGroups(List<CacheGroup> cacheGroups) {
        this.cacheGroups = cacheGroups;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(host);
        hash = 53 * hash + port;
        hash = 53 * hash + Objects.hashCode(type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceNode other = (ResourceNode) obj;
        if (port != other.port) {
            return false;
        }
        if (!Objects.equals(host, other.host)) {
            return false;
        }
        return type == other.type;
    }

    public String getApplicationUrl() {
        if (applicationUrl == null) {
            buildURLs();
        }
        return applicationUrl;
    }

    public String getContextUrl() {
        if (contextUrl == null) {
            buildURLs();
        }
        return contextUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private void buildURLs() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("http://");
        builder.append(host);
        builder.append(":");
        builder.append(port);
        baseUrl = builder.toString();

        if (type != null) {
            builder.append("/");
            builder.append(type.toString().toLowerCase());
        }
        contextUrl = builder.toString();
        builder.append("/rest");

        applicationUrl = builder.toString();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String infoString() {
        return nodeId + " : " + type;
    }
}
