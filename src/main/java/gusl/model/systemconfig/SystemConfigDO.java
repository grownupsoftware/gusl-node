package gusl.model.systemconfig;

import gusl.core.tostring.ToString;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;


@NoArgsConstructor
@Getter
@Setter
public class SystemConfigDO implements SystemConfig {

    public static final int VERSION = 1;
    public static final String DEFAULT_ID = "1";

    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    private String id;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true, notNull = true)
    private LocalDateTime dateCreated;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true, notNull = true)
    private LocalDateTime dateUpdated;

    @PostgresField(type = PostgresFieldType.JSONB)
    private SystemConfigPropertiesDO properties;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
