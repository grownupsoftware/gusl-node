package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AutoGradingTimesDO {

    @UiField(type = FieldType.nested_table, label = "Auto Grading Times", canAdd = "true")
    private List<AutoTimeDO> gradingTimes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
