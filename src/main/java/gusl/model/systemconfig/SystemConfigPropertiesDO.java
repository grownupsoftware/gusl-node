package gusl.model.systemconfig;

import gusl.annotations.form.UiPosition;
import gusl.core.tostring.ToString;
import gusl.model.systemconfig.email.EmailSettingsConfigDO;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemConfigPropertiesDO {

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/session", header = "Session")
    private SessionConfigDO sessionConfig;

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/geofence", header = "Geo Fence")
    private String geoFenceId;

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/frontend", header = "Frontend")
    private FrontendConfigDO frontendConfig;

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/gradesettle", header = "Grading and Settling")
    private GradeAndSettleConfigDO gradeAndSettleConfig;

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/backoffice", header = "Backoffice")
    private BackofficeConfigDO backofficeConfig;

    @UiPosition(fxFlex = 32, nested = true, updateUrl = "/system/v1/email-settings", header = "Email Settings")
    private EmailSettingsConfigDO emailSettingsConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
