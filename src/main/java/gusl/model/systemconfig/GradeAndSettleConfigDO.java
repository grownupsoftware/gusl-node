package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GradeAndSettleConfigDO {

    @UiField(type = FieldType.checkbox)
    private Boolean autoGradeResulted;

    @UiField(type = FieldType.nested_table, label = "Auto Grading Times", canAdd = "true")
    private List<AutoTimeDO> gradingTimes;

    @UiField(type = FieldType.checkbox) // not used - present to fix a layout bug with the BO
    private Boolean autoSettle;

    @UiField(type = FieldType.nested_table, label = "Auto Settle Times", canAdd = "true")
    private List<AutoTimeDO> settlingTimes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
