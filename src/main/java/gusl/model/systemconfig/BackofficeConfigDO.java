package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BackofficeConfigDO {
    @UiField(type = FieldType.checkbox)
    private Boolean autoInsertOtc;

    @UiField(type = FieldType.number)
    private Integer numberBetsInTicker;

    @DocField(description = "Time mm:ss added to UTC end of day to give system end of day (used in customer trading summary) i.e. Canada ET")
    @UiField(type = FieldType.time, displayInTable = true, label = "Trading offset time")
    private String tradingOffset;

    @DocField(description = "If 'true' then 'TradingOffset' taken as 'start of next day' plus offset, otherwise it is current day' 'start of day' plus offset")
    @UiField(type = FieldType.checkbox)
    private Boolean tradingOffsetAfterUtc;

    @DocField(description = "Event lookup - limits events # days in the future")
    @UiField(type = FieldType.number)
    private Integer lookupNumberDaysBefore;

    @DocField(description = "Event lookup - limits events # days in the past")
    @UiField(type = FieldType.number)
    private Integer lookupNumberDaysAfter;

    @DocField(description = "Event lookup - limits events # days in the future")
    @UiField(type = FieldType.number)
    private Integer tickerNumberDaysBefore;

    @DocField(description = "Event ticker - limits events # days in the past")
    @UiField(type = FieldType.number)
    private Integer tickerNumberDaysAfter;

    @DocField(description = "If 'true' then when it is possible then auto create an outcome ")
    @UiField(type = FieldType.checkbox)
    private Boolean autoCreateOutcome;

    @DocField(description = "Account Id for betonline for otc paste screen")
    @UiField(type = FieldType.text)
    private String betonlineId;

    @DocField(description = "Account Id for lowvig for otc paste screen")
    @UiField(type = FieldType.text)
    private String lowvigId;

    @DocField(description = "Minutes to keep polling after event has started to perform WebAccount scrapes")
    @UiField(type = FieldType.number)
    private Integer pollMinutesAfterEventStart;

    @DocField(description = "Interval in minutes to poll WebAccounts")
    @UiField(type = FieldType.number)
    private Integer pollInterval;

    @DocField(description = "Allow interval based web site account scraping")
    @UiField(type = FieldType.checkbox)
    private Boolean allowAutoWebSiteScraping;


    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
