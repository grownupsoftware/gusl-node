package gusl.model.systemconfig.email;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmailSettingsConfigDO {

    @UiField(type = FieldType.option)
    private EmailProviderType type;

    @UiField(type = FieldType.text)
    private String iamUsername;

    @UiField(type = FieldType.text)
    private String smtpHost;

    @UiField(type = FieldType.text)
    private String smtpPort;

    @UiField(type = FieldType.text)
    private String smtpUsername;

    @UiField(type = FieldType.text)
    private String smtpPassword;

    @UiField(type = FieldType.text)
    private String smtpSsl;

    @UiField(type = FieldType.checkbox)
    private boolean startTls;

    @UiField(type = FieldType.checkbox)
    private boolean auth;

    @UiField(type = FieldType.text)
    private String sslTrust;

    @UiField(type = FieldType.text)
    private String region;

    @UiField(type = FieldType.text)
    private String bcc;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
