package gusl.model.systemconfig.email;

public enum EmailProviderType {
    AWS,
    GOOGLE,
    SMTP
}
