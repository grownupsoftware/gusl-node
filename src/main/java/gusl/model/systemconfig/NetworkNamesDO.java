package gusl.model.systemconfig;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NetworkNamesDO {

    private String name;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
