package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AutoTimeDO {

    private Integer index;

    @UiField(type = FieldType.time, displayInTable = true, label = "UTC Time")
    private String time;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
