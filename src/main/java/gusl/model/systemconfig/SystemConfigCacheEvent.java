package gusl.model.systemconfig;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SystemConfigCacheEvent extends AbstractCacheActionEvent<SystemConfigDO> {

    public SystemConfigCacheEvent() {
    }

    public SystemConfigCacheEvent(CacheAction action, SystemConfigDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.SYSTEM;
    }
}
