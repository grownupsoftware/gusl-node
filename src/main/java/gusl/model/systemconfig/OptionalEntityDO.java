package gusl.model.systemconfig;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OptionalEntityDO<T> {
    private T content;

    public static <T> OptionalEntityDO<T> empty() {
        return new OptionalEntityDO<>();
    }

    @JsonIgnore
    public Optional<T> getContentOpt() {
        return Optional.ofNullable(content);
    }

}
