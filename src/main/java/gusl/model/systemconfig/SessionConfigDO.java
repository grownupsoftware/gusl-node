package gusl.model.systemconfig;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.config.ConfigUtils;
import gusl.core.exceptions.GUSLException;
import gusl.core.tostring.ToString;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SessionConfigDO {

    private static final String DEFAULT_SESSION_TIMEOUT = "10m";
    private static final String PASSWORD_RESET_TIMEOUT = "6h";
    private static final String SESSION_RENEW_TIME = "30s";

    private static final String VALIDATION_LINK_DURATION = "1h";

    @UiField(type = FieldType.text, label = "Session Timeout")
    @NotNull
    private String sessionTimeout;

    @UiField(type = FieldType.text, label = "Password Reset Timeout")
    @NotNull
    private String passwordResetTimeout;

    @UiField(type = FieldType.text, label = "Session Renew")
    @NotNull
    private String sessionRenew;

    @UiField(type = FieldType.text, label = "Validation link duration")
    private String validationLinkDuration;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @JsonIgnore
    private long getMillisWithDefault(String value, String defaultMillis) throws GUSLException {
        try {
            return ConfigUtils.millisNumberResolver(value);
        } catch (Throwable t) {
            return ConfigUtils.millisNumberResolver(defaultMillis);
        }
    }

    @JsonIgnore
    public long getSessionTimeoutAsMilli() throws GUSLException {
        return getMillisWithDefault(sessionTimeout, DEFAULT_SESSION_TIMEOUT);
    }

    @JsonIgnore
    public long getPasswordResetAsMilli() throws GUSLException {
        return getMillisWithDefault(passwordResetTimeout, PASSWORD_RESET_TIMEOUT);
    }

    @JsonIgnore
    public long getSessionRenewAsMilli() throws GUSLException {
        return getMillisWithDefault(sessionRenew, SESSION_RENEW_TIME);
    }

}
