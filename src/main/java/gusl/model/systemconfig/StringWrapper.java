package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StringWrapper {

    @UiField(type = FieldType.text)
    private String string;

}
