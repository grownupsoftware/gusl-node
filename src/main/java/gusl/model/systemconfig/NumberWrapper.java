package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NumberWrapper {

    @UiField(type = FieldType.text, displayInTable = true)
    private Long number;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
