package gusl.model.systemconfig;

import gusl.annotations.form.FieldType;
import gusl.annotations.form.UiField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FrontendConfigDO {

    @UiField(type = FieldType.checkbox)
    private Boolean showPrices;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
