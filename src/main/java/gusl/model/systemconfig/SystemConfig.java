package gusl.model.systemconfig;

import gusl.model.Identifiable;

import java.time.LocalDateTime;

public interface SystemConfig extends Identifiable<String> {

    String getId();

    LocalDateTime getDateUpdated();

    SystemConfigPropertiesDO getProperties();

}
