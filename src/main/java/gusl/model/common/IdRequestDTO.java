package gusl.model.common;

import gusl.core.tostring.ToString;
import lombok.*;

import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IdRequestDTO {

    @NotNull
    private String id;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
