package gusl.model.country;

import gusl.model.Identifiable;

public interface Country extends Identifiable<String> {

    String getId();

    String getCode();

    String getName();

    CountryPropertyDO getProperties();

    CountryStatus getStatus();

    String getAlpha3();

    String getDialCode();

    Boolean getTopCountry();

    Boolean getHideInRegistration();

    Boolean getBlockInRegistration();

    Boolean getBlockAccess();

    Boolean getSupportPostCode();

    Integer getTopOrder();

    Boolean getMobileLookup();

}
