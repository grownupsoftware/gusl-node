package gusl.model.country;

import gusl.annotations.form.page.AbstractPageResponse;
import gusl.annotations.form.page.PageResponse;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 * @since 23/07/2021
 */
@NoArgsConstructor
@Getter
@Setter
public class PagedCountryDO extends AbstractPageResponse implements PageResponse<CountryDO> {

    @ToStringCount
    private List<CountryDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
