package gusl.model.country;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheGroup;

public class CountryCacheEvent extends AbstractCacheActionEvent<CountryDO> {

    public CountryCacheEvent() {
    }

    public CountryCacheEvent(CacheAction action, CountryDO entity) {
        super(action, entity);
    }

    @Override
    public CacheGroup getCacheGroup() {
        return CacheGroup.APP;
    }
}
