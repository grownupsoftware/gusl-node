package gusl.model.country;

public enum CountryStatus {
    ACTIVE,
    IN_ACTIVE
}
