package gusl.model.country;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CountriesDO {

    @ToStringCount
    @Singular
    private List<CountryDO> countries;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
