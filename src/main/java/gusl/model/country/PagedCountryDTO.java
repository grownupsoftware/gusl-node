package gusl.model.country;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;
import gusl.model.dataservice.dtos.paged.AbstractPagedDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class PagedCountryDTO extends AbstractPagedDTO {

    @ToStringCount
    private List<CountryDO> content;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
