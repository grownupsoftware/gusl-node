package gusl.model.country;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.elastic.model.ESType;
import gusl.elastic.model.ElasticField;
import gusl.postgres.model.PostgresField;
import gusl.postgres.model.PostgresFieldType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@NoArgsConstructor
@Getter
@Setter
public class CountryDO implements Country {

    public static final int VERSION = 1;

    @DocField(description = "ISO3166-1-Alpha-2")
    @PostgresField(type = PostgresFieldType.TEXT, primaryKey = true, notNull = true)
    @ElasticField(type = ESType.KEYWORD, populate = true, primaryKey = true)
    private String code;

    @DocField(description = "Country Name")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String name;

    @DocField(description = "ISO3166-1-Alpha-3")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String alpha3;

    @DocField(description = "Dial Code")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String dialCode;

    @DocField(description = "Is it a 'top' country")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean topCountry;

    @DocField(description = "Do not show country in registration")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean hideInRegistration;

    @DocField(description = "Block country in registration")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean blockInRegistration;

    @DocField(description = "Block country access")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean blockAccess;

    @DocField(description = "ID of the Currency for this country")
    @PostgresField(type = PostgresFieldType.TEXT)
    @ElasticField(type = ESType.KEYWORD)
    private String currencyCode;

    @DocField(description = "Country supports post code")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean supportPostCode;

    @DocField(description = "Ordering for top countries")
    @PostgresField(type = PostgresFieldType.INTEGER)
    @ElasticField(type = ESType.INTEGER)
    private Integer topOrder;

    @DocField(description = "Use country in mobile lookup")
    @PostgresField(type = PostgresFieldType.BOOLEAN)
    @ElasticField(type = ESType.BOOLEAN)
    private Boolean mobileLookup;

    @PostgresField(type = PostgresFieldType.TEXT, notNull = true)
    @ElasticField(type = ESType.KEYWORD)
    private CountryStatus status;

    @PostgresField(type = PostgresFieldType.JSONB)
    @ElasticField(type = ESType.OBJECT)
    private CountryPropertyDO properties;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateCreated = true)
    private LocalDateTime dateCreated;

    @PostgresField(type = PostgresFieldType.TIMESTAMP, dateUpdated = true)
    private LocalDateTime dateUpdated;

    @ElasticField(type = ESType.DATE, dateCreated = true)
    private ZonedDateTime createdAt;

    @ElasticField(type = ESType.DATE, dateUpdated = true)
    private ZonedDateTime updatedAt;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    @Override
    public String getId() {
        return code;
    }

    @Override
    public void setId(String code) {
        this.code = code;
    }
}
