package gusl.model.latency;

import java.util.List;
import java.util.Map;

public class LatencyResponseDO {

    private List<Map<String, Object>> metrics;

    public LatencyResponseDO() {
    }

    public LatencyResponseDO(List<Map<String, Object>> metrics) {
        this.metrics = metrics;
    }

    public List<Map<String, Object>> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<Map<String, Object>> metrics) {
        this.metrics = metrics;
    }
}
