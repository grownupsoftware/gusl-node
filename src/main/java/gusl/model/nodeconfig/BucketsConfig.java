package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson on 2019-07-22
 */
@Getter
@Setter
@NoArgsConstructor
public class BucketsConfig {

    private String serviceFile;
    private List<BucketConfig> buckets;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
