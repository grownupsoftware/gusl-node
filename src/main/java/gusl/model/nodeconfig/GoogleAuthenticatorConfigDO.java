package gusl.model.nodeconfig;

import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GoogleAuthenticatorConfigDO {

    @DocField(description = "Qr Code generator endpoint")
    private String uriFormat;

    @DocField(description = "Issue")
    private String issuer;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
