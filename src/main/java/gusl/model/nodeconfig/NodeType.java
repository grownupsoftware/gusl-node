package gusl.model.nodeconfig;

/**
 * Nodes
 * <p>
 * NB: These Types must reflect the context of the webapp.
 * </p>
 *
 * @author dhudson
 */
public enum NodeType {
    AUTH,
    EDGE,
    DATASERVICE,
    FIXER,
    LEDGER,
    MARKET,
    ROUTER,
    BACKOFFICE,
    GATEWAY
}
