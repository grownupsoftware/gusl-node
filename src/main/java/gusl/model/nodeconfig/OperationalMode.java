package gusl.model.nodeconfig;

/**
 * List of available modes.
 *
 * @author dhudson
 */
public enum OperationalMode {

    DEV,
    QA,
    TEST,
    PROD
}
