package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PrometheusConfig {
    private boolean active;
    private boolean addJvmMetrics;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
