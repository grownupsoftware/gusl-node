package gusl.model.nodeconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FrontendConfig {

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String publicUrl;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String title;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
