package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import gusl.core.utils.SystemPropertyUtils;
import gusl.model.cache.CacheGroup;
import gusl.model.routerconfig.ResourceNode;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.util.List;

import static gusl.model.nodeconfig.InstanceType.UK;
import static gusl.model.nodeconfig.OperationalMode.PROD;

/**
 * Details for the Node.
 * <p>
 * Pojo which can hold details of the node.
 *
 * @author dhudson
 */
@Service
@CustomLog
public class NodeDetails {

    private String theVersion;
    private String theReleaseDate;
    private String theWebContext;
    private String theBranchName;
    private NodeStatus theStatus;
    private final OperationalMode theMode;
    private volatile int theNodeId;
    private NodeType theNodeType;
    private boolean isMaster;
    // Whats required for this node
    private List<NodeType> theRequiredNodes;
    private List<CacheGroup> theCacheGroups;

    // If this is set to true, and splashRequired is true all HTTP requests will send a 503
    private boolean isSplash = false;
    private boolean splashRequired;

    private InstanceType theInstanceType;
    private List<ResourceNode> nodes;
    private final String constructName;

    public NodeDetails() {
        String operationalMode = SystemPropertyUtils.getOperationalMode();
        if (operationalMode == null) {
            // Its not set, so lets assume PROD
            theMode = PROD;
        } else {
            theMode = OperationalMode.valueOf(operationalMode.toUpperCase());
        }
        String type = SystemPropertyUtils.getInstanceType();
        if (type == null) {
            theInstanceType = UK;
        } else {
            theInstanceType = InstanceType.valueOf(type.toUpperCase());
        }
        constructName = SystemPropertyUtils.getConstructName();
    }

    public String getVersion() {
        return theVersion;
    }

    public void setVersion(String version) {
        theVersion = version;
    }

    public String getBranchName() {
        return theBranchName;
    }

    public void setBranchName(String theBranchName) {
        this.theBranchName = theBranchName;
    }

    public String getReleaseDate() {
        return theReleaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        theReleaseDate = releaseDate;
    }

    public String getWebContext() {
        return theWebContext;
    }

    public void setWebContext(String webContext) {
        theWebContext = webContext;
    }

    public OperationalMode getOperationalMode() {
        return theMode;
    }

    public int getNodeId() {
        return theNodeId;
    }

    public void setNodeId(int nodeId) {
        theNodeId = nodeId;
    }

    public InstanceType getInstanceType() {
        return theInstanceType;
    }

    public NodeStatus getStatus() {
        return theStatus;
    }

    public void setStatus(NodeStatus status) {
        theStatus = status;
    }

    public NodeMetrics getNodeMetrics() {
        return NodeMetrics.populateFromPlatform();
    }

    public NodeType getNodeType() {
        return theNodeType;
    }

    public void setNodeType(NodeType nodeType) {
        theNodeType = nodeType;
    }

    public boolean isMaster() {
        return isMaster;
    }

    public void setMaster(boolean isMaster) {
        this.isMaster = isMaster;
    }

    public List<NodeType> getRequiredNodes() {
        return theRequiredNodes;
    }

    public void setRequiredNodes(List<NodeType> requiredNodes) {
        this.theRequiredNodes = requiredNodes;
    }

    public List<CacheGroup> getCacheGroups() {
        return theCacheGroups;
    }

    public void setCacheGroups(List<CacheGroup> groups) {
        theCacheGroups = groups;
    }

    public boolean isIsSplash() {
        return isSplash;
    }

    public void setIsSplash(boolean isSplash) {
        this.isSplash = isSplash;
    }

    public boolean isSplashRequired() {
        return splashRequired;
    }

    public void setSplashRequired(boolean splashRequired) {
        this.splashRequired = splashRequired;
    }

    public List<ResourceNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<ResourceNode> nodes) {
        this.nodes = nodes;
    }

    public String getConstructName() {
        return constructName;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
