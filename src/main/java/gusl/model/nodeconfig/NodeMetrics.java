package gusl.model.nodeconfig;

import gusl.core.utils.Platform;

/**
 *
 * @author dhudson
 */
public class NodeMetrics {

    private long maxMemory;
    private long usedMemory;
    private long upTime;
    private double systemLoad;

    public NodeMetrics() {
    }

    public static NodeMetrics populateFromPlatform() {
        NodeMetrics nodeMetrics = new NodeMetrics();
        nodeMetrics.setMaxMemory(Platform.getMaxMemory());
        nodeMetrics.setUsedMemory(Platform.getUsedMemory());
        nodeMetrics.setUpTime(Platform.getUpTime());
        nodeMetrics.setSystemLoad(Platform.getSystemLoad());
        return nodeMetrics;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(long theMaxMemory) {
        this.maxMemory = theMaxMemory;
    }

    public long getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(long theUsedMemory) {
        this.usedMemory = theUsedMemory;
    }

    public long getUpTime() {
        return upTime;
    }

    public void setUpTime(long theUpTime) {
        this.upTime = theUpTime;
    }

    public double getSystemLoad() {
        return systemLoad;
    }

    public void setSystemLoad(double theSystemLoad) {
        this.systemLoad = theSystemLoad;
    }

    @Override
    public String toString() {
        return "NodeMetrics{" + "maxMemory=" + maxMemory + ", usedMemory=" + usedMemory + ", upTime=" + upTime + ", systemLoad=" + systemLoad + '}';
    }

}
