package gusl.model.nodeconfig;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson on 18/07/2020
 */
@NoArgsConstructor
@Getter
@Setter
public class SheetsConfig {
    private String serviceAccount;
}
