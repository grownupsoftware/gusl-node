package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;

/**
 * @author dhudson on 2019-08-08
 */
public class BucketConfig {

    private String name;
    private BucketType type;
    private String subscriber;

    public BucketConfig() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BucketType getType() {
        return type;
    }

    public void setType(BucketType type) {
        this.type = type;
    }

    public String getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(String subscriber) {
        this.subscriber = subscriber;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
