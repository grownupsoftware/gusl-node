package gusl.model.nodeconfig;

/**
 * @author dhudson
 * @since 01/09/2022
 */
public enum MessageService {
    SLACK,
    TEAMS
}
