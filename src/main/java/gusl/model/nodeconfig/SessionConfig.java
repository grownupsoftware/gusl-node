package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SessionConfig {

    private String sessionTimeout;
    private String passwordResetTimeout;
    private String sessionRenew;
    private String validationLinkDuration;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
