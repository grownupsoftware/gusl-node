package gusl.model.nodeconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;

/**
 *
 * @author dhudson
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class RouterClientConfig {

    private HttpClientConfig httpClientConfig;
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String url;
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    // This is host and port
    private String registraAddress;

    public RouterClientConfig() {
    }

    public HttpClientConfig getHttpClientConfig() {
        return httpClientConfig;
    }

    public void setClientConfig(HttpClientConfig clientConfig) {
        this.httpClientConfig = clientConfig;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRegistraAddress() {
        return registraAddress;
    }

    public void setRegistraAddress(String registraAddress) {
        this.registraAddress = registraAddress;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
