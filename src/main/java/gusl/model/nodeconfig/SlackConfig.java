package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class SlackConfig {

    private String opsChannel;
    private String alertChannel;
    private String resultChannel;
    private String devChannel;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
