package gusl.model.nodeconfig.dto;

/**
 * Transport wants something in response.
 *
 * @author dhudson
 */
public class OKResponse {

    private boolean ok;

    public OKResponse() {
    }

    public OKResponse(boolean value) {
        ok = value;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public static OKResponse buildResponse() {
        return new OKResponse(true);
    }
}
