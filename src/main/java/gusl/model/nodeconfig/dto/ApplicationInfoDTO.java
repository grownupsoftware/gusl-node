package gusl.model.nodeconfig.dto;

import gusl.core.tostring.ToString;

public class ApplicationInfoDTO {

    private String vendor;
    private String name;
    private String version;
    private Integer major;
    private Integer minor;
    private Integer patch;
    private String gitRevision;
    private String branchName;
    private String buildDate;
    private String label;

    /**
     * @return the vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the major
     */
    public Integer getMajor() {
        return major;
    }

    /**
     * @param major the major to set
     */
    public void setMajor(Integer major) {
        this.major = major;
    }

    /**
     * @return the minor
     */
    public Integer getMinor() {
        return minor;
    }

    /**
     * @param minor the minor to set
     */
    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    /**
     * @return the patch
     */
    public Integer getPatch() {
        return patch;
    }

    /**
     * @param patch the patch to set
     */
    public void setPatch(Integer patch) {
        this.patch = patch;
    }

    /**
     * @return the gitRevision
     */
    public String getGitRevision() {
        return gitRevision;
    }

    /**
     * @param gitRevision the gitRevision to set
     */
    public void setGitRevision(String gitRevision) {
        this.gitRevision = gitRevision;
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
