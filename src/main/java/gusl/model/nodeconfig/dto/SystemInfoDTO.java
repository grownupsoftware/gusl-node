package gusl.model.nodeconfig.dto;

import gusl.core.tostring.ToString;

import java.io.Serializable;


public class SystemInfoDTO implements Serializable {

    private static final long serialVersionUID = -1692536657701414879L;

    private String hostname;
    private String ipAddress;
    private String systemArchitecture;
    private Integer numberOfProcessors;
    private String osName;
    private String osVersion;
    private String jvmVendor;
    private String jvmName;
    private String jvmVersion;
    private Long upTime;
    private Double systemLoad;
    private Long usedMemory;
    private Long maxMemory;

    /**
     * @return the hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * @param hostname the hostname to set
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /**
     * @return the ipAddress
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress the ipAddress to set
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return the systemArchitecture
     */
    public String getSystemArchitecture() {
        return systemArchitecture;
    }

    /**
     * @param systemArchitecture the systemArchitecture to set
     */
    public void setSystemArchitecture(String systemArchitecture) {
        this.systemArchitecture = systemArchitecture;
    }

    /**
     * @return the numberOfProcessors
     */
    public Integer getNumberOfProcessors() {
        return numberOfProcessors;
    }

    /**
     * @param numberOfProcessors the numberOfProcessors to set
     */
    public void setNumberOfProcessors(Integer numberOfProcessors) {
        this.numberOfProcessors = numberOfProcessors;
    }

    /**
     * @return the osName
     */
    public String getOsName() {
        return osName;
    }

    /**
     * @param osName the osName to set
     */
    public void setOsName(String osName) {
        this.osName = osName;
    }

    /**
     * @return the osVersion
     */
    public String getOsVersion() {
        return osVersion;
    }

    /**
     * @param osVersion the osVersion to set
     */
    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    /**
     * @return the jvmVendor
     */
    public String getJvmVendor() {
        return jvmVendor;
    }

    /**
     * @param jvmVendor the jvmVendor to set
     */
    public void setJvmVendor(String jvmVendor) {
        this.jvmVendor = jvmVendor;
    }

    /**
     * @return the jvmName
     */
    public String getJvmName() {
        return jvmName;
    }

    /**
     * @param jvmName the jvmName to set
     */
    public void setJvmName(String jvmName) {
        this.jvmName = jvmName;
    }

    /**
     * @return the jvmVersion
     */
    public String getJvmVersion() {
        return jvmVersion;
    }

    /**
     * @param jvmVersion the jvmVersion to set
     */
    public void setJvmVersion(String jvmVersion) {
        this.jvmVersion = jvmVersion;
    }

    /**
     * @return the upTime
     */
    public Long getUpTime() {
        return upTime;
    }

    /**
     * @param upTime the upTime to set
     */
    public void setUpTime(Long upTime) {
        this.upTime = upTime;
    }

    /**
     * @return the systemLoad
     */
    public Double getSystemLoad() {
        return systemLoad;
    }

    /**
     * @param systemLoad the systemLoad to set
     */
    public void setSystemLoad(Double systemLoad) {
        this.systemLoad = systemLoad;
    }

    /**
     * @return the usedMemory
     */
    public Long getUsedMemory() {
        return usedMemory;
    }

    /**
     * @param usedMemory the usedMemory to set
     */
    public void setUsedMemory(Long usedMemory) {
        this.usedMemory = usedMemory;
    }

    /**
     * @return the maxMemory
     */
    public Long getMaxMemory() {
        return maxMemory;
    }

    /**
     * @param maxMemory the maxMemory to set
     */
    public void setMaxMemory(Long maxMemory) {
        this.maxMemory = maxMemory;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
