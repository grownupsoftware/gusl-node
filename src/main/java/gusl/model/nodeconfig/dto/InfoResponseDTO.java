package gusl.model.nodeconfig.dto;

import gusl.core.tostring.ToString;
import gusl.model.nodeconfig.NodeDetails;

/**
 *
 * @author grant
 */
public class InfoResponseDTO {

    private ApplicationInfoDTO applicationInfo;
    private SystemInfoDTO systemInfo;
    private NodeDetails nodeDetails;
    
    /**
     * @return the applicationInfo
     */
    public ApplicationInfoDTO getApplicationInfo() {
        return applicationInfo;
    }

    /**
     * @param applicationInfo the applicationInfo to set
     */
    public void setApplicationInfo(ApplicationInfoDTO applicationInfo) {
        this.applicationInfo = applicationInfo;
    }

    /**
     * @return the systemInfo
     */
    public SystemInfoDTO getSystemInfo() {
        return systemInfo;
    }

    /**
     * @param systemInfo the systemInfo to set
     */
    public void setSystemInfo(SystemInfoDTO systemInfo) {
        this.systemInfo = systemInfo;
    }

    public NodeDetails getNodeDetails() {
        return nodeDetails;
    }

    public void setNodeDetails(NodeDetails nodeDetails) {
        this.nodeDetails = nodeDetails;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
