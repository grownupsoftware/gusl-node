package gusl.model.nodeconfig;

/**
 * @author dhudson on 2019-07-22
 */
public enum BucketType {

    IMAGE,
    DOCUMENTATION,
    REPOSITORY,
    CAMPAIGN
    
}
