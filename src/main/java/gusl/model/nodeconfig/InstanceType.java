package gusl.model.nodeconfig;

/**
 *
 * @author dhudson
 */
public enum InstanceType {

    CA,
    UK,
    IE,
    COM
}
