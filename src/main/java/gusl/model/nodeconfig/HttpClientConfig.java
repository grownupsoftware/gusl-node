package gusl.model.nodeconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.config.MillisNumberDeserializer;
import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Data Client Config
 *
 * @author dhudson
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Getter
@Setter
@NoArgsConstructor
public class HttpClientConfig {

    @JsonDeserialize(using = CoreCountDeserializer.class)
    private int ioThreads;
    private int maxConnections;
    @JsonDeserialize(using = MillisNumberDeserializer.class)
    private long connectTimeout;
    @JsonDeserialize(using = MillisNumberDeserializer.class)
    private long socketTimeout;
    private boolean debug;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
