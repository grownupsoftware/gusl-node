package gusl.model.nodeconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.CoreCountDeserializer;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;

/**
 *
 * @author dhudson
 */
public class EventBusConfig {

    @JsonDeserialize(using = CoreCountDeserializer.class)
    private int threads;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String repositoryLocation;

    public EventBusConfig() {
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public String getRepositoryLocation() {
        return repositoryLocation;
    }

    public void setRepositoryLocation(String repositoryLocation) {
        this.repositoryLocation = repositoryLocation;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
