package gusl.model.nodeconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.config.SecretDeserializer;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtSecurityConfig {

    @JsonDeserialize(using = SecretDeserializer.class)
    private String customerSecurityKey;
    @JsonDeserialize(using = SecretDeserializer.class)
    private String anonSecurityKey;
    @JsonDeserialize(using = SecretDeserializer.class)
    private String analyticsSecurityKey;
    @JsonDeserialize(using = SecretDeserializer.class)
    private String sessionSecurityKey;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
