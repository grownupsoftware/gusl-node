package gusl.model.nodeconfig;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringIgnoreNull;
import gusl.model.cache.CacheGroup;
import gusl.model.nodeconfig.telegram.TelegramConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Base Config for all node types
 *
 * @author dhudson
 */
@NoArgsConstructor
@Getter
@Setter
// Don't want default values
@JsonInclude(Include.NON_DEFAULT)
@ToStringIgnoreNull
public abstract class NodeConfig {

    private NodeType nodeType;

//    private String timeZone;

    private RouterClientConfig routerClientConfig;

    private EventBusConfig eventBusConfig;

    @DocField(description = "Java Web Token Settings")
    private JwtSecurityConfig jwtSecurityConfig;

    // What's required for this node to start
    private List<NodeType> requiredNodes;
    private List<CacheGroup> cacheGroups;

    private SessionConfig sessionConfig;

    private BucketsConfig bucketsConfig;

    private FirebaseConfig firebaseConfig;

    private SheetsConfig sheetsConfig;

    private SlackConfig slackConfig;
    private TeamsConfig teamsConfig;
    private MessagingConfig messagingConfig;
    private EmailServerConfig emailServerConfig;
    private RegistrationConfig registrationConfig;
    private GoogleAuthenticatorConfigDO googleAuthenticatorConfig;
    private FrontendConfig frontendConfig;
    private TelegramConfig telegramConfig;
    private PrometheusConfig prometheusConfig;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
