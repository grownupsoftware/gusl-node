package gusl.model.nodeconfig.telegram;

import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandlerConfig {

    private String name;
    private String userName;
    private String token;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
