package gusl.model.nodeconfig.telegram;

import gusl.core.tostring.ToString;
import lombok.*;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TelegramConfig {

    @Singular
    private List<HandlerConfig> handlers;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
