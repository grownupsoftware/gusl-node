package gusl.model.nodeconfig;

/**
 * Status for a node.
 *
 * @author dhudson
 */
public enum NodeStatus {

    /**
     * Stop has been issued
     */
    STOPPING,
    /**
     * Stop completed
     */
    STOPPED,
    /**
     * Start command issued
     */
    STARTING,
    /**
     * Tried to start, but couldn't
     */
    FAILED,
    /**
     * Alive and Well
     */
    ACTIVE,
    /**
     * Server died
     */
    CRASHED,
    /**
     * Quiesced
     */
    QUIESCED
}
