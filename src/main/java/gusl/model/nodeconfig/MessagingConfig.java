package gusl.model.nodeconfig;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 * @since 01/09/2022
 */
@Getter
@Setter
@NoArgsConstructor
public class MessagingConfig {

    private MessageService service;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
