package gusl.model.nodeconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.annotations.DocField;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmailServerConfig {

    @DocField(description = "Email provider type [AWS, GOOGLE, SMTP")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String type;

    @DocField(description = "AWS - IAM Username")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String iamUsername;

    @DocField(description = "Provider SMTP - host")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String smtpHost;

    @DocField(description = "Provider SMTP - port")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String smtpPort;

    @DocField(description = "Provider SMTP - username")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String smtpUsername;

    @DocField(description = "Provider SMTP - password")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String smtpPassword;

    @DocField(description = "Provider SMTP - ssl")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String smtpSsl;

    @DocField(description = "")
    // @JsonDeserialize(using = SystemProperyDeserializer.class)
    private boolean startTls;

    @DocField(description = "")
    // @JsonDeserialize(using = SystemProperyDeserializer.class)
    private boolean auth;

    @DocField(description = "")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String sslTrust;

    @DocField(description = "AWS - Region")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String region;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String bcc;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
