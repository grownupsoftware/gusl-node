package gusl.model.nodeconfig;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.SystemProperyDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RegistrationConfig {
    public boolean validateEmail;

    public boolean kycRequired;

    @JsonDeserialize(using = SystemProperyDeserializer.class)
    public String qaEmail;


}
