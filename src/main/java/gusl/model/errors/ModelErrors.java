package gusl.model.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author dhudson
 */
public enum ModelErrors {

    PAYMENT_METHOD_NOT_SUPPORTED("GUSLM01 Payment type {0} not supported", "payment.type.not.supported"),
    ORDER_LINE_TYPE_NOT_SUPPORTED("GUSLM02 Order line type {0} not supported", "order.line.type.not.supported"),
    PROMO_EVENT_TYPE_NOT_SUPPORTED("GUSLM03 Promo event {0} type not supported", "promo.event.type.not.supported"),
    KIBANA_QUERY_TYPE_NOT_SUPPORTED("GUSLM04 Kibana query type {0} not supported", "kibana.query.type.not.supported"),
    CART_OPERATION_TYPE_NOT_SUPPORTED("GUSLM05 Cart operation action {0} not supported", "cart.action.not.supported"),
    BETSLIP_OPERATION_TYPE_NOT_SUPPORTED("GUSLM06 Betslip operation action {0} not supported", "betslip.action.not.supported"),
    CANT_FIND_CONSENT("GUSLM07 Can't find consent {0}", "missing.consent");

    private final String message;
    private final String messageKey;

    ModelErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

//    public ErrorDO getError(PlayerDO player) {
//        return getError(player.toInfoString());
//    }

    public ErrorDO getError(Long id) {
        if (id != null) {
            return new ErrorDO(null, message, messageKey, String.valueOf(id));
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }
}
