package gusl.model.status;

import gusl.model.routerconfig.ResourceNode;

/**
 * Websocket Message.
 *
 * @author dhudson
 */
public class NodeChangedMessage {

    private ResourceNode node;
    private NodeChangedMessageType type;

    public NodeChangedMessage() {
    }

    public NodeChangedMessage(NodeChangedMessageType type) {
        this.type = type;
    }

    public NodeChangedMessage(NodeChangedMessageType type, ResourceNode node) {
        this(type);
        this.node = node;
    }

    public ResourceNode getNode() {
        return node;
    }

    public void setNode(ResourceNode node) {
        this.node = node;
    }

    public NodeChangedMessageType getType() {
        return type;
    }

    public void setType(NodeChangedMessageType type) {
        this.type = type;
    }

}
