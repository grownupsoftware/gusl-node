package gusl.model.status;

/**
 * Indicates message type for the web socket.
 *
 * @author dhudson
 */
public enum NodeChangedMessageType {

    STATUS_CHANGE,
    REGISTER,
    DEREGISTER

}
