package gusl.model.log;

import gusl.core.logging.GUSLLogger;
import gusl.core.tostring.ToString;

/**
 * @author dhudson on 03/10/2019
 */
public class LoggerDTO {

    private String name;
    private String level;

    public LoggerDTO() {
    }

    public LoggerDTO(GUSLLogger logger) {
        name = logger.getName();
        level = logger.getLevel().name();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
