package gusl.model.log;

import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringCount;

import java.util.List;

/**
 * @author dhudson on 03/10/2019
 */
public class LoggersDTO {

    @ToStringCount
    private List<LoggerDTO> loggers;

    public LoggersDTO() {
    }

    public List<LoggerDTO> getLoggers() {
        return loggers;
    }

    public void setLoggers(List<LoggerDTO> loggers) {
        this.loggers = loggers;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
