package gusl.model.log;

import java.util.ArrayList;
import java.util.List;

/**
 * Collection of Logs
 *
 * @author dhudson
 */
public class LogsDTO {

    private List<LogDTO> logs;
    private String level;

    public LogsDTO() {
    }

    public List<LogDTO> getLogs() {
        return logs;
    }

    public void setLogs(List<LogDTO> logs) {
        this.logs = logs;
    }

    public void addLog(LogDTO log) {
        if (logs == null) {
            logs = new ArrayList<>();
        }

        logs.add(log);
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

}
