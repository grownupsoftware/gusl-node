package gusl.model.log;

import gusl.core.tostring.ToString;

/**
 * @author dhudson on 03/10/2019
 */
public class LoggerEvent { //extends AbstractEvent {

    private LoggerDTO logger;

    public LoggerEvent() {
    }

    public LoggerEvent(LoggerDTO loggerDTO) {
        logger = loggerDTO;
    }

    public LoggerDTO getLogger() {
        return logger;
    }

    public void setLogger(LoggerDTO logger) {
        this.logger = logger;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
