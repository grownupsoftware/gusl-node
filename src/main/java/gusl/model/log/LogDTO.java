package gusl.model.log;

import java.util.Date;
import gusl.core.logging.LogEventHelper;
import gusl.core.utils.Utils;
import org.apache.logging.log4j.core.LogEvent;

/**
 * Simple Log DTO
 *
 * @author dhudson
 */
public class LogDTO {

    private String level;
    private String message;
    private String threadName;
    private String loggerName;
    private long time;

    public LogDTO() {
    }

    public LogDTO(LogEvent logEvent) {
        level = logEvent.getLevel().name();
        threadName = logEvent.getThreadName();
        loggerName = logEvent.getLoggerName();
        message = LogEventHelper.getMessageWithPossibleException(logEvent);
        time = logEvent.getTimeMillis();
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }

    public String getLoggerName() {
        return loggerName;
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getFormattedTime() {
        return Utils.getTimestamp(time);
    }

    public String getDate() {
        return Utils.formatDate(new Date(time));
    }
}
