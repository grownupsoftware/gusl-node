package gusl.node.config;

import gusl.core.config.ConfigUtils;
import gusl.core.utils.IOUtils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeDetails;
import lombok.CustomLog;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

/**
 * @author dhudson
 */
@CustomLog
public class NodeConfigUtils {

    private NodeConfigUtils() {
    }

    /**
     * Load the config from either the given location or the default file on the
     * class path.
     *
     * @param <T>
     * @param config
     * @return
     * @throws IOException
     */
    public static <T extends NodeConfig> T loadConfig(Class<T> config) throws IOException {
        return ConfigUtils.loadConfig(config);
    }

    public static void processManifest(ServletContext context, NodeDetails nodeDetails) {
        if (context != null) {
            nodeDetails.setWebContext(context.getContextPath());
            try {
                Properties manifest = new Properties();
                InputStream resourceAsStream = context.getResourceAsStream(IOUtils.WAR_MANIFEST_LOCATION);
                if (resourceAsStream != null) {
                    manifest.load(resourceAsStream);
                    nodeDetails.setReleaseDate(manifest.getProperty("Build-Timestamp"));
                    nodeDetails.setVersion(manifest.getProperty("Build-Version"));
                    nodeDetails.setBranchName(manifest.getProperty("Build-Branch"));
                } else {
                    // more likely running in test mode
                    nodeDetails.setReleaseDate(new Date().toString());
                    nodeDetails.setVersion("testing");
                    nodeDetails.setBranchName("?");
                }
            } catch (IOException ignore) {

            }
        }
    }
}
