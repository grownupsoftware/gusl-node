/* Copyright lottomart */
package gusl.node.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.node.hk2utils.AsyncInterceptionService;
import org.glassfish.hk2.api.InterceptionService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.inject.Singleton;

/**
 * @author grant
 */
@Singleton
public class NodeInjection extends AbstractBinder {

    @Override
    public void configure() {
        bind(ObjectMapperFactory.getDefaultObjectMapper()).to(ObjectMapper.class);
        bind(AsyncInterceptionService.class).to(InterceptionService.class).in(Singleton.class);
    }

}
