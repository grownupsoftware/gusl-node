/*
 * Grownup Software Limited.
 */
package gusl.node.eventbus;

import com.codahale.metrics.Gauge;
import gusl.core.eventbus.JsonFileEventBusRepository;
import gusl.core.eventbus.LmEventBusImpl;
import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLException;
import gusl.core.metrics.MetricsFactory;
import gusl.model.nodeconfig.EventBusConfig;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.properties.GUSLConfigurable;
import org.glassfish.hk2.api.PreDestroy;
import org.jvnet.hk2.annotations.Service;

import java.io.File;

/**
 * Simple internal Event Bus.
 *
 * @author dhudson - Apr 10, 2017 - 3:06:02 PM
 */
@Service
public class NodeEventBusImpl extends LmEventBusImpl implements NodeEventBus, PreDestroy, GUSLConfigurable {

    public NodeEventBusImpl() {
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        EventBusConfig ebConfig = config.getEventBusConfig();

        if (ebConfig == null) {
            throw new GUSLException("No EventBus Config found");
        }

        configure(ebConfig.getThreads());

        // perform event registration
        registerAll(GUSLServiceLocator.getInstancesWithAnnotatedMethods(OnEvent.class));

        // Lets set the repository, need to add the data service, as there can be many nodes at one location.
        File repoLocation = new File(ebConfig.getRepositoryLocation() + "/eventbus/" + config.getNodeType().toString());

        installRepositoryHandler(new JsonFileEventBusRepository(repoLocation));

        String metricName = "gusl.eventbus.thread.pool.size";
        MetricsFactory.getRegistry().remove(metricName);
        MetricsFactory.getRegistry().register(metricName, new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return getThreadPoolSize();
            }
        });

        metricName = "gusl.eventbus.queue.size";
        MetricsFactory.getRegistry().remove(metricName);
        MetricsFactory.getRegistry().register(metricName, new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return getQueueSize();
            }
        });
    }

    @Override
    public void preDestroy() {
        shutdown();
    }
}
