/* Copyright lottomart */
package gusl.node.eventbus;

import gusl.core.annotations.DocApi;
import gusl.core.annotations.DocClass;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;

/**
 * Receives POST messages, converts to EventBus events and posts onto internal
 * event bus
 *
 * @author grant
 */
@DocClass(description = "Receives eventbus events via http, converts to events and posts onto internal event bus")
@Singleton
@Path(value = "eventbus")
public class EventbusResource extends AbstractBaseResource {

    @Inject
    private NodeEventBus theEventBus;

    @POST
    @DocApi(description = "Convert inbound request into an event bus event")
    @PermitAll
    public void eventbusRequest(@Suspended final AsyncResponse asyncResponse,
                                @Context final HttpServletRequest servletRequest) throws GUSLException {

        //logHttpRequest(servletRequest);

        Object event = HttpUtils.getPayloadFromHttpRequest(servletRequest, asyncResponse);
        if (event == null) {
            logger.warn("Unable to handle event message");
        } else {
            theEventBus.post(event);
        }

        HttpUtils.resumeAsyncOK(asyncResponse);
    }

    @POST()
    @DocApi(description = "Blocking event bus post")
    @PermitAll
    @Path(value = "/wait")
    public void waitingEventBusRequest(@Suspended final AsyncResponse asyncResponse,
                                       @Context final HttpServletRequest servletRequest) {

        Object event = HttpUtils.getPayloadFromHttpRequest(servletRequest, asyncResponse);
        if (event == null) {
            logger.warn("Unable to handle event message");
        } else {
            try {
                HttpUtils.waitForFuture(theEventBus.postWithFuture(event));
            } catch (GUSLErrorException ex) {
                logger.warn("Error whilst waiting for future to complete", ex);
            }
        }

        HttpUtils.resumeAsyncOK(asyncResponse);
    }
}
