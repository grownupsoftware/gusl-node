package gusl.node.eventbus;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import gusl.core.tostring.ToString;

/**
 *
 * @author dhudson
 */
public class EventBusRequest {

    private boolean waitFor;
    private boolean persist;
    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
    private Object event;

    public EventBusRequest() {
    }

    public EventBusRequest(Object event) {
        this.event = event;
    }

    public boolean isWaitFor() {
        return waitFor;
    }

    public void setWaitFor(boolean waitFor) {
        this.waitFor = waitFor;
    }

    public boolean isPersist() {
        return persist;
    }

    public void setPersist(boolean persist) {
        this.persist = persist;
    }

    public Object getEvent() {
        return event;
    }

    public void setEvent(Object event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
