/*
 * Grownup Software Limited.
 */
package gusl.node.eventbus;

import gusl.core.eventbus.LmEventBus;
import org.glassfish.hk2.api.PreDestroy;
import org.jvnet.hk2.annotations.Contract;

/**
 * Interface for the internal Event Bus.
 *
 * @author dhudson - Mar 23, 2017 - 8:49:32 AM
 */
@Contract
public interface NodeEventBus extends LmEventBus, PreDestroy {

}
