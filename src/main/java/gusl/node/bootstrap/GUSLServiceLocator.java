package gusl.node.bootstrap;

import gusl.core.exceptions.GUSLException;
import gusl.core.utils.Utils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.systemconfig.SystemConfigDO;
import gusl.node.hk2utils.HK2RuntimeInitializer;
import gusl.node.properties.GUSLConfigurable;
import gusl.node.properties.GUSLSystemConfigurable;
import lombok.CustomLog;
import org.glassfish.hk2.api.Descriptor;
import org.glassfish.hk2.api.ServiceHandle;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * LmServiceLocator is a wrap around the Hk2 Service Locator.
 *
 * @author dhudson
 */
@CustomLog
public class GUSLServiceLocator {

    private static ServiceLocator theServiceLocator = null;

    public static final String[] PACKAGE_PREFIXES = new String[]{"gusl", "supernova","com.smartech","alvar"};

    /**
     * Called by DefaultJerseyApplication - bootstrap with existing Service
     * Locator
     *
     * @param serviceLocator
     */
    public static void bootstrap(ServiceLocator serviceLocator) {
        logger.debug("-- bootstrap with service locator --");
        theServiceLocator = serviceLocator;

        try {
            HK2RuntimeInitializer.update(serviceLocator, PACKAGE_PREFIXES);
        } catch (IOException | ClassNotFoundException ex) {
            logger.error("Error scanning classes", ex);
        }

        //logServices();
    }

    /**
     * Likely to be called in non-servlet mode
     */
    public static synchronized void bootstrap() {
        logger.debug("-- bootstrap --");
        if (theServiceLocator == null) {
            try {
                theServiceLocator = HK2RuntimeInitializer.init("bootstrap", false, PACKAGE_PREFIXES);
                //logServices();
            } catch (IOException | ClassNotFoundException ex) {
                logger.error("Failed to bootstrap", ex);
            }
            //theServiceLocator = ServiceLocatorUtilities.createAndPopulateServiceLocator();
        }
    }

    public static <T> T getInstanceOf(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.createAndInitialize(clazz);
    }

    public static <T> T getService(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.getService(clazz);
    }

    public static ServiceLocator getServiceLocator() {
        return theServiceLocator;
    }

    public static void inject(Object injectMe) {
        theServiceLocator.inject(injectMe);
    }

    /**
     * Find all @Service annotations for a particular class
     *
     * @param <T>   type of class
     * @param clazz the class
     * @return list of service handlers that have the underlying service
     */
    public static <T> List<ServiceHandle<T>> getAllServiceHandlers(Class<T> clazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        return theServiceLocator.getAllServiceHandles(clazz);
    }

    public static void logServices() {
        if (theServiceLocator == null) {
            bootstrap();
        }

        // ServiceLocatorUtilities.dumpAllDescriptors(theServiceLocator);
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", "Scope", "Class", "Interface(s)"));
        builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", "-----", "-----", "------------"));

        Utils.safeStream(theServiceLocator.getDescriptors((Descriptor d) -> true))
                .forEach(descriptor -> {
                    try {
                        if (descriptor.getImplementationClass().getCanonicalName() != null
                                && hasCorrectPrefix(descriptor.getImplementationClass().getCanonicalName())) {
                            builder.append(String.format("\t %-10s\t\t%-60s\t%s\n", descriptor.getScope()
                                            .replace("javax.inject.", "")
                                            .replace("org.glassfish.hk2.api.", "")
                                            .replace("org.glassfish.jersey.process.internal.", ""),
                                    descriptor.getImplementationClass().getCanonicalName(),
                                    descriptor.getContractTypes()
                                    //descriptor.getInjectees()
                            ));
                        }
                    } catch (java.lang.IllegalStateException ex) {
                        //ignore 
                    }
                });

        logger.info(
                "Services: [{}]\n{}", theServiceLocator.hashCode(), builder.toString());

    }

    public static boolean hasCorrectPrefix(String name) {
        for (String prefix : PACKAGE_PREFIXES) {
            if (name.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets Instances of a class that implements or extends the member class
     *
     * @param <T>         the type of instance
     * @param memberClazz the member class
     * @return the list of instances
     */
    public static <T> List<T> getInstancesWithSubTypeOf(Class<T> memberClazz) {
        if (theServiceLocator == null) {
            bootstrap();
        }
        List<T> result = new ArrayList<>();
        Utils.safeStream(theServiceLocator.getDescriptors((Descriptor d) -> true))
                .forEach(descriptor -> {
                    try {
                        if (descriptor.getImplementationClass().getCanonicalName() != null
                                && hasCorrectPrefix(descriptor.getImplementationClass().getCanonicalName())) {

                            Class<?> implementationClass = descriptor.getImplementationClass();

                            logger.debug("--> class: {} is member of {} : {}", implementationClass.getSimpleName(), memberClazz.getSimpleName(), memberClazz.isAssignableFrom(implementationClass));

                            if (memberClazz.isAssignableFrom(implementationClass)) {
                                //T object = (T) theServiceLocator.createAndInitialize(implementationClass);
                                @SuppressWarnings("unchecked")
                                T object = (T) theServiceLocator.getService(implementationClass);
                                result.add(object);
                            }
                        }
                    } catch (java.lang.IllegalStateException ex) {
                        // ignore 
                    }

                });
        return result;

    }

    public static List<Object> getInstanceWithClassAnnotation(Class<? extends Annotation> annotationType) {
        if (theServiceLocator == null) {
            bootstrap();
        }

        List<Object> result = new ArrayList<>();

        return result;
    }

    /**
     * Get all instances of a class (service) that contains methods that are
     * annotated with annotationType
     *
     * @param annotationType
     * @return List of instances that match the criteria
     */
    public static List<Object> getInstancesWithAnnotatedMethods(Class<? extends Annotation> annotationType) {
        if (theServiceLocator == null) {
            bootstrap();
        }

        List<Object> result = new ArrayList<>();
        Utils.safeStream(theServiceLocator.getDescriptors((Descriptor d) -> true))
                .forEach(descriptor -> {
                    try {
                        if (descriptor.getImplementationClass().getCanonicalName() != null
                                && hasCorrectPrefix(descriptor.getImplementationClass().getCanonicalName())) {

                            Class<?> implementationClass = descriptor.getImplementationClass();
                            if (walkClassLookingForAnnotations(implementationClass, annotationType)) {
                                result.add(theServiceLocator.getService(implementationClass));
                            }
                        }
                    } catch (java.lang.IllegalStateException ignore) {
                    }
                });
        return result;

    }

    private static boolean walkClassLookingForAnnotations(Class<?> implementationClass, Class<? extends Annotation> annotationType) {
        while (implementationClass != null) {
            for (Method m : implementationClass.getDeclaredMethods()) {
                if (m.isAnnotationPresent(annotationType)) {
                    return true;
                }
            }

            implementationClass = implementationClass.getSuperclass();
        }
        return false;
    }

    /**
     * Close the context so that a new one can be re-loaded.
     * <b>Only to be used in testing!</b>
     */
    public static void close() {
        if (theServiceLocator != null) {
            theServiceLocator.shutdown();
        }
    }

    public static void injectConfig(NodeConfig config) throws RuntimeException {
        if (theServiceLocator == null) {
            bootstrap();
        }

        logger.debug("Configuring with: {} ", config);
        List<GUSLConfigurable> configurables = getInstancesWithSubTypeOf(GUSLConfigurable.class);
        configurables.forEach((configurable) -> {
            try {
                logger.debug("\t configuring {}", configurable.getClass().getName());
                configurable.configure(config);
            } catch (GUSLException ex) {
                logger.warn("Exception caught when trying to configure application", ex);
                throw new RuntimeException("Exception caught when trying to configure application", ex);
            }
        });
    }

    public static void injectSystemConfig(Map<String, SystemConfigDO> systemConfigMap) {
        if (theServiceLocator == null) {
            bootstrap();
        }

        logger.debug("Configuring System with: {} ", systemConfigMap);
        List<GUSLSystemConfigurable> configurables = getInstancesWithSubTypeOf(GUSLSystemConfigurable.class);
        configurables.forEach((configurable) -> {
            try {
                logger.debug("\t configuring {}", configurable.getClass().getName());
                configurable.configure(systemConfigMap);
            } catch (GUSLException ex) {
                logger.warn("Exception caught when trying to configure application", ex);
                throw new RuntimeException("Exception caught when trying to configure application", ex);
            }
        });
    }

    public static void bindAbstractBinders() {
        if (theServiceLocator == null) {
            bootstrap();
        }
        List<AbstractBinder> binders = getInstancesWithSubTypeOf(AbstractBinder.class);
        logger.debug("found binders: {}", binders);
        ServiceLocatorUtilities.bind(theServiceLocator, binders.toArray(new AbstractBinder[binders.size()]));
    }

}
