/* Copyright lottomart */
package gusl.node.bootstrap;

import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.glassfish.hk2.utilities.ClasspathDescriptorFileFinder;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author grant
 */
@CustomLog
public class JerseyDescriptorFinder extends ClasspathDescriptorFileFinder {

    final ServletContext servletContext;

    // private struct for saving output of Java 8 lambda
    private class UrlMapResult {

        final InputStream inputStream;
        final IOException ioException;

        public UrlMapResult(InputStream inputStreamIn, IOException ioExceptionIn) {
            inputStream = inputStreamIn;
            ioException = ioExceptionIn;
        }

        public InputStream getInputStream() {
            return inputStream;
        }

        public IOException getIoException() {
            return ioException;
        }
    }

    JerseyDescriptorFinder(final ServletContext servletContextIn) {
        servletContext = servletContextIn;
    }

    @Override
    public List findDescriptorFiles() throws IOException {
        //this section will read through all of the JARs for inhabitant files
        final Enumeration metaInfUrls = this.getClass().getClassLoader()
                .getResources("META-INF/hk2-locator/default");
        Collection<URL> urls = new ArrayList<>();
        urls.addAll(Collections.list(metaInfUrls));

        // this section will search through the WAR for inhabitant files - it needs to access the servlet for this
        URL webInfUrl;
        if (servletContext != null) {
            webInfUrl = servletContext.getResource("/WEB-INF/classes/hk2-locator/default");

            logger.info("web-inf locator: {}", webInfUrl);

            if (webInfUrl != null) {
                urls.add(webInfUrl);
            }
        }

        Utils.safeStream(urls).forEach(url -> {
            logger.info("scanning: {}", url.toString());
        });

        List<UrlMapResult> mapResults = urls.stream().map(this::openStream).collect(Collectors.toList());

        return mapResults.stream()
                .filter(x -> {
                    if (x.getIoException() == null) {
                        return true;
                    }
                    logger.error("IO Exception when scanning resources", x.getIoException());
                    return false;
                })
                .map(UrlMapResult::getInputStream)
                .collect(Collectors.toList());
    }

    private UrlMapResult openStream(final URL url) {
        logger.info("Scanning: {}", url.toString());
        InputStream inputStream = null;
        IOException ioException = null;
        try {
            inputStream = url.openStream();
        } catch (final IOException e) {
            ioException = e;
        }

        return new UrlMapResult(inputStream, ioException);
    }
}
