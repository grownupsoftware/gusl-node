/* Copyright lottomart */
package gusl.node.bootstrap;

import java.io.IOException;
import javax.servlet.ServletContext;
import org.glassfish.hk2.api.DynamicConfigurationService;
import org.glassfish.hk2.api.MultiException;
import org.glassfish.hk2.api.Populator;
import org.glassfish.hk2.api.ServiceLocator;

/**
 *
 * @author grant
 */
public class JerseyAutoScan {

    final ServiceLocator serviceLocator;
    final ServletContext servletContext;

    public JerseyAutoScan(final ServiceLocator serviceLocatorIn, final ServletContext servletContextIn) {
        serviceLocator = serviceLocatorIn;
        servletContext = servletContextIn;
    }

    public void scan() {
        try {
            DynamicConfigurationService dcs = serviceLocator.getService(DynamicConfigurationService.class);
            Populator populator = dcs.getPopulator();
            populator.populate(new JerseyDescriptorFinder(servletContext));
        } catch (IOException e) {
            throw new MultiException(e);
        }
    }

}
