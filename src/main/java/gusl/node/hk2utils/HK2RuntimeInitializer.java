package gusl.node.hk2utils;

import eu.infomas.annotation.AnnotationDetector;
import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.glassfish.hk2.api.ActiveDescriptor;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.api.ServiceLocatorFactory;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.Arrays;

/**
 * HK2RuntimeInitializer
 * <p>
 * HK2 currently doesn't provide any mechanism to configure itself via
 * annotations at runtime - instead, it is done via a tool that runs at build
 * time which creates 'inhabitant' files. But having those files around is
 * problematic at best, and makes debugging in Eclipse or other environments
 * difficult, since it relies on having the maven-tool generated file.
 * <p>
 * This utility alleviates those issues by allowing HK2 to be configured at
 * runtime.
 */
@CustomLog
public class HK2RuntimeInitializer {

    /**
     * Scan the requested packages on the classpath for HK2 'Service' and
     * 'Contract' annotated classes. Load the metadata for those classes into
     * the HK2 Service Locator.
     * <p>
     * This implementation should support all Annotations that are supported by
     * HK2 - however - if you are using HK2 older than 2.3.0 - note that it is
     * impacted by this bug: https://java.net/jira/browse/HK2-187
     * <p>
     * For an implementation that is not impacted by that bug, see
     * {@link HK2RuntimeInitializerCustom}
     *
     * @param serviceLocatorName  - The name of the ServiceLocator to find (or
     *                            create if it doesn't yet exist)
     * @param readInhabitantFiles - Read and process inhabitant files before
     *                            doing the classpath scan. Annotated items found during the scan will
     *                            override items found in the inhabitant files, if they collide.
     * @param packageNames        -- The set of package names to scan recursively - for
     *                            example - new String[]{"org.foo", "com.bar"} If not provided, the entire
     *                            classpath is scanned
     * @return - The created ServiceLocator (but in practice, you can lookup
     * this ServiceLocator by doing:      <pre>
     * {@code
     * ServiceLocatorFactory.getInstance().create("SomeName");
     * }
     * </pre>
     * @throws IOException
     * @throws ClassNotFoundException
     * @see org.glassfish.hk2.api.ServiceLocatorFactory#create(String)
     * @see ServiceLocatorUtilities#createAndPopulateServiceLocator(String)
     */
    public static ServiceLocator init(String serviceLocatorName, boolean readInhabitantFiles, String... packageNames) throws IOException, ClassNotFoundException {
        AnnotatedClasses ac = new AnnotatedClasses();

        @SuppressWarnings("unchecked")
        AnnotationDetector cf = new AnnotationDetector(new AnnotationReporter(ac, new Class[]{Service.class, Singleton.class}));
        if (packageNames == null || packageNames.length == 0) {
            cf.detect();
        } else {
            cf.detect(packageNames);
        }

        ServiceLocator locator = null;

        if (readInhabitantFiles) {
            locator = ServiceLocatorUtilities.createAndPopulateServiceLocator(serviceLocatorName);
        } else {
            ServiceLocatorFactory factory = ServiceLocatorFactory.getInstance();
            locator = factory.create(serviceLocatorName);
        }

        for (ActiveDescriptor<?> ad : ServiceLocatorUtilities.addClasses(locator, ac.getAnnotatedClasses())) {
            logger.debug("Added Scope: {} Impl {} ", (ad.getScope() == null ? "proto" : ad.getScope().replace("javax.inject.", "")), ad.getImplementation());
        }

        return locator;
    }

    @SuppressWarnings("unchecked")
    public static void update(ServiceLocator locator, String... packageNames) throws IOException, ClassNotFoundException {
        logger.debug("ServiceLocator: {} packages: {}", locator.hashCode(), Arrays.asList(packageNames));

        AnnotatedClasses ac = new AnnotatedClasses();

        AnnotationDetector cf = new AnnotationDetector(new AnnotationReporter(ac, new Class[]{Service.class, Singleton.class}));

        if (packageNames == null || packageNames.length == 0) {
            cf.detect();
        } else {
            cf.detect(packageNames);
        }

        if (logger.isDebugEnabled()) {
            Utils.safeStream(ac.getAnnotatedClasses()).forEach(klass -> {
                logger.info("Found class: {}", klass);
            });
        }

        //ServiceLocatorUtilities.enableImmediateScope(locator);
        for (ActiveDescriptor<?> ad : ServiceLocatorUtilities.addClasses(locator, ac.getAnnotatedClasses())) {
            logger.debug("Added Scope: {} Impl {} ", (ad.getScope() == null ? "proto" : ad.getScope().replace("javax.inject.", "")), ad.getImplementation());
        }
    }
}
