package gusl.node.hk2utils;

import eu.infomas.annotation.AnnotationDetector;
import org.glassfish.hk2.api.*;
import org.glassfish.hk2.utilities.DescriptorImpl;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.jvnet.hk2.annotations.Contract;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;

/**
 * HK2RuntimeInitializer
 * <p>
 * HK2 currently doesn't provide any mechanism to configure itself via
 * annotations at runtime - instead, it is done via a tool that runs at build
 * time which creates 'inhabitant' files. But having those files around is
 * problematic at best, and makes debugging in Eclipse or other environments
 * difficult, since it relies on having the maven-tool generated file.
 * <p>
 * This utility alleviates those issues by allowing HK2 to be configured at
 * runtime.
 * <p>
 * <p>
 * ***************
 * <p>
 * If you are using HK2 2.3.0 or newer, you should instead use
 * {@link HK2RuntimeInitializer}!
 * <p>
 * This class only exists as a workaround to a bug in older versions of HK2.
 * https://java.net/jira/browse/HK2-187
 * <p>
 * ***************
 *
 * @author <a href="mailto:daniel.armbrust.list@gmail.com">Dan Armbrust</a>
 */
public class HK2RuntimeInitializerCustom {

    /**
     * Scan the requested packages on the classpath for HK2 'Service' and
     * 'Contract' annotated classes. Load the metadata for those classes into
     * the HK2 Service Locator.
     * <p>
     * Currently supports the annotations: Service Contract
     * <p>
     * The 'Named' annotation may also be utilized.
     * <p>
     * Supports the Scopes of: Singleton PerLookup PerThread (but you need to
     * enable this in HK2 with a call to
     * ServiceLocatorUtilities.enablePerThreadScope(locator);)
     * <p>
     * Also supports the Annotations of: Proxiable Unproxiable
     * <p>
     * Any other annotations that are supported by HK2 are not yet supported by
     * this utility. Note that {@link HK2RuntimeInitializer} doesn't have these
     * limitations, but does suffer from https://java.net/jira/browse/HK2-187
     * <p>
     * If you are using HK2 2.3.0 or newer, you should prefer
     * {@link HK2RuntimeInitializer}
     *
     * @param serviceLocatorName  - The name of the ServiceLocator to find (or
     *                            create if it doesn't yet exist)
     * @param readInhabitantFiles - Read and process inhabitant files before
     *                            doing the classpath scan. Annotated items found during the scan will
     *                            override items found in the inhabitant files, if they collide.
     * @param packageNames        -- The set of package names to scan recursively - for
     *                            example - new String[]{"org.foo", "com.bar"} If not provided, the entire
     *                            classpath is scanned
     * @return - The created ServiceLocator (but in practice, you can lookup
     * this ServiceLocator by doing:      <pre>
     * {@code
     * ServiceLocatorFactory.getInstance().create("SomeName");
     * }
     * </pre>
     * @throws IOException
     * @throws ClassNotFoundException
     * @see org.glassfish.hk2.api.ServiceLocatorFactory#create(String)
     * @see ServiceLocatorUtilities#createAndPopulateServiceLocator(String)
     */
    public static ServiceLocator init(String serviceLocatorName, boolean readInhabitantFiles, String... packageNames) throws IOException, ClassNotFoundException {
        AnnotatedClasses ac = new AnnotatedClasses();

        @SuppressWarnings("unchecked")
        AnnotationDetector cf = new AnnotationDetector(new AnnotationReporter(ac, new Class[]{Service.class, Contract.class, Named.class,
                Singleton.class, PerLookup.class, PerThread.class, Proxiable.class, Unproxiable.class}));
        if (packageNames == null || packageNames.length == 0) {
            cf.detect();
        } else {
            cf.detect(packageNames);
        }

        ServiceLocator locator = null;

        if (readInhabitantFiles) {
            locator = ServiceLocatorUtilities.createAndPopulateServiceLocator(serviceLocatorName);
        } else {
            ServiceLocatorFactory factory = ServiceLocatorFactory.getInstance();
            locator = factory.create(serviceLocatorName);
        }

        DynamicConfigurationService dcs = locator.getService(DynamicConfigurationService.class);
        DynamicConfiguration config = dcs.createDynamicConfiguration();

        for (DescriptorImpl di : ac.createDescriptors()) {
            config.bind(di);
        }

        config.commit();

        return locator;
    }
}
