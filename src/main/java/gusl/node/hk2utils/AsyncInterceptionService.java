package gusl.node.hk2utils;

import gusl.core.utils.SystemPropertyUtils;
import gusl.node.async.AsyncTimeoutConstants;
import gusl.node.errors.NodeErrors;
import gusl.node.providers.AsyncTimeout;
import gusl.node.transport.HttpUtils;
import lombok.CustomLog;
import org.aopalliance.intercept.ConstructorInterceptor;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.glassfish.hk2.api.Descriptor;
import org.glassfish.hk2.api.Filter;
import org.glassfish.hk2.api.InterceptionService;

import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import static gusl.node.bootstrap.GUSLServiceLocator.PACKAGE_PREFIXES;

/**
 * Trap All suspended Async Requests
 *
 * @author dhudson
 */
@CustomLog
public class AsyncInterceptionService implements InterceptionService {

    private final List<MethodInterceptor> theMethodInterceptor;

    public AsyncInterceptionService() {
        theMethodInterceptor = Collections.singletonList(new AsyncMethodInterceptor());
    }

    @Override
    public Filter getDescriptorFilter() {
        return new Filter() {
            @Override
            public boolean matches(final Descriptor d) {
                return hasCorrectPrefix(d.getImplementation());
            }
        };
    }

    private static boolean hasCorrectPrefix(String name) {
        for (String prefix : PACKAGE_PREFIXES) {
            if (name.startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<MethodInterceptor> getMethodInterceptors(Method method) {

        if (method.isSynthetic()) {
            return null;
        }

        // As everything extends object, no point checking these
        if (!method.getDeclaringClass().getName().startsWith("java.")) {

            if (method.getDeclaringClass().getAnnotation(Path.class) != null) {
                for (Annotation[] row : method.getParameterAnnotations()) {
                    for (Annotation annotation : row) {
                        if (annotation instanceof Suspended) {
                            logger.debug("Returning the interceptor for method {}", method.getName());
                            return theMethodInterceptor;
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public List<ConstructorInterceptor> getConstructorInterceptors(Constructor<?> constructor) {
        return Collections.emptyList();
    }

    private class AsyncMethodInterceptor implements MethodInterceptor {

        @Override
        public Object invoke(MethodInvocation invocation) throws Throwable {

            for (Object arg : invocation.getArguments()) {
                if (arg instanceof AsyncResponse) {
                    AsyncResponse response = (AsyncResponse) arg;
                    if (!response.isDone()) {

                        int seconds = AsyncTimeoutConstants.DEFAULT_TIMEOUT;

                        if (invocation.getMethod().isAnnotationPresent(AsyncTimeout.class)) {
                            AsyncTimeout timeout = invocation.getMethod().getAnnotation(AsyncTimeout.class);
                            seconds = timeout.timeout();
                        }

                        final int timeoutval = seconds;
                        response.setTimeout(seconds, AsyncTimeoutConstants.DEFAULT_TIME_UNIT);
                        response.setTimeoutHandler(response1 -> {
                            logger.warn("Sending timeout as {} : {} took over {}", invocation.getMethod().getDeclaringClass().getName(), invocation.getMethod().getName(), timeoutval);
                            HttpUtils.resumeAsyncError(response1, NodeErrors.ERR_ASYNC_RESPONSE_TIMEOUT.getError(String.valueOf(timeoutval)));
                        });

                        if (SystemPropertyUtils.isDebugging()) {
                            logger.info("Setting Async Timeout for {}:{} to {} seconds", invocation.getMethod().getDeclaringClass().getName(), invocation.getMethod().getName(), seconds);
                        }

                        // There can only be one AsyncResponse 
                        break;
                    }
                }
            }
            return invocation.proceed();
        }
    }

}
