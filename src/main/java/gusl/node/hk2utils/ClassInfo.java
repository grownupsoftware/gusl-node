package gusl.node.hk2utils;

import lombok.CustomLog;
import org.glassfish.hk2.api.PerLookup;
import org.glassfish.hk2.api.PerThread;
import org.glassfish.hk2.api.Proxiable;
import org.glassfish.hk2.api.Unproxiable;
import org.jvnet.hk2.annotations.Contract;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Singleton;
import java.util.HashSet;

/**
 * ClassInfo
 */
@CustomLog
public class ClassInfo {

    private final String theClassName;
    private final HashSet<String> theAnnotations = new HashSet<>();

    protected ClassInfo(String className) {
        theClassName = className;
        logger.debug("Found annotated class {}", className);
    }

    protected void addAnnotation(String annotation) {
        theAnnotations.add(annotation);
        logger.debug("Added annotation {} to {}", annotation, theClassName);
    }

    public String getName() {
        return theClassName;
    }

    public boolean hasAnnotation(String annotation) {
        return theAnnotations.contains(annotation);
    }

    public boolean isService() {
        return hasAnnotation(Service.class.getName());
    }

    public boolean isContract() {
        return hasAnnotation(Contract.class.getName());
    }

    public boolean isProxyable() {
        return hasAnnotation(Proxiable.class.getName());
    }

    public boolean isUnproxyable() {
        return hasAnnotation(Unproxiable.class.getName());
    }

    public String getScope() {
        if (hasAnnotation(Singleton.class.getName())) {
            return Singleton.class.getName();
        } else if (hasAnnotation(PerLookup.class.getName())) {
            return PerLookup.class.getName();
        } else if (hasAnnotation(PerThread.class.getName())) {
            return PerThread.class.getName();
        }
        return null;
    }
}
