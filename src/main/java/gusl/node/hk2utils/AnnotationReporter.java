package gusl.node.hk2utils;

import eu.infomas.annotation.AnnotationDetector.TypeReporter;
import lombok.CustomLog;

import java.lang.annotation.Annotation;

/**
 * Reporter
 */
@CustomLog
public class AnnotationReporter implements TypeReporter {

    AnnotatedClasses theAnnotatedClasses;
    Class<? extends Annotation>[] theAnnotationsToLookFor;

    public AnnotationReporter(AnnotatedClasses annotatedClasses, Class<? extends Annotation>[] annotationsToLookFor) {
        theAnnotatedClasses = annotatedClasses;
        theAnnotationsToLookFor = annotationsToLookFor;
    }

    @Override
    public Class<? extends Annotation>[] annotations() {
        return theAnnotationsToLookFor;
    }

    @Override
    public void reportTypeAnnotation(Class<? extends Annotation> annotation, String className) {
        logger.debug("For annotation: {} found class: {}", annotation.getSimpleName(), className);

//        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
//
//        try {
//            Class<?> klass = Class.forName(className, false, loader);
//            logger.info("\t\tclass: {}", klass.getCanonicalName());
//        } catch (ClassNotFoundException e) {
//            logger.info("\t\tfailed: {}", className);
//            //logger.error("Could not load detected annotated class", e);
//        }

        theAnnotatedClasses.addAnnotation(annotation, className);
    }
}
