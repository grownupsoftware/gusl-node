package gusl.node.hk2utils;

import lombok.CustomLog;
import org.glassfish.hk2.utilities.DescriptorImpl;

import javax.inject.Named;
import javax.inject.Singleton;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AnnotatedClasses
 */
@CustomLog
public class AnnotatedClasses {

    private Map<String, ClassInfo> theAnnotations = new HashMap<>();

    private final ClassLoader theClassLoader = Thread.currentThread().getContextClassLoader();

    public AnnotatedClasses() {
    }

    protected void addAnnotation(Class<? extends Annotation> annotation, String className) {
        ClassInfo ci = theAnnotations.get(className);
        if (ci == null) {
            ci = new ClassInfo(className);
            theAnnotations.put(className, ci);
        }
        ci.addAnnotation(annotation.getName());
    }

    protected boolean isContract(String className) {
        ClassInfo ci = theAnnotations.get(className);
        if (ci != null) {
            return ci.isContract();
        }
        return false;
    }

    protected boolean isService(String className) {
        ClassInfo ci = theAnnotations.get(className);
        if (ci != null) {
            return ci.isService();
        }
        return false;
    }

    public Class<?>[] getAnnotatedClasses() throws ClassNotFoundException {
        Class<?>[] result = new Class<?>[theAnnotations.size()];

        int i = 0;
        for (String className : theAnnotations.keySet()) {
            result[i++] = Class.forName(className, false, theClassLoader);
        }
        return result;
    }

    public List<DescriptorImpl> createDescriptors() throws ClassNotFoundException {
        ArrayList<DescriptorImpl> results = new ArrayList<>();

        for (ClassInfo ci : theAnnotations.values()) {
            if (ci.isService()) {
                Class<?> c = Class.forName(ci.getName(), false, theClassLoader);

                DescriptorImpl di = new DescriptorImpl();
                di.setImplementation(ci.getName());

                String name = null;
                if (ci.hasAnnotation(Named.class.getName())) {
                    name = ((Named) c.getAnnotation(Named.class)).value();
                }
                if (name == null || name.length() == 0) {
                    name = ci.getName().substring(ci.getName().lastIndexOf('.') + 1);
                }

                di.setName(name);
                di.addAdvertisedContract(ci.getName());

                for (String contract : getParentContracts(c.getInterfaces())) {
                    di.addAdvertisedContract(contract);
                }

                for (String contract : getParentContracts(Class.forName(ci.getName(), false, theClassLoader).getSuperclass())) {
                    di.addAdvertisedContract(contract);
                }

                String scope = ci.getScope();
                if (scope != null) {
                    di.setScope(ci.getScope());
                } else {
                    di.setScope(Singleton.class.getName());
                }

                if (ci.isProxyable()) {
                    di.setProxiable(true);
                } else if (ci.isUnproxyable()) {
                    di.setProxiable(false);
                }

                results.add(di);
                logger.info("Created descriptor {}", di.toString());
            }
        }
        return results;
    }

    /**
     * for parent classes
     */
    private ArrayList<String> getParentContracts(Class<?> parentClass) {
        ArrayList<String> result = new ArrayList<>();
        if (parentClass == null) {
            return result;
        }
        if (isContract(parentClass.getName()) || isService(parentClass.getName())) {
            result.add(parentClass.getName());
        }

        for (String contract : getParentContracts(parentClass.getInterfaces())) {
            result.add(contract);
        }
        result.addAll(getParentContracts(parentClass.getSuperclass()));
        return result;
    }

    /**
     * For interfaces
     */
    private ArrayList<String> getParentContracts(Class<?>[] parentInterfaces) {
        ArrayList<String> result = new ArrayList<>();
        if (parentInterfaces == null) {
            return result;
        }

        for (Class<?> c : parentInterfaces) {
            if (isContract(c.getName()) || isService(c.getName())) {
                result.add(c.getName());
            }
            result.addAll(getParentContracts(c.getInterfaces()));
        }
        return result;
    }
}
