package gusl.node.messaging;

import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 01/09/2022
 */
@Contract
public interface GenericMessaginService extends MessagingService {
}
