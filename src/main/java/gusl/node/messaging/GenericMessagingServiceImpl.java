package gusl.node.messaging;

import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.MessagingConfig;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.slack.SlackService;
import gusl.node.teams.TeamsService;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

/**
 * @author dhudson
 * @since 01/09/2022
 */
@Service
public class GenericMessagingServiceImpl implements GenericMessaginService {

    private MessagingService theDefaultService;

    @Inject
    private SlackService theSlackService;

    @Inject
    private TeamsService theTeamsService;

    @Override
    public void sendOpsMessage(String message) {
        theDefaultService.sendOpsMessage(message);
    }

    @Override
    public void sendAlertMessage(String message) {
        theDefaultService.sendAlertMessage(message);
    }

    @Override
    public void sendResultMessage(String message) {
        theDefaultService.sendResultMessage(message);
    }

    @Override
    public void sendDevOpsMessage(String message) {
        theDefaultService.sendDevOpsMessage(message);
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theDefaultService = theSlackService;
        if (config.getMessagingConfig() != null) {
            MessagingConfig messageConfig = config.getMessagingConfig();
            if (messageConfig.getService() != null) {
                switch (messageConfig.getService()) {
                    case SLACK:
                        theDefaultService = theSlackService;
                        break;
                    case TEAMS:
                        theDefaultService = theTeamsService;
                        break;
                }
            }
        }
    }
}
