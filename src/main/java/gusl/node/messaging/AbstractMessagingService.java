/* Copyright lottomart */
package gusl.node.messaging;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.router.client.RouterHttpClientImpl;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.concurrent.Future;

/**
 * @author grant
 */
public abstract class AbstractMessagingService implements MessagingService {

    public final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    protected final String CONSTRUCT_NAME = SystemPropertyUtils.getConstructName();

    @Inject
    protected RouterHttpClientImpl theRouterClient;

    protected boolean hasConstruct() {
        return StringUtils.isNotBlank(CONSTRUCT_NAME);
    }

    protected String addConstructToMessage(String message) {
        return "[" + CONSTRUCT_NAME + "] " + message;
    }

    protected Future<Response> sendMessage(String url, Object message) {
        //theRouterClient.getHttpClient().setDebugLogging(true);
        logger.info("Sending .... {} : {} from {}", url, message, this.getClass().getName());
        return theRouterClient.execute(url, message);
    }
}
