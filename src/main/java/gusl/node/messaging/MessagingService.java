/* Copyright lottomart */
package gusl.node.messaging;

import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author grant
 */
public interface MessagingService extends GUSLConfigurable {

    void sendOpsMessage(String message);

    void sendAlertMessage(String message);

    void sendResultMessage(String message);

    void sendDevOpsMessage(String message);

}
