package gusl.node.service;

import gusl.core.exceptions.GUSLException;

/**
 * Standard startup / shutdown interface.
 *
 * @author dhudson
 */
public interface Controllable {

    public void startup() throws GUSLException;

    public void shutdown();
}
