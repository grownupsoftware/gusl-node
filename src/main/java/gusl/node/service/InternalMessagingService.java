package gusl.node.service;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.nodeconfig.NodeType;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 09/11/2022
 */
@Contract
public interface InternalMessagingService {

    <T> void broadcastEvent(T event) throws GUSLErrorException;

    <T> void publishEvent(T event) throws GUSLErrorException;

    <T> void publishEventReportException(T event);

    public <T> void broadcastEventReportException(T event);

    void publishToOneNodeOfType(NodeType type, Object event) throws GUSLErrorException;

    void publishTo(NodeType type, Object event);

    void publishToAllNodesInList(Object event, NodeType... types) throws GUSLErrorException;

    void publishToAllNodesOfType(Object event, NodeType type) throws GUSLErrorException;
}
