package gusl.node.service;

import gusl.annotations.form.page.DownloadRequest;
import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * @author dhudson
 * @since 31/10/2022
 */
@Contract
public interface CSVService {

    <T> String process(Class<T> type, Collection<T> stuff) throws GUSLErrorException;

    <T> String process(Class<T> type, Collection<T> stuff, List<String> headers, List<String> fields) throws GUSLErrorException;

    <T, S> String process(DownloadRequest downloadRequest, Class<T> type, Collection<T> stuff, Class<S> shape) throws GUSLErrorException;

    void register(Class<?> aClass, Function<Object, String> parser);

}
