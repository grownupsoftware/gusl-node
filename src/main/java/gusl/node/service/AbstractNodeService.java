package gusl.node.service;

import com.fasterxml.jackson.core.type.TypeReference;
import gusl.annotations.form.page.OrderDirection;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.QueryCondition;
import gusl.annotations.form.page.QueryConditions;
import gusl.core.eventbus.EventBusRepositoryException;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.StringUtils;
import gusl.model.Identifiable;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.nodeconfig.NodeType;
import gusl.node.converter.ModelConverter;
import gusl.node.eventbus.NodeEventBus;
import gusl.router.client.RouterHttpClient;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author dhudson
 */
public abstract class AbstractNodeService {

    public final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    public static final CacheAction UPDATE_ACTION = CacheAction.UPDATE;
    public static final CacheAction INSERT_ACTION = CacheAction.ADD;
    public static final CacheAction REMOVE_ACTION = CacheAction.REMOVE;

    @Inject
    protected NodeEventBus theInternalBus;

    @Inject
    protected Provider<ModelConverter> theModelConverterProvider;

    @Inject
    protected RouterHttpClient theRouterClient;

    public ModelConverter getModelConverter() {
        return theModelConverterProvider.get();
    }

    public NodeEventBus getInternalBus() {
        return theInternalBus;
    }

    public <IN, OUT> OUT convert(IN in, Class<OUT> klass) {
        return getModelConverter().convert(in, klass);
    }

    public <T> void prettyPrint(T value) {
        logger.info("{}", JsonUtils.limitedPrettyPrint(value));
    }

    private final TypeReference<HashMap<String, Object>> theTypeReference = new TypeReference<HashMap<String, Object>>() {
    };

    protected <T> Map<String, Object> convertToMap(T value) {
        return ObjectMapperFactory.getDefaultObjectMapper().convertValue(value, theTypeReference);
    }

    protected <T> T convertFromMap(Map<String, Object> value, Class<T> klass) {
        return ObjectMapperFactory.getDefaultObjectMapper().convertValue(value, klass);
    }

    protected <T> T convertFromString(String data, Class<T> dataClass) {
        try {
            return ObjectMapperFactory.getDefaultObjectMapper().readValue(data, dataClass);
        } catch (IOException ex) {
            logger.warn("Failed to parse object [{}] into {}", data, dataClass.getCanonicalName());
            return null;
        }
    }

    protected <T> String convertToString(T data) {
        try {
            return ObjectMapperFactory.getDefaultObjectMapper().writeValueAsString(data);
        } catch (IOException ex) {
            logger.warn("Failed to parse object [{}] to string", data);
            return null;
        }
    }

    /**
     * Post the event on the internal event bus only.
     *
     * @param <T>
     * @param event
     */
    public <T> void postEvent(T event) {
        theInternalBus.post(event);
    }

    public <T> void persistAndPost(T event) throws EventBusRepositoryException {
        theInternalBus.persistAndPost(event);
    }

    public <S, D> List<D> convert(List<S> sourceList, Class<D> destinationType) {
        return getModelConverter().convert(sourceList, destinationType);
    }

    /**
     * Post the event on the internal and external event bus.
     *
     * @param <T>
     * @param event
     * @throws GUSLErrorException
     */
    public <T> void broadcastEvent(T event) throws GUSLErrorException {
        theInternalBus.post(event);
        theRouterClient.publishEvent(event);
    }

    /**
     * Post the event on the external event bus only.
     * <p>
     * NB: the caller will not get this event unless it calls postEvent
     *
     * @param <T>
     * @param event
     * @throws GUSLErrorException
     */
    public <T> void publishEvent(T event) throws GUSLErrorException {
        theRouterClient.publishEvent(event);
    }

    /**
     * Post the event on the external event bus only, trapping the exception and
     * reporting it.
     *
     * @param <T>
     * @param event
     */
    public <T> void publishEventReportException(T event) {
        try {
            publishEvent(event);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to publish event {}:{}", event.getClass().getName(), event, ex);
        }
    }

    public <T> void broadcastEventReportException(T event) {
        try {
            broadcastEvent(event);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to broadcast event {}:{}", event.getClass().getName(), event, ex);
        }
    }

    public void publishToOneNodeOfType(NodeType type, Object event) throws GUSLErrorException {
        theRouterClient.publishToOneNodeOfType(type, event);
    }

    public void publishTo(NodeType type, Object event) {
        try {
            theRouterClient.publishToOneNodeOfType(type, event);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to broadcast event {}:{}", event.getClass().getName(), event, ex);
        }
    }

    public void publishToAllNodesInList(Object event, NodeType... types) throws GUSLErrorException {
        theRouterClient.publishToAllNodesInList(event, types);
    }

    public void publishToAllNodesOfType(Object event, NodeType type) throws GUSLErrorException {
        theRouterClient.publishToAllNodesOfType(type, event);
    }

    public boolean hasEvent(AbstractCacheActionEvent event) {
        return nonNull(event) && nonNull(event.getCacheAction()) && nonNull(event.getDataObject());
    }

    public void setId(Identifiable<String> entity) {
        if (entity.getId() == null) {
            entity.setId(IdGenerator.generateUniqueNodeIdAsString());
        }
    }

    public boolean hasQueryByStatus(PageRequest request) {
        if (isNull(request)
                || isNull(request.getQueryConditions())
                || isNull(request.getQueryConditions().getConditions())
                || request.getQueryConditions().getConditions().isEmpty()) {
            return false;
        }
        return safeStream(request.getQueryConditions().getConditions())
                .filter(condition -> nonNull(condition.getFieldName()))
                .anyMatch(condition -> "status".equals(condition.getFieldName()));
    }

    public void addDefaultSortIfNone(PageRequest request, String orderParam, OrderDirection orderDirection) {
        if (isNull(request) || StringUtils.isNotBlank(orderParam)) {
            return;
        }
        request.setOrderParam(orderParam);
        request.setOrderDirection(orderDirection);
    }

    public void addQueryCondition(PageRequest request, QueryCondition queryCondition) {
        if (isNull(request)) {
            return;
        }
        if (isNull(request.getQueryConditions())
                || isNull(request.getQueryConditions().getConditions())) {
            request.setQueryConditions(QueryConditions.builder().build());
        }
        final boolean conditionExists = safeStream(request.getQueryConditions().getConditions())
                .anyMatch(condition -> condition.getFieldName().equals(queryCondition.getFieldName()));

        if (!conditionExists) {
            request.getQueryConditions().getConditions().add(queryCondition);
        }
    }

}
