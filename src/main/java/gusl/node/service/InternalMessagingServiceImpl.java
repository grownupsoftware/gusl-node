package gusl.node.service;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.nodeconfig.NodeType;
import gusl.node.eventbus.NodeEventBus;
import gusl.router.client.RouterHttpClient;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

/**
 * @author dhudson
 * @since 09/11/2022
 */
@Service
@CustomLog
public class InternalMessagingServiceImpl implements InternalMessagingService {

    @Inject
    private NodeEventBus theInternalBus;
    @Inject
    private RouterHttpClient theRouterClient;

    @Override
    public <T> void broadcastEvent(T event) throws GUSLErrorException {
        theInternalBus.post(event);
        theRouterClient.publishEvent(event);
    }

    @Override
    public <T> void publishEvent(T event) throws GUSLErrorException {
        theRouterClient.publishEvent(event);
    }

    @Override
    public <T> void publishEventReportException(T event) {
        try {
            publishEvent(event);
        } catch (GUSLErrorException ex) {
            this.logger.warn("Unable to publish event {}:{}", new Object[]{event.getClass().getName(), event, ex});
        }

    }

    @Override
    public <T> void broadcastEventReportException(T event) {
        try {
            broadcastEvent(event);
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to broadcast event {}:{}", new Object[]{event.getClass().getName(), event, ex});
        }

    }

    @Override
    public void publishToOneNodeOfType(NodeType type, Object event) throws GUSLErrorException {
        theRouterClient.publishToOneNodeOfType(type, event);
    }

    @Override
    public void publishTo(NodeType type, Object event) {
        try {
            theRouterClient.publishToOneNodeOfType(type, event);
        } catch (GUSLErrorException ex) {
            this.logger.warn("Unable to broadcast event {}:{}", new Object[]{event.getClass().getName(), event, ex});
        }

    }

    @Override
    public void publishToAllNodesInList(Object event, NodeType... types) throws GUSLErrorException {
        theRouterClient.publishToAllNodesInList(event, types);
    }

    @Override
    public void publishToAllNodesOfType(Object event, NodeType type) throws GUSLErrorException {
        theRouterClient.publishToAllNodesOfType(type, event);
    }
}
