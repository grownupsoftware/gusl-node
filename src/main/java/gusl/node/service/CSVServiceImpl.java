package gusl.node.service;

import gusl.annotations.form.UiField;
import gusl.annotations.form.page.DownloadRequest;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.StringUtils;
import gusl.loaders.StaticLoader;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jvnet.hk2.annotations.Service;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;

/**
 * @author dhudson
 * @since 31/10/2022
 */
@Service
@NoArgsConstructor
public class CSVServiceImpl implements CSVService {

    private final Map<Class<?>, Function<Object, String>> theParserMap = new HashMap<>();

    public void register(Class<?> aClass, Function<Object, String> parser) {
        theParserMap.put(aClass, parser);
    }

    @Override
    public <T> String process(Class<T> type, Collection<T> stuff) throws GUSLErrorException {
        List<String> headers = ClassUtils.getFieldNamesFor(type);
        return process(type, stuff, headers, headers);
    }

    public <T, S> String process(DownloadRequest downloadRequest, Class<T> type, Collection<T> stuff, Class<S> shape) throws GUSLErrorException {

        Map<String, Field> shapeFields = ClassUtils.getFieldForAsMap(shape, true);
        List<String> headers = new ArrayList<>();

        for (String field : downloadRequest.getFields()) {
            Field boField = shapeFields.get(field);
            UiField uiField = boField.getAnnotation(UiField.class);
            if (uiField != null) {
                if (StringUtils.isNotBlank(uiField.label())) {
                    headers.add(uiField.label());
                } else {
                    headers.add(parseCodeToLabel(boField.getName()));
                }
            } else {
                headers.add(field);
            }
        }

        return process(type, stuff, headers, downloadRequest.getFields());
    }

    private static String parseCodeToLabel(String name) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(name.charAt(0));
        for (int i = 0; i < name.length() - 1; i++) {
            char c1 = name.charAt(i);
            char c2 = name.charAt(i + 1);

            if (Character.isLowerCase(c1) && Character.isUpperCase(c2)) {
                stringBuilder.append(" ");
            }
            stringBuilder.append(c2);
        }

        String s = stringBuilder.toString();
        return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }


    @Override
    public <T> String process(Class<T> type, Collection<T> stuff, List<String> headers, List<String> fields) throws GUSLErrorException {
        return StaticLoader.dumpCSV(type, stuff, headers, fields, theParserMap);
    }


}
