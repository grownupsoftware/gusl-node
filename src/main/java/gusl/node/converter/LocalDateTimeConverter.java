package gusl.node.converter;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.jvnet.hk2.annotations.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.util.Objects.isNull;

@Service
public class LocalDateTimeConverter extends BidirectionalConverter<LocalDateTime, Date> {
    @Override
    public Date convertTo(LocalDateTime source, Type<Date> destinationType, MappingContext mappingContext) {
        return isNull(source) ? null : Date.from(source.atZone(ZoneId.of("UTC")).toInstant());
    }

    @Override
    public LocalDateTime convertFrom(Date source, Type<LocalDateTime> destinationType, MappingContext mappingContext) {
        return isNull(source) ? null : source.toInstant()
                .atZone(ZoneId.of("UTC"))
                .toLocalDateTime();
    }
}
