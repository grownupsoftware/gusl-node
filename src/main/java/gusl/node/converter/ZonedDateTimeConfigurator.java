/* Copyright lottomart */
package gusl.node.converter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import org.jvnet.hk2.annotations.Service;

/**
 *
 * @author grant
 */
@Service
public class ZonedDateTimeConfigurator implements ModelConverterConfigurator {

    public final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

//    //  Wed Oct 12 1977 00:00:00 GMT+0100
//    private static final ThreadLocal<DateTimeFormatter> ZONED_DATE_PARSER
//            = new ThreadLocal<DateTimeFormatter>() {
//        @Override
//        protected DateTimeFormatter initialValue() {
//            return DateTimeFormatter.ofPattern("EEE MMM dd yyyy HH:mm:ss O");
//        }
//    };

    
// 2011-12-03T10:15:30+01:00[Europe/Paris]    
    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {

        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<String, ZonedDateTime>() {
            @Override
            public ZonedDateTime convert(String dateStr, Type<? extends ZonedDateTime> type, MappingContext mc) {
                try {
//                    return ZonedDateTime.parse(
//                            dateStr,
//                            ZONED_DATE_PARSER.get());
                    return ZonedDateTime.parse(
                            dateStr,
                            DateTimeFormatter.ISO_ZONED_DATE_TIME);
                } catch (DateTimeParseException ex) {
                    logger.error("Failed to parse {}", dateStr);
                    return null;
                }

            }
        });
    }

}
