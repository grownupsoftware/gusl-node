package gusl.node.converter;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import org.jvnet.hk2.annotations.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
public class LocalDateTimeConfigurator implements ModelConverterConfigurator {

    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.getConverterFactory().registerConverter(new LocalDateConverter());
        mapperFactory.getConverterFactory().registerConverter(new LocalDateTimeConverter());

        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDateTime, LocalDateTime>() {
            @Override
            public LocalDateTime convert(LocalDateTime source, Type<? extends LocalDateTime> destinationType, MappingContext mc) {
                return LocalDateTime.from(source);
            }
        });
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDate, LocalDate>() {
            @Override
            public LocalDate convert(LocalDate source, Type<? extends LocalDate> destinationType, MappingContext mc) {
                return LocalDate.from(source);
            }
        });
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDateTime, LocalDate>() {
            @Override
            public LocalDate convert(LocalDateTime dateTime, Type<? extends LocalDate> type, MappingContext mappingContext) {
                return dateTime.toLocalDate();
            }
        });
    }
}
