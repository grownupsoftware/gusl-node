package gusl.node.converter;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import org.jvnet.hk2.annotations.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static java.util.Objects.isNull;

@Service
public class LocalDateConverter extends BidirectionalConverter<LocalDate, Date> {

    @Override
    public Date convertTo(LocalDate source, Type<Date> destinationType, MappingContext mappingContext) {
        return isNull(source) ? null : Date.from(source.atStartOfDay().atZone(ZoneId.of("UTC")).toInstant());
    }

    @Override
    public LocalDate convertFrom(Date source, Type<LocalDate> destinationType, MappingContext mappingContext) {
        return isNull(source) ? null : source.toInstant()
                .atZone(ZoneId.of("UTC"))
                .toLocalDate();
    }
}
