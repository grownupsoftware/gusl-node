package gusl.node.converter;

import ma.glasnost.orika.MapperFactory;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface ModelConverterConfigurator {

    void configureMapperFactory(MapperFactory mapperFactory);
}
