package gusl.node.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;

/**
 * @author grant
 */
public abstract class AbstractModelConfigurator {

    public final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    @Inject
    private Provider<ModelConverter> theModelConverterProvider;

    protected final ObjectMapper theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();

    protected final TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
    };

    protected ModelConverter getModelConverter() {
        return theModelConverterProvider.get();
    }

    protected <IN, OUT> OUT convert(IN response, Class<OUT> responseClass) {
        return getModelConverter().convert(response, responseClass);
    }

    protected <IN, OUT> List<OUT> convertList(List<IN> list, Class<OUT> responseClass) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        return getModelConverter().convert(list, responseClass);
    }

    public <S, D> void merge(S sourceObject, D destinationObject) {
        getModelConverter().merge(sourceObject, destinationObject);
    }

    private final TypeReference<HashMap<String, Object>> theTypeReference = new TypeReference<HashMap<String, Object>>() {
    };

    protected <T> Map<String, Object> convertToMap(T value) {
        return ObjectMapperFactory.getDefaultObjectMapper().convertValue(value, theTypeReference);
    }

    protected <T> T convertFromMap(Map<String, Object> value, Class<T> klass) {
        return ObjectMapperFactory.getDefaultObjectMapper().convertValue(value, klass);
    }

    protected void log(Object a, Object b) {
        if (a == null || b == null) {
            return;
        }
        try {
            logger.info("Converting: \nfrom {}: {}\n  to {}: {}",
                    a.getClass().getCanonicalName(),
                    theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(a),
                    b.getClass().getCanonicalName(),
                    theObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(b));
        } catch (JsonProcessingException | NullPointerException ex) {
            logger.error("Error pretty printing object", ex);
        }

    }

    protected <T> T getDataObject(Class<T> dataClass, String data) {
        try {
            return ObjectMapperFactory.getDefaultObjectMapper().readValue(data, dataClass);
        } catch (IOException ex) {
            logger.warn("Failed to parse object [{}] into {}", data, dataClass.getCanonicalName());
            return null;
        }
    }

    protected <T> String getDataAsString(T data) {
        try {
            return ObjectMapperFactory.getDefaultObjectMapper().writeValueAsString(data);
        } catch (IOException ex) {
            logger.warn("Failed to convert object [{}] into String", data, ex);
            return null;
        }
    }

    public static <T, T1> T1 mergeFirstIntoSecond(T sourceObject, T1 targetObject, boolean notAssignable, ModelConverter converter) {
        if (!notAssignable && !targetObject.getClass().isAssignableFrom(sourceObject.getClass())) {
            return targetObject;
        }
        if (isNull(sourceObject)) {
            return targetObject;
        }

        converter.merge(sourceObject, targetObject);
        return targetObject;
    }

}
