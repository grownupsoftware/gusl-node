package gusl.node.converter;

import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import lombok.Getter;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

/**
 * @author dhudson
 * @since 24/10/2022
 */
public abstract class GUSLCustomMapper<A, B> extends CustomMapper<A, B> {

    private final GUSLLogger theLogger;
    @Getter
    private MappingContext theMappingContext;

    public GUSLCustomMapper() {
        this(GUSLLogManager.APPLICATION_LOGGER);
    }

    public GUSLCustomMapper(GUSLLogger logger) {
        super();
        if (logger != null) {
            theLogger = logger;
        } else {
            theLogger = GUSLLogManager.APPLICATION_LOGGER;
        }
    }

    @Override
    public final void mapAtoB(A a, B b, MappingContext context) {
        try {
            theMappingContext = context;
            mapAtoB(a, b);
        } catch (Throwable t) {
            theLogger.warn("Exception caught mapping {} -> {} [{}]", getAType(), getBType(), a, t);
        }
    }

    @Override
    public final void mapBtoA(B b, A a, MappingContext context) {
        try {
            theMappingContext = context;
            mapBtoA(b, a);
        } catch (Throwable t) {
            theLogger.warn("Exception caught mapping {} -> {} [{}]", getBType(), getAType(), b, t);
        }
    }

    public void mapAtoB(A from, B to) {
    }

    public void mapBtoA(B from, A to) {
    }

}
