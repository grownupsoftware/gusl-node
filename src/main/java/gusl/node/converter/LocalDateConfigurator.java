package gusl.node.converter;

import java.time.LocalDate;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;

public class LocalDateConfigurator implements ModelConverterConfigurator {

    @Override
    public void configureMapperFactory(MapperFactory mapperFactory) {
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<LocalDate, LocalDate>() {
            @Override
            public LocalDate convert(LocalDate source, Type<? extends LocalDate> destinationType, MappingContext mc) {
                return LocalDate.from(source);
            }
        });
    }
}
