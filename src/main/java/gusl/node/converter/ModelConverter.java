package gusl.node.converter;

import gusl.core.utils.Utils;
import gusl.node.bootstrap.GUSLServiceLocator;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.glassfish.hk2.api.ServiceHandle;
import org.jvnet.hk2.annotations.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Service
@CustomLog
@NoArgsConstructor
public class ModelConverter {

    private MapperFactory mapperFactory;
    private MapperFacade mapper;

    @PostConstruct
    public void init() {
        mapperFactory = new DefaultMapperFactory.Builder().build();
        configureMapperFactory(mapperFactory);
        mapper = mapperFactory.getMapperFacade();
    }

    public <S, D> D convert(S sourceObject, Class<D> destinationClass) {
        if (sourceObject == null) {
            return null;
        }
        try {
            return mapper.map(sourceObject, destinationClass);
        } catch (Throwable t) {
            logger.error("Error converting Source Object {} -> [{}] Destination Class [{}]", sourceObject.getClass().getCanonicalName(), sourceObject,
                    (destinationClass == null ? "null" : destinationClass.getCanonicalName()), t);
            return null;
        }
    }

    public <S, D> List<D> convert(List<S> sourceList, Class<D> destinationType) {
        if (sourceList == null || sourceList.isEmpty()) {
            return Collections.emptyList();
        }

        try {
            return mapper.mapAsList(sourceList, destinationType);
        } catch (Throwable t) {
            logger.error("Error converting Source List [{}] Destination Type [{}]", sourceList, (destinationType == null ? "null" : destinationType.getCanonicalName()), t);
            return null;
        }
    }

    public <S, D> void merge(S sourceObject, D destinationObject) {
        if (sourceObject == null || destinationObject == null) {
            return;
        }
        try {
            mapper.map(sourceObject, destinationObject);
        } catch (Throwable t) {
            logger.error("Error merging Source Object [{}] Destination Class [{}]",
                    sourceObject.getClass().getCanonicalName(), destinationObject.getClass().getCanonicalName(), t);
        }
    }

    private void configureMapperFactory(MapperFactory mapperFactory) {
        List<ServiceHandle<ModelConverterConfigurator>> allServiceHandlers = GUSLServiceLocator.getAllServiceHandlers(ModelConverterConfigurator.class);

        logger.debug("Applying model converter configurations ...");

        Utils.safeStream(allServiceHandlers).forEach(serviceHandler -> {
            logger.debug("Model Converter: {}", serviceHandler.getService().getClass().getCanonicalName());
            serviceHandler.getService().configureMapperFactory(mapperFactory);
        });
        logger.debug("Applied model converter configurations.");

    }

    public MapperFacade getMapperFacade() {
        return mapper;
    }

    public MapperFactory getMapperFactory() {
        return mapperFactory;
    }
}
