package gusl.node.teams;

import gusl.core.tostring.ToString;
import lombok.*;

/**
 * @author dhudson
 * @since 31/08/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamsMessage {

    private String text;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
