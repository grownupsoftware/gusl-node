package gusl.node.teams;

import gusl.node.messaging.MessagingService;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 * @since 31/08/2022
 */
@Contract
public interface TeamsService extends MessagingService {
}
