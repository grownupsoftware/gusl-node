package gusl.node.teams;

import gusl.core.exceptions.GUSLException;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.TeamsConfig;
import gusl.node.jersey.JerseyHttpConfig;
import gusl.node.jersey.JerseyNodeClient;
import gusl.node.messaging.AbstractMessagingService;
import org.jvnet.hk2.annotations.Service;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

/**
 * @author dhudson
 * @since 31/08/2022
 */
@Service
public class TeamsServiceImpl extends AbstractMessagingService implements TeamsService {

    private TeamsConfig theConfig;
    private final JerseyNodeClient theClient;

    public TeamsServiceImpl() {
        theClient = new JerseyNodeClient(JerseyHttpConfig.of(ObjectMapperFactory.createDefaultObjectMapper()));
    }

    @Override
    public void sendOpsMessage(String message) {
        sendMessageToTeams(theConfig.getOpsChannel(), message);
    }

    @Override
    public void sendAlertMessage(String message) {
        sendMessageToTeams(theConfig.getAlertChannel(), message);
    }

    @Override
    public void sendResultMessage(String message) {
        sendMessageToTeams(theConfig.getResultChannel(), message);
    }

    @Override
    public void sendDevOpsMessage(String message) {
        sendMessageToTeams(theConfig.getOpsChannel(), message);
    }

    private void sendMessageToTeams(String webHook, String message) {
        if (hasConstruct()) {
            if (StringUtils.isNotBlank(webHook)) {
                BackgroundThreadPoolExecutor.submit(() -> {
                    try {
                        Invocation.Builder builder = theClient.getJsonBuilder(webHook);
                        Response response = builder.post(Entity.json(TeamsMessage.builder().text(addConstructToMessage(message)).build()));
                        logger.debug("Response {}", response);
                    } catch (Throwable t) {
                        logger.warn("WTF!", t);
                    }
                });
            }
        }
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        config(config.getTeamsConfig());
    }

    public void config(TeamsConfig config) {
        if (config == null) {
            theConfig = new TeamsConfig();
        } else {
            theConfig = config;
        }
    }
}
