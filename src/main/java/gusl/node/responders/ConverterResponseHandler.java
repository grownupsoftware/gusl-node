package gusl.node.responders;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.node.converter.ModelConverter;
import gusl.query.QueryParams;

import javax.inject.Inject;
import javax.ws.rs.container.AsyncResponse;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static gusl.core.utils.Utils.safeStream;
import static java.util.stream.Collectors.toList;

/**
 * @author dhudson
 * @since 22/09/2022
 */
public abstract class ConverterResponseHandler<T> extends InjectedResponseHandler<T> {

    @Inject
    protected ModelConverter theModelConverter;

    public ConverterResponseHandler(AsyncResponse asyncResponse) {
        super(asyncResponse);
    }

    protected <S, D> D convert(S sourceObject, Class<D> destinationClass) {
        return theModelConverter.convert(sourceObject, destinationClass);
    }

    protected <S, D> List<D> convert(List<S> sourceList, Class<D> destinationType) {
        return theModelConverter.convert(sourceList, destinationType);
    }

    protected <S, D> void merge(S sourceObject, D destinationObject) {
        theModelConverter.merge(sourceObject, destinationObject);
    }

    protected <T> PagedDAOResponse<T> buildPagedResultFrom(QueryParams params, Class<T> dtoClass, List<?> stuff) {
        if (stuff == null) {
            return PagedDAOResponse.<T>builder().content(Collections.emptyList()).total(0).queryParams(params).build();
        }
        return PagedDAOResponse.<T>builder().content(convert(stuff, dtoClass)).total(stuff.size()).queryParams(params).build();
    }

    protected <T> PagedDAOResponse<T> transformPagedResponse(PagedDAOResponse<?> pagedResponse, Class<T> dtoClass) {
        if (pagedResponse == null) {
            return PagedDAOResponse.<T>builder().content(Collections.emptyList()).total(0).build();
        }

        return PagedDAOResponse.<T>builder().queryParams(pagedResponse.getQueryParams())
                .content(safeStream(pagedResponse.getContent()).filter(Objects::nonNull).map(item -> (T) convert(item, dtoClass)).collect(toList()))
                .total(pagedResponse.getTotal())
                .build();
    }
}
