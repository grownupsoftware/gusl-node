package gusl.node.responders;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.exceptions.UnAuthorizedException;
import gusl.model.exceptions.ValidationException;
import gusl.node.transport.HttpUtils;

import javax.ws.rs.container.AsyncResponse;

import static gusl.node.providers.GUSLErrorExceptionMapper.convertToErrors;
import static gusl.node.transport.HttpUtils.resumeAsyncError;

/**
 * @author dhudson
 * @since 22/09/2022
 */
public abstract class AbstractResponseHandler<T> {

    public GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private final AsyncResponse theAsyncResponse;

    public AbstractResponseHandler(AsyncResponse asyncResponse) {
        theAsyncResponse = asyncResponse;
    }

    public void consume(T data, Throwable t) {
        if (t != null) {
            handleError(t);
        } else {
            try {
                process(data);
            } catch (Throwable throwable) {
                handleError(throwable);
            }
        }
    }

    protected void handleError(Throwable t) {
        if (t instanceof UnAuthorizedException) {
            logger.warn("UnAuthorizedException in ResponseHandler {}", this.getClass().getName());
            resumeAsyncError(theAsyncResponse, t);
        } else if (t instanceof ValidationException) {
            // no error
            resumeAsyncError(theAsyncResponse, t);
        } else if (t instanceof GUSLErrorException) {
            GUSLErrorException exception = (GUSLErrorException) t;
            if (!exception.getErrors().isEmpty()) {
                if (!exception.getErrors().get(0).isIgnoreLogging() && !exception.isSuppressStack()) {
                    logger.warn("WRN001 GUSLErrorException in ResponseHandler {}", this.getClass().getName(), t);
                }
            } else {
                logger.warn("WRN002 GUSLErrorException in ResponseHandler {}", this.getClass().getName(), t);
            }
            resumeAsyncError(theAsyncResponse, convertToErrors((GUSLErrorException) t));
        } else {
            logger.warn("WRN003 Error in ResponseHandler {}", this.getClass().getName(), t);
            resumeAsyncError(theAsyncResponse, t);
        }
    }

    public AsyncResponse getAsyncResponse() {
        return theAsyncResponse;
    }

    public <RESPONSE> void respond(RESPONSE data) {
        HttpUtils.resumeAsyncOK(theAsyncResponse, data);
    }

    public abstract void process(T data);
}
