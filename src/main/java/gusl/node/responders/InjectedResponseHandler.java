package gusl.node.responders;

import gusl.node.bootstrap.GUSLServiceLocator;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author dhudson
 * @since 22/09/2022
 */
public abstract class InjectedResponseHandler<T> extends AbstractResponseHandler<T> {

    public InjectedResponseHandler(AsyncResponse asyncResponse) {
        super(asyncResponse);
        GUSLServiceLocator.inject(this);
    }

}
