package gusl.node.responders;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author dhudson
 * @since 22/09/2022
 */
public class SimpleResponseHandler<T> extends AbstractResponseHandler<T> {

    public SimpleResponseHandler(AsyncResponse asyncResponse) {
        super(asyncResponse);
    }

    @Override
    public void process(T data) {
        respond(data);
    }
}
