package gusl.node.responders;

import gusl.node.responders.ResponseBuilders.Builder.FunctionWithExceptions;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author dhudson
 * @since 26/09/2022
 */
public class FunctionalResponseHandler<T> extends AbstractResponseHandler<T> {

    // private Function<T, Object> theFunction;
    FunctionWithExceptions<T, Object> theFunction;

    public FunctionalResponseHandler(AsyncResponse asyncResponse) {
        super((asyncResponse));
    }

    public FunctionalResponseHandler supply(FunctionWithExceptions<T, Object> function) {
        theFunction = function;
        return this;
    }

    @Override
    public void process(T data) {
        try {
            respond(theFunction.apply(data));
        } catch (Throwable exception) {
            handleError(exception);
        }
    }
}
