package gusl.node.responders;

import gusl.core.exceptions.GUSLException;
import gusl.core.reactive.Reactive;

import javax.ws.rs.container.AsyncResponse;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.nonNull;

public class ResponseBuilders {

    public static <T, R> ResponseBuilders.Builder<T, R> respond(final AsyncResponse asyncResponse) {
        return new ResponseBuilders.Builder<T, R>(asyncResponse);
    }

    public static class Builder<T, R> {

        final AsyncResponse asyncResponse;

        CompletableFuture<T> future;

        FunctionWithExceptions<T, R> supplyFunction;

        CompletableFutureWithExceptions<CompletableFuture<T>> futureFunction;

        T value;

        FunctionValueWithExceptions<T> valueFunction;

        Reactive reactive;

        boolean longLived;

        public <T, R> Builder(AsyncResponse asyncResponse) {
            this.asyncResponse = asyncResponse;
        }

        @FunctionalInterface
        public interface CompletableFutureWithExceptions<T> {
            T apply() throws Throwable;
        }

        @FunctionalInterface
        public interface FutureWithExceptions<T> {

            T apply() throws Throwable;
        }

        @FunctionalInterface
        public interface FunctionWithExceptions<T, R> {
            R apply(T t) throws Throwable;
        }

        @FunctionalInterface
        public interface FunctionValueWithExceptions<T> {
            T apply() throws Throwable;
        }

        @FunctionalInterface
        public interface ConsumerWithExceptions<T, E extends GUSLException> {

            T apply(T t) throws E;
        }

        //        public Builder<T, R> givenAFuture(FunctionWithExceptions<CompletableFuture<T>, R> future) {
//            this.future = future;
//            return this;
//        }

        public Builder<T, R> given(Reactive reactive) {
            this.reactive = reactive;
            return this;
        }

        public Builder<T, R> given(T value) {
            this.value = value;
            return this;
        }

        public Builder<T, R> given(FunctionValueWithExceptions<T> valueFunction) {
            this.valueFunction = valueFunction;
            return this;
        }

        public Builder<T, R> given(CompletableFuture<T> future) {
            this.future = future;
            return this;
        }

        public Builder<T, R> given(CompletableFutureWithExceptions<CompletableFuture<T>> futureFunction) {
            this.futureFunction = futureFunction;
            return this;
        }

        public Builder<T, R> givenAFuture(CompletableFutureWithExceptions<CompletableFuture<T>> futureFunction) {
            this.futureFunction = futureFunction;
            return this;
        }

        public Builder<T, R> then(FunctionWithExceptions<T, R> supplyFunction) {
            this.supplyFunction = supplyFunction;
            return this;
        }

        public Builder<T, R> longLived() {
            this.longLived = true;
            return this;
        }

        public void build() {
            FunctionalResponseHandler responder = new FunctionalResponseHandler<T>(asyncResponse);
            responder.supply(supplyFunction);
            try {
                if (nonNull(future)) {
                    if (longLived) {
                        future.whenComplete((value, throwable) -> {
                            Reactive.process(() -> {
                                responder.consume(value, throwable);
                            });
                        });
                    } else {
                        future.whenComplete((value, throwable) -> {
                            responder.consume(value, throwable);
                        });
                    }
                } else if (nonNull(futureFunction)) {
                    futureFunction.apply().whenComplete((value, throwable) -> {
                        responder.consume(value, throwable);
                    });
                } else if (nonNull(valueFunction)) {
                    responder.consume(valueFunction.apply(), null);
                } else if (nonNull(value)) {
                    responder.consume(value, null);
                } else if (nonNull(reactive)) {
                    reactive.onError((t) -> responder.handleError(t));
                    responder.consume(reactive.waitForResult(), null);
                } else {
                    responder.consume(null, null);
                }
            } catch (Throwable e) {
                responder.handleError(e);
            }
        }

    }
}
