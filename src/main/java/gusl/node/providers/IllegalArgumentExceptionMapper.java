package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@CustomLog
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

    @Override
    public Response toResponse(IllegalArgumentException ex) {
        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.ACCESS_DENIED.getError(ex.getMessage()));
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.FORBIDDEN)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
