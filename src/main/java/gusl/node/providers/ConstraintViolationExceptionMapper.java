package gusl.node.providers;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.Path.Node;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.text.MessageFormat;
import java.util.*;

@Provider
@CustomLog
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private static final PropertyNamingStrategies.KebabCaseStrategy theStrategy = new PropertyNamingStrategies.KebabCaseStrategy();

    @Override
    public Response toResponse(ConstraintViolationException exception) {

        Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
        List<ErrorDO> errorDos = new ArrayList<>(violations.size());
        violations.forEach(violation -> {
            String field = null;

            Path path = violation.getPropertyPath();
            Iterator<Node> pathNodes = path.iterator();
            boolean isFirst = true;
            String methodName = "";
            while (pathNodes.hasNext()) {
                field = pathNodes.next().getName();
                if (isFirst) {
                    isFirst = false;
                    methodName = field;
                }
            }
            if (field != null) {
                field = theStrategy.translate(field);
            }

            Class<?> resourceClass = violation.getRootBeanClass();

            // logger.info("violation: {}", violation.toString());
            ErrorDO error = NodeErrors.CONSTRAINT_VIOLATION.getError(
                    new String[]{
                            resourceClass == null ? "" : resourceClass.getCanonicalName(),
                            methodName,
                            field,
                            violation.getMessage()
                    });
            error.setField(field);
            errorDos.add(error);
        });

        ErrorsDTO errorsDto = new ErrorsDTO();
        errorsDto.setErrors(convertToErrors(errorDos));

        logger.error("Constraint violation errors: {}", errorsDto);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(errorsDto)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    protected List<ErrorDTO> convertToErrors(List<ErrorDO> errorDos) {
        List<ErrorDTO> errorDtos = new ArrayList<>(errorDos.size());
        errorDos.forEach(errorDo -> {
            String message = extractMessage(errorDo, Locale.getDefault());
            if (message != null) {
                String fieldName = errorDo.getField();
                if (fieldName != null) {
                    // enforce lower strategy for field names
                    fieldName = theStrategy.translate(fieldName);
                }

                ErrorDTO errorDto = new ErrorDTO();
                errorDto.setField(fieldName);
                errorDto.setMessage(message);
                errorDto.setMessageKey(errorDo.getMessageKey());
                errorDto.setParameters(errorDo.getParameters());
                errorDtos.add(errorDto);
            }
        });

        return errorDtos;
    }

    private String extractMessage(ErrorDO errorDo, Locale locale) {
        String message;
        if (errorDo.getMessageKey() != null) {
            try {
                int length = errorDo.getParameters().length;
                Object[] objParams = new Object[length];
                System.arraycopy(errorDo.getParameters(), 0, objParams, 0, length);

                message = MessageFormat.format(errorDo.getMessage(), objParams);
                // when using message bundles ...
                // message = Messages.getLocalisedMessage(errorDo.getMessageKey(), errorDo.getParameters(), locale);
            } catch (MissingResourceException e) {
//                logger.warn("Messages resource bundle does not contain any message for key \"{}\"."
//                        + " Error message will not be localised.", errorDo.getMessageKey());

                // message = errorDo.getMessageKey();
                message = errorDo.getMessage();
            }
        } else {
            message = errorDo.getMessage();
        }
        return message;
    }
}
