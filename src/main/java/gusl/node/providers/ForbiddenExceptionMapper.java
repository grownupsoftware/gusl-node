package gusl.node.providers;

import java.util.Collections;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;

/**
 *
 * @author grant
 */
@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {

    @Override
    public Response toResponse(ForbiddenException ex) {

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.ACCESS_DENIED.getError());
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.FORBIDDEN)
                .entity(errors)
                .build();
    }
}
