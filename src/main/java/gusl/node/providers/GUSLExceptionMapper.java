package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLException;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@CustomLog
public class GUSLExceptionMapper implements ExceptionMapper<GUSLException> {

    private static final String UNEXPECTED_ERROR_KEY = "resource.error.unexpected";

    @Override
    public Response toResponse(GUSLException exception) {

        logger.error("Error handling request. Error: {}", exception.getMessage());

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.GUSL_SERVER_ERROR.getError(exception.getMessage()));
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errors)
                .build();
    }
}
