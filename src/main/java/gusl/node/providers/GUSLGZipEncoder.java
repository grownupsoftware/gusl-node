package gusl.node.providers;

import org.glassfish.jersey.spi.ContentEncoder;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;

/**
 * We only really want inbound decompression.
 *
 * @author dhudson
 */
@Priority(Priorities.ENTITY_CODER)
public class GUSLGZipEncoder extends ContentEncoder {

    public static final String ENCODING_GZIP = "gzip";

    public GUSLGZipEncoder() {
        super(ENCODING_GZIP, "x-gzip");
    }

    @Override
    public InputStream decode(String contentEncoding, InputStream encodedStream)
            throws IOException {
        return new GZIPInputStream(encodedStream);
    }

    @Override
    public OutputStream encode(String contentEncoding, OutputStream entityStream)
            throws IOException {
        return entityStream;
    }
}
