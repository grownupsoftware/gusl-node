package gusl.node.providers;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.lambda.MutableBoolean;
import lombok.CustomLog;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.nonNull;

@Provider
@CustomLog
public class GUSLErrorExceptionMapper implements ExceptionMapper<GUSLErrorException> {

    private static final PropertyNamingStrategies.KebabCaseStrategy theStrategy = new PropertyNamingStrategies.KebabCaseStrategy();

    @Override
    public Response toResponse(GUSLErrorException ex) {

        if (!ignoreLogging(ex)) {
            if (ex.isSuppressStack()) {
                logger.error("Error exception errors: {}", ex.getErrors());
            } else {
                logger.error("Error exception errors: {}", ex.getErrors(), ex);
            }
        } else {
            logger.debug("Error exception errors: {}", ex.getErrors());
        }

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(convertToErrors(ex))
                .build();
    }

    private boolean ignoreLogging(GUSLErrorException ex) {
        MutableBoolean ignoreLogging = new MutableBoolean(false);
        if (nonNull(ex.getErrors()) && !ex.getErrors().isEmpty()) {
            safeStream(ex.getErrors())
                    .filter(error -> error.isIgnoreLogging())
                    .forEach(error -> ignoreLogging.set(true));
        }

        if (ignoreLogging.get()) {
            return true;
        } else {
            if (nonNull(ex.getErrors()) && !ex.getErrors().isEmpty()) {
                safeStream(ex.getErrors())
                        .filter(error -> "id.not.found".equals(error.getMessageKey()))
                        .forEach(error -> ignoreLogging.set(true));
            }

        }
        return ignoreLogging.get();
    }

    public static ErrorsDTO convertToErrors(GUSLErrorException exception) {
        List<ErrorDO> errorDos = exception.getErrors();
        List<ErrorDTO> errorDtos = new ArrayList<>(errorDos.size());
        errorDos.forEach(errorDo -> {
            String message = extractMessage(errorDo, exception.getLocale());
            if (message != null) {
                String fieldName = errorDo.getField();
                if (fieldName != null) {
                    // enforce lower strategy for field names
                    fieldName = theStrategy.translate(fieldName);
                }

                ErrorDTO errorDto = new ErrorDTO();
                errorDto.setField(fieldName);
                errorDto.setMessage(message);
                errorDto.setMessageKey(errorDo.getMessageKey());
                errorDto.setParameters(errorDo.getParameters());
                errorDtos.add(errorDto);
            }
        });

        ErrorsDTO errors = new ErrorsDTO();
        errors.setErrors(errorDtos);
        return errors;
    }

    /**
     * Get the localised message from the error data object using the provided
     * locale.
     *
     * @param errorDo
     * @param locale
     * @return
     */
    public static String extractMessage(ErrorDO errorDo, Locale locale) {
        String message;
        if (errorDo.getMessageKey() != null) {
            try {
                if (errorDo.getParameters() != null) {
                    int length = errorDo.getParameters().length;
                    Object[] objParams = new Object[length];
                    System.arraycopy(errorDo.getParameters(), 0, objParams, 0, length);

                    message = String.format(errorDo.getMessage(), objParams);
                } else {
                    message = errorDo.getMessage();
                }
                // when using message bundles ...
                // message = Messages.getLocalisedMessage(errorDo.getMessageKey(), errorDo.getParameters(), locale);
            } catch (MissingResourceException e) {
//                logger.warn("Messages resource bundle does not contain any message for key \"{}\"."
//                        + " Error message will not be localised.", errorDo.getMessageKey());

                // message = errorDo.getMessageKey();
                message = errorDo.getMessage();
            }
        } else {
            message = errorDo.getMessage();
        }
        return message;
    }
}
