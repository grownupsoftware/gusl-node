package gusl.node.providers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import javax.ws.rs.core.MediaType;

/**
 * @author dhudson
 * @since 20/09/2021
 */
public class GUSLJacksonJaxbJsonProvider extends JacksonJaxbJsonProvider {

    private final ObjectMapper theObjectMapper;

    public GUSLJacksonJaxbJsonProvider(ObjectMapper mapper) {
        super(mapper, DEFAULT_ANNOTATIONS);
        theObjectMapper = mapper;
    }

    @Override
    protected ObjectMapper _locateMapperViaProvider(Class<?> type, MediaType mediaType) {
        return theObjectMapper;
    }
}
