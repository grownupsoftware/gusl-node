package gusl.node.providers;

import gusl.core.errors.ErrorType;
import gusl.model.exceptions.ValidationException;
import lombok.CustomLog;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@CustomLog
public class ValidationExceptionMapper extends AbstractErrorExceptionMapper<ValidationException> {

    @Override
    public Response toResponse(ValidationException ex) {

        logger.debug("validation exception errors: {}", ex.getErrors(), ex);

        if (!ex.getErrors().isEmpty()) {
            if (ex.getErrorType() != null && ex.getErrorType() == ErrorType.NOT_FOUND) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity(convertToErrors(ex))
                        .build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(convertToErrors(ex))
                .build();
    }
}
