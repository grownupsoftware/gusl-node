package gusl.node.providers;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.jaxrs.cfg.EndpointConfigBase;
import com.fasterxml.jackson.jaxrs.cfg.ObjectWriterInjector;
import com.fasterxml.jackson.jaxrs.cfg.ObjectWriterModifier;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.zip.GZIPOutputStream;

import static gusl.node.transport.HttpUtils.*;

/**
 * Used by the node client to add the X-Body-Type and keep the mappers the same.
 *
 * @author dhudson
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@CustomLog
public class GUSLJacksonProvider extends JacksonJaxbJsonProvider {

    @Override
    public void writeTo(Object value, Class<?> type, Type genericType, Annotation[] annotations,
                        MediaType mediaType,
                        MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
            throws IOException {
        EndpointConfigBase<?> endpoint = _endpointForWriting(value, type, genericType, annotations,
                mediaType, httpHeaders);

        ObjectWriter writer = endpoint.getWriter();

        // Where can we find desired encoding? Within HTTP headers?
        JsonEncoding enc = findEncoding(mediaType, httpHeaders);
        JsonGenerator g = _createGenerator(writer, entityStream, enc);

        // Lets add our header
        if (value != null) {
            httpHeaders.add(BODY_CONTENT_CLASS_HEADER_NAME, value.getClass().getName());
        }

        try {
            // Want indentation?
            if (writer.isEnabled(SerializationFeature.INDENT_OUTPUT)) {
                g.useDefaultPrettyPrinter();
            }
            JavaType rootType = null;

            if ((genericType != null) && (value != null)) {
                // 10-Jan-2011, tatu: as per [JACKSON-456], it's not safe to just force root
                //    type since it prevents polymorphic type serialization. Since we really
                //    just need this for generics, let's only use generic type if it's truly generic.

                if (!(genericType instanceof Class<?>)) { // generic types are other impls of 'java.lang.reflect.Type'
                    // This is still not exactly right; should root type be further
                    // specialized with 'value.getClass()'? Let's see how well this works before
                    // trying to come up with more complete solution.

                    // 18-Mar-2015, tatu: As per [#60], there is now a problem with non-polymorphic lists,
                    //    since forcing of type will then force use of content serializer, which is
                    //    generally not the intent. Fix may require addition of functionality in databind
                    TypeFactory typeFactory = writer.getTypeFactory();
                    JavaType baseType = typeFactory.constructType(genericType);
                    rootType = typeFactory.constructSpecializedType(baseType, type);
                    /* 26-Feb-2011, tatu: To help with [JACKSON-518], we better recognize cases where
                     *    type degenerates back into "Object.class" (as is the case with plain TypeVariable,
                     *    for example), and not use that.
                     */
                    if (rootType.getRawClass() == Object.class) {
                        rootType = null;
                    }
                }
            }

            // Most of the configuration now handled through EndpointConfig, ObjectWriter
            // but we may need to force root type:
            if (rootType != null) {
                writer = writer.forType(rootType);
            }
            value = endpoint.modifyBeforeWrite(value);

            // [Issue#32]: allow modification by filter-injectible thing
            ObjectWriterModifier mod = ObjectWriterInjector.getAndClear();
            if (mod != null) {
                writer = mod.modify(endpoint, httpHeaders, value, writer, g);
            }

            // Get the bytes to be written
            byte[] bytes = writer.writeValueAsBytes(value);

            if (hasNoCompress(annotations) || bytes.length <= COMPRESSION_THRESHOLD) {
                entityStream.write(bytes);
            } else {
                // Lets compress it
                if (bytes.length > COMPRESSION_THRESHOLD) {
                    httpHeaders.add(CONTENT_ENCODING_HEADER_NAME, CONTENT_ENCODING_HEADER_VALUE);
                    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                        try (GZIPOutputStream gos = new GZIPOutputStream(baos)) {
                            gos.write(bytes);
                        }
                        entityStream.write(baos.toByteArray());
                    }
                }
            }
        } finally {
            IOUtils.closeQuietly(g);
        }
    }

    @Override
    protected ObjectMapper _locateMapperViaProvider(Class<?> type, MediaType mediaType) {
        return ObjectMapperFactory.getDefaultObjectMapper();
    }

    private boolean hasNoCompress(Annotation[] annotations) {
        if (annotations == null) {
            return false;
        }

        for (Annotation annotation : annotations) {
            if (annotation.annotationType() == NoCompress.class) {
                return true;
            }
        }

        return false;
    }
}
