package gusl.node.providers;

import javax.ws.rs.core.Response;

/**
 *
 * @author grant
 */
public class StatusCode440 implements Response.StatusType {

    @Override
    public int getStatusCode() {
        return 440;
    }

    @Override
    public Response.Status.Family getFamily() {
        return Response.Status.Family.familyOf(401);
    }

    @Override
    public String getReasonPhrase() {
        return "Login required password failed";
    }

}
