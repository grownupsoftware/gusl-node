package gusl.node.providers;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

/**
 * @author dhudson
 * @since 25/03/2021
 */

@Provider
@CORS
public class CORSProvider implements WriterInterceptor {
    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        addCORSIfRequired(context.getHeaders());
        context.proceed();
    }

    private static void addIfNotExists(MultivaluedMap<String, Object> headers, String key, String value) {
        if (!headers.containsKey(key)) {
            headers.add(key, value);
        }
    }

    public static void addCORSIfRequired(MultivaluedMap<String, Object> headers) {
        addIfNotExists(headers, "Access-Control-Allow-Origin", "*");
        addIfNotExists(headers, "Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        addIfNotExists(headers, "Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Forwarded-For, access_token, X-Access-Token, session-token, user-token,totp-user-token,totp-session-token, msal-token, Media-Type, Orientation");
        addIfNotExists(headers, "Access-Control-Max-Age", "600");
    }
}

