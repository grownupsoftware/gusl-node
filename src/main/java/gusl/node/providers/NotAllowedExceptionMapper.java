package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

/**
 * @author grant
 */
@Provider
@CustomLog
public class NotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {

    @Override
    public Response toResponse(NotAllowedException exception) {
        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.NOT_ALLOWED.getError());
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.NOT_FOUND)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
