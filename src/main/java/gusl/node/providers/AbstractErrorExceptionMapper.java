package gusl.node.providers;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;

import javax.ws.rs.ext.ExceptionMapper;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;

@CustomLog
public abstract class AbstractErrorExceptionMapper<E extends GUSLErrorException> implements ExceptionMapper<E> {

    // private static final PropertyNamingStrategy.KebabCaseStrategy theStrategy = new PropertyNamingStrategy.KebabCaseStrategy();
    private static final PropertyNamingStrategies.KebabCaseStrategy theStrategy = new PropertyNamingStrategies.KebabCaseStrategy();

    /**
     * Extract the errors from the exception and convert to an ErrorsDTO.
     *
     * @param exception
     * @return
     */
    protected ErrorsDTO convertToErrors(E exception) {
        List<ErrorDO> errorDos = exception.getErrors();
        List<ErrorDTO> errorDtos = new ArrayList<>(errorDos.size());
        errorDos.forEach(errorDo -> {
            String message = extractMessage(errorDo, exception.getLocale());
            if (message != null) {
                String fieldName = errorDo.getField();
                if (fieldName != null) {
                    // enforce lower strategy for field names
                    fieldName = theStrategy.translate(fieldName);
                }

                ErrorDTO errorDto = new ErrorDTO();
                errorDto.setField(fieldName);
                errorDto.setMessage(message);
                errorDto.setMessageKey(errorDo.getMessageKey());
                errorDto.setParameters(errorDo.getParameters());
                errorDtos.add(errorDto);
            }
        });

        ErrorsDTO errors = new ErrorsDTO();
        errors.setErrors(errorDtos);
        return errors;
    }

    /**
     * Get the localised message from the error data object using the provided
     * locale.
     *
     * @param errorDo
     * @param locale
     * @return
     */
    private String extractMessage(ErrorDO errorDo, Locale locale) {
        String message = null;
        if (errorDo.getMessageKey() != null) {
            try {
                message = Messages.getLocalisedMessage(errorDo.getMessageKey(), errorDo.getParameters(), locale);
            } catch (MissingResourceException e) {
                logger.debug("Messages resource bundle does not contain any message for key \"{}\"."
                        + " Error message will not be localised.", errorDo.getMessageKey());

                if (errorDo.getParameters() != null) {
                    int length = errorDo.getParameters().length;
                    Object[] objParams = new Object[length];
                    System.arraycopy(errorDo.getParameters(), 0, objParams, 0, length);

                    message = MessageFormat.format(errorDo.getMessage(), objParams);
                } else {
                    message = errorDo.getMessage();
                }
            }
        } else {
            message = errorDo.getMessage();
        }
        return message;
    }
}
