package gusl.node.providers;

import gusl.model.exceptions.IPRestrictedException;
import gusl.node.transport.HttpUtils;
import lombok.CustomLog;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author dhudson
 */
@Provider
@CustomLog
public class IPRestrictedExceptionMapper implements ExceptionMapper<IPRestrictedException> {

    @Override
    public Response toResponse(IPRestrictedException e) {

        logger.warn("Sending Restrict Code {} due to {} Address returns {} country code", HttpUtils.IP_RESTRICTION_STATUS, e.getIp(), e.getCountry());
        return Response.status(HttpUtils.IP_RESTRICTION_STATUS).entity(Entity.json(""))
                .build();

    }

}
