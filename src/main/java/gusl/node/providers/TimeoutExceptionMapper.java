package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;
import java.util.concurrent.TimeoutException;

@Provider
@CustomLog
public class TimeoutExceptionMapper implements ExceptionMapper<TimeoutException> {

    @Override
    public Response toResponse(TimeoutException exception) {

        logger.error("Error - timeout exception", exception);
        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.ERR_TIMEOUT.getError());
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.fromStatusCode(504))
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
