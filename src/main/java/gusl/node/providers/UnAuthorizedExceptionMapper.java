package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.model.exceptions.UnAuthorizedException;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

/**
 * @author grant
 */
@Provider
@CustomLog
public class UnAuthorizedExceptionMapper implements ExceptionMapper<UnAuthorizedException> {


    @Override
    public Response toResponse(UnAuthorizedException exception) {

        ErrorsDTO errors = new ErrorsDTO();

        ErrorDTO errorDto = new ErrorDTO();
        errorDto.setField(exception.getError().getField());
        errorDto.setMessage(exception.getError().getMessage());
        errorDto.setMessageKey(exception.getError().getMessageKey());
        errorDto.setParameters(exception.getError().getParameters());
        errors.setErrors(Collections.singletonList(errorDto));

        // logger.debug("Error: {}", exception.getMessage(),exception);

        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
