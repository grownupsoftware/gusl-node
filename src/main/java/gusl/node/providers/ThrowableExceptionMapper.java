package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLException;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Collections;

/**
 * @author grant
 */
@Provider
@CustomLog
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private UriInfo uriInfo;

    @Override
    public Response toResponse(Throwable ex) {
        String message = ex.getMessage();

        String method = request.getMethod();
        String pathInfo = request.getPathInfo();

//        Class<?> resourceClass = resourceInfo.getResourceClass();
//        Method resourceMethod = resourceInfo.getResourceMethod();
//        URI resourcePath = getResourcePath(resourceClass, resourceMethod);

        logger.error("*** Unexpected Error (Throwable) in resource error: [{}] : Path[{}] : Method [{}] {}", message, pathInfo, method,
                GUSLException.getFullExceptionMessage(ex, true));

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.UNEXPECTED_ERROR.getError(ex.getMessage()));
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errors)
                .build();
    }

    private static URI getResourcePath(Class<?> clazz, Method method) {
        if (clazz == null || !clazz.isAnnotationPresent(Path.class)) {
            return null;
        }

        UriBuilder builder = UriBuilder.fromResource(clazz);
        if (method != null && method.isAnnotationPresent(Path.class)) {
            builder.path(method);
        }

        return builder.build();
    }
}
