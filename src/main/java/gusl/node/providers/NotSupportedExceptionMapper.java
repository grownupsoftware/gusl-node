package gusl.node.providers;

import java.util.Collections;

import javax.ws.rs.NotSupportedException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;

@Provider
public class NotSupportedExceptionMapper implements ExceptionMapper<NotSupportedException> {

    @Override
    public Response toResponse(NotSupportedException exception) {
        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.NOT_SUPPORTED.getError());
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
