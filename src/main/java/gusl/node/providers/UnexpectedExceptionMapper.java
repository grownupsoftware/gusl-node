package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@CustomLog
public class UnexpectedExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {

        logger.error("Throwable Error ", exception);

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.UNEXPECTED_ERROR.getError(exception.getMessage()));
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(errors)
                .build();
    }
}
