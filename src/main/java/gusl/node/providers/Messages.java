package gusl.node.providers;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Messages {

    private static final String MESSAGES_BASE_NAME = "Messages";

    public static String getLocalisedMessage(String messageKey) {
        return getLocalisedMessage(messageKey, null, Locale.getDefault());
    }

    public static String getLocalisedMessage(String messageKey, String[] parameters) {
        return getLocalisedMessage(messageKey, parameters, Locale.getDefault());
    }

    public static String getLocalisedMessage(String messageKey, Locale locale) {
        return getLocalisedMessage(messageKey, null, locale);
    }

    public static String getLocalisedMessage(String messageKey, String[] parameters, Locale locale) {
        String localisedMessage = null;

        ResourceBundle bundle = ResourceBundle.getBundle(MESSAGES_BASE_NAME, locale);
        String parameterisedMessage = bundle.getString(messageKey);
        if (parameters != null) {
            int length = parameters.length;
            Object[] objParams = new Object[length];
            System.arraycopy(parameters, 0, objParams, 0, length);
            localisedMessage = MessageFormat.format(parameterisedMessage, objParams);
        } else {
            localisedMessage = parameterisedMessage;
        }
        return localisedMessage;
    }
}
