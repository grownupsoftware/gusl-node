package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.model.exceptions.AccessDeniedException;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

/**
 * @author grant
 */
@Provider
@CustomLog
public class AccessDeniedExceptionMapper implements ExceptionMapper<AccessDeniedException> {

    @Override
    public Response toResponse(AccessDeniedException exception) {

        ErrorsDTO errors = new ErrorsDTO();

        ErrorDTO errorDto = new ErrorDTO();
        errorDto.setField(exception.getError().getField());
        errorDto.setMessage(exception.getError().getMessage());
        errorDto.setMessageKey(exception.getError().getMessageKey());
        errorDto.setParameters(exception.getError().getParameters());
        errors.setErrors(Collections.singletonList(errorDto));

        logger.error("Error: {}", exception.getMessage());

        return Response.status(Response.Status.FORBIDDEN)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
