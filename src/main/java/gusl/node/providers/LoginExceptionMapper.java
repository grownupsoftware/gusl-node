package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.model.exceptions.LoginExpiredException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

/**
 * @author grant
 */
@Provider
public class LoginExceptionMapper implements ExceptionMapper<LoginExpiredException> {

    @Override
    public Response toResponse(LoginExpiredException exception) {

        ErrorsDTO errors = new ErrorsDTO();

        ErrorDTO errorDto = new ErrorDTO();
        errorDto.setField(exception.getError().getField());
        errorDto.setMessage(exception.getError().getMessage());
        errorDto.setMessageKey(exception.getError().getMessageKey());
        errorDto.setParameters(exception.getError().getParameters());
        errors.setErrors(Collections.singletonList(errorDto));

        // steal Microsoft's IIS 440 Login Time-out
        return Response.status(new StatusCode440())
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
