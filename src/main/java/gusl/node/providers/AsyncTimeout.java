package gusl.node.providers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import gusl.node.async.AsyncTimeoutConstants;

/**
 * Allow for the changing of the default timeout (in seconds) for Async
 * Requests.
 *
 * @author dhudson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface AsyncTimeout {

    int timeout() default AsyncTimeoutConstants.DEFAULT_TIMEOUT;
}
