package gusl.node.providers;

/**
 * @author dhudson
 * @since 25/03/2021
 */

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface CORS {
}
