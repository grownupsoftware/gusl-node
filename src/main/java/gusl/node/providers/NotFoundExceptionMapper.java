package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

/**
 * Catches the NotFoundExceptions and converts them to REST responses.
 *
 * @author grant
 */
@Provider
@CustomLog
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    @Context
    private HttpServletRequest request;

    @Override
    public Response toResponse(NotFoundException exception) {

        logger.warn("Not Found exception {}", request.getPathInfo());

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.RESOURCE_NOT_FOUND.getError(request.getPathInfo()));
        errors.setErrors(Collections.singletonList(error));

        return Response.status(Response.Status.NOT_FOUND)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
