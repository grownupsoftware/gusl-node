package gusl.node.providers;

import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.model.exceptions.TotpNotConfiguredException;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@CustomLog
public class TotpNotConfiguredExceptionMapper implements ExceptionMapper<TotpNotConfiguredException> {

    @Override
    public Response toResponse(TotpNotConfiguredException exception) {

        ErrorsDTO errors = new ErrorsDTO();

        ErrorDTO errorDto = new ErrorDTO();
        errorDto.setField(exception.getError().getField());
        errorDto.setMessage(exception.getError().getMessage());
        errorDto.setMessageKey(exception.getError().getMessageKey());
        errorDto.setParameters(exception.getError().getParameters());
        errors.setErrors(Collections.singletonList(errorDto));

        // logger.debug("Error: {}", exception.getMessage(),exception);

        return Response.status(Response.Status.PAYMENT_REQUIRED) // <-- used http error code 402
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
