package gusl.node.providers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.ws.rs.NameBinding;

/**
 *
 * @author grant
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Compress {
}
