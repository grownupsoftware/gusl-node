package gusl.node.providers;

import com.fasterxml.jackson.databind.JsonMappingException;
import gusl.core.errors.ErrorDTO;
import gusl.core.errors.ErrorsDTO;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Collections;

@Provider
@CustomLog
public class JsonMappingExceptionMapper implements ExceptionMapper<JsonMappingException> {

    @Override
    public Response toResponse(JsonMappingException exception) {

        ErrorsDTO errors = new ErrorsDTO();
        ErrorDTO error = new ErrorDTO(NodeErrors.BAD_JSON_INPUT_ERROR_KEY.getError());
        errors.setErrors(Collections.singletonList(error));

        logger.error("Error", exception);

        return Response.status(Response.Status.BAD_REQUEST)
                .entity(errors)
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
