package gusl.node.providers;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author dhudson
 * @since 12/04/2023
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface NoCompress {
}
