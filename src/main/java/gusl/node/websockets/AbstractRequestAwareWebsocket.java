package gusl.node.websockets;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.HandshakeRequest;

/**
 * AbstractRequestAwareWebsocket.
 *
 * <p>
 * Capture the request and provide util methods</p>
 *
 * @author dhudson
 */
public class AbstractRequestAwareWebsocket extends AbstractWebSocketEndpoint {

    private HandshakeRequest theHandshakeRequest;

    public AbstractRequestAwareWebsocket() {
    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        super.onOpen(session, config);
        theHandshakeRequest = WebSocketConfigurator.getHandshakeRequestFrom(config);
    }

    public HandshakeRequest getRequest() {
        return theHandshakeRequest;
    }

    public Map<String, List<String>> getHeaders() {
        if(theHandshakeRequest== null) {
            return Collections.emptyMap();
        } else {
            return theHandshakeRequest.getHeaders();
        }
    }

    public Map<String, List<String>> getParameterMap() {
        return theHandshakeRequest.getParameterMap();
    }

    public String getQueryString() {
        return theHandshakeRequest.getQueryString();
    }

    /**
     * Return the first header value or null.
     *
     * @param key
     * @return header value or null
     */
    public String getFirstHeaderValue(String key) {
        List<String> headers = getHeaders().get(key);

        if (headers == null || headers.isEmpty()) {
            return null;
        }

        return headers.get(0);
    }
}
