package gusl.node.websockets;

import javax.websocket.EndpointConfig;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

/**
 * This class is used to configure WebSockets.
 *
 * For this to work, the ServerEnpoint annotation needs to be configured with
 * configurator = WebSocketConfigurator.class
 *
 * @author dhudson
 */
public class WebSocketConfigurator extends ServerEndpointConfig.Configurator {

    public static final String WEB_SCOKET_REQUEST = "websocket.request";

    @Override
    public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
        // Add a lot of the lovely stuff from request to the user config so that it can be used later.
        sec.getUserProperties().put(WEB_SCOKET_REQUEST, request);
    }

    /**
     * Return the handshake request from the config, if set.
     *
     * @param config to fetch the handshake request
     * @return the request or null if not found.
     */
    public static HandshakeRequest getHandshakeRequestFrom(EndpointConfig config) {
        return (HandshakeRequest) config.getUserProperties().get(WEB_SCOKET_REQUEST);
    }
}
