package gusl.node.websockets.messages;

/**
 * Message Data Types.
 *
 * @author dhudson
 */
public enum DataType {

    // Outbound
    events,
    markets,
    runners,
    prices,
    positions,
    offers,
    balance,
    debug,
    notifications,
    overrounds,
    volumes,
    // Inbound
    login,
    logout,
    parameters,
    subscriptions
}
