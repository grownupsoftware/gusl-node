package gusl.node.websockets.messages;

/**
 * Base Class for Websocket Messages.
 *
 * @author dhudson
 * @param <T> Data Type Class
 */
public abstract class AbstractWebsocketMessage<T> {

    private T data;

    public AbstractWebsocketMessage() {
    }

    public T getData() {
        return data;
    }

    public void setData(T payload) {
        data = payload;
    }

    /**
     * Return true if the message payloads match.
     *
     * @param message
     * @return true if candidate for conflation
     */
    public abstract boolean matches(AbstractWebsocketMessage<?> message);

    /**
     * Return true if this message is legible for conflation.
     *
     * @return
     */
    public abstract boolean canConflate();

    /**
     * Return the data type for the data
     *
     * @return data type
     */
    public abstract DataType getType();

}
