package gusl.node.websockets;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import io.undertow.UndertowOptions;
import io.undertow.websockets.core.WebSocketCallback;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.jsr.UndertowSession;
import lombok.CustomLog;
import org.xnio.Options;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.List;
import java.util.Map;

/**
 * Simple Base Class for WebSocket Endpoints.
 *
 * <p>
 * Sub classes will need the @ServerEndpoint annotation</p>
 *
 * <p>
 * NB: Don't implement onMessage, use handle message instead
 * </p>
 *
 * @author dhudson
 */
@CustomLog
public abstract class AbstractWebSocketEndpoint extends Endpoint implements WebSocketCallback {

    private static final int READ_TIMEOUT_MS = 5000;
    private static final int WRITE_TIMEOUT_MS = 5000;

    private static final int OUTPUT_BUFFER_SIZE = 10240;

    private static final ThreadLocal<ByteBuffer> PING_BUFFER
            = new ThreadLocal<ByteBuffer>() {
        @Override
        protected ByteBuffer initialValue() {
            ByteBuffer buffer = ByteBuffer.allocate(8);
            return buffer;
        }
    };

    private static final ThreadLocal<ByteBuffer> WRITE_BUFFER = new ThreadLocal<ByteBuffer>() {
        @Override
        protected ByteBuffer initialValue() {
            ByteBuffer buffer = ByteBuffer.allocate(OUTPUT_BUFFER_SIZE);
            return buffer;
        }
    };


    private long theLastCommunique;
    private final ObjectMapper theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();

    // Websocket stuff
    private Session theSession;
    private RemoteEndpoint.Basic theEndPoint;
    private EndpointConfig theEndpointConfig;
    private WebSocketChannel theWebSocketChannel;

    public AbstractWebSocketEndpoint() {
    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {

        theEndpointConfig = config;
        try {
            theWebSocketChannel = ((UndertowSession) session).getWebSocketChannel();

            // Set the write Timeout 10 seconds
            theWebSocketChannel.setOption(Options.WRITE_TIMEOUT, WRITE_TIMEOUT_MS);
            // Set the read Timeout 10 seconds
            theWebSocketChannel.setOption(Options.READ_TIMEOUT, READ_TIMEOUT_MS);
            // Set the idle timeout
            theWebSocketChannel.setOption(UndertowOptions.IDLE_TIMEOUT, 0);

            // Don't timeout, this currently doesn't work, see above
            session.setMaxIdleTimeout(0);
        } catch (Throwable t) {
            logger.warn("Unable to condition web socket", t);
        }

        theSession = session;
        theEndPoint = session.getBasicRemote();
        // Lets add the message handlers
        session.addMessageHandler(new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String text) {
                theLastCommunique = System.currentTimeMillis();
                try {
                    handleMessage(text);
                } catch (Throwable t) {
                    // As this is a user implemented abstract method, catch any exceptions
                    logger.warn("Exception thrown in handleMessage", t);
                }
            }

        });

        session.addMessageHandler(new MessageHandler.Whole<PongMessage>() {
            @Override
            public void onMessage(PongMessage t) {
                theLastCommunique = System.currentTimeMillis();
                handlePong(t);
            }
        });
    }

    @Override
    public void onClose(Session session, CloseReason reason) {
        logger.debug("Web Socket on Close reason [{}], client [{}]", getClientId(), reason);
        close();
    }

    public void close(CloseReason reason) {
        logger.debug("Web Socket as asked to Close reason [{}], client [{}]", getClientId(), reason);
        close();
    }

    protected void handleMessage(String message) {
        // Sub class impl

    }

    protected void handlePong(PongMessage message) {
        // Sub class impl
    }

    @Override
    public void onError(Session session, Throwable t) {
        onClose(session, new CloseReason(CloseReason.CloseCodes.UNEXPECTED_CONDITION, "Unable to write"));
    }

    public void close() {
        IOUtils.closeQuietly(theSession);
        theSession = null;
        theEndPoint = null;
    }

    public URI getRequestURI() {
        return theSession.getRequestURI();
    }

    public Map<String, List<String>> getRequestParameterMap() {
        return theSession.getRequestParameterMap();
    }

    public Map<String, String> getPathParameters() {
        return theSession.getPathParameters();
    }

    public EndpointConfig getEndpointConfig() {
        return theEndpointConfig;
    }

    public Session getSession() {
        return theSession;
    }

    public String getClientId() {
        if (theSession == null) {
            return null;
        }

        return theSession.getId();
    }

    // Allow the impl to batch the messages
    public void setBatchingMode() {
        try {
            theEndPoint.setBatchingAllowed(true);
        } catch (IOException ex) {
            logger.warn("Unable to switch on batching mode [{}]", getClientId(), ex);
        }
    }

    public void flushBatch() {
        try {
            theEndPoint.flushBatch();
        } catch (IOException ignore) {
        }
    }

    // This is all async now
    public void writeJSON(Object json) {
        if (json != null) {
            try {
                writeString(theObjectMapper.writeValueAsString(json));
            } catch (IOException ex) {
                logger.error("Failed to convert object to JSON {} [{}]", getClientId(), ex.getMessage());
            }
        }
    }

    public int writeString(String message) {
        if (isOpen()) {
            if (theEndPoint != null) {
                synchronized (this) {
                    //theEndPoint.sendText(message);
                    WebSockets.sendText(message, theWebSocketChannel, this, READ_TIMEOUT_MS);
                    theLastCommunique = System.currentTimeMillis();
                    return message.length();
                }
            }
        }
        return 0;
    }

    public void writeBytes(byte[] bytes) {
        if (isOpen()) {
            if (theEndPoint != null) {
                synchronized (this) {
                    try {
                        if (bytes.length >= OUTPUT_BUFFER_SIZE) {
                            throw new IOException("Message too large " + bytes.length);
                        }
                        ByteBuffer buffer = WRITE_BUFFER.get();
                        buffer.rewind();
                        buffer.put(bytes);
                        buffer.flip();
                        theEndPoint.sendBinary(buffer);
                        theLastCommunique = System.currentTimeMillis();
                    } catch (IOException | IllegalArgumentException ex) {
                        logger.warn("Error sending binary message [{}]", getClientId());
                        // Call on Error and let the impl deal with it
                        onError(theSession, ex);
                    }
                }
            }
        }
    }

    public void sendPingMessage() {
        if (isOpen()) {
            if (theEndPoint != null) {
                // This oddly, isn't async, so we can use thread local buffers
                ByteBuffer buffer = PING_BUFFER.get();
                buffer.rewind();
                buffer.putLong(System.currentTimeMillis());
                buffer.flip();

                try {
                    theEndPoint.sendPing(buffer);
                } catch (IOException | IllegalArgumentException ex) {
                    logger.warn("Error sending pong message [{}", getClientId());
                    // Call on Error and let the impl deal with it
                    onError(theSession, ex);
                }
            }
        }
    }

    /**
     * Return the timestamp of the last time a message was received or sent.
     *
     * @return timestamp
     */
    public long getLastCommunique() {
        return theLastCommunique;
    }

    // this is the write callback
    @Override
    public void complete(WebSocketChannel channel, Object context) {

    }

    @Override
    public void onError(WebSocketChannel channel, Object context, Throwable throwable) {
        if (getClientId() != null) {
            if (!(throwable instanceof ClosedChannelException)) {
                logger.warn("Error sending string message [{}]", getClientId());
            }
        }
        onError(theSession, throwable);
    }

    private boolean isOpen() {
        if (theSession == null) {
            return false;
        }

        return theSession.isOpen();
    }
}
