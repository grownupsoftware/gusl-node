package gusl.node.controller;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Basic Controller class to handle listeners.
 *
 * @author dhudson
 * @param <E> listener interface / type
 */
public abstract class AbstractController<E> {

    private final List<E> theListeners;

    public AbstractController() {
        theListeners = new CopyOnWriteArrayList<>();
    }

    public List<E> getListeners() {
        return theListeners;
    }

    public void addListener(E listener) {
        if (listener == null) {
            throw new RuntimeException("Listener can't be null");
        } else {
            theListeners.add(listener);
        }
    }

    public void removeListener(E listener) {
        theListeners.remove(listener);
    }

    /**
     * Return the number of listeners
     *
     * @return number of listeners
     */
    public int getListenerCount() {
        return theListeners.size();
    }

    public void removeAllListeners() {
        theListeners.clear();
    }
}
