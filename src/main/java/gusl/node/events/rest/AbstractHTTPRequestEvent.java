/* Copyright lottomart */
package gusl.node.events.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.UriInfo;
import gusl.core.annotations.DocField;

/**
 *
 * @author grant
 */
public class AbstractHTTPRequestEvent {

    @DocField(description = "URI Information")
    private final UriInfo uriInfo;

    @DocField(description = "The HTTP Servlet Request")
    private final HttpServletRequest servletRequest;

    @DocField(description = "The Async Response Handler")
    private final AsyncResponse asyncResponse;

    public AbstractHTTPRequestEvent(UriInfo uriInfo, HttpServletRequest servletRequest, AsyncResponse asyncResponse) {
        this.uriInfo = uriInfo;
        this.servletRequest = servletRequest;
        this.asyncResponse = asyncResponse;
    }

}
