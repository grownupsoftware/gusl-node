/* Copyright lottomart */
package gusl.node.events.rest;

import com.codahale.metrics.MetricRegistry;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;
import gusl.node.errors.NodeErrors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static gusl.node.errors.NodeErrors.ERR_REST_RESPONSE_TIMEOUT;

/**
 * @param <DO>  The Response DO Object
 * @param <DTO> The Response DTO class
 * @author grant
 */
@DocClass(description = "Completes the async REST call")
public class RestResponseEvent<DO, DTO> {

    public final static GUSLLogger logger = GUSLLogManager.getLogger(RestResponseEvent.class);

    private static final int DEFAULT_TIMEOUT_SECONDS = 10;

    @DocField(description = "The future that this response is based on")
    private final Future<DO> theFuture;

    @DocField(description = "URI Information")
    protected final Latency.Context theLatencyContext;

    @DocField(description = "URI Information")
    private final UriInfo theUriInfo;

    @DocField(description = "The HTTP Servlet Request")
    private final HttpServletRequest theServletRequest;

    @DocField(description = "The Async Response Handler")
    private final AsyncResponse theAsyncResponse;

    @DocField(description = "The DTO class")
    private Class<DTO> theDtoClass;

    public RestResponseEvent(UriInfo uriInfo, HttpServletRequest servletRequest, AsyncResponse asyncResponse, Future<DO> future, Class<DTO> dtoClass) {
        this(uriInfo, servletRequest, asyncResponse, future, dtoClass, DEFAULT_TIMEOUT_SECONDS);
    }

    public RestResponseEvent(UriInfo uriInfo, HttpServletRequest servletRequest, AsyncResponse asyncResponse, Future<DO> future, Class<DTO> dtoClass, int timoutInSeconds) {
        this.theUriInfo = uriInfo;
        this.theServletRequest = servletRequest;
        this.theAsyncResponse = asyncResponse;
        this.theFuture = future;
        this.theDtoClass = dtoClass;

        // create metric
        theLatencyContext = MetricsFactory.getLatency(MetricRegistry.name("rest-api", String.format("%s /%s", servletRequest == null ? "null" : servletRequest.getMethod(), uriInfo == null ? "null" : uriInfo.getPath()))).time();

        logger.info("Received: {} {} Params: {} Path: {}", servletRequest == null ? "null" : servletRequest.getMethod(), uriInfo == null ? "null" : uriInfo.getPath(), uriInfo == null ? "null" : uriInfo.getQueryParameters(), uriInfo == null ? "null" : uriInfo.getPathParameters());

        // set async timeout
        asyncResponse.setTimeout(timoutInSeconds, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler((AsyncResponse response) -> {
            logger.warn("Request timeout [{}]: {} {} Params: {} Path: {}", timoutInSeconds, (servletRequest == null ? "null" : servletRequest.getMethod()), (uriInfo == null ? "null" : uriInfo.getPath()), (uriInfo == null ? "null" : uriInfo.getQueryParameters()), (uriInfo == null ? "null" : uriInfo.getPathParameters()));
            response.resume(new GUSLErrorException(ERR_REST_RESPONSE_TIMEOUT.getError()));
        });

    }

    public Class<DTO> getResponseClass() {
        return this.theDtoClass;
    }

    public HttpMethod getMethod() {
        return HttpMethod.valueOf(theServletRequest.getMethod().toUpperCase());
    }

    public DO getResponseObject() {
        try {
            return theFuture.get();
        } catch (InterruptedException | ExecutionException ex) {
            logger.error("Error get response from future", ex);
            resumeException(new GUSLErrorException(NodeErrors.AUTH_REST_FUTURE_ERROR.getError(ex.getMessage())));
            return null;
        }
    }

    public void resume(Object response) {
        if (response == null) {
            return; // already handled
        }
        try {
            switch (getMethod()) {
                case GET:
                case POST:
                case PUT:
                    resumeResponse(response);
                    break;
                case DELETE:
                    theAsyncResponse.resume(Response.noContent().build());
                    break;

            }
        } finally {
            long duration = theLatencyContext.stop();
            theLatencyContext.close();
            if (theUriInfo != null) {
                logger.info("Completed: {} {} Params: {} Path: {} in: {} ms.", theServletRequest.getMethod(), theUriInfo.getPath(), theUriInfo.getQueryParameters(), theUriInfo.getPathParameters(), duration / 1e6);
            }
        }
    }

    public void resumeResponse(Object response) {
        if (!theAsyncResponse.isDone()) {
            if (response != null) {
                theAsyncResponse.resume(response);
            } else {
                resumeException(new GUSLErrorException(NodeErrors.REST_RESUME_WITH_EXCEPTION.getError("Response was null")));
            }
        } else {
            logger.warn("Connection has gone when replying with: {}", response);
        }
    }

    public void resumeException(GUSLErrorException errorException) {
        logger.error("REST resource error", errorException);
        if (!theAsyncResponse.isDone()) {
            if (errorException.getCause() != null) {
                theAsyncResponse.resume(errorException.getCause());
            } else {
                theAsyncResponse.resume(errorException);
            }
        }

    }

    public void resumeException(Throwable t) {
        logger.error("Throwable Error ", t);
        resumeException(new GUSLErrorException(NodeErrors.REST_RESUME_WITH_EXCEPTION.getError(t.getMessage())));
    }

}
