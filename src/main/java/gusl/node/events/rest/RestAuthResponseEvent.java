/* Copyright lottomart */
package gusl.node.events.rest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.node.errors.NodeErrors;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.exceptions.GUSLErrorException;
import static gusl.node.errors.NodeErrors.ERR_REST_AUTH_TIMEOUT;

/**
 *
 * @author grant
 * @param <DO> The Response DO Object
 * @param <DTO> The Response DTO class
 */
@DocClass(description = "Completes the async REST call")
public class RestAuthResponseEvent<DO, DTO> {

    public final static GUSLLogger logger = GUSLLogManager.getLogger(RestAuthResponseEvent.class);

    private static final int DEFAULT_TIMEOUT_SECONDS = 10;

    @DocField(description = "The future that this response is based on")
    private final Future<DO> theFuture;

    @DocField(description = "The Async Response Handler")
    private final AsyncResponse theAsyncResponse;

    @DocField(description = "The DTO class")
    private Class<DTO> theDtoClass;

    public RestAuthResponseEvent(AsyncResponse asyncResponse, Future<DO> future, Class<DTO> dtoClass) {
        this(asyncResponse, future, dtoClass, DEFAULT_TIMEOUT_SECONDS);
    }

    public RestAuthResponseEvent(AsyncResponse asyncResponse, Future<DO> future, Class<DTO> dtoClass, int timoutInSeconds) {
        this.theAsyncResponse = asyncResponse;
        this.theFuture = future;
        this.theDtoClass = dtoClass;

        // set async timeout
        asyncResponse.setTimeout(timoutInSeconds, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler((AsyncResponse response) -> {
            response.resume(new GUSLErrorException(ERR_REST_AUTH_TIMEOUT.getError()));
        });

    }

    public Class<DTO> getResponseClass() {
        return this.theDtoClass;
    }

    public DO getResponseObject() {
        try {
            return theFuture.get();
        } catch (InterruptedException | ExecutionException ex) {
            logger.error("Error get response from future", ex);
            resumeException(new GUSLErrorException(NodeErrors.AUTH_REST_FUTURE_ERROR.getError(ex.getMessage())));
            return null;
        }
    }

    public void resume(Object response) {

        if (!theAsyncResponse.isDone()) {
            if (response != null) {
                theAsyncResponse.resume(response);
            } else {
                theAsyncResponse.resume(Response.noContent().build());
            }
        } else {
            logger.warn("Connection has gone when replying with: {}", response);
        }
    }

    public void resumeException(GUSLErrorException errorException) {
        logger.error("REST resource error", errorException);
        if (!theAsyncResponse.isDone()) {
            if (errorException.getCause() != null) {
                theAsyncResponse.resume(errorException.getCause());
            } else {
                theAsyncResponse.resume(errorException);
            }
        }

    }

    public void resumeException(Throwable t) {
        logger.error("Throwable Error ", t);
        resumeException(new GUSLErrorException(NodeErrors.REST_RESUME_WITH_EXCEPTION.getError(t.getMessage())));
    }

}
