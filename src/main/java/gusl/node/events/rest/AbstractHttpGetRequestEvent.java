/* Copyright lottomart */
package gusl.node.events.rest;

import com.codahale.metrics.MetricRegistry;
import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.UriInfo;


/**
 * @author grant
 */
public abstract class AbstractHttpGetRequestEvent extends AbstractHTTPRequestEvent {

    protected final Latency latency;

    public AbstractHttpGetRequestEvent(UriInfo uriInfo, HttpServletRequest servletRequest, AsyncResponse asyncResponse) {
        super(uriInfo, servletRequest, asyncResponse);

        latency = MetricsFactory.getLatency(MetricRegistry.name(this.getClass(), String.format("GET /%s", uriInfo.getAbsolutePath().toASCIIString())));
    }

}
