package gusl.node.events;

import gusl.core.tostring.ToString;

/**
 *
 * @author dhudson
 */
public class QuiesceRequestEvent {

    private boolean quiesceRequested;

    public QuiesceRequestEvent() {
    }

    public boolean isQuiesceRequested() {
        return quiesceRequested;
    }

    public void setQuiesceRequested(boolean quiesceRequested) {
        this.quiesceRequested = quiesceRequested;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
