package gusl.node.events;

import java.util.Date;

/**
 *
 * @author dhudson
 */
public class SystemReadyEvent {

    private Date timestamp;

    public SystemReadyEvent() {

    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}
