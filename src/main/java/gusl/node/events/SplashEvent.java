package gusl.node.events;

/**
 *
 * @author dhudson
 */
public class SplashEvent {

    private long timestamp;

    public SplashEvent() {
        timestamp = System.currentTimeMillis();
    }

    public long getTimestamp() {
        return timestamp;
    }

}
