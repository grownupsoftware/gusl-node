package gusl.node.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.core.reactive.Reactive;
import gusl.model.nodeconfig.NodeType;
import gusl.router.client.RouterHttpClient;
import gusl.router.model.HttpNodeRequest;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

import static gusl.node.transport.HttpUtils.waitForFuture;

/**
 * @author dhudson
 */
public abstract class AbstractNodeClient {

    @Inject
    private RouterHttpClient theRouterClient;

    private final NodeType theNodeType;

    public AbstractNodeClient(NodeType nodeType) {
        theNodeType = nodeType;
    }

    public <RESPONSE> CompletableFuture<RESPONSE> sendRequest(HttpNodeRequest request) {
        return theRouterClient.routeRequest(request);
    }

    public <RESPONSE> HttpNodeRequest<RESPONSE> createHttpNodeRequest(String path, Object request, Class<RESPONSE> response) {
        return createHttpNodeRequest(path, request, response, null);
    }

    public <RESPONSE> HttpNodeRequest<RESPONSE> createHttpNodeRequest(String path, Object request, Class<RESPONSE> response, Class<?> genericHint) {
        HttpNodeRequest<RESPONSE> nodeRequest = theRouterClient.createHttpNodeRequest(path, request, response);
        nodeRequest.setNodeType(theNodeType);
        nodeRequest.setGenericHint(genericHint);
        return nodeRequest;
    }

    public <RESPONSE> RESPONSE waitForRequest(HttpNodeRequest<RESPONSE> request) throws GUSLErrorException {
        return waitForFuture(sendRequest(request));
    }

    public <RESPONSE> Reactive getAsReactive(CompletableFuture<RESPONSE> future) {
        return Reactive.from(future);
    }
}
