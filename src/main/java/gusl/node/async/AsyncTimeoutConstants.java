package gusl.node.async;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * @author dhudson
 */
public interface AsyncTimeoutConstants {

    int MINUTES_10 = 600;
    int MINUTES_20 = 1200;
    int MINUTES_30 = 1800;
    int DEFAULT_TIMEOUT = 120;
    TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;
    Duration DEFAULT_DURATION = Duration.ofMillis(DEFAULT_TIME_UNIT.toMillis(DEFAULT_TIMEOUT));
}
