package gusl.node.async;

import gusl.core.reactive.Reactive;
import gusl.core.reactive.ReactiveResult;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author dhudson
 * @since 25/03/2021
 */
public abstract class AsyncReactiveResponder extends BaseAsyncResponder {

    public AsyncReactiveResponder(AsyncResponse response) {
        super(response);
    }

    public void wrap(Reactive reactive) {
        reactive.onError(throwable -> {
            respond(throwable);
        }).onSuccess(result -> {
            handle(result);
        });
    }

    public abstract void handle(ReactiveResult result);

}
