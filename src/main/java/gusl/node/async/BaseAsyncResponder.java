package gusl.node.async;

import gusl.node.transport.HttpUtils;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author dhudson
 * @since 25/03/2021
 */
public abstract class BaseAsyncResponder {

    private final AsyncResponse asyncResponse;

    public BaseAsyncResponder(AsyncResponse response) {
        asyncResponse = response;
    }

    public void respond(Throwable throwable) {
        HttpUtils.resumeAsyncError(asyncResponse, throwable);
    }

    public <T> void respond(T response) {
        HttpUtils.resumeAsyncOK(asyncResponse, response);
    }

    public AsyncResponse getAsyncResponse() {
        return asyncResponse;
    }
}
