package gusl.node.async;

import javax.ws.rs.container.AsyncResponse;
import java.util.function.BiConsumer;

/**
 * @author dhudson
 * @since 25/03/2021
 */
public abstract class AsyncResponder<RESPONSE> extends BaseAsyncResponder implements BiConsumer<RESPONSE, Throwable> {

    public AsyncResponder(AsyncResponse response) {
        super(response);
    }

    @Override
    public void accept(RESPONSE response, Throwable throwable) {
        if (throwable != null) {
            respond(throwable);
        } else {
            handleResponse(response);
        }
    }

    public abstract void handleResponse(RESPONSE response);
}
