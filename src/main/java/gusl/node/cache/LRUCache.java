package gusl.node.cache;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Simple LRU Cache, just to stop stuff getting too large.
 *
 * @author dhudson
 * @param <K> key
 * @param <V> value
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = -5582439589766100759L;
    private final ReadWriteLock theReadWriteLock;
    private final int theLimit;

    public LRUCache(int limit) {
        super(16, 0.75f, true);
        theLimit = limit;
        // Make it thread fair
        theReadWriteLock = new ReentrantReadWriteLock(true);
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return super.size() > theLimit;
    }

    @Override
    public V put(K key, V value) {
        theReadWriteLock.writeLock().lock();
        try {
            return super.put(key, value);
        } finally {
            theReadWriteLock.writeLock().unlock();
        }
    }

    @Override
    public V get(Object key) {
        theReadWriteLock.writeLock().lock();
        try {
            return super.get(key);
        } finally {
            theReadWriteLock.writeLock().unlock();
        }
    }

    @Override
    public V remove(Object key) {
        theReadWriteLock.writeLock().lock();
        try {
            return super.remove(key);
        } finally {
            theReadWriteLock.writeLock().unlock();
        }
    }

    @Override
    public int size() {
        theReadWriteLock.readLock().lock();
        try {
            return super.size();
        } finally {
            theReadWriteLock.readLock().unlock();
        }
    }

    @Override
    public String toString() {
        theReadWriteLock.readLock().lock();
        try {
            return super.toString();
        } finally {
            theReadWriteLock.readLock().unlock();
        }
    }

}
