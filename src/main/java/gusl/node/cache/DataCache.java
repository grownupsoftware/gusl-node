package gusl.node.cache;

/**
 * Eddie Data Cache, with Key of long.
 *
 * @author dhudson
 * @param <V> value
 */
public class DataCache<V> extends LRUCache<Long, V> {

    private static final long serialVersionUID = -5162771950853773200L;

    public DataCache(int limit) {
        super(limit);
    }

    public V get(Long id) {
        return super.get(id);
    }
}
