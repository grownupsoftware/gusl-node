package gusl.node.cache;

/**
 * Interface for expired cached element
 * @author gbw
 * @param <K>
 * @param <T>
 */
public interface CacheExpiredListener<K, T> {

    public void cacheExpired(K key, T value);

}
