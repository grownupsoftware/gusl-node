package gusl.node.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import static com.google.common.cache.RemovalCause.EXPIRED;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import gusl.node.controller.AbstractController;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

/**
 * Cache implementation allowing for the ability to listen to cache evictions
 *
 * @author gbw
 * @param <K> Key
 * @param <V> Value
 */
public class LmCache<K, V> extends AbstractController<CacheExpiredListener<K, V>> implements RemovalListener<K, V> {

    private static final GUSLLogger logger = GUSLLogManager.getLogger(LmCache.class);

    private final Cache<K, V> theCache;
    private final String theCacheName;

    private static final Map<String, ScheduledFuture<?>> theFutureMap = new ConcurrentHashMap<>();
    private final int theCleanUpTime;
    private final TimeUnit theCleanUpTimeUnit;

    public LmCache(String cacheName, int ttlTime, TimeUnit ttlTimeUnit, int cleanUpTime, TimeUnit cleanUpTimeUnit) {
        theCache = CacheBuilder.newBuilder()
                .expireAfterAccess(ttlTime, ttlTimeUnit)
                .removalListener(this).build();
        theCacheName = cacheName;
        theCleanUpTime = cleanUpTime;
        theCleanUpTimeUnit = cleanUpTimeUnit;
        scheduleCleanUp();
    }

    public LmCache(String cacheName, int ttlTime, TimeUnit ttlTimeUnit) {
        this(cacheName, ttlTime, ttlTimeUnit, 0, TimeUnit.DAYS);
    }

    public LmCache(String cacheName, int ttlTime, TimeUnit ttlTimeUnit, int cleanUpTime, TimeUnit cleanUpTimeUnit, CacheExpiredListener<K, V> listener) {
        this(cacheName, ttlTime, ttlTimeUnit, cleanUpTime, cleanUpTimeUnit);
        addListener(listener);
    }

    private void scheduleCleanUp() {
        if (theCleanUpTime != 0) {
            // only add scheduled clean up if there is a 'time'
            ScheduledFuture<?> shedulesFuture = BackgroundThreadPoolExecutor.scheduleAtFixedRate(() -> {
                logger.debug("Clear cache [{}] - start", theCacheName);
                theCache.cleanUp();
                logger.debug("Clear cache [{}] - end", theCacheName);
            }, 0, theCleanUpTime, theCleanUpTimeUnit);

            theFutureMap.put(theCacheName, shedulesFuture);
        }

    }

    /**
     * Cancels the clean up task
     */
    public void close() {
        ScheduledFuture<?> future = theFutureMap.get(theCacheName);
        if (future != null) {
            if (!future.isDone()) {
                future.cancel(true);
            }
        }

        theFutureMap.remove(theCacheName);
    }

    /**
     * Shuts down ALL clean up futures
     */
    public static void shutdown() {
        theFutureMap.entrySet().stream().forEach(entry -> {
            logger.info("Shutting down MbCache for: {}", entry.getKey());
            ScheduledFuture<?> future = entry.getValue();
            if (future != null) {
                if (!future.isDone()) {
                    future.cancel(true);
                }
            }
        });
    }

    public void put(K key, V value) {
        theCache.put(key, value);
    }

    public List<V> getList() {
        return new ArrayList<>(theCache.asMap().values());
    }

    public V getIfPresent(K key) {
        return theCache.getIfPresent(key);
    }

    public void invalidate(K key) {
        theCache.invalidate(key);
    }

    public void invalidateAll() {
        theCache.invalidateAll();
    }

    public boolean isEmpty() {
        return theCache.size() == 0;
    }

    public long size() {
        return theCache.size();
    }

    @Override
    public void onRemoval(RemovalNotification<K, V> notification) {
        logger.debug("Removing from cache. Value: {}", notification.getValue());
        if (notification.getCause() == EXPIRED) {
            getListeners().stream().forEach((listener) -> {
                logger.debug("Removing from cache. Informing: {}", listener.getClass().getCanonicalName());
                listener.cacheExpired(notification.getKey(), notification.getValue());
            });
        }
    }
}
