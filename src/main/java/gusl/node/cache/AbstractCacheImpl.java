/* Copyright lottomart */
package gusl.node.cache;

import javax.inject.Inject;
import gusl.core.json.JsonUtils;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.node.converter.ModelConverter;

/**
 *
 * @author grant
 */
public class AbstractCacheImpl {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    @Inject
    protected ModelConverter theModelConverter;

    // if anything in cache changes we update this
    private long theLastUpdateTime;

    protected <IN, OUT> OUT convert(IN response, Class<OUT> responseClass) {
        return theModelConverter.convert(response, responseClass);
    }

    protected void setLastUpdateTime() {
        theLastUpdateTime = System.currentTimeMillis();
    }

    protected <T> void prettyPrint(T value) {
        logger.info("{}", JsonUtils.prettyPrint(value));
    }

    public long getLastUpdateTime() {
        return theLastUpdateTime;
    }
}
