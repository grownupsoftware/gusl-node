package gusl.node.threads;

import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.properties.GUSLConfigurable;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Thread Deadlock Detector.
 * <p>
 * Handy little utility which will log if it finds thread deadlocks.
 *
 * @author dhudson
 */
@Service
@CustomLog
public class ThreadDeadlockDetector implements Runnable, GUSLConfigurable {

    private final ThreadMXBean theMXBean = ManagementFactory.getThreadMXBean();

    private final ScheduledExecutorService theScheduler;

    public ThreadDeadlockDetector() {
        theScheduler = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void run() {
        long ids[] = theMXBean.findMonitorDeadlockedThreads();

        if (ids != null) {
            ThreadInfo threadInfos[] = theMXBean.getThreadInfo(ids);

            for (ThreadInfo threadInfo : threadInfos) {
                final StringBuilder stackTrace = new StringBuilder();
                for (StackTraceElement element : threadInfo.getStackTrace()) {
                    stackTrace.append("\t at ")
                            .append(element.toString())
                            .append(String.format("%n"));
                }
                logger.info("Deadlocked thread {}:{} Locked by {}:{}-{}\n{}", threadInfo.getThreadId(),
                        threadInfo.getThreadName(), threadInfo.getLockName(), threadInfo.getLockOwnerId(),
                        threadInfo.getLockOwnerName(), stackTrace);
            }

        }
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theScheduler.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
    }

}
