package gusl.node.properties;

import gusl.core.exceptions.GUSLException;
import gusl.model.systemconfig.SystemConfigDO;

import java.util.Map;

public interface GUSLSystemConfigurable {

    void configure(Map<String, SystemConfigDO> config) throws GUSLException;
}
