/* Copyright lottomart */
package gusl.node.properties;

import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;

/**
 * Called as part of the Application Life Cycle.
 *
 * @author grant
 */
public interface GUSLConfigurable {

    /**
     * Applications will need to cast this to the Application Config.
     *
     * @param config
     * @throws GUSLException
     */
    public void configure(NodeConfig config) throws GUSLException;
}
