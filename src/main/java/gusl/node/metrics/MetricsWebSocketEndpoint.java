package gusl.node.metrics;

import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.websockets.AbstractWebSocketEndpoint;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * Annotated Web Socket Endpoint for metrics.
 *
 * @author dhudson
 */
@ServerEndpoint(value = "/wsmetrics")
public class MetricsWebSocketEndpoint extends AbstractWebSocketEndpoint {

    private final MetricClientController theMetricClientController;

    public MetricsWebSocketEndpoint() {
        theMetricClientController = GUSLServiceLocator.getService(MetricClientController.class);
    }

    @Override
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        super.onOpen(session, config);
        theMetricClientController.addClient(this);
    }

    // called when the connection closed
    @Override
    @OnClose
    public void onClose(Session session, CloseReason reason) {
        super.onClose(session, reason);
        theMetricClientController.removeClient(this);
    }

    @Override
    @OnError
    public void onError(Session session, Throwable t) {
        super.onError(session, t);
        theMetricClientController.removeClient(this);
    }

    // called when a message received from the browser
    // sends message to browser
    public void send(String message) {
        writeString(message);
    }

}
