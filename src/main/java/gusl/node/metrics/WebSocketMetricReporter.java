package gusl.node.metrics;

import com.codahale.metrics.MetricFilter;
import gusl.core.metrics.GUSLMetricRegistry;
import gusl.core.metrics.GUSLMetrics;
import gusl.core.metrics.GUSLScheduledReporter;

import java.util.concurrent.TimeUnit;

/**
 * Sends all metrics to a web socket
 *
 * @author Grant
 */
public class WebSocketMetricReporter extends GUSLScheduledReporter {

    private MetricClientController theMetricClientController;

    public void setMetricClientController(MetricClientController metricClientController) {
        theMetricClientController = metricClientController;
    }

    public static Builder forRegistry(GUSLMetricRegistry registry) {
        return new Builder(registry);
    }

    public static class Builder {

        private final GUSLMetricRegistry registry;
        private TimeUnit rateUnit;
        private TimeUnit durationUnit;
        private MetricFilter filter;

        private Builder(GUSLMetricRegistry registry) {
            this.registry = registry;
            this.rateUnit = TimeUnit.SECONDS;
            this.durationUnit = TimeUnit.MILLISECONDS;
            this.filter = MetricFilter.ALL;
        }

        /**
         * Convert rates to the given time unit.
         *
         * @param rateUnit a unit of time
         * @return {@code this}
         */
        public Builder convertRatesTo(TimeUnit rateUnit) {
            this.rateUnit = rateUnit;
            return this;
        }

        /**
         * Convert durations to the given time unit.
         *
         * @param durationUnit a unit of time
         * @return {@code this}
         */
        public Builder convertDurationsTo(TimeUnit durationUnit) {
            this.durationUnit = durationUnit;
            return this;
        }

        /**
         * Only report metrics which match the given filter.
         *
         * @param filter a {@link MetricFilter}
         * @return {@code this}
         */
        public Builder filter(MetricFilter filter) {
            this.filter = filter;
            return this;
        }

        public WebSocketMetricReporter build() {
            return new WebSocketMetricReporter(
                    registry,
                    rateUnit,
                    durationUnit,
                    filter);
        }
    }

    protected WebSocketMetricReporter(GUSLMetricRegistry registry, String name, MetricFilter filter, TimeUnit rateUnit,
                                      TimeUnit durationUnit) {
        super(registry, name, filter, rateUnit, durationUnit);
    }

    private WebSocketMetricReporter(GUSLMetricRegistry registry, TimeUnit rateUnit, TimeUnit durationUnit, MetricFilter filter) {
        super(registry, "websocket-reporter", filter, rateUnit, durationUnit);
    }

    @Override
    public void report(GUSLMetrics guslMetrics) {
        theMetricClientController.report(guslMetrics);
    }
}
