package gusl.node.metrics;

import gusl.core.metrics.GUSLMetricRegistry;
import gusl.core.metrics.MetricsFactory;

/**
 * @author grant
 */
public class MetricsFilterContextListener extends GUSLInstrumentedFilterContextListener {

    @Override
    protected GUSLMetricRegistry getMetricRegistry() {
        return MetricsFactory.getRegistry();
    }
}
