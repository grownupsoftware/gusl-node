package gusl.node.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.servlet.InstrumentedFilter;
import gusl.core.metrics.GUSLMetricRegistry;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author grant
 */
public abstract class GUSLInstrumentedFilterContextListener implements ServletContextListener {

    /**
     * Returns the {@link MetricRegistry} to inject into the servlet context.
     */
    protected abstract GUSLMetricRegistry getMetricRegistry();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(InstrumentedFilter.REGISTRY_ATTRIBUTE, getMetricRegistry());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
