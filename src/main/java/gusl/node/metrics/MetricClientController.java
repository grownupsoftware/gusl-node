package gusl.node.metrics;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.metrics.GUSLMetrics;
import gusl.core.metrics.MetricsFactory;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.properties.GUSLConfigurable;
import lombok.CustomLog;
import org.glassfish.hk2.api.PreDestroy;
import org.jvnet.hk2.annotations.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author Grant
 */
@Service
@CustomLog
public class MetricClientController implements GUSLConfigurable, PreDestroy {

    private final Map<String, MetricsWebSocketEndpoint> theClients = new ConcurrentHashMap<>();

    private final WebSocketMetricReporter theWebSocketMetricReporter;

    private final ObjectMapper theObjectMapper;

    // use this to send to any new connected clients
    private String theLastMetric = null;

    public MetricClientController() {
        logger.debug("-- MetricClientController [{}]", this.hashCode());
        theWebSocketMetricReporter = WebSocketMetricReporter.forRegistry(MetricsFactory.getRegistry())
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.SECONDS)
                .build();

        theObjectMapper = ObjectMapperFactory.createDefaultObjectMapper();
        ObjectMapperFactory.installJSDoubleSerialiser(theObjectMapper);
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        startup();
    }

    public void startup() {
        theWebSocketMetricReporter.setMetricClientController(this);
        //TODO - the time should be a value in a property file
        theWebSocketMetricReporter.start(10, TimeUnit.SECONDS);
    }

    @Override
    public void preDestroy() {
        if (theWebSocketMetricReporter != null) {
            theWebSocketMetricReporter.close();
        }
    }

    /**
     * remove a client from the client map
     *
     * @param metricsSocket the client
     */
    public void removeClient(MetricsWebSocketEndpoint metricsSocket) {
        if (theClients.containsKey(metricsSocket.getClientId())) {
            theClients.remove(metricsSocket.getClientId());
        }
    }

    /**
     * add a client to the client map
     *
     * @param metricsSocket the client
     */
    public void addClient(MetricsWebSocketEndpoint metricsSocket) {
        theClients.put(metricsSocket.getClientId(), metricsSocket);

        // for newly connected clients send the last received metrics
        if (theLastMetric != null) {
            metricsSocket.send(theLastMetric);
        }
    }

    public void report(GUSLMetrics metrics) {

        // Don't want latency
        GUSLMetrics webMetrics = GUSLMetrics.builder()
                .queues(metrics.getQueues())
                .meters(metrics.getMeters())
                .timers(metrics.getTimers())
                .counters(metrics.getCounters())
                .gauges(metrics.getGauges())
                .histograms(metrics.getHistograms()).build();

        // although there may be no connections will still grab the metrics 
        // so a connecting client gets the metrics immediately
        try {
            theLastMetric = theObjectMapper.writeValueAsString(webMetrics);
            logger.debug("Metrics  ..   ReportConfig ....\n{}", theLastMetric);

            if (theClients.size() > 0) {
                logger.debug("-- sending metrics to {} clients", theClients.size());
                // send message to all clients
                sendMessage(theLastMetric);
            }

        } catch (JsonProcessingException ex) {
            logger.error("Failed to parse metrics {}", ex.getMessage(), ex);
        }
    }

    /**
     * send message to all connected clients
     *
     * @param message the message to send
     */
    private void sendMessage(String message) {
        for (MetricsWebSocketEndpoint metricsSocket : theClients.values()) {
            metricsSocket.send(message);
        }
    }

}
