/* Copyright lottomart */
package gusl.node.blast;

import gusl.core.exceptions.GUSLException;
import org.jvnet.hk2.annotations.Contract;

/**
 *
 * @author grant
 */
@Contract
public interface NodeBlaster {

    public void initialise() throws GUSLException;

}
