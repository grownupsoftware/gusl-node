package gusl.node.slack;

import gusl.node.messaging.MessagingService;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 */
@Contract
public interface SlackService extends MessagingService {
}
