package gusl.node.slack;

/**
 * Simple event to send message to slack.
 *
 * @author dhudson
 */
public class SendSlackMessageEvent {

    private SlackMessage theMessage;

    public SendSlackMessageEvent() {
    }

    public SendSlackMessageEvent(SlackMessage message) {
        theMessage = message;
    }

    public SlackMessage getMessage() {
        return theMessage;
    }

    public void setMessage(SlackMessage message) {
        theMessage = message;
    }

}
