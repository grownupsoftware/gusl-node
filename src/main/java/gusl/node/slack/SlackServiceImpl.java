package gusl.node.slack;

import gusl.core.exceptions.GUSLException;
import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.SlackConfig;
import gusl.node.messaging.AbstractMessagingService;
import lombok.CustomLog;
import lombok.NoArgsConstructor;
import org.jvnet.hk2.annotations.Service;

/**
 * @author dhudson
 */
@Service
@NoArgsConstructor
@CustomLog
public class SlackServiceImpl extends AbstractMessagingService implements SlackService {

    private SlackConfig theConfig;

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        configure(config.getSlackConfig());
    }

    public void configure(SlackConfig config) throws GUSLException {
        if (config == null) {
            theConfig = new SlackConfig();
        } else {
            theConfig = config;
        }
    }

    @Override
    public void sendOpsMessage(String message) {
        sendMessageToSlack(theConfig.getOpsChannel(), message);
    }

    @Override
    public void sendResultMessage(String message) {
        sendMessageToSlack(theConfig.getResultChannel(), message);
    }

    @Override
    public void sendAlertMessage(String message) {
        sendMessageToSlack(theConfig.getAlertChannel(), message);
    }

    @Override
    public void sendDevOpsMessage(String message) {
        sendMessageToSlack(theConfig.getOpsChannel(), message);
    }

    private void sendMessageToSlack(String webHook, String message) {
        logger.debug("Hook {}, Message {}, hasConstruct ", webHook, message, hasConstruct());
        if (hasConstruct()) {
            if (StringUtils.isNotBlank(webHook)) {
                sendMessage(webHook, SlackMessage.builder().text(addConstructToMessage(message)).build());
            }
        }
    }

}
