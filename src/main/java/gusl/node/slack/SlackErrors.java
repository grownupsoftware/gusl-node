package gusl.node.slack;

public enum SlackErrors {
    PARTICIPANT_CACHE_RELOAD_ERROR("ERR001 - Failed to refresh participant cache - stop platform and resolve");

    private final String message;

    SlackErrors(String message) {
        this.message = message;
    }

    public String getError() {
        return message;
    }
}
