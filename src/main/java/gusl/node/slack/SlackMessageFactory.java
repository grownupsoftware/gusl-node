package gusl.node.slack;

import gusl.core.utils.SystemPropertyUtils;

/**
 * @author dhudson
 */
public class SlackMessageFactory {

    private SlackMessageFactory() {
    }

    public static SlackMessage createSlackMessage(String channel, String message) {
        SlackMessage slackMessage = new SlackMessage();
        slackMessage.setChannel(channel);
        slackMessage.setText(message);
        return slackMessage;
    }

    public static String buildMessageWithConstruct(String message) {
        StringBuilder builder = new StringBuilder();
        builder.append("-- ");
        builder.append(SystemPropertyUtils.getConstructName());
        builder.append(" -- ");
        builder.append(message);
        return builder.toString();
    }
}
