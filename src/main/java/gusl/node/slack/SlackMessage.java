package gusl.node.slack;

import com.fasterxml.jackson.annotation.JsonProperty;
import gusl.core.tostring.ToString;
import lombok.*;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SlackMessage {

    public static final String THUMBS_UP = ":thumbsup:";
    public static final String THUMBS_DOWN = ":thumbsdown:";

    private String text;
    private String channel;
    @JsonProperty("icon_emoji")
    private String iconEmoji;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
