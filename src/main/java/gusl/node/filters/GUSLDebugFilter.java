package gusl.node.filters;

import gusl.core.annotations.DocClass;
import gusl.core.utils.Platform;
import gusl.core.utils.SystemPropertyUtils;
import lombok.CustomLog;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Is the System Env DUMP_HEADERS is set, then log the headers for inbound and
 * outbound requests.
 *
 * @author dhudson
 */
@Provider
@DocClass(description = "Provides header information for requests")
@CustomLog
public class GUSLDebugFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private final boolean isDebugging;

    public GUSLDebugFilter() {
        isDebugging = SystemPropertyUtils.isDebugging();
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        if (isDebugging) {
            logger.info("{}{}{}", dumpRequest(requestContext), Platform.LINE_SEPARATOR, dumpResponse(responseContext));
        }
    }

    static String dumpRequest(ContainerRequestContext requestContext) {
        StringBuilder builder = new StringBuilder(200);
        builder.append("Request : ");
        builder.append(requestContext.getUriInfo().getPath());
        builder.append(" : Method : ");
        builder.append(requestContext.getMethod());
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(dumpHeaders(requestContext.getHeaders()));
        return builder.toString();
    }

    static String dumpResponse(ContainerResponseContext responseContext) {
        StringBuilder builder = new StringBuilder(200);
        builder.append("Response Status : ");
        builder.append(responseContext.getStatus());
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(dumpHeaders(responseContext.getHeaders()));
        return builder.toString();
    }

    static String dumpHeaders(MultivaluedMap<String, ?> headers) {
        StringBuilder builder = new StringBuilder(200);
        builder.append("Headers");
        headers.forEach((k, v) -> {
            builder.append(Platform.LINE_SEPARATOR);
            builder.append(k);
            builder.append(" : ");
            builder.append(v);
        });
        builder.append(Platform.LINE_SEPARATOR);
        return builder.toString();
    }

}
