package gusl.node.filters;

import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Platform;
import gusl.core.utils.StringUtils;
import org.glassfish.jersey.message.MessageUtils;

import javax.annotation.Priority;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.*;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @author dhudson on 19/09/2019
 */
@PreMatching
@Priority(Integer.MAX_VALUE)
public class JerseyClientLoggingFilter implements ClientRequestFilter, ClientResponseFilter, WriterInterceptor {

    private final int MAX_ENTITY_SIZE = 2048;

    private final String OUTBOUND_PREFIX = "> ";
    private final String INBOUND_PREFIX = "< ";

    final AtomicLong theRequestId = new AtomicLong(0);

    private final String LOG_REQUEST_PROPERTY = "GUSLLogRequest";
    private final String ENTITY_LOGGER_PROPERTY = "GUSLLogEntity";

    private final GUSLLogger theLogger;

    private static final Set<MediaType> READABLE_APP_MEDIA_TYPES = new HashSet<MediaType>() {{
        add(new MediaType("text", "*"));
        add(MediaType.APPLICATION_ATOM_XML_TYPE);
        add(MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        add(MediaType.APPLICATION_JSON_TYPE);
        add(MediaType.APPLICATION_SVG_XML_TYPE);
        add(MediaType.APPLICATION_XHTML_XML_TYPE);
        add(MediaType.APPLICATION_XML_TYPE);
    }};

    public JerseyClientLoggingFilter(GUSLLogger logger) {
        theLogger = logger;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if (theLogger.isDebugEnabled()) {
            final StringBuilder builder = new StringBuilder(200);

            long requestId = theRequestId.getAndIncrement();
            builder.append(OUTBOUND_PREFIX);
            builder.append("[");
            builder.append(requestId);
            builder.append("] ");
            builder.append(requestContext.getMethod());
            builder.append(" ");
            builder.append(requestContext.getUri());
            builder.append(Platform.LINE_SEPARATOR);
            builder.append(appendHeaders(requestContext.getStringHeaders(), OUTBOUND_PREFIX));

            requestContext.setProperty(LOG_REQUEST_PROPERTY, requestId);

            if (requestContext.hasEntity() && isReadable(requestContext.getMediaType())) {
                final OutputStream stream = new LoggingStream(builder, requestContext.getEntityStream());
                requestContext.setEntityStream(stream);
                requestContext.setProperty(ENTITY_LOGGER_PROPERTY, stream);
            } else {
                theLogger.debug("{}", builder.toString());
            }
        }
    }

    @Override
    public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
        if (theLogger.isDebugEnabled()) {
            final Object requestId = requestContext.getProperty(LOG_REQUEST_PROPERTY);

            final StringBuilder builder = new StringBuilder(200);
            builder.append(INBOUND_PREFIX);
            builder.append("[");
            builder.append(requestId);
            builder.append("] ");
            builder.append(responseContext.getStatus());
            builder.append(" ");
            builder.append(appendHeaders(responseContext.getHeaders(), INBOUND_PREFIX));

            if (responseContext.hasEntity() && isReadable(responseContext.getMediaType())) {
                responseContext.setEntityStream(logInboundEntity(builder, responseContext.getEntityStream(),
                        MessageUtils.getCharset(responseContext.getMediaType())));
            }

            theLogger.debug("{}", builder.toString());

        }
    }

    // This is one for Raf, if only I could get a reduce in there somewhere
    private String appendHeaders(MultivaluedMap<String, String> headers, String prefix) {
        return headers.entrySet()
                .stream()
                .map(e -> prefix + e.getKey() + " = " + StringUtils.elipseString(50, e.getValue().toString()))
                .collect(Collectors.joining(Platform.LINE_SEPARATOR));
    }

    InputStream logInboundEntity(final StringBuilder b, InputStream stream, final Charset charset) throws IOException {
        if (!stream.markSupported()) {
            stream = new BufferedInputStream(stream);
        }
        stream.mark(MAX_ENTITY_SIZE + 1);
        final byte[] entity = new byte[MAX_ENTITY_SIZE + 1];
        final int entitySize = stream.read(entity);
        b.append(new String(entity, 0, Math.min(entitySize, MAX_ENTITY_SIZE), charset));
        if (entitySize > MAX_ENTITY_SIZE) {
            b.append("...more...");
        }
        b.append('\n');
        stream.reset();
        return stream;
    }

    /**
     * Helper class used to log an entity to the output stream up to the specified maximum number of bytes.
     */
    class LoggingStream extends FilterOutputStream {

        private final StringBuilder b;
        private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

        /**
         * Creates {@code LoggingStream} with the entity and the underlying output stream as parameters.
         *
         * @param b     contains the entity to log.
         * @param inner the underlying output stream.
         */
        LoggingStream(final StringBuilder b, final OutputStream inner) {
            super(inner);

            this.b = b;
        }

        StringBuilder getStringBuilder(final Charset charset) {
            // write entity to the builder
            final byte[] entity = baos.toByteArray();

            b.append(new String(entity, 0, Math.min(entity.length, MAX_ENTITY_SIZE), charset));
            if (entity.length > MAX_ENTITY_SIZE) {
                b.append("...more...");
            }
            b.append('\n');

            return b;
        }

        @Override
        public void write(final int i) throws IOException {
            if (baos.size() <= MAX_ENTITY_SIZE) {
                baos.write(i);
            }
            out.write(i);
        }
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext context) throws IOException, WebApplicationException {
        final LoggingStream stream = (LoggingStream) context.getProperty(ENTITY_LOGGER_PROPERTY);
        context.proceed();

        if (stream != null) {
            theLogger.debug("{}", stream.getStringBuilder(MessageUtils.getCharset(context.getMediaType())));
        }
    }

    private boolean isReadable(MediaType mediaType) {
        if (mediaType != null) {
            for (MediaType readableMediaType : READABLE_APP_MEDIA_TYPES) {
                if (readableMediaType.isCompatible(mediaType)) {
                    return true;
                }
            }
        }
        return false;
    }

}
