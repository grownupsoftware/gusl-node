package gusl.node.filters;

import lombok.CustomLog;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author dhudson
 * @since 30/06/2021
 */
@Logged
@Provider
@CustomLog
public class RequestLoggingFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        logger.info("Inbound {}", GUSLDebugFilter.dumpRequest(containerRequestContext));
    }
}
