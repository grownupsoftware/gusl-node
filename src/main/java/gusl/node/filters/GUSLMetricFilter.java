/* Copyright lottomart */
package gusl.node.filters;

import com.codahale.metrics.MetricRegistry;
import gusl.core.annotations.DocClass;
import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;
import lombok.CustomLog;

import javax.ws.rs.container.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author grant
 */
@Provider
@DocClass(description = "Provides latency measurements for all API calls")
@CustomLog
public class GUSLMetricFilter implements ContainerRequestFilter, ContainerResponseFilter {

    public static final Map<String, Latency> latencyMap = new ConcurrentHashMap<>();

    public static final Map<Integer, Latency.Context> requestMap = new ConcurrentHashMap<>();

    @Context
    private ResourceInfo resourceInfo;

    public GUSLMetricFilter() {
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Latency latency = latencyMap.get(getName());
        if (latency == null) {
            String name = getName();
            latency = MetricsFactory.getLatency(MetricRegistry.name(name));
            latencyMap.put(name, latency);
        }

        requestMap.put(requestContext.hashCode(), latency.time());
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        Latency.Context time = requestMap.remove(requestContext.hashCode());
        if (time != null) {
            long duration = time.stop();
            //time.close();
            logger.debug("Metric: {} Duration: {} ms", getName(), (duration / 1e6));
        }
    }

    private String getName() {
        return resourceInfo.getResourceClass().getSimpleName() + "." + resourceInfo.getResourceMethod().getName();
    }
}
