package gusl.node.filters;

import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author dhudson
 * @since 11/11/2021
 */
@NameBinding
@Retention(RUNTIME)
@Target({TYPE, METHOD})
public @interface TestingEndpoint {
}
