package gusl.node.filters;

import gusl.core.eventbus.OnEvent;
import gusl.node.events.QuiesceRequestEvent;
import lombok.CustomLog;

import javax.annotation.Priority;
import javax.inject.Singleton;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author dhudson
 * @since 12/04/2023
 */
@PreMatching
@Priority(value = 3)
@Provider
@Singleton
@CustomLog
public class QuiesceFilter implements ContainerRequestFilter {

    private boolean isQuiesced;

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        if (isQuiesced) {
            logger.warn("Service is Quiesced {}", crc.getUriInfo().getPath());
            crc.abortWith(Response.status(Response.Status.GONE).build());
        }
    }

    @OnEvent
    public void handleQuiesceEvent(QuiesceRequestEvent event) {
        isQuiesced = event.isQuiesceRequested();
    }
}
