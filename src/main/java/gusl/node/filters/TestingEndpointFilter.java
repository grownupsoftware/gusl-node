package gusl.node.filters;

import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.OperationalMode;
import lombok.CustomLog;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author dhudson
 * @since 11/11/2021
 */
@TestingEndpoint
@Provider
@CustomLog
public class TestingEndpointFilter implements ContainerRequestFilter {

    @Inject
    private NodeDetails theNodeDetails;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        if (theNodeDetails.getOperationalMode() == OperationalMode.PROD) {
            logger.warn("Blocking {} as operational mode is production", containerRequestContext.getUriInfo().getPath());
            containerRequestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
        }
    }
}
