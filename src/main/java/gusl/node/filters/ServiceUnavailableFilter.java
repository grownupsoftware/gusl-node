package gusl.node.filters;

import gusl.core.eventbus.OnEvent;
import gusl.model.nodeconfig.NodeDetails;
import gusl.node.events.SplashEvent;
import lombok.CustomLog;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author dhudson
 */
@PreMatching
@Priority(value = 3)
@Provider
@Singleton
@CustomLog
public class ServiceUnavailableFilter implements ContainerRequestFilter {

    @Inject
    private NodeDetails theNodeDetails;

    public ServiceUnavailableFilter() {
    }

    @Override
    public void filter(ContainerRequestContext crc) throws IOException {
        if (theNodeDetails.isSplashRequired()) {
            if (theNodeDetails.isIsSplash() && !HttpMethod.OPTIONS.equalsIgnoreCase(crc.getMethod())) {
                logger.warn("Service is unavailable {}", crc.getUriInfo().getPath());
                crc.abortWith(Response.status(Response.Status.SERVICE_UNAVAILABLE).build());
            }
        }
    }

    @OnEvent
    public void handleSplashEvent(SplashEvent event) {
        if (theNodeDetails.isSplashRequired()) {
            setSplash(!theNodeDetails.isIsSplash());
        }
    }

    private void setSplash(boolean splash) {
        theNodeDetails.setIsSplash(splash);
        logger.info("Set Splash to {}", theNodeDetails.isIsSplash());
    }

}
