package gusl.node.filters;

import lombok.CustomLog;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * @author dhudson
 * @since 30/06/2021
 */
@Logged
@Provider
@CustomLog
public class ResponseLoggingFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        logger.info("Outbound {}", GUSLDebugFilter.dumpResponse(containerResponseContext));
    }
}
