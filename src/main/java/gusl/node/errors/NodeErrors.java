package gusl.node.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author grant
 */
public enum NodeErrors {

    // System wide api errors
    NO_ROUTE_TO_NODE("GUSLSE02 No route to node {0} {1}", "no.route.to.node"),
    IO_EXCEPTION("LMSE03 IO Exception {0}", "io.exception"),
    ACCESS_DENIED("GUSLSE16 Access Denied", "access.denied"),
    CONSTRAINT_VIOLATION("GUSLSE21 Constraint violation error: Resource: {0} Method: {1} Field: {2} Violation: {3}", "constraint.violation"),
    BAD_JSON_INPUT_ERROR_KEY("GUSLSE22 Bad Json", "resource.error.bad_json"),
    NOT_ALLOWED("GUSLSE24 Not allowed", "not.allowed"),
    NOT_SUPPORTED("GUSLSE25 Unsupported media type", "not.supported"),
    RESOURCE_NOT_FOUND("GUSLSE27 Resource (endpoint) does not exist {0}", "resource.not.found"),
    ERR_TIMEOUT("GUSLSE29 Request timed out", "server.timeout.error"),
    ENCODING_ERROR("GUSLSE34 Request must be un encoded or use gzip", "server.error.request.content_encoding"),
    DESERIALISE_ERROR("GUSLSE28 Error reading and deserializing request content", "server.error.request.derserializing"),
    CLASS_NOT_FOUND("GUSLSE39 Class of type {0} does not exist", "server.error.class.notfound"),
    MISSING_X_BODY("GUSLSE40 Invalid request. The request does not contain a X-Body-Type header", "server.error.request.no.xbody"),
    HTTP_SERVICE_NOT_CONFIGURED("GUSLSE41 HTTP Service has not been configured", "server.error.http.service.notconfigured"),
    AUTH_REST_FUTURE_ERROR("GUSLSE54 Interrupted Rest Auth future: {0}", "interrupted.future.for.auth.error"),
    REST_RESUME_WITH_EXCEPTION("GUSLSE55 REST resume async response with exception: {0}", "resume.rest.async.with.exception"),
    HTTP_RESUME_WITH_EXCEPTION("GUSLSE56 HTTP resume async response with exception: {0}", "resume.http.async.with.exception"),
    UNEXPECTED_ERROR("GUSLSE57 Unexpected exception: {0}", "unexpected.exception"),
    GUSL_SERVER_ERROR("GUSLSE58 Lm exception: {0}", "gusl.exception"),
    SERIALISATION_ERROR("GUSLSE59 Serialisation exception: {0}", "serialisation.exception"),
    NON_200_HTTP_RESPONSE("GUSLSE60 Non 200 response exception: {0}", "non.200.http.response"),
    HTTP_TIMEOUT("GUSLSE68 HTTP timeout", "http.timeout"),
    HTTP_FUTURE_ERROR("GUSLSE69 Error waiting for future", "error.waiting.fo.future"),
    ERR_REST_AUTH_TIMEOUT("GUSLSE72 Request timed out for rest auth", "server.timeout.error"),
    ERR_REST_RESPONSE_TIMEOUT("GUSLSE73 Request timed out for rest auth", "server.timeout.error"),
    ERR_ASYNC_RESPONSE_TIMEOUT("GUSLSE74 Request async timed out timeout [{0}] seconds", "server.timeout.error"),
    LOG_CONTROL_ERROR("GUSLSE95 Log Control Error {0}", "log.error.control"),
    ROUTER_CLIENT_SEND_FAILED("GUSLSE98 Failed to send node message {0}, {1}", "failed.node.message");

    private String field;
    private final String message;
    private final String messageKey;

    NodeErrors(String field, String message, String messageKey) {
        this.field = field;
        this.message = message;
        this.messageKey = messageKey;
    }

    NodeErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        if (field != null) {
            return new ErrorDO(field, message, messageKey);
        } else {
            return new ErrorDO(message, messageKey);
        }

    }

    public ErrorDO getError(Long id) {
        if (field != null) {
            if (id != null) {
                return new ErrorDO(field, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (id != null) {
                return new ErrorDO(null, message, messageKey, String.valueOf(id));
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public ErrorDO getError(String... params) {
        if (field != null) {
            if (params != null) {
                return new ErrorDO(field, message, messageKey, params);
            } else {
                return new ErrorDO(field, message, messageKey);
            }
        } else {
            if (params != null) {
                return new ErrorDO(null, message, messageKey, params);
            } else {
                return new ErrorDO(message, messageKey);
            }
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }
}
