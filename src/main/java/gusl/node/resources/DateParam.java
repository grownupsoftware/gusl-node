package gusl.node.resources;

import java.text.ParseException;
import java.util.Date;

import javax.ws.rs.ext.ParamConverter;
import gusl.core.utils.DateFormats;

public class DateParam implements ParamConverter<Date> {

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    @Override
    public Date fromString(String value) {
        if (value == null) {
            return null;
        }
        try {
            return DateFormats.getUTCFormatFor(DATE_FORMAT).parse(value);
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public String toString(Date value) {
        return DateFormats.formatUTCDate(DATE_FORMAT, value);
    }
}
