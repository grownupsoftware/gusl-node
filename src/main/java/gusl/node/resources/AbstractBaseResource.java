package gusl.node.resources;

import gusl.core.json.JsonUtils;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.IOUtils;
import gusl.core.utils.Platform;
import gusl.node.converter.ModelConverter;
import gusl.node.transport.HttpUtils;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.zip.GZIPInputStream;

/**
 * @author gbw
 */
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public abstract class AbstractBaseResource {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    protected ModelConverter theModelConverter;

    public AbstractBaseResource() {
    }

    public <IN, OUT> OUT convert(IN response, Class<OUT> responseClass) {
        return theModelConverter.convert(response, responseClass);
    }

    public ModelConverter getModelConverter() {
        return theModelConverter;
    }

    protected <T> void prettyPrint(T value) {
        logger.info("{}", JsonUtils.limitedPrettyPrint(value));
    }

    public Map<String, String> getHeaders(HttpServletRequest request) {
        return HttpUtils.getHeaders(request);
    }

    public InetAddress getRemoteIpAddress(HttpServletRequest request) {
        return HttpUtils.getRemoteIpAddress(request);
    }

    public void respond(AsyncResponse response, CompletableFuture<?> future) {
        HttpUtils.respond(response, future);
    }

    public Response buildResponse(StreamingOutput stream, String fileName) {
        Response.ResponseBuilder responseBuilder = Response.ok(stream);
        responseBuilder.header("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        return responseBuilder.build();
    }

    public void logHttpRequest(HttpServletRequest request) {

        StringBuilder builder = new StringBuilder(1000);
        Enumeration<String> headers = request.getHeaderNames();

        builder.append("{Headers}");
        builder.append(Platform.LINE_SEPARATOR);

        while (headers.hasMoreElements()) {
            String name = headers.nextElement();
            builder.append(name);
            builder.append(" : ");
            builder.append(request.getHeader(name));
            builder.append(Platform.LINE_SEPARATOR);
        }

        builder.append(getParams(request));

        boolean compressed = HttpUtils.isGZip(request);
        builder.append("{Payload Compressed=");
        builder.append(compressed);
        builder.append("}");

        builder.append(Platform.LINE_SEPARATOR);

        try {
            if (HttpUtils.isGZip(request)) {
                builder.append(IOUtils.inputStreamAsString(new GZIPInputStream(request.getInputStream())));
            } else {
                builder.append(IOUtils.inputStreamAsString(request.getInputStream()));
            }
        } catch (IOException ex) {
            logger.warn("Error whilst processing payload", ex);
            builder.append("null");
        }

        logger.info("{}", builder.toString());
    }

    public String getParams(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder(100);
        builder.append("{Parameters}");
        builder.append(Platform.LINE_SEPARATOR);
        Enumeration<String> params = request.getParameterNames();

        while (params.hasMoreElements()) {
            String name = params.nextElement();
            builder.append(name);
            builder.append(" : ");
            builder.append(request.getParameter(name));
            builder.append(Platform.LINE_SEPARATOR);
        }

        return builder.toString();
    }

}
