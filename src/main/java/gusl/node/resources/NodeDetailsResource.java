package gusl.node.resources;

import gusl.core.annotations.DocClass;
import gusl.model.nodeconfig.NodeDetails;
import gusl.node.providers.CORS;
import lombok.NoArgsConstructor;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author dhudson
 */
@DocClass(description = "Display the details / status of the node")
@Path("/details")
@Singleton
@PermitAll
@NoArgsConstructor
public class NodeDetailsResource extends AbstractBaseResource {

    @Inject
    private NodeDetails theDetails;

    @CORS
    @OPTIONS
    public Response getNodeOptions(@Context HttpServletResponse httpResponse) {
        return Response.ok().build();
    }

    @CORS
    @POST
    @PermitAll
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public NodeDetails getNodeDetails(@Context HttpServletResponse httpResponse) {
        return theDetails;
    }
}
