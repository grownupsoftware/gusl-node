package gusl.node.resources;

import gusl.core.utils.DateFormats;
import lombok.CustomLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

@CustomLog
public class DateTimeParam {

    private final Date date;

    public DateTimeParam(String value) {
        Date temp = null;
        try {
            temp = Date.from(DateTimeFormatter.ISO_DATE_TIME.parse(value, OffsetDateTime::from).toInstant());
        } catch (DateTimeParseException e) {
            SimpleDateFormat finalDateFormatter = DateFormats.getFormatFor("yyyy-MM-dd");
            try {
                temp = finalDateFormatter.parse(value);
            } catch (ParseException ex) {
                logger.info("Ignoring unparseable date: {}", value);
            }
        }
        date = temp;
    }

    public DateTimeParam(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "DateTimeParam{" + "date=" + date + '}';
    }

}
