/* Copyright lottomart */
package gusl.node.resources;

import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;
import gusl.core.metrics.QueueMetric;
import gusl.model.latency.LatencyResponseDO;

import javax.annotation.security.PermitAll;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author grant
 */
@Singleton
@Path(value = "latency")
@PermitAll
public class LatencyResource extends AbstractBaseResource {

    @GET
    public List<Map<String, Object>> get(@Context HttpServletResponse httpResponse) {
        return getMetrics(httpResponse);
    }

    @POST
    public LatencyResponseDO post(@Context HttpServletResponse httpResponse) {
        return new LatencyResponseDO(getMetrics(httpResponse));
    }

    private List<Map<String, Object>> getMetrics(HttpServletResponse httpResponse) {

        List<Map<String, Object>> allMetrics = new ArrayList<>();

        SortedMap<String, Latency> latencies = MetricsFactory.getRegistry().getLatencies();
        if (latencies != null) {
            allMetrics.addAll(latencies.entrySet().stream().map(entry -> {

                Map<String, Object> latencyMap = new HashMap<>();
                latencyMap.put("name", entry.getKey());
                latencyMap.put("type", "latency");
                latencyMap.put("metrics", getLatencyMetrics(entry.getValue()));
                return latencyMap;
            }).collect(Collectors.toList()));

        }

        SortedMap<String, QueueMetric> queues = MetricsFactory.getRegistry().getQueues();
        if (queues != null) {
            allMetrics.addAll(queues.entrySet().stream().map(entry -> {

                Map<String, Object> queueMap = new HashMap<>();
                queueMap.put("name", entry.getKey());
                queueMap.put("type", "queue");
                queueMap.put("metrics", getQueueMetrics(entry.getValue()));
                return queueMap;
            }).collect(Collectors.toList()));

        }

        return allMetrics;
    }

    private List<Map<String, Object>> getLatencyMetrics(Latency latency) {

        List<Map<String, Object>> list = new ArrayList<>();
        long range[] = latency.getRange();
        int bucketSize[] = latency.getBucketSizes();

        int y = 0;
        for (Latency.BucketCounter bucketCounter : latency.getBucketCounters()) {
            Map<String, Object> bucketMap = new HashMap<>();
            long total = 0;

            String bucketLabel;
            if (y == 0) {
                bucketLabel = "= " + bucketSize[y];
            } else if (y == range.length - 1) {
                bucketLabel = ">" + bucketSize[y];
            } else {
                bucketLabel = bucketSize[y - 1] + " - " + bucketSize[y];
            }

            bucketMap.put("name", bucketLabel);

            List<Map<String, Object>> latencyList = new ArrayList<>();
            for (int x = 0; x < range.length; x++) {
                Map<String, Object> data = new HashMap<>();
                long value = bucketCounter.getValue(x);
                String label;
                if (x == 0) {
                    label = "< " + latency.getTag(range[x]);
                } else if (x == range.length - 1) {
                    label = ">" + latency.getTag(range[x]);
                } else {
                    label = latency.getTag(range[x - 1]) + " - " + latency.getTag(range[x]);
                }
                data.put("range", label);
                data.put("value", value);
                latencyList.add(data);
                total += value;
            }

            Map<String, Object> data = new HashMap<>();
            data.put("range", "total");
            data.put("value", total);
            latencyList.add(data);

            data = new HashMap<>();
            data.put("range", "avg ms");
            data.put("value", total == 0 ? 0 : String.format("%8.2f", (bucketCounter.getTotalElapsed() / 1000000.0d) / (double) total));
            latencyList.add(data);

            bucketMap.put("latency", latencyList);
            bucketMap.put("avg", total == 0 ? 0 : String.format("%8.2f", (bucketCounter.getTotalElapsed() / 1000000.0d) / (double) total));
            bucketMap.put("total", total);
            list.add(bucketMap);
            y++;
        }
        return list;

    }

    private List<Map<String, Object>> getQueueMetrics(QueueMetric queue) {
        List<Map<String, Object>> list = new ArrayList<>();

        int range[] = queue.getRange();

        for (int x = 0; x < range.length; x++) {
            Map<String, Object> data = new HashMap<>();
            long value = queue.getValue(x);
            String label;
            if (x == 0) {
                label = "= " + range[x];
            } else if (x == range.length - 1) {
                label = ">" + range[x];
            } else {
                label = range[x - 1] + " - " + range[x];
            }
            data.put("range", label);
            data.put("value", value);
            list.add(data);
        }

        return list;

    }

    @POST
    @Path("/reset")
    public Map<String, Object> reset() {
        Map<String, Object> data = new HashMap<>();

        SortedMap<String, Latency> latencies = MetricsFactory.getRegistry().getLatencies();
        if (latencies != null) {
            latencies.entrySet().stream().forEach(entry -> {
                entry.getValue().reset();
            });
        }

        SortedMap<String, QueueMetric> queues = MetricsFactory.getRegistry().getQueues();
        if (queues != null) {
            queues.entrySet().stream().forEach(entry -> {
                entry.getValue().reset();
            });
        }

        data.put("success", true);
        return data;
    }
}
