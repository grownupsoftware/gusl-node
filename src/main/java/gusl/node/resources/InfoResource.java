package gusl.node.resources;

import gusl.core.utils.Platform;
import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.dto.ApplicationInfoDTO;
import gusl.model.nodeconfig.dto.InfoResponseDTO;
import gusl.model.nodeconfig.dto.SystemInfoDTO;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * Info Response. Shared across all nodes.
 *
 * @author dhudson
 */
@Singleton
@Path(value = "/info")
@PermitAll
public class InfoResource extends AbstractBaseResource {


    @Inject
    private NodeDetails theDetails;

    @GET
    public InfoResponseDTO getInfo(@Context HttpServletResponse httpResponse) {

        InfoResponseDTO response = new InfoResponseDTO();

        SystemInfoDTO systemInfo = new SystemInfoDTO();
        systemInfo.setNumberOfProcessors(Platform.getNumberOfCores());
        systemInfo.setJvmName(Platform.getJavaName());
        systemInfo.setJvmVendor(Platform.getJavaVendor());
        systemInfo.setJvmVersion(Platform.getJavaVersion());

        systemInfo.setSystemArchitecture(Platform.getArchitecture());
        systemInfo.setOsName(Platform.getOSName());
        systemInfo.setOsVersion(Platform.getOSVersion());

        systemInfo.setHostname(Platform.getHostName());
        systemInfo.setIpAddress(Platform.getIPAddress());

        systemInfo.setMaxMemory(Platform.getMaxMemory());
        systemInfo.setSystemLoad(Platform.getSystemLoad());
        systemInfo.setUpTime(Platform.getUpTime());
        systemInfo.setUsedMemory(Platform.getUsedMemory());

        response.setSystemInfo(systemInfo);

        ApplicationInfoDTO appInfo = new ApplicationInfoDTO();
        appInfo.setName("Lottomart");

        appInfo.setVersion(theDetails.getVersion());
        appInfo.setBuildDate(theDetails.getReleaseDate());
        appInfo.setBranchName(theDetails.getBranchName());

        response.setNodeDetails(theDetails);

        response.setApplicationInfo(appInfo);

        return response;
    }

}
