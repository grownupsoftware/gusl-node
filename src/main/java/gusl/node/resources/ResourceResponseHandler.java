/* Copyright lottomart */
package gusl.node.resources;

import javax.inject.Inject;
import gusl.core.eventbus.OnEvent;
import gusl.node.events.rest.RestResponseEvent;
import gusl.node.converter.ModelConverter;
import org.jvnet.hk2.annotations.Service;

/**
 * Completes the async HTTP Rest call response
 *
 * @author grant
 */
@Service
public class ResourceResponseHandler {

    @Inject
    private ModelConverter theModelConverter;

    @OnEvent
    @SuppressWarnings("unchecked")
    public void resumeRestCall(RestResponseEvent event) {
        try {
            event.resume(theModelConverter.convert(event.getResponseObject(), event.getResponseClass()));
        } catch (Throwable t) {
            // sometimes model converter can throw 'horrible errors'.
            event.resumeException(t);
        }
    }

}
