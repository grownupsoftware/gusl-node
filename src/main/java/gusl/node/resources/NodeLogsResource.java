package gusl.node.resources;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import gusl.model.log.LogsDTO;
import gusl.core.logging.GUSLLogController;
import gusl.node.logging.logstack.LogStack;

/**
 *
 * @author dhudson
 */
@Singleton
@Path("/logs")
@PermitAll
public class NodeLogsResource extends AbstractBaseResource {

    @Inject
    private LogStack theLogStack;

    @GET
    public LogsDTO getLogs() {
        LogsDTO response = new LogsDTO();
        response.setLogs(theLogStack.getLogs());
        response.setLevel(GUSLLogController.getCurrentLevel().name());
        return response;
    }
}
