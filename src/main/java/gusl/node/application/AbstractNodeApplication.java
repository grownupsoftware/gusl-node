package gusl.node.application;

import gusl.core.logging.GUSLLogController;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.SystemPropertyUtils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.node.application.events.ContainerStartedEvent;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.config.NodeConfigUtils;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.logging.logstack.StackAppender;
import gusl.node.providers.GUSLGZipEncoder;
import org.apache.logging.log4j.Level;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.jersey.client.filter.EncodingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import java.io.IOException;
import java.net.URL;

import static java.util.Objects.isNull;

/**
 * This is the main Jersey Application.
 * <p>
 * It is configured during the Servlet Container loader.
 * <p>
 * As the container is "deploying" this application, it blocks so that the
 * Container port will only listen when everything is deployed.
 * <p>
 * The issue we have is that the node is only ready once we have registered with
 * the Registra and loaded caches etc.
 * <p>
 * So we need to un-block from the AbstractCasanovaApplication, and await for the
 * startup method to be invoked.
 *
 * @author dhudson
 */
@ApplicationPath("/rest/*")
public abstract class AbstractNodeApplication extends ResourceConfig {

    private ServletContext theServletContext;
    public GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    public AbstractNodeApplication(final ServiceLocator serviceLocator, final ServletContext context) {

        // Don't want WADL support thank you.
        property(ServerProperties.WADL_FEATURE_DISABLE, true);

        // Lets load the current log4j file
        try {
            URL url = context.getResource("/WEB-INF/classes/log4j2.xml");
            GUSLLogController.reloadConfig(url.toURI());
            logger.info("Loading log config from {}", url);
        } catch (Throwable t) {
            logger.warn("Can't find log4j2.xml", t);
        }

        ServiceLocatorUtilities.enableImmediateScope(serviceLocator);
        GUSLServiceLocator.bootstrap(serviceLocator);

        if (SystemPropertyUtils.isDebugging()) {
            GUSLLogController.changeRootLogLevel(Level.DEBUG);
            logger.debug("Log Level change to debug");
        }

        theServletContext = context;

        setApplicationName(context.getContextPath().replace("/", "").toUpperCase());

        NodeDetails details = getNodeDetails();
        NodeConfigUtils.processManifest(context, details);
        details.setStatus(NodeStatus.STARTING);
        details.setNodeType(getNodeType());

        // Install the unavailability filter if required.
        details.setIsSplash(requiresServiceFilter());
        details.setSplashRequired(requiresServiceFilter());

        packages(true, GUSLServiceLocator.PACKAGE_PREFIXES);
        NodeConfig config = null;

        try {
            config = NodeConfigUtils.loadConfig(getNodeConfigClass());

            if (config.getNodeType() != getNodeType()) {
                details.setStatus(NodeStatus.FAILED);
                // Config does match Application type ..
                throw new RuntimeException("Config NodeType and Application Node types do not match");
            }

            details.setRequiredNodes(config.getRequiredNodes());
            details.setCacheGroups(config.getCacheGroups());

            // binds all the config/#Injection classes e.g. CoreInjection, EdgeInjection etc
            GUSLServiceLocator.bindAbstractBinders();

            // configure all configurables
            GUSLServiceLocator.injectConfig(config);

            // Enable Log Stack
            GUSLLogController.addAppender(new StackAppender(GUSLServiceLocator.getService(NodeEventBus.class)));

        } catch (IOException ex) {
            details.setStatus(NodeStatus.FAILED);
            logger.warn("Unable to load config", ex);
            throw new RuntimeException("Unable to load config", ex);
        }

        // Gzip compression
        register(new EncodingFeature(GUSLGZipEncoder.ENCODING_GZIP, GUSLGZipEncoder.class));
        register(MultiPartFeature.class);

        if (SystemPropertyUtils.isDebugging()) {
            GUSLServiceLocator.logServices();
            // Enable Tracing support.
            property(ServerProperties.TRACING, "ALL");
        }

    }

    public String getContextPath() {
        return getServletContext().getContextPath();
    }

    public ServletContext getServletContext() {
        return theServletContext;
    }

    private NodeDetails getNodeDetails() {
        NodeDetails nodeDetails = GUSLServiceLocator.getService(NodeDetails.class);
        if (nodeDetails == null) {
            return new NodeDetails();
        }
        return nodeDetails;
    }

    public abstract NodeType getNodeType();

    public abstract Class<? extends NodeConfig> getNodeConfigClass();

    /**
     * This is called from the launcher when the port is listening.
     */
    public static void startup() {
        GUSLLogger logger = GUSLLogManager.getLogger("Application Startup");
        try {
            NodeEventBus eventBus = GUSLServiceLocator.getService(NodeEventBus.class);
            if (isNull(eventBus)) {
                logger.error("This is going to hurt!");
            }
            eventBus.post(new ContainerStartedEvent());
        } catch (Throwable t) {
            logger.error("ERR001 - error sending new ContainerStartedEvent", t);
        }
    }

    /**
     * Only outside connected node will require this, like the edge or gateway.
     *
     * @return
     */
    public boolean requiresServiceFilter() {
        return false;
    }

}
