package gusl.node.application;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.Platform;
import gusl.core.utils.StringUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.NodeStatus;
import gusl.node.application.events.CachesLoadedEvent;
import gusl.node.application.events.ContainerStartedEvent;
import gusl.node.application.events.SetupCompleteEvent;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.messaging.GenericMessaginService;
import gusl.router.client.RouterJsonClient;
import gusl.router.dto.RegisterRequestDTO;
import gusl.router.dto.UpdateStatusDTO;

import javax.inject.Inject;

/**
 * This class handles the main loading of the application.
 * <p>
 * Sub classes of this should have the Class annotation @Singleton
 * <p>
 * This class will listen for the Container started event, and then register
 * with the registra.
 * <p>
 * Once Registration is complete, a SetupCompleteEvent is fired, which this
 * class listens to and re-acts with.
 *
 * @author dhudson
 */
public abstract class AbstractApplicationLoader {

    public final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    @Inject
    private NodeDetails theNodeDetails;

    @Inject
    private RouterJsonClient theRouterJsonClient;

    @Inject
    private NodeEventBus theEventBus;

    @Inject
    private GenericMessaginService theMessageService;

    public AbstractApplicationLoader() {
    }

    @OnEvent
    public void registerNode(ContainerStartedEvent event) {
        NodeDetails details = getNodeDetails();
        RegisterRequestDTO registerDTO = new RegisterRequestDTO();
        registerDTO.setType(details.getNodeType());
        //registerDTO.setPort(8080);
        registerDTO.setPort(SystemPropertyUtils.getApplicationPort());
        registerDTO.setRequiredNodes(getNodeDetails().getRequiredNodes());
        registerDTO.setCacheGroups(details.getCacheGroups());

        registerDTO.setVersion(details.getVersion() + " : " + details.getReleaseDate() + " : " + details.getBranchName());
        theRouterJsonClient.register(registerDTO);

    }

    // Only grumble if it takes more that 10 seconds
    @OnEvent(executeTime = "25s")
    public void setupComplete(SetupCompleteEvent event) {
        try {

            loadCaches();
            theEventBus.post(new CachesLoadedEvent());

            // Its all good
            theNodeDetails.setIsSplash(false);

            updateState(NodeStatus.ACTIVE, null);

            StringBuilder builder = getDisplayBanner();
            builder.append(Platform.LINE_SEPARATOR);
            builder.append(getReleaseVersion());
            builder.append(" - ");
            builder.append(getBranchDetails());
            builder.append(Platform.LINE_SEPARATOR);
            logger.info(builder.toString());

        } catch (GUSLErrorException ex) {
            logger.warn("Unable to load cache for node {}", theNodeDetails.getNodeType(), ex);
            updateState(NodeStatus.FAILED, ex);
        } catch (Throwable ex) {
            logger.warn("Unable to load cache for node {}", theNodeDetails.getNodeType(), ex);
            updateState(NodeStatus.FAILED, ex);
        }
    }

    public NodeDetails getNodeDetails() {
        return theNodeDetails;
    }

    public NodeEventBus getEventBus() {
        return theEventBus;
    }

    public void updateState(NodeStatus state, Throwable exception) {
        // Update the internal state
        getNodeDetails().setStatus(state);

        UpdateStatusDTO updateDTO = new UpdateStatusDTO();
        updateDTO.setNodeId(theNodeDetails.getNodeId());
        updateDTO.setStatus(state);

        theRouterJsonClient.nodeStatusChange(updateDTO);

        if (StringUtils.isNotBlank(SystemPropertyUtils.getConstructName())) {
            switch (state) {
                case ACTIVE:
                    theMessageService.sendDevOpsMessage("Started " + theNodeDetails.getNodeType() + " : " + theNodeDetails.getNodeId() + " : " + theNodeDetails.getVersion());
                    break;
                case FAILED:
                    theMessageService.sendDevOpsMessage("Failed to start " + theNodeDetails.getNodeType() + " : " + " with " + exception.getLocalizedMessage());
                    break;
            }
        }
    }

    public String getReleaseVersion() {
        return getNodeDetails().getVersion();
    }

    public String getBranchName() {
        return getNodeDetails().getBranchName();
    }

    public String getBranchDetails() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("BRANCH('");
        builder.append(getBranchName());
        builder.append("')");
        return builder.toString();
    }

    /**
     * Get the Banner String.
     *
     * @return
     */
    public abstract StringBuilder getDisplayBanner();

    /**
     * Load any cache information the node requires.
     * <p>
     * If the method completes without error, then the node status is changed to
     * ACTIVE and the router will start accepting requests.
     * <p>
     * If an exception is thrown, the Node Status will be set to FAILED.
     *
     * @throws GUSLErrorException
     */
    public abstract void loadCaches() throws GUSLErrorException;

}
