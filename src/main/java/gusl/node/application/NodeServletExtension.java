package gusl.node.application;

import gusl.core.utils.IOUtils;
import io.undertow.servlet.ServletExtension;
import io.undertow.servlet.api.DeploymentInfo;
import lombok.CustomLog;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * This is a servlet extension, where we set up the Jersey Container.
 *
 * @author dhudson
 */
@CustomLog
public class NodeServletExtension implements ServletExtension {

    // Manifest Attributes
    public static final String APPLICATION_KEY = "Application";
    public static final String WEBSOCKET_SUPPORT_KEY = "WebSocketSupport";
    public static final String WEBSOCKET_SUPPORT_PACKAGE_KEY = "WebSocketPackage";

    public static final String ACCESS_LOG_NAME_KEY = "AccessLogName";
    public static final String ACCESS_LOG_FORMAT_KEY = "AccessLogFormat";

    @Override
    public void handleDeployment(DeploymentInfo di, ServletContext servletContext) {
        System.out.println("Handle Deployment ...");

        // Lets save the System Class loader from Launcher
        ClassLoader original = Thread.currentThread().getContextClassLoader();
        try {
            // This is a WebAppClassLoader from Launcher
            ClassLoader loader = di.getClassLoader();
            // Swap to the new Class loader
            Thread.currentThread().setContextClassLoader(loader);
            String absoluteDiskPath = servletContext.getRealPath(IOUtils.WAR_MANIFEST_LOCATION);

            // Load the manifest to see what we need to do
            Properties manifest = new Properties();

            manifest.load(new FileInputStream(new File(absoluteDiskPath)));

            addApplication(servletContext, manifest);
            addAccessLogs(di, manifest, loader);

        } catch (IOException | ServletException ex) {
            logger.warn(ex);
        } finally {
            Thread.currentThread().setContextClassLoader(original);
        }
    }

    private void addApplication(ServletContext servletContext, Properties manifest) throws ServletException {

        if (!manifest.containsKey(APPLICATION_KEY)) {
            logger.warn("Unable to find {} in the manifest of the war", APPLICATION_KEY);
            throw new ServletException("Application not specified in war manifest");
        }

        final ServletRegistration.Dynamic appServlet = servletContext.addServlet("jerseyServlet", new ServletContainer());

        if (appServlet != null) {
            appServlet.setInitParameter("javax.ws.rs.Application", manifest.getProperty(APPLICATION_KEY));
            appServlet.setAsyncSupported(true);
            appServlet.setLoadOnStartup(1);
            appServlet.addMapping("/rest/*");
        }
    }

    private void addAccessLogs(final DeploymentInfo servletBuilder, Properties manifest, ClassLoader classLoader) throws ServletException {

        String logName = null;

        if (manifest.containsKey(ACCESS_LOG_NAME_KEY)) {
            logName = manifest.getProperty(ACCESS_LOG_NAME_KEY);
        }

        if (logName == null) {
            return;
        }

        String logFormat = null;
        if (manifest.containsKey(ACCESS_LOG_FORMAT_KEY)) {
            logFormat = manifest.getProperty(ACCESS_LOG_FORMAT_KEY);
        }

        if (logFormat == null) {
            logFormat = "common";
        }

        // This will always add log to the name, and we want .log so..
        logName = logName + ".";

        servletBuilder.addInitialHandlerChainWrapper(new AccessLogHandlerWrapper(logName, logFormat, classLoader));
    }

}
