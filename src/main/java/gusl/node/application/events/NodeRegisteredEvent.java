package gusl.node.application.events;

import gusl.model.nodeconfig.NodeDetails;

/**
 * Fired when a Node ID is assigned.
 *
 * @author dhudson
 */
public class NodeRegisteredEvent {

    private final NodeDetails theDetails;

    public NodeRegisteredEvent(NodeDetails details) {
        theDetails = details;
    }

    public NodeDetails getNodeDetails() {
        return theDetails;
    }
}
