package gusl.node.application.events;

import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.NodeStatus;

/**
 * This Event is called when the Application has done its thing and we have
 * connected to a router.
 *
 * The application should change the status of the details.
 *
 * @author dhudson
 */
public class SetupCompleteEvent {

    private final NodeDetails theDetails;

    public SetupCompleteEvent(NodeDetails details) {
        theDetails = details;
    }

    public NodeDetails getNodeDetails() {
        return theDetails;
    }

    /**
     * Simple helper method to set the state ACTIVE.
     */
    public void setNodeActive() {
        theDetails.setStatus(NodeStatus.ACTIVE);
    }

    /**
     * Simple helper method to set the state FAILED.
     */
    public void setNodeFailed() {
        theDetails.setStatus(NodeStatus.FAILED);
    }
}
