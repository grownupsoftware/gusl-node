package gusl.node.application.events;

import java.util.Date;

/**
 *
 * @author dhudson
 */
public class CachesLoadedEvent {

    private final Date timestamp;

    public CachesLoadedEvent() {
        timestamp = new Date();
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
