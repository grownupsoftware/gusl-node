package gusl.node.application;

import gusl.core.utils.SystemPropertyUtils;
import io.undertow.server.HandlerWrapper;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.accesslog.AccessLogHandler;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;
import io.undertow.server.handlers.accesslog.DefaultAccessLogReceiver;
import lombok.CustomLog;
import org.xnio.OptionMap;
import org.xnio.Xnio;
import org.xnio.XnioWorker;

import java.io.File;
import java.io.IOException;

/**
 * Apache HTTP Access logs for Undertow.
 * <p>
 * This factory produces token handlers for the following patterns</p>
 * <ul>
 * <li><b>%a</b> - Remote IP address
 * <li><b>%A</b> - Local IP address
 * <li><b>%b</b> - Bytes sent, excluding HTTP headers, or '-' if no bytes were
 * sent
 * <li><b>%B</b> - Bytes sent, excluding HTTP headers
 * <li><b>%h</b> - Remote host name
 * <li><b>%H</b> - Request protocol
 * <li><b>%l</b> - Remote logical username from identd (always returns '-')
 * <li><b>%m</b> - Request method
 * <li><b>%p</b> - Local port
 * <li><b>%q</b> - Query string (excluding the '?' character)
 * <li><b>%r</b> - First line of the request
 * <li><b>%s</b> - HTTP status code of the response
 * <li><b>%t</b> - Date and time, in Common Log Format format
 * <li><b>%u</b> - Remote user that was authenticated
 * <li><b>%U</b> - Requested URL path
 * <li><b>%v</b> - Local server name
 * <li><b>%D</b> - Time taken to process the request, in millis
 * <li><b>%T</b> - Time taken to process the request, in seconds
 * <li><b>%I</b> - current Request thread name (can compare later with
 * stacktraces)
 * </ul>
 * <p>
 * In addition, the caller can specify one of the following aliases for commonly
 * utilized patterns:</p>
 * <ul>
 * <li><b>common</b> - <code>%h %l %u %t "%r" %s %b</code>
 * <li><b>combined</b> -
 * <code>%h %l %u %t "%r" %s %b "%{i,Referer}" "%{i,User-Agent}"</code>
 * </ul>
 * <p>
 * <p>
 * There is also support to write information from the cookie, incoming header,
 * or the session<br>
 * It is modeled after the apache syntax:
 * <ul>
 * <li><code>%{i,xxx}</code> for incoming headers
 * <li><code>%{o,xxx}</code> for outgoing response headers
 * <li><code>%{c,xxx}</code> for a specific cookie
 * <li><code>%{r,xxx}</code> xxx is an attribute in the ServletRequest
 * <li><code>%{s,xxx}</code> xxx is an attribute in the HttpSession
 * </ul>
 *
 * @author dhudson
 */
@CustomLog
public class AccessLogHandlerWrapper implements HandlerWrapper {

    private final String theLogName;
    private final String theLogFormat;
    private final ClassLoader theClassLoader;

    public AccessLogHandlerWrapper(String logName, String logFormat, ClassLoader classLoader) {
        theLogName = logName;
        theLogFormat = logFormat;
        theClassLoader = classLoader;
    }

    @Override
    public HttpHandler wrap(HttpHandler handler) {
        return createAccessLogHandler(handler);
    }

    private AccessLogHandler createAccessLogHandler(HttpHandler handler) {
        try {
            logger.info("Creating access logger {} with format of {}", theLogName, theLogFormat);

            AccessLogReceiver accessLogReceiver = new DefaultAccessLogReceiver(
                    createWorker(), new File(System.getProperty(SystemPropertyUtils.LOGS_LOCATION_KEY)), theLogName);
            return new AccessLogHandler(handler, accessLogReceiver, theLogFormat, theClassLoader);
        } catch (IOException ex) {
            throw new IllegalStateException("Failed to create AccessLogHandler", ex);
        }
    }

    private XnioWorker createWorker() throws IOException {
        Xnio xnio = Xnio.getInstance();
        return xnio.createWorker(OptionMap.builder().set(org.xnio.Options.THREAD_DAEMON, true).getMap());
    }
}
