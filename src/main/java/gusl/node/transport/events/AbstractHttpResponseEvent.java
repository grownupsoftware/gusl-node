/* Copyright lottomart */
package gusl.node.transport.events;

import gusl.core.annotations.DocField;
import gusl.core.metrics.Latency;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import static gusl.node.transport.HttpUtils.*;

/**
 * @author grant
 */
public abstract class AbstractHttpResponseEvent {

    @DocField(description = "URI Information")
    private final Latency.Context theLatencyContext;

    @DocField(description = "The Async Response Handler")
    private final AsyncResponse theAsyncResponse;

    public AbstractHttpResponseEvent(AsyncResponse theAsyncResponse, Latency.Context theLatencyContext) {
        this.theLatencyContext = theLatencyContext;
        this.theAsyncResponse = theAsyncResponse;
    }

    public <T> Response.ResponseBuilder okResponse(T responseObject) {
        Response.ResponseBuilder builder = Response.ok(responseObject);
        builder.header(ACCEPT_HEADER_NAME, CONTENT_TYPE_VALUE);
        builder.header(CONTENT_TYPE_HEADER_NAME, CONTENT_TYPE_VALUE);
        builder.header(BODY_CONTENT_CLASS_HEADER_NAME, responseObject.getClass().getName());
        builder.header(ACCEPT_ENCODING_HEADER_NAME, CONTENT_ENCODING_HEADER_VALUE);
        builder.header(CONTENT_ENCODING_HEADER_NAME, CONTENT_ENCODING_HEADER_VALUE);

        return builder;
    }

    public Latency.Context getLatencyContext() {
        return theLatencyContext;
    }

    public AsyncResponse getAsyncResponse() {
        return theAsyncResponse;
    }


}
