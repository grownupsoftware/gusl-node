/* Copyright lottomart */
package gusl.node.transport.events;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.Latency;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author grant
 */
@DocClass(description = "Completes the async REST call with an exception")
public class HttpExceptionEvent extends AbstractHttpResponseEvent {

    private static final GUSLLogger logger = GUSLLogManager.getLogger(HttpExceptionEvent.class);

    @DocField(description = "The exception")
    private final GUSLErrorException theErrorException;

    public HttpExceptionEvent(AsyncResponse asyncResponse, Latency.Context latencyContext, GUSLErrorException errorException) {
        super(asyncResponse, latencyContext);
        this.theErrorException = errorException;
    }

    public void resumeExceptionally() {
        logger.error("REST resource error", theErrorException);
        if (!getAsyncResponse().isDone()) {
            if (theErrorException.getCause() != null) {
                getAsyncResponse().resume(theErrorException.getCause());
            } else {
                getAsyncResponse().resume(theErrorException);
            }
        }
    }
}
