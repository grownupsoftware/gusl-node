/* Copyright lottomart */
package gusl.node.transport.events;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.Latency;
import gusl.node.errors.NodeErrors;

import javax.ws.rs.container.AsyncResponse;

/**
 * @author grant
 */
@DocClass(description = "Completes the async REST call")
public class HttpResponseEvent extends AbstractHttpResponseEvent {

    private static final GUSLLogger logger = GUSLLogManager.getLogger(HttpResponseEvent.class);

    @DocField(description = "The response object")
    protected final Object responseObject;

    public HttpResponseEvent(AsyncResponse asyncResponse, Latency.Context latencyContext, Object responseObject) {
        super(asyncResponse, latencyContext);
        this.responseObject = responseObject;
    }

    public void resume() {
        getLatencyContext().close();

        if (!getAsyncResponse().isDone()) {
            if (responseObject != null) {
                getAsyncResponse().resume(okResponse(responseObject).build());

                getAsyncResponse().resume(responseObject);
            } else {
                resumeExceptionally(new GUSLErrorException(NodeErrors.HTTP_RESUME_WITH_EXCEPTION.getError("Response was null")));
            }
        } else {
            logger.warn("Connection has gone when replying with: {}", responseObject);
        }
    }

    private void resumeExceptionally(GUSLErrorException errorException) {
        logger.error("REST resource error", errorException);
        if (!getAsyncResponse().isDone()) {
            if (errorException.getCause() != null) {
                getAsyncResponse().resume(errorException.getCause());
            } else {
                getAsyncResponse().resume(errorException);
            }
        }
    }

}
