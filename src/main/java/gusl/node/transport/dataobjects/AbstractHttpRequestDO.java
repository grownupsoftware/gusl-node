/* Copyright lottomart */
package gusl.node.transport.dataobjects;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;

/**
 *
 * @author grant
 */
@DocClass(description = "Fields used by the data service")
public class AbstractHttpRequestDO {

    @DocField(description = "Unique http request ID")
    private Long httpRequestId;

    public Long getHttpRequestId() {
        return httpRequestId;
    }

    public void setHttpRequestId(Long httpRequestId) {
        this.httpRequestId = httpRequestId;
    }

}
