/* Copyright lottomart */
package gusl.node.transport.dataobjects;

/**
 * Any DO that uses the EventbusHTTPService must extend this
 *
 * @author grant
 */
public interface HttpRequestDO {

    public Long getHttpRequestId();

    public void setHttpRequestId(Long requestId);
//    /**
//     * called when a DO is first received from an http request
//     *
//     * @param asyncResponse the HTTP async response
//     * @param eventBus the internal event bus
//     * @param gzip request supports gzip
//     */
//    public void initialise(AsyncResponse asyncResponse, LmEventBus eventBus, boolean gzip);
//
//    /**
//     * called when cloning an one HTTPRequestDO into another DO that will
//     * complete the HTTP request
//     *
//     * @param asyncResponse
//     * @param latencyContext
//     * @param eventBus
//     * @param gzip
//     */
//    public void implant(AsyncResponse asyncResponse, Latency.Context latencyContext, LmEventBus eventBus, boolean gzip);
//
//    public Latency.Context getLatencyContext();
//
//    public AsyncResponse getAsyncResponse();
//
//    public LmEventBus getEventBus();
//
//    public boolean isGzip();

}
