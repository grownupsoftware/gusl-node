package gusl.node.transport;

import gusl.core.logging.GUSLLogger;

import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.core.Response;

/**
 * @author dhudson
 */
public class LoggedFutureCallback implements InvocationCallback<Response> {

    private final GUSLLogger logger;
    private final String path;

    public LoggedFutureCallback(GUSLLogger logger, String path) {
        this.logger = logger;
        this.path = path;
    }

    @Override
    public void completed(Response result) {
    }

    @Override
    public void failed(Throwable throwable) {
        logger.warn("Sending of http request failed. [{}]", path, throwable);
    }
}
