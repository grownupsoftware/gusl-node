/* Copyright lottomart */
package gusl.node.transport;

import gusl.node.jersey.JerseyNodeClient;
import gusl.router.model.HttpNodeRequest;

import java.util.concurrent.CompletableFuture;

/**
 * @author grant
 */
public interface HttpRequestClient {

    boolean isConfigured();

    void destroy();

    <RESPONSE> CompletableFuture<RESPONSE> sendRequest(HttpNodeRequest<RESPONSE> nodeRequest);

    JerseyNodeClient getHttpClient();
}
