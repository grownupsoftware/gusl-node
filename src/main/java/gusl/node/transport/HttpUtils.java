package gusl.node.transport;

import com.codahale.metrics.json.MetricsModule;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.annotations.Unfinished;
import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.utils.IOUtils;
import gusl.core.utils.Platform;
import gusl.node.async.AsyncTimeoutConstants;
import gusl.node.errors.NodeErrors;
import lombok.CustomLog;
import org.glassfish.jersey.server.internal.process.MappableException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;

/**
 * @author dhudson
 */
@CustomLog
public class HttpUtils {

    public static final String AUTHORISATION_HEADER_NAME = "Authorization";
    public static final String ACCEPT_HEADER_NAME = "Accept";
    public static final String ACCEPT_ENCODING_HEADER_NAME = "Accept-Encoding";
    public static final String CONTENT_TYPE_HEADER_NAME = "Content-Type";
    public static final String CONTENT_TYPE_VALUE = "application/json";
    public static final String CONTENT_ENCODING_HEADER_NAME = "Content-Encoding";
    public static final String CONTENT_ENCODING_HEADER_VALUE = "gzip";
    public static final String TRANSFER_ENCODING = "Transfer-Encoding";

    public static final String CHUNKED_ENCODING = "chunked";
    public static final String USER_AGENT_HEADER_NAME = "User-Agent";
    /*
     * X-Forwarded-For is a well-established HTTP header used by proxies,
     * including Cloudflare, to pass along other IP addresses in the request.
     * This is often the same as CF-Connecting-IP, but there may be multiple
     * layers of proxies in a request path.
     *
     * There are two possible outcomes:
     *
     * First, if there was no existing "X-Forwarded-For" header in the request
     * sent to Cloudflare, then the header would have an identical value to the
     * CF-Connecting-IP header, like this:
     *
     * "X-Forwarded-For: A.B.C.D"
     *
     * where A.B.C.D is the client's IP address, also known as the original
     * visitor IP address.
     *
     * Second, if there was an "X-Forwarded-For" header present in the request
     * sent to Cloudflare, Cloudflare appends the IP address of the HTTP proxy
     * to its value, as the last in the list.
     */
    public static final String X_FORWARD_FOR_HEADER_NAME = "X-Forwarded-For";

    // Casanova Headers
    public static final String BODY_CONTENT_CLASS_HEADER_NAME = "X-Body-Type";
    public static final String NODE_ROUTE_HEADER_NAME = "X-Node-Route";
    public static final String NODE_ROUTER_ID_HEADER_NAME = "X-Node-Router-Id";

    // When publishing, you need to specify your Node ID so you don't get the message back.
    public static final String NODE_PUBLISH_NODE_ID = "X-Node-Id";

    // Cloudflare Headers
    /*
     * The CF-Ray header is a hash with the data center that the request came through
     */
    public static final String CF_RAY_HEADER_NAME = "CF-RAY";
    /*
     * This header holds the country code of the originating visitor. It is a
     * two character value that will have the country code, if the country code
     * is unknown, it will be "XX". This header is added to requests by enabling
     * Cloudflare IP Geolocation in the dashboard.
     */
    public static final String CF_COUNTRY_HEADER_NAME = "CF-IPCountry";
    /*
     * To provide the client (visitor) IP address for every request to the
     * origin, Cloudflare adds the CF-Connecting-IP header.
     */
    public static final String CF_CLIENT_IP_HEADER_NAME = "CF-ConnectingIP";
    /*
     * Currently this header is a JSON object, containing only one key called
     * "scheme". The meaning is identical to that of X-Forwarded-Proto above -
     * e.g. it will be either HTTP or HTTPS, and it is only relevant if you need
     * to enable Flexible SSL in your Cloudflare settings.
     *
     * "Cf-Visitor: { \"scheme\":\"https\"}"
     */
    public static final String CF_VISITOR_HEADER_NAME = "CF-Visitor";

    public static final String X_API_HEADER_NAME = "X-Api-Key";

    private static final Map<String, Class<?>> theClassMap = new HashMap<>();
    private static final int BUFFER_SIZE = 65536;
    private static final ObjectMapper theMapper = ObjectMapperFactory.getDefaultObjectMapper();

    @Unfinished(createdBy = "Darren", priority = Unfinished.Priority.MEDIUM, description = "Change to config value")
    public static final int COMPRESSION_THRESHOLD = 1024;

    public static final Response OK_200 = Response.ok().build();

    public static final int IP_RESTRICTION_STATUS = 406;

    static {
        theMapper.registerModule(new MetricsModule(TimeUnit.MINUTES, TimeUnit.MINUTES, true));
    }

    private HttpUtils() {
    }

    public static boolean isGZip(HttpServletRequest servletRequest) {
        return isGZip(servletRequest.getHeader(CONTENT_ENCODING_HEADER_NAME));
    }

    public static boolean isGZip(Response response) {
        return isGZip(response.getHeaderString(CONTENT_ENCODING_HEADER_NAME));
    }

    public static boolean isGZip(String contentEncoding) {
        if (contentEncoding == null) {
            return false;
        }
        return contentEncoding.equalsIgnoreCase(CONTENT_ENCODING_HEADER_VALUE);
    }

    public static boolean isChunkedEncoding(HttpServletRequest servletRequest) {
        String header = servletRequest.getHeader(TRANSFER_ENCODING);
        if (header == null) {
            return false;
        }

        return header.equals(CHUNKED_ENCODING);
    }

    /**
     * Return the Entity payload.
     *
     * @param servletRequest
     * @param asyncResponse
     * @return the payload as the type given in BODY_CONTENT header, or null, if
     * unable to process the request.
     */
    public static Object getPayloadFromHttpRequest(HttpServletRequest servletRequest, AsyncResponse asyncResponse) {
        // get content encoding
        boolean requestGzip = isGZip(servletRequest);

        String requestClassName = servletRequest.getHeader(BODY_CONTENT_CLASS_HEADER_NAME);
        logger.debug("requestClassName : {}", requestClassName);
        if (requestClassName == null) {
            logger.warn("  .............   Its suggesting that I don't have a X_BODY ");
            Enumeration<String> headers = servletRequest.getHeaderNames();
            StringBuilder builder = new StringBuilder();
            builder.append("{Headers}");
            builder.append(Platform.LINE_SEPARATOR);

            while (headers.hasMoreElements()) {
                String name = headers.nextElement();
                builder.append(name);
                builder.append(" : ");
                builder.append(servletRequest.getHeader(name));
                builder.append(Platform.LINE_SEPARATOR);
            }
            logger.warn("{}", builder.toString());

            asyncResponse.resume(new GUSLErrorException(NodeErrors.MISSING_X_BODY.getError()));
            return null;
        }

        Class<?> bodyType;
        try {
            bodyType = getType(requestClassName);
        } catch (ClassNotFoundException e) {
            logger.error("Invalid response. No response class named {} exists.", requestClassName, e);
            asyncResponse.resume(new GUSLErrorException(NodeErrors.CLASS_NOT_FOUND.getError(requestClassName), e));
            return null;
        }

        Object requestDo;
        try {
            requestDo = deserializeRequest(servletRequest, bodyType, requestGzip);
        } catch (IOException e) {
            logger.error("DESERIALISE_ERROR. Class named {}", requestClassName, e);
            asyncResponse.resume(new GUSLErrorException(NodeErrors.DESERIALISE_ERROR.getError()));
            return null;
        }

        return requestDo;
    }

    private static Object deserializeRequest(HttpServletRequest httpRequest, Class<?> requestType, boolean gzippedRequest) throws IOException {
        try (InputStream ins = gzippedRequest ? new GZIPInputStream(httpRequest.getInputStream(), BUFFER_SIZE) : httpRequest.getInputStream()) {
            return theMapper.readValue(ins, requestType);
        }
    }

    private static Class<?> getType(String typeName) throws ClassNotFoundException {
        Class<?> type = theClassMap.get(typeName);
        if (type == null) {
            type = Class.forName(typeName);
            theClassMap.put(typeName, type);
        }
        return type;
    }

    public static void resumeAsyncOK(AsyncResponse asyncResponse) {
        if (asyncResponse != null) {
            asyncResponse.resume(Response.ok().build());
        }
    }

    public static <T> void resumeAsyncOK(final AsyncResponse asyncResponse, T response) {
        if (asyncResponse != null) {
            try {
                asyncResponse.resume(Response.ok().entity(response).build());
            } catch (MappableException ex) {
                logger.warn("Error completing async response. [{}]", ex.getMessage());
            }
        }
    }

    public static <T> void resumeAsyncOK(final AsyncResponse asyncResponse, T response, NewCookie cookie) {
        asyncResponse.resume(Response.ok(response).cookie(cookie).build());
    }

    public static void resumeAsyncOK(final AsyncResponse asyncResponse, NewCookie cookie) {
        asyncResponse.resume(Response.ok().cookie(cookie).build());
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, Response.Status status) {
        asyncResponse.resume(Response.status(status).build());
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, GUSLErrorException exception) {
        resumeAsyncError(asyncResponse, Response.Status.INTERNAL_SERVER_ERROR, exception);
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, Response.Status status, ErrorDO errorDO) {
        resumeAsyncError(asyncResponse, status, new GUSLErrorException(errorDO));
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, ErrorDO errorDO) {
        resumeAsyncError(asyncResponse, Response.Status.INTERNAL_SERVER_ERROR, new GUSLErrorException(errorDO));
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, ErrorsDTO errorDO) {
        asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorDO).type(MediaType.APPLICATION_JSON).build());
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, Response.Status status, GUSLErrorException exception) {
        asyncResponse.resume(Response.status(status).entity(exception).type(MediaType.APPLICATION_JSON).build());
    }

    public static void resumeAsyncError(final AsyncResponse asyncResponse, Throwable throwable) {
        if (handle(asyncResponse, throwable)) {
            return;
        }

        if (throwable.getCause() != null) {
            if (handle(asyncResponse, throwable.getCause())) {
                return;
            }
        }

        asyncResponse.resume(throwable);
    }

    private static boolean handle(AsyncResponse asyncResponse, Throwable throwable) {
        if (throwable instanceof GUSLErrorException) {
            resumeAsyncError(asyncResponse, (GUSLErrorException) throwable);
            return true;
        }
        if (throwable instanceof TimeoutException) {
            resumeAsyncError(asyncResponse, new GUSLErrorException(NodeErrors.HTTP_TIMEOUT.getError()));
            return true;
        }
        return false;
    }

    public static void resumeAsyncIpRestrictionError(final AsyncResponse asyncResponse) {
        asyncResponse.resume(Response.status(IP_RESTRICTION_STATUS).type(MediaType.APPLICATION_JSON).build());
    }

    public static <I> I waitForFuture(Future<I> future, int seconds) throws GUSLErrorException {
        try {
            return future.get(seconds, AsyncTimeoutConstants.DEFAULT_TIME_UNIT);
        } catch (InterruptedException | ExecutionException ex) {
            if (ex.getCause() != null && ex.getCause() instanceof GUSLErrorException) {
                throw (GUSLErrorException) ex.getCause();
            }
            throw new GUSLErrorException(NodeErrors.HTTP_FUTURE_ERROR.getError(), ex);
        } catch (TimeoutException ex) {
            future.cancel(true);
            throw new GUSLErrorException(NodeErrors.HTTP_TIMEOUT.getError(), ex);
        }
    }

    public static <I> I waitForFuture(Future<I> future) throws GUSLErrorException {
        return waitForFuture(future, AsyncTimeoutConstants.DEFAULT_TIMEOUT);
    }

    public static Map<String, String> getHeaders(HttpServletRequest request) {

        Map<String, String> headers = new HashMap<>();

        if (request.getHeaderNames() != null) {
            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                headers.put(name, request.getHeader(name));
            }
        }

        return headers;
    }

    public static String getClientIpAddress(HttpServletRequest request) {
        InetAddress address = getRemoteIpAddress(request);
        if (address != null) {
            return address.getHostAddress();
        }

        return null;
    }

    public static InetAddress getRemoteIpAddress(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        InetAddress address = null;
        try {
            // Cloudflare
            String cfHeader = request.getHeader(CF_CLIENT_IP_HEADER_NAME);
            if (cfHeader != null) {
                address = InetAddress.getByName(cfHeader.trim());

                if (address != null) {
                    return address;
                }
            }

            // Cascade to X-Forwarded-for
            String xHeader = request.getHeader(X_FORWARD_FOR_HEADER_NAME);
            if (xHeader != null) {
                String[] addresses = xHeader.split(",");
                if (addresses.length > 0) {
                    address = InetAddress.getByName(addresses[0].trim());
                }
            }

            if (address == null) {
                address = InetAddress.getByName(request.getRemoteAddr());
            }

            if (address == null) {
                logger.warn("Unable to ascertain IP Address from CF: {} X-Forward : {}, Remote IP {}", cfHeader, xHeader, request.getRemoteAddr());
            }
        } catch (UnknownHostException ex) {
            logger.warn("Unable to resolve host", ex);
        }
        return address;
    }

    public static Map<String, List<String>> splitQuery(String url) {
        final Map<String, List<String>> queryPairs = new LinkedHashMap<>();
        final String[] pairs = url.split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            try {
                final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
                if (!queryPairs.containsKey(key)) {
                    queryPairs.put(key, new LinkedList<>());
                }
                final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
                queryPairs.get(key).add(value);
            } catch (UnsupportedEncodingException ex) {
                logger.warn("Unable to decode query", ex);
            }
        }
        return queryPairs;
    }

    public static Response getOKResponse() {
        return OK_200;
    }

    public static SSLContext getNoTrustSLLContext() throws KeyManagementException, NoSuchAlgorithmException {
        TrustManager[] trustAllCerts = new X509TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustAllCerts, null);

        return sslContext;
    }

    public static <RESPONSE> void respond(AsyncResponse asyncResponse, CompletableFuture<RESPONSE> future) {
        future.whenComplete((response, throwable) -> {
            if (throwable != null) {
                errorConsumer(asyncResponse).accept(throwable);
            } else {
                responseConsumer(asyncResponse).accept(response);
            }
        });
    }

    public static Consumer<Throwable> errorConsumer(final AsyncResponse asyncResponse) {
        return throwable -> resumeAsyncError(asyncResponse, throwable);
    }

    public static <RESPONSE> Consumer<RESPONSE> responseConsumer(final AsyncResponse asyncResponse) {
        return response -> resumeAsyncOK(asyncResponse, response);
    }

    public static String getPayloadAsString(Response response) throws IOException {
        try (response) {
            try (InputStream ins = isGZip(response) ? new GZIPInputStream((InputStream) response.getEntity()) : (InputStream) response.getEntity()) {
                return IOUtils.inputStreamAsString(ins);
            }
        }
    }

    public static <RESPONSE> RESPONSE parseResponse(ObjectMapper mapper, Response response, Class<RESPONSE> responseClass) throws IOException {
        return mapper.readValue(getPayloadAsString(response), responseClass);
    }

    public static <RESPONSE> RESPONSE parseResponse(ObjectMapper mapper, Response httpResponse,
                                                    Class<RESPONSE> responseClass, Class<?> genericHint) throws IOException {
        if (genericHint == null) {
            return parseResponse(mapper, httpResponse, responseClass);
        }

        JavaType javaType = mapper.getTypeFactory().constructParametricType(responseClass, genericHint);
        return mapper.readValue(getPayloadAsString(httpResponse), javaType);
    }
}
