package gusl.node.transport;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Simple class to have a Map of Strings to Classes.
 *
 * In fairness this could be a singleton / static, as you can't have duplicate
 * classes.
 *
 * @author dhudson
 */
public class ClassTypeMapper {

    private final Map<String, Class<?>> theClassMap;

    public ClassTypeMapper() {
        theClassMap = new ConcurrentHashMap<>();
    }

    /**
     * Get the type of the response.
     *
     * @param typeName
     * @return
     * @throws ClassNotFoundException
     */
    public Class<?> getType(String typeName) throws ClassNotFoundException {
        Class<?> type = theClassMap.get(typeName);
        if (type == null) {
            type = Class.forName(typeName);
            theClassMap.put(typeName, type);
        }
        return type;
    }

}
