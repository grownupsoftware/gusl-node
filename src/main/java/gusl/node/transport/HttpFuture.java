/* Copyright lottomart */
package gusl.node.transport;

import lombok.CustomLog;

import java.util.concurrent.*;

/**
 * @author grant
 */
@CustomLog
public class HttpFuture<T> implements Future<T> {

    private final CountDownLatch theLatch = new CountDownLatch(1);

    private T value;

    private static enum State {
        WAITING, DONE, CANCELLED, EXCEPTION
    }

    private volatile State state = State.WAITING;
    private Throwable theThrowable;

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        state = State.CANCELLED;
        return true;
    }

    @Override
    public boolean isCancelled() {
        return state == State.CANCELLED;
    }

    @Override
    public boolean isDone() {
        return state == State.DONE;
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        if (state == State.EXCEPTION) {
            logger.warn("Get is an execution exception");
            throw new ExecutionException(theThrowable.getMessage(), theThrowable);
        }
        theLatch.await();
        if (state == State.EXCEPTION) {
            throw new ExecutionException(theThrowable.getMessage(), theThrowable);
        }
        return value;
    }

    @Override
    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        if (state == State.EXCEPTION) {
            throw new ExecutionException(theThrowable.getMessage(), theThrowable);
        }
        theLatch.await(timeout, unit);
        if (state == State.EXCEPTION) {
            throw new ExecutionException(theThrowable.getMessage(), theThrowable);
        }
        if (state == State.DONE) {
            return value;
        }

        throw new TimeoutException("Future Timed Out");
    }

    public void complete(T object) {
        state = State.DONE;
        this.value = object;
        theLatch.countDown();
    }

    public void completeExceptionally(Throwable throwable) {
//        logger.warn("Completing exceptionally: {}", throwable.getMessage());
        state = State.EXCEPTION;
        theThrowable = throwable;
        theLatch.countDown();
    }

}
