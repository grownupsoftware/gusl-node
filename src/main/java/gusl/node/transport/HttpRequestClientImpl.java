package gusl.node.transport;

import com.codahale.metrics.Gauge;
import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.annotations.FixMe;
import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorsDTO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.NamedFixedPoolExecutor;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.metrics.MetricsFactory;
import gusl.core.utils.StringUtils;
import gusl.core.utils.Utils;
import gusl.model.nodeconfig.HttpClientConfig;
import gusl.model.nodeconfig.NodeType;
import gusl.node.errors.NodeErrors;
import gusl.node.jersey.JerseyHttpConfig;
import gusl.node.jersey.JerseyNodeClient;
import gusl.router.model.HttpNodeRequest;
import lombok.CustomLog;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.core.Response;
import java.io.Closeable;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static gusl.node.jersey.JerseyHttpConfig.*;
import static gusl.node.transport.HttpUtils.BODY_CONTENT_CLASS_HEADER_NAME;
import static java.util.Objects.nonNull;

/**
 * @author grant
 */
@CustomLog
public class HttpRequestClientImpl implements HttpRequestClient, Closeable {

    private JerseyNodeClient httpClient;

    private final ClassTypeMapper theClassMapper;

    private String serverUrl;
    private final ObjectMapper objectMapper = ObjectMapperFactory.getDefaultObjectMapper();
    private boolean configured;

    public HttpRequestClientImpl() {
        theClassMapper = new ClassTypeMapper();
    }

    @Override
    public void close() throws IOException {
        destroy();
    }

    /**
     * Destroying the request client.
     */
    @Override
    public synchronized void destroy() {
        if (httpClient != null) {
            logger.debug("Destroying request client ...");
            httpClient.close();
            httpClient = null;
            logger.debug("Destroyed request client.");
        }
    }

    public <RESPONSE> CompletableFuture<RESPONSE> sendRequest(HttpNodeRequest<RESPONSE> nodeRequest) {
        logger.debug("Sending request [{}] to {}", nodeRequest.getPayload() == null ? "no payload" : nodeRequest.getPayload().getClass().getSimpleName(), nodeRequest.getPath());

        CompletableFuture<RESPONSE> future = new CompletableFuture<>();
        if (!configured) {
            future.completeExceptionally(new GUSLErrorException(NodeErrors.HTTP_SERVICE_NOT_CONFIGURED.getError()));
        }

        if (nonNull(httpClient)) {
            Invocation.Builder invocationBuilder = httpClient.getJsonBuilder(nodeRequest.getUrl());

            final Object payload = nodeRequest.getPayload();
            if (payload != null) {
                invocationBuilder.header(BODY_CONTENT_CLASS_HEADER_NAME, payload.getClass().getName());
            }

            final Map<String, String> headers = nodeRequest.getHeaders();
            if (headers != null) {
                headers.forEach((s, s2) -> invocationBuilder.header(s, s2));
            }

            invocationBuilder.async().post(Entity.json(payload), new InvocationCallback<Response>() {
                @Override
                public void completed(Response response) {
                    try (response) {
                        handleResponse(nodeRequest, response, future, nodeRequest.getResponseClass());
                    }
                }

                @Override
                public void failed(Throwable throwable) {
                    StringBuilder error = new StringBuilder(nodeRequest.getUrl());
                    error.append(" ");
                    error.append(nodeRequest.getNodeRequestDetails());
                    error.append(" [");
                    if (payload == null) {
                        error.append("No Payload");
                    } else {
                        error.append(StringUtils.elipseString(1000, payload.toString()));
                    }
                    error.append("]");

                    logger.warn("Sending of request {}:{} failed.", error, throwable);
                    future.completeExceptionally(throwable);
                }
            });

        } else {
            future.completeExceptionally(new GUSLErrorException(NodeErrors.HTTP_SERVICE_NOT_CONFIGURED.getError()));
        }
        return future;
    }

    @Override
    public JerseyNodeClient getHttpClient() {
        return httpClient;
    }

    /**
     * Handle the response.
     *
     * @param httpResponse
     * @param future
     */
    @SuppressWarnings("unchecked")
    @FixMe(createdBy = "dhudson", description = "Its needs a major refactor, the fact that Exceptions can't be serialised is a killer")
    private <RESPONSE> void handleResponse(HttpNodeRequest<RESPONSE> nodeRequest, Response httpResponse, CompletableFuture<RESPONSE> future, Class<?> expectedResultPayload) {
        Class<? extends GUSLErrorException> errorClass = null;
        String responseTypeName = httpResponse.getHeaderString(BODY_CONTENT_CLASS_HEADER_NAME);
        int status = httpResponse.getStatus();

        try {
            logger.debug("----> Request {}, Http Response - status code: {}  expected payload: {} recv class header: {}", nodeRequest.getUrl(), status, expectedResultPayload, responseTypeName);

            // unknown expected result or there is an error
            if (expectedResultPayload == null || status != 200) {
                if (responseTypeName == null) {
                    if (status == 200) {
                        // ok - so no payload is expected, nor is there a return class, so complete the future
                        future.complete(null);
                        // future.completeExceptionally(new GUSLErrorException(SystemErrors.ERR_SERVER_ERROR.getError("Invalid response. The response does not contain a X-Body-Type header.")));
                    } else {
                        logger.warn("Request failed {} {}, Http Response - status code: {}  expected payload: {}", nodeRequest.getUrl(), nodeRequest.getPayload(), status, expectedResultPayload);
                        future.completeExceptionally(new GUSLErrorException(NodeErrors.NON_200_HTTP_RESPONSE.getError(MessageFormat.format("Error {0}. {1}", status, nodeRequest.getUrl()))));
                    }
                    return;
                }

                if (status != 200) {
                    errorClass = (Class<? extends GUSLErrorException>) theClassMapper.getType(responseTypeName);
                }
            } else {
                if (expectedResultPayload.getName().equals("javax.ws.rs.core.Response")) {
                    // Its a raw request / response
                    future.complete((RESPONSE) httpResponse);
                    return;
                }
            }

            if (errorClass != null) {
                // we have an error
                ErrorsDTO errors = HttpUtils.parseResponse(objectMapper, httpResponse, ErrorsDTO.class);
                List<ErrorDO> listOfErrors = Utils.safeStream(errors.getErrors()).map(errorDTO -> {
                    ErrorDO errorDo = new ErrorDO(errorDTO.getField(), errorDTO.getMessage(), errorDTO.getMessageKey(), errorDTO.getParameters());
                    return errorDo;
                }).collect(Collectors.toList());

                GUSLErrorException error;
                // Create correct subclass of GUSLErrorException
                if (GUSLErrorException.class.isAssignableFrom(errorClass) && "gusl.core.exceptions.GUSLErrorException".equals(errorClass.getCanonicalName())) {
                    error = new GUSLErrorException(listOfErrors);
                } else if (GUSLErrorException.class.isAssignableFrom(errorClass)) {
                    try {
                        error = errorClass.getDeclaredConstructor().newInstance();
                        error.setErrors(listOfErrors);
                    } catch (IllegalAccessException | InstantiationException | AbstractMethodError ex) {
                        logger.warn("Unable to create {}", errorClass.getName(), ex);
                        error = new GUSLErrorException(listOfErrors);
                    }

                } else {
                    error = new GUSLErrorException(listOfErrors);
                }
                // There is no point having this in the stack traceØ
                StackTraceElement[] stack = new StackTraceElement[0];
                error.setStackTrace(stack);
                // error.setErrors(listOfErrors);
                future.completeExceptionally(error);
            } else {
                future.complete(HttpUtils.parseResponse(objectMapper, httpResponse, nodeRequest.getResponseClass(), nodeRequest.getGenericHint()));
            }

        } catch (ClassNotFoundException e) {
            logger.error("Invalid response. No response class named [{}] exists for request {}", responseTypeName, nodeRequest.getUrl(), e);
            future.completeExceptionally(new GUSLErrorException(NodeErrors.CLASS_NOT_FOUND.getError(new String[]{responseTypeName}), e));
        } catch (Throwable e) {
            logger.error("Error processing response from request of {} with a status of {}", nodeRequest.getUrl(), status, e);
            future.completeExceptionally(new GUSLErrorException(NodeErrors.UNEXPECTED_ERROR.getError(new String[]{e.getMessage()}), e));
        }
    }

    public void configure(NodeType nodeType, String serverUrl, HttpClientConfig config) {
        if (configured) {
            return;
        }

        if (nodeType == null) {
            nodeType = NodeType.FIXER;
        }

        String clientName = nodeType.name().toLowerCase() + ".router.client";

        JerseyHttpConfig.JerseyHttpConfigBuilder builder = JerseyHttpConfig.builder();

        int ioThreads;

        this.serverUrl = serverUrl;
        if (config != null) {
            builder.maxConnections(config.getMaxConnections()).readTimeout((int) config.getSocketTimeout()).connectionTimeout((int) config.getConnectTimeout());
            ioThreads = config.getIoThreads();
        } else {
            logger.warn("Configure called with null config, using defaults");
            builder.maxConnections(MAX_CONNECTIONS_DEFAULT).readTimeout(READ_TIMEOUT_DEFAULT).connectionTimeout(CONNECT_TIMEOUT_DEFAULT);
            ioThreads = IO_THREADS_DEFAULT;
        }

        builder.debugging(config.isDebug());

        NamedFixedPoolExecutor executor = new NamedFixedPoolExecutor(ioThreads, clientName);
        executor.setRejectedExecutionHandler((r, executor1) -> logger.warn("Rejected runnable {}", r));

        JerseyHttpConfig jerseyHttpConfig = builder.properties(JerseyHttpConfig.defaultClientProperties()).objectMapper(ObjectMapperFactory.getDefaultObjectMapper())
                .executorService(executor).name(clientName).requiresConnectionPool(true).build();

        httpClient = new JerseyNodeClient(jerseyHttpConfig);
        configured = true;

        MetricsFactory.registerWithoutClass(clientName + ".thread.queue.size", (Gauge<Integer>) () -> executor.getQueue().size());
        MetricsFactory.registerWithoutClass(clientName + ".thread.pool.size", (Gauge<Integer>) () -> executor.getPoolSize());
        MetricsFactory.registerWithoutClass(clientName + ".thread.active.count", (Gauge<Integer>) () -> executor.getActiveCount());
        MetricsFactory.registerWithoutClass(clientName + ".thread.executed", (Gauge<Long>) () -> executor.getCompletedTaskCount());

        logger.info("Initialised request client. {}", jerseyHttpConfig);
    }

    public String getServerURL() {
        return serverUrl;
    }

    @Override
    public boolean isConfigured() {
        return configured;
    }

}
