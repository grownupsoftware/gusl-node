package gusl.node.executors;

import com.codahale.metrics.Timer;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import gusl.core.executors.ExecutorState;
import static gusl.core.executors.ExecutorState.STOP;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;

/**
 * Worker thread for the Keyed Executor.
 * <p>
 * This class opportunistically extends AbstractQueuedSynchronizer to simplify
 * acquiring and releasing a lock surrounding each task execution. This protects
 * against interrupts that are intended to wake up a worker thread waiting for a
 * task from instead interrupting a task being run.</p>
 *
 * @author dhudson
 */
public class KeyedQueueWorker extends AbstractQueuedSynchronizer implements Runnable {

    private final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    private static final long serialVersionUID = 6138294804551838833L;

    /**
     * Thread this worker is running in.
     */
    private final Thread theThread;

    // Flag to state that the executor wants to shut down
    private volatile boolean isClosing;

    // Flag to indicate if the thread is running
    private boolean isRunning;

    private final ExecutorState theState;
    private final EventBusQueue theQueue;
    private final Timer theExecutionTimer;

    /**
     * Creates with given first task and thread from ThreadFactory.
     *
     * @param firstTask the first task (null if none)
     */
    KeyedQueueWorker(Timer executionTimer, ExecutorState state, EventBusQueue queue, String name, int priority) {
        // inhibit interrupts until run
        setState(-1);

        theExecutionTimer = executionTimer;
        theState = state;
        theQueue = queue;

        // Create a thread
        theThread = new Thread(this);
        theThread.setName(name);
        theThread.setPriority(priority);
    }

    /**
     * Repeatedly gets tasks from queue and executes them.
     *
     * If getTask returns null, then exit from the loop.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        logger.debug("Starting ... {} ", getName());
        isRunning = true;
        StagedQueuedEntry entry;

        // allow interrupts
        unlock();

        while ((entry = getTask()) != null) {
            lock();

            try {
                // If pool is stopping, ensure thread is interrupted;
                // if not, ensure thread is not interrupted.  This
                // requires a recheck in second case to deal with
                // shutdownNow race while clearing interrupt
                if ((theState.runStateAtLeast(STOP)
                        || (Thread.interrupted()
                        && theState.runStateAtLeast(STOP)))
                        && !theThread.isInterrupted()) {
                    theThread.interrupt();
                }

                KeyedExecutorFuture future = entry.getFuture();
                final Timer.Context context = theExecutionTimer.time();
                final long startTime = System.currentTimeMillis();

                try {
                    if (future != null) {
                        // Don't execute a cancelled future
                        if (!future.isCancelled()) {
                            future.complete(entry.getEntry().call());
                        }
                    } else {
                        // Ignore the result
                        entry.getEntry().call();
                    }
                } catch (Throwable ex) {

                    if (future != null) {
                        future.completeExceptionally(ex);
                    } else {
                        // Lets log this
                        logger.warn("Keyed Queued Executor caught", ex);
                    }

                    // Break out of the run loop
                    if (isClosing) {
                        logger.debug("Stopping ... {} ", getName());
                        isRunning = false;
                        return;
                    }

                } finally {
                    // Stop timing
                    context.close();
                    // This probably is not required, but we are seeing some odd behaviour
                    // on the edge so lets see if something is waiting for locks.
                    if ((System.currentTimeMillis() - startTime) > 1000) {
                        String className = entry.getEntry().getClass().getCanonicalName();
                        if (className == null) {
                            className = entry.getEntry().getClass().getName();
                        }
                        logger.warn("{} has a long running invocation with key of {} time of {}ms - class: {} ",
                                theThread.getName(), entry.getKey(), (System.currentTimeMillis() - startTime), className);
                    }
                    entry.resetIsRunning();
                }
            } finally {
                unlock();
            }
        }

        isRunning = false;
        logger.debug("Stopping ... {} ", getName());
    }

    /**
     * Performs blocking timed wait for a task.
     *
     * @return task, or null if the worker must exit.
     */
    private StagedQueuedEntry getTask() {

        while (true) {
            try {
                // Check if queue empty only if necessary.
                if (theState.isShutdown() && theQueue.isEmpty()) {
                    isClosing = true;
                    return null;
                }

                // Wait for the queue to assign work
                return theQueue.take();
            } catch (InterruptedException ex) {
                // Been woken up by a change of state
            }
        }
    }

    // Lock methods
    //
    // The value 0 represents the unlocked state.
    // The value 1 represents the locked state.
    @Override
    protected boolean isHeldExclusively() {
        return getState() != 0;
    }

    @Override
    protected boolean tryAcquire(int unused) {
        if (compareAndSetState(0, 1)) {
            setExclusiveOwnerThread(Thread.currentThread());
            return true;
        }
        return false;
    }

    @Override
    protected boolean tryRelease(int unused) {
        setExclusiveOwnerThread(null);
        setState(0);
        return true;
    }

    void lock() {
        acquire(1);
    }

    boolean tryLock() {
        return tryAcquire(1);
    }

    void unlock() {
        release(1);
    }

    boolean isLocked() {
        return isHeldExclusively();
    }

    void interruptIfStarted() {
        isClosing = true;
        Thread t;
        if (getState() >= 0 && (t = theThread) != null && !t.isInterrupted()) {
            try {
                t.interrupt();
            } catch (SecurityException ignore) {
            }
        }
    }

    Thread getThread() {
        return theThread;
    }

    boolean isRunning() {
        return isRunning;
    }

    String getName() {
        return theThread.getName();
    }

    boolean isClosing() {
        return isClosing;
    }

}
