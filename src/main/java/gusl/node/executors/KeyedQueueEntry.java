package gusl.node.executors;

import java.util.concurrent.Callable;

/**
 * Queue Entries must implement this.
 *
 * @author dhudson
 * @param <T> return type of the callable
 */
public interface KeyedQueueEntry<T> extends Callable<T> {

    /**
     * Processing Key
     *
     * @return key
     */
    public long getKey();

    /**
     * There may be cases when you still wish to retain the lock on the key
     * until some future time, if this is the case then this should return
     * false.
     *
     * @return true
     */
    public default boolean releaseWhenComplete() {
        return true;
    }
}
