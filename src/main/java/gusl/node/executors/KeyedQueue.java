package gusl.node.executors;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Timer;
import gusl.core.metrics.MetricsFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Queue which handles the processing of items in order, but only one
 * <code>Key</code> at a time.
 * <p>
 * take is the interesting method, as it will only take an entry from the queue
 * if an existing entry is not running.
 *
 * @author dhudson
 */
class EventBusQueue extends LinkedBlockingQueue<StagedQueuedEntry> {

    private static final long serialVersionUID = 8174900193073830878L;
    private final Set<Long> theRunningKeys;

    private final ReentrantLock theLock;
    private final Object sync = new Object();
    private volatile boolean isWaiting;

    // Time how long things are on the queue
    private final Timer theQueueTimer;

    private final List<Long> theRejectedKeys = new ArrayList<>();
    private final String theExecutorName;

    private boolean isConflating = false;

    EventBusQueue(String executorName) {
        this(executorName, Integer.MAX_VALUE, true);
    }

    EventBusQueue(String executorName, int capacity) {
        this(executorName, capacity, true);
    }

    /**
     * Create a new keyed queue with the given fixed capacity and whether
     * locking is fair or not.
     *
     * @param executorName
     * @param capacity     Fixed queue capacity.
     * @param useFairLock  If true all threads get a fair chance to take from the
     *                     queue, if false take will be faster but not fair meaning some threads
     *                     will wait longer than others.
     */
    EventBusQueue(String executorName, int capacity, boolean useFairLock) {
        super(capacity);
        theExecutorName = executorName;
        theLock = new ReentrantLock(useFairLock);
        theRunningKeys = Collections.newSetFromMap(new ConcurrentHashMap<>());
        String metricName = "gusl.executors.KeyedQueueExecutor." + executorName + ".queue.size";

        // Because we are registering directly, make sure there isn't already one
        MetricsFactory.getRegistry().remove(metricName);
        MetricsFactory.getRegistry().register(metricName, new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return size();
            }
        });

        // Add metric for the running keys
        metricName = "gusl.executors.KeyedQueueExecutor." + executorName + ".running.keys.size";
        MetricsFactory.getRegistry().remove(metricName);
        MetricsFactory.getRegistry().register(metricName, new Gauge<Integer>() {
            @Override
            public Integer getValue() {
                return theRunningKeys.size();
            }
        });

        theQueueTimer = MetricsFactory.getTimer(EventBusQueue.class, executorName + ".queue.latancy");
    }

    @Override
    public boolean offer(StagedQueuedEntry e) {
        e.setTimerContext(theQueueTimer.time());
        boolean result;

        if (isConflating) {
            Iterator<StagedQueuedEntry> iterator = iterator();
            StagedQueuedEntry entry;
            while (iterator.hasNext()) {
                entry = iterator.next();
                if (entry.getKey() == e.getKey()) {
                    iterator.remove();
                }
            }
        }

        result = super.offer(e);

        if (isWaiting) {
            synchronized (sync) {
                sync.notify();
            }
        }
        return result;
    }

    public boolean isConflating() {
        return isConflating;
    }

    public void setConflating(boolean value) {
        isConflating = value;
    }

    /**
     * Take in order and await forever ..
     *
     * @return
     */
    @Override
    public StagedQueuedEntry take() throws InterruptedException {
        try {
            // At this point in time, only one thread will access this
            theLock.lock();
            while (true) {

                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }

                // Are we empty
                if (isEmpty()) {
                    isWaiting = true;
                    synchronized (sync) {
                        sync.wait(5l);
                    }
                    isWaiting = false;
                    continue;
                }

                // List in order
                Iterator<StagedQueuedEntry> iterator = iterator();

                StagedQueuedEntry entry;
                theRejectedKeys.clear();

                while (iterator.hasNext()) {
                    entry = iterator.next();
                    long key = entry.getKey();

                    // Check to see if there has been an interruption
                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }

                    if (theRejectedKeys.contains(key)) {
                        // We have already gone past this one, so we will have to wait until we start the list again
                        continue;
                    }

                    if (theRunningKeys.contains(key)) {
                        // Thread alreay running with this key
                        theRejectedKeys.add(key);
                        continue;
                    }

                    theRunningKeys.add(key);
                    iterator.remove();

                    // Stop the clock of how long we have been waiting
                    entry.updateTimer();
                    return entry;
                }
            }

        } finally {
            theLock.unlock();
        }
    }

    /**
     * Needs to be called after execution, to allow for future executions of
     * this key.
     *
     * @param key to reset
     */
    void resetRunning(long key) {
        theRunningKeys.remove(key);
    }

    /**
     * Check to see if there are any more entries for this key.
     *
     * @param key
     * @return
     */
    boolean hasMore(long key) {
        return stream().anyMatch(k -> k.getKey() == key);
    }

    /**
     * Remove all entries for this key.
     * <p>
     * This will remove all of the entries for this key.
     *
     * @param key
     */
    void removeEntriesFor(long key) {
        Iterator<StagedQueuedEntry> iterator = iterator();

        StagedQueuedEntry entry;
        InterruptedException iEx = new InterruptedException(theExecutorName + " removeEntries");

        while (iterator.hasNext()) {
            entry = iterator.next();
            if (entry.getKey() == key) {
                if (entry.hasFuture()) {
                    entry.getFuture().completeExceptionally(iEx);
                }
                iterator.remove();
            }
        }

        //resetRunning(key);
    }
}
