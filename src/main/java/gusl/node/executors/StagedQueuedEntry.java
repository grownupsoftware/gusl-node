package gusl.node.executors;

import com.codahale.metrics.Timer;

/**
 * Once a KeyedEntry has been added, a Staged Entry is created.
 *
 * As with these type of things, the class is immutable.
 *
 * @author dhudson
 */
public class StagedQueuedEntry {

    private final KeyedQueueEntry theEntry;
    private final KeyedExecutorFuture<?> theFuture;
    private final EventBusQueue theQueue;
    private final long theKey;

    private Timer.Context theTimerContext;

    StagedQueuedEntry(EventBusQueue queue, KeyedQueueEntry entry) {
        this(queue, entry, null);
    }

    StagedQueuedEntry(EventBusQueue queue, KeyedQueueEntry entry, KeyedExecutorFuture<?> future) {
        theQueue = queue;
        theEntry = entry;
        theFuture = future;
        // Take a copy of the key so that it can't change when on the queue
        theKey = entry.getKey();
    }

    long getKey() {
        return theKey;
    }

    /**
     * Do we have a future, no, is it a low life scum!
     *
     * @return true if there is a future
     */
    boolean hasFuture() {
        return theFuture != null;
    }

    public KeyedQueueEntry getEntry() {
        return theEntry;
    }

    /**
     * Return the future, which could be null
     *
     * @return a future
     */
    public KeyedExecutorFuture<?> getFuture() {
        return theFuture;
    }

    void resetIsRunning() {
        if (theEntry.releaseWhenComplete()) {
            theQueue.resetRunning(theKey);
        }
    }

    void setTimerContext(Timer.Context context) {
        theTimerContext = context;
    }

    void updateTimer() {
        theTimerContext.stop();
    }
}
