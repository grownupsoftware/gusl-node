package gusl.node.executors;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Timer;
import gusl.core.executors.ExecutorState;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.MetricsFactory;
import gusl.core.utils.Platform;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static gusl.core.executors.ExecutorState.*;

/**
 * Keyed Executor.
 * <p>
 * Based off a Thread Pool Executor, but with some differences.
 * <p>
 * First off, there is a fixed thread pool size, which will never change in
 * size.
 * <p>
 * Submissions are executed one at a time in KeyedQueueEntry.getKey() order
 *
 * @author dhudson
 */
public class KeyedQueueExecutor {

    private final GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    private final EventBusQueue theQueue;
    private final ExecutorState theState;

    /**
     * Permission required for callers of shutdown and shutdownNow. We
     * additionally require (see checkShutdownAccess) that callers have
     * permission to actually interrupt threads in the worker set (as governed
     * by Thread.interrupt, which relies on ThreadGroup.checkAccess, which in
     * turn relies on SecurityManager.checkAccess). Shutdowns are attempted only
     * if these checks pass.
     * <p>
     * All actual invocations of Thread.interrupt (see interruptIdleWorkers and
     * interruptWorkers) ignore SecurityExceptions, meaning that the attempted
     * interrupts silently fail. In the case of shutdown, they should not fail
     * unless the SecurityManager has inconsistent policies, sometimes allowing
     * access to a thread and sometimes not. In such cases, failure to actually
     * interrupt threads may disable or delay full termination. Other uses of
     * interruptIdleWorkers are advisory, and failure to actually interrupt will
     * merely delay response to configuration changes so is not handled
     * exceptionally.
     */
    private static final RuntimePermission shutdownPerm = new RuntimePermission("modifyThread");

    /**
     * Set containing all worker threads in pool.
     */
    private final KeyedQueueWorker[] theWorkers;

    private final int theThreadPoolSize;
    private final int theThreadPriority;
    private final String theExecutorName;
    private final Meter theSubmitMeter;
    private final Meter theExecuteMeter;
    private final Counter theRequestCounter;

    /**
     * Create an executor with the given name, a thread priority of Normal and
     * the pool size of the number of cores on the system.
     *
     * @param name of the executor
     */
    public KeyedQueueExecutor(String name) {
        this(name, Thread.NORM_PRIORITY, Platform.getNumberOfCores(), Integer.MAX_VALUE, true);
    }

    /**
     * Create an executor with the given name, and pool size. The thread
     * priority will be normal.
     *
     * @param name     of the executor
     * @param poolSize of the thread pool
     */
    public KeyedQueueExecutor(String name, int poolSize) {
        this(name, Thread.NORM_PRIORITY, poolSize, Integer.MAX_VALUE, true);
    }

    public KeyedQueueExecutor(String name, int priority, int poolSize) {
        this(name, priority, poolSize, Integer.MAX_VALUE, true);
    }

    public KeyedQueueExecutor(String name, int priority, int poolSize, int queueCapacity) {
        this(name, priority, poolSize, queueCapacity, true);
    }

    /**
     * Create an executor with the given name.
     * <p>
     * The threads will be given the allotted priority and a fixed pool size.
     *
     * @param name          executor name
     * @param priority      of the threads
     * @param poolSize      number of threads in the pool
     * @param queueCapacity fixed capacity of queue
     * @param useFairLock   if threads use fair locking when taking from the
     *                      queue. Unfair is faster but some threads will wait longer than others.
     */
    public KeyedQueueExecutor(String name, int priority, int poolSize, int queueCapacity, boolean useFairLock) {
        // Validate
        if (name == null) {
            throw new NullPointerException("Invalid Executor Name");
        }

        if (priority < Thread.MIN_PRIORITY || priority > Thread.MAX_PRIORITY) {
            throw new IllegalArgumentException("Invalid Thread Priority");
        }

        // Comes from a config
        if (poolSize == -1) {
            poolSize = Platform.getNumberOfCores();
        }

        if (poolSize < 1 || poolSize > 32) {
            throw new IllegalArgumentException("Invalid Pool Size [1-32] {" + poolSize + "}");
        }

        theExecutorName = name;
        theThreadPriority = priority;
        theThreadPoolSize = poolSize;
        theWorkers = new KeyedQueueWorker[poolSize];
        theState = new ExecutorState();
        theQueue = new EventBusQueue(name);

        theSubmitMeter = MetricsFactory.getMeter(KeyedQueueExecutor.class, name + ".submit.requests");
        theExecuteMeter = MetricsFactory.getMeter(KeyedQueueExecutor.class, name + ".execute.requests");
        theRequestCounter = MetricsFactory.getCounter(KeyedQueueExecutor.class, name + ".requests");

        prestartThreads();
    }

    public void setConflating() {
        theQueue.setConflating(true);
    }

    /**
     * Starts all threads, causing them to idly wait for work.
     */
    private void prestartThreads() {

        Timer executionTimer = MetricsFactory.getTimer(getClass(), theExecutorName + ".execution");

        for (int i = 0; i < theThreadPoolSize; i++) {

            String workerName = theExecutorName + " worker " + i;
            KeyedQueueWorker worker = new KeyedQueueWorker(executionTimer, theState, theQueue,
                    workerName, theThreadPriority);

            final Thread t = worker.getThread();
            if (t != null) {
                theWorkers[i] = worker;
                t.start();
            } else {
                logger.error("Unable to create thread");
            }
        }
    }

    /**
     * Submit an entry for execution.
     * <p>
     * <b>As this method returns no future, then there is no way of telling if
     * this task ever got executed</b></p>
     *
     * @param entry for execution
     */
    public void execute(KeyedQueueEntry entry) {
        checkSubmission(entry);
        theQueue.offer(new StagedQueuedEntry(theQueue, entry));
        theExecuteMeter.mark();
        theRequestCounter.inc();
    }

    /**
     * Submit a entry for execution with a future.
     *
     * @param <V>   result of callable
     * @param entry to process
     * @return a newly created future
     */
    public <V> KeyedExecutorFuture<V> submit(KeyedQueueEntry<V> entry) {
        return internalSubmit(entry, true);
    }

    private <V> KeyedExecutorFuture<V> internalSubmit(KeyedQueueEntry<V> entry, boolean offer) {
        checkSubmission(entry);
        KeyedExecutorFuture<V> future = new KeyedExecutorFuture<>(entry);
        if (offer) {
            StagedQueuedEntry stagedEntry = new StagedQueuedEntry(theQueue, entry, future);
            theQueue.offer(stagedEntry);
        }
        theSubmitMeter.mark();
        theRequestCounter.inc();
        return future;
    }

    private void checkSubmission(KeyedQueueEntry entry) {
        if (entry == null) {
            throw new NullPointerException();
        }

        if (!theState.isRunning()) {
            throw new IllegalStateException("Executor not running");
        }
    }

    /**
     * Initiates an orderly shutdown in which previously submitted tasks are
     * executed, but no new tasks will be accepted.
     *
     * <p>
     * Invocation has no additional effect if already shut down.
     * </p>
     *
     * <p>
     * This method does not wait for previously submitted tasks to complete
     * execution. Use {@link #awaitTermination awaitTermination} to do that.
     * </p>
     *
     * @throws SecurityException {@inheritDoc}
     */
    public synchronized void shutdown() {
        if (theState.isRunning()) {
            checkShutdownAccess();
            theState.set(SHUTDOWN);
            interruptIdleWorkers();
            tryTerminate();
        }
    }

    /**
     * Attempts to stop all actively executing tasks, halts the processing of
     * waiting tasks, and returns a list of the tasks that were awaiting
     * execution.
     *
     * <p>
     * These tasks are drained (removed) from the task queue upon return from
     * this method.</p>
     *
     * <p>
     * This method does not wait for actively executing tasks to terminate. Use
     * {@link #awaitTermination awaitTermination} to do that.
     * </p>
     *
     * <p>
     * There are no guarantees beyond best-effort attempts to stop processing
     * actively executing tasks. This implementation cancels tasks via
     * {@link Thread#interrupt}, so any task that fails to respond to interrupts
     * may never terminate.
     * </p>
     *
     * @return list of Queued Entries, or empty list if already shutdown, or
     * nothing on the queue
     * @throws SecurityException {@inheritDoc}
     */
    public synchronized List<StagedQueuedEntry> shutdownNow() {
        if (theState.isShutdown()) {
            return Collections.<StagedQueuedEntry>emptyList();
        }

        checkShutdownAccess();

        theState.set(STOP);
        interruptWorkers();
        List<StagedQueuedEntry> tasks = drainQueue();
        tryTerminate();
        return tasks;
    }

    /**
     * Shutdown and cancel all futures.
     * <p>
     * This calls <code>shutdownNow</code> and will set an
     * <code>InterruptedException</code> on all available futures.</p>
     */
    public synchronized void shutdownNowAndCancel() {
        List<StagedQueuedEntry> tasks = shutdownNow();

        InterruptedException iEx = new InterruptedException(theExecutorName + " shutdown");

        for (StagedQueuedEntry entry : tasks) {
            if (entry.hasFuture()) {
                entry.getFuture().completeExceptionally(iEx);
            }
        }
    }

    /**
     * Block, waiting for the shutdown process to happen.
     *
     * @param timeout time to wait
     * @param unit    unit of time
     * @return true if shutdown successful
     * @throws InterruptedException
     */
    @SuppressWarnings("CallToThreadYield")
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        long nanos = unit.toNanos(timeout);
        long endTime = System.nanoTime() + nanos;

        while (!theState.isTerminated()) {

            if (Thread.interrupted()) {
                throw new InterruptedException();
            }

            if (theQueue.isEmpty() && !hasRunningWorkers()) {
                theState.set(TERMINATED);
                break;
            } else {
                // If the queue is not empty, then there is still work to do
                if (theQueue.isEmpty()) {
                    if (theState.getState() == STOP) {
                        interruptWorkers();
                    } else {
                        interruptIdleWorkers();
                    }
                }
                // Don't chew the CPU
                Thread.yield();
            }

            if (System.nanoTime() > endTime) {
                // Timeout
                return false;
            }
        }

        return true;
    }

    public boolean isShutdown() {
        return !theState.isRunning();
    }

    /**
     * If there is a security manager, makes sure caller has permission to shut
     * down threads in general (see shutdownPerm). If this passes, additionally
     * makes sure the caller is allowed to interrupt each worker thread. This
     * might not be true even if first check passed, if the SecurityManager
     * treats some threads specially.
     */
    private void checkShutdownAccess() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(shutdownPerm);
            for (KeyedQueueWorker w : theWorkers) {
                security.checkAccess(w.getThread());
            }

        }
    }

    /**
     * Drains the task queue into a new list, normally using drainTo. But if the
     * queue is a DelayQueue or any other kind of queue for which poll or
     * drainTo may fail to remove some elements, it deletes them one by one.
     */
    private List<StagedQueuedEntry> drainQueue() {
        ArrayList<StagedQueuedEntry> taskList = new ArrayList<>();
        theQueue.drainTo(taskList);
        return taskList;
    }

    /**
     * Interrupts all threads, even if active.
     * <p>
     * This sets the cancel flag, to stop the threads running.
     * <p>
     * Ignores SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     */
    private void interruptWorkers() {
        for (KeyedQueueWorker w : theWorkers) {
            w.interruptIfStarted();
        }
    }

    /**
     * Transitions to TERMINATED state if either (SHUTDOWN and pool and queue
     * empty) or (STOP and pool empty).
     */
    private void tryTerminate() {

        // Stuff still running..
        if (hasRunningWorkers()) {
            return;
        }

        if (theState.isShutdown() && theQueue.isEmpty()) {
            theState.set(TERMINATED);
        }

    }

    private boolean hasRunningWorkers() {
        for (KeyedQueueWorker worker : theWorkers) {
            if (worker.isRunning()) {
                return true;
            }
        }

        return false;
    }

//    /**
//     * Invokes {@code shutdown} when this executor is no longer referenced and
//     * it has no threads.
//     */
//    @Override
//    protected void finalize() {
//        try {
//            shutdownNowAndCancel();
//        } finally {
//            try {
//                super.finalize();
//            } catch (Throwable ex) {
//                // All a bit late now
//            }
//        }
//    }

    /**
     * Interrupts threads that might be waiting for tasks (as indicated by not
     * being locked) so they can check for termination or configuration changes.
     * Ignores SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     */
    private void interruptIdleWorkers() {
        for (KeyedQueueWorker worker : theWorkers) {
            if (worker.isRunning()) {
                Thread t = worker.getThread();
                if (!t.isInterrupted() && worker.tryLock()) {
                    try {
                        t.interrupt();
                    } catch (SecurityException ignore) {
                    } finally {
                        worker.unlock();
                    }

                }
            }
        }
    }

    /**
     * Return the number of work threads
     *
     * @return number of worker threads
     */
    public int getThreadPoolSize() {
        return theThreadPoolSize;
    }

    /**
     * Return the thread priority
     *
     * @return
     */
    public int getThreadPriority() {
        return theThreadPriority;
    }

    @Override
    public String toString() {
        return "KeyedQueueExecutor{" + "Pool Size=" + theThreadPoolSize + ", Thread Priority=" + theThreadPriority + ", Executor Name=" + theExecutorName + '}';
    }

    public int getQueueSize() {
        return theQueue.size();
    }

    /**
     * Check the queue and see if there are any more entries for this key.
     *
     * @param key to check
     * @return true if there are more entries. It is possible that this may
     * return a false positive as the entry may have been processed during the
     * time of this execution.
     */
    public boolean hasMoreEntriesFor(long key) {
        return theQueue.hasMore(key);
    }

    /**
     * If releaseWhenComplete on the KeyedQueueEntry is false, then is down to
     * the implementation to call this at the end of execution.
     *
     * @param key
     */
    public void resetRunning(long key) {
        theQueue.resetRunning(key);
    }

    /**
     * Remove any remaining task for this key.
     *
     * @param key
     */
    public void removeEntriesFor(long key) {
        theQueue.removeEntriesFor(key);
    }

}
