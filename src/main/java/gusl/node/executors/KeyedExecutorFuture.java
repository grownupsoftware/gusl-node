package gusl.node.executors;

import java.util.concurrent.CompletableFuture;

/**
 * Keyed Executor Future.
 *
 * @author dhudson
 * @param <T> result
 */
public class KeyedExecutorFuture<T> extends CompletableFuture<T> {

    private final KeyedQueueEntry<T> theEntry;

    public KeyedExecutorFuture(KeyedQueueEntry<T> entry) {
        theEntry = entry;
    }

    /**
     * Return the entry for this future.
     *
     * @return entry
     */
    public KeyedQueueEntry<T> getEntry() {
        return theEntry;
    }
}
