package gusl.node.executors;

/**
 * @author dhudson on 27/08/2020
 */
public abstract class StringKeyedQueueEntry<T> implements KeyedQueueEntry<T> {

    private final long longKey;

    public StringKeyedQueueEntry(String key) {
        longKey = key.hashCode();
    }

    @Override
    public final long getKey() {
        return longKey;
    }

}
