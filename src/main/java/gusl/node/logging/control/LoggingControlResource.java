package gusl.node.logging.control;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.log.LoggerDTO;
import gusl.model.log.LoggersDTO;
import gusl.node.resources.AbstractBaseResource;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * @author dhudson on 03/10/2019
 */
@Singleton
@Path("/logControl")
@PermitAll
public class LoggingControlResource extends AbstractBaseResource {

    @Inject
    private LoggingControl theService;

    @POST()
    @Path("getAll")
    public LoggersDTO getLoggers() {
        return theService.getLoggers();
    }

    @POST
    @Path("filtered/{filter}")
    public LoggersDTO getLoggers(@PathParam("filter") String filter) throws GUSLErrorException {
        return theService.getLoggersWithPackageFilter(filter);
    }

    @POST
    @Path("change")
    public LoggerDTO change(LoggerDTO loggerDTO) {
        return theService.changeLogger(loggerDTO);
    }

    @POST
    @Path("changeAll")
    public LoggerDTO changeAll(LoggerDTO loggerDTO) {
        return theService.changeAll(loggerDTO);
    }

    @POST
    @Path("toggle")
    public LoggerDTO toggle(LoggerDTO loggerDTO) {
        return theService.toggle(loggerDTO);
    }
}
