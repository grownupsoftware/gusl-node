package gusl.node.logging.control;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.SystemPropertyUtils;
import gusl.model.log.LoggerDTO;
import gusl.model.log.LoggerEvent;
import gusl.model.log.LoggersDTO;
import gusl.model.nodeconfig.NodeConfig;
import gusl.node.application.events.CachesLoadedEvent;
import gusl.node.errors.NodeErrors;
import gusl.router.client.RouterHttpClient;
import lombok.CustomLog;
import org.apache.logging.log4j.Level;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author dhudson on 03/10/2019
 */
@Service
@CustomLog
public class LoggingControlImpl implements LoggingControl {

    private static final Comparator<LoggerDTO> NAME_SORT = (a, b) -> a.getName().compareTo(b.getName());

    private File theControlPath;

    @Inject
    private RouterHttpClient routerHttpClient;

    @Override
    public LoggersDTO getLoggers() {
        LoggersDTO response = new LoggersDTO();
        List<LoggerDTO> loggers = new ArrayList<>();

        GUSLLogManager.getLoggers().values().stream().forEach(logger -> loggers.add(new LoggerDTO(logger)));
        Collections.sort(loggers, NAME_SORT);
        response.setLoggers(loggers);

        return response;
    }

    @Override
    public LoggersDTO getLoggersWithPackageFilter(String packageFilter) throws GUSLErrorException {
        LoggersDTO response = new LoggersDTO();
        List<LoggerDTO> loggers = new ArrayList<>();
        response.setLoggers(loggers);

        GUSLLogManager.getLoggers().values().stream().filter(logger -> logger.getName()
                        .startsWith(packageFilter))
                .forEach(logger -> loggers.add(new LoggerDTO(logger)));
        Collections.sort(loggers, NAME_SORT);
        response.setLoggers(loggers);

        return response;
    }

    @Override
    public LoggerDTO getLoggerByName(String name) throws GUSLErrorException {
        GUSLLogger aLogger = GUSLLogManager.getLogger(name);
        if (aLogger == null) {
            throw NodeErrors.LOG_CONTROL_ERROR.generateException("Logger not found " + name);
        }

        return new LoggerDTO(aLogger);
    }

    @Override
    public LoggerDTO changeLogger(LoggerDTO loggerDTO) {
        GUSLLogger GUSLLogger = GUSLLogManager.getLogger(loggerDTO.getName());
        if (GUSLLogger != null) {
            Level newLevel;
            if (loggerDTO.getLevel() == null) {
                // Basic reset, change the level to what ever the parent was
                newLevel = getParentLevel(GUSLLogger);
            } else {
                newLevel = Level.getLevel(loggerDTO.getLevel());
            }

            loggerDTO.setLevel(newLevel.name());
            changeLogLevel(GUSLLogger, newLevel, true);
        }

        return loggerDTO;
    }

    @Override
    public LoggerDTO toggle(LoggerDTO loggerDTO) {
        GUSLLogger GUSLLogger = GUSLLogManager.getLogger(loggerDTO.getName());
        if (GUSLLogger != null) {
            if (GUSLLogger.getLevel() == Level.DEBUG) {
                changeLogLevel(GUSLLogger, getParentLevel(GUSLLogger), true);
            } else {
                changeLogLevel(GUSLLogger, Level.DEBUG, true);
            }
        }

        return loggerDTO;
    }

    @Override
    public LoggerDTO changeAll(LoggerDTO loggerDTO) {
        try {
            routerHttpClient.publishEvent(new LoggerEvent(loggerDTO));
        } catch (GUSLErrorException ex) {
            logger.warn("Unable to publish event", ex);
        }
        return changeLogger(loggerDTO);
    }

    @OnEvent
    public void handleLoggerEvent(LoggerEvent event) {
        changeLogger(event.getLogger());
    }

    private void changeLogLevel(GUSLLogger logger, Level level, boolean record) {
        if (record) {
            File entry = new File(theControlPath, logger.getName());
            if (level == Level.DEBUG) {
                try {
                    // Create entry
                    entry.createNewFile();
                } catch (IOException ex) {
                    logger.warn("Unable to create logger control entry {}", entry, ex);
                }
            } else {
                // Remove entry
                entry.delete();
            }
        }

        ((org.apache.logging.log4j.core.Logger) logger.getRawLogger()).setLevel(level);
        logger.info("Changing log {} to level {}", logger.getName(), level);
    }

    private Level getParentLevel(GUSLLogger logger) {
        return ((org.apache.logging.log4j.core.Logger) logger.getRawLogger()).getParent().getLevel();
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theControlPath = new File(SystemPropertyUtils.getRepositoryLocation() + "/loggercontrol/" + config.getNodeType().toString());
        logger.info("Logger Control Repo {}", theControlPath.getAbsolutePath());
        if (!theControlPath.exists()) {
            theControlPath.mkdirs();
        }
    }

    @OnEvent
    public void handleCachesLoadedEvent(CachesLoadedEvent event) {
        File[] files = theControlPath.listFiles();
        for (File file : files) {
            GUSLLogger logger = GUSLLogManager.getLogger(file.getName());
            if (logger != null) {
                changeLogLevel(logger, Level.DEBUG, false);
            } else {
                logger.temp("Can't log  {}", file.getName());
            }
        }
    }
}
