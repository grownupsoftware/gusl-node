package gusl.node.logging.control;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.log.LoggerDTO;
import gusl.model.log.LoggersDTO;
import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson on 03/10/2019
 */
@Contract
public interface LoggingControl extends GUSLConfigurable {

    LoggersDTO getLoggers();

    LoggersDTO getLoggersWithPackageFilter(String packageFilter) throws GUSLErrorException;

    LoggerDTO getLoggerByName(String name) throws GUSLErrorException;

    LoggerDTO changeLogger(LoggerDTO logger);

    LoggerDTO changeAll(LoggerDTO logger);

    LoggerDTO toggle(LoggerDTO logger);
}
