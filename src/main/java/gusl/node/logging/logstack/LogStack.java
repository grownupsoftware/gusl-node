package gusl.node.logging.logstack;

import java.util.List;
import gusl.model.log.LogDTO;
import org.jvnet.hk2.annotations.Contract;

/**
 * Log Stack just remembers the last x number of logs for rest calls.
 *
 * @author dhudson
 */
@Contract
public interface LogStack {

    /**
     * Return the current stack
     *
     * @return a list of Event Logs, never null
     */
    public List<LogDTO> getLogs();

}
