package gusl.node.logging.logstack;

import java.util.ArrayList;
import java.util.List;
import gusl.core.eventbus.OnEvent;
import gusl.model.log.LogDTO;
import org.jvnet.hk2.annotations.Service;

/**
 * Implementation of LogStack.
 *
 * @author dhudson
 */
@Service
public class LogStackImpl implements LogStack {

    private static final int STACK_SIZE = 200;

    private final List<LogDTO> theLogEvents;

    public LogStackImpl() {
        theLogEvents = new ArrayList<>(STACK_SIZE);
    }

    @OnEvent
    public void add(LogDTO logEvent) {
        theLogEvents.add(logEvent);
        if (theLogEvents.size() >= STACK_SIZE) {
            theLogEvents.remove(0);
        }

    }

    @Override
    public List<LogDTO> getLogs() {
        return theLogEvents;
    }
}
