package gusl.node.logging.logstack;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import gusl.core.eventbus.OnEvent;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.model.log.LogDTO;
import gusl.core.logging.GUSLLogController;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.websockets.AbstractWebSocketEndpoint;
import org.apache.logging.log4j.Level;

/**
 * Log Stack Web Socket sever.
 *
 * @author dhudson
 */
@ServerEndpoint(value = "/wslogs")
public class LogStackWebSocketEndpoint extends AbstractWebSocketEndpoint {

    private final NodeEventBus theNodeEventBus;

    public LogStackWebSocketEndpoint() {
        theNodeEventBus = GUSLServiceLocator.getService(NodeEventBus.class);
    }

    @Override
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        super.onOpen(session, config);
        theNodeEventBus.register(this);
    }

    @Override
    @OnClose
    public void onClose(Session session, CloseReason reason) {
        super.onClose(session, reason);
        theNodeEventBus.unregister(this);
    }

    @Override
    @OnError
    public void onError(Session session, Throwable t) {
        super.onError(session, t);
        theNodeEventBus.unregister(this);
    }

    @OnEvent
    public void logAdded(LogDTO logDTO) {
        writeJSON(logDTO);
    }

    @Override
    public void handleMessage(String message) {
        Level newLevel = Level.getLevel(message);
        if (newLevel != null) {
            GUSLLogController.changeRootLogLevel(newLevel);
        }
    }
}
