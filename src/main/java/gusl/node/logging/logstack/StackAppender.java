package gusl.node.logging.logstack;

import gusl.model.log.LogDTO;
import gusl.node.eventbus.NodeEventBus;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;

/**
 * Stack Appender.
 *
 * Just keep the last x.
 *
 * @author dhudson
 */
public class StackAppender extends AbstractAppender {

    public static final String APPENDER_NAME = "StackAppender";

    private final NodeEventBus theEventBus;

    public StackAppender(NodeEventBus eventBus) {
        // Don't care about filter and format
        super(APPENDER_NAME, null, null, false, null);
        theEventBus = eventBus;
    }

    @Override
    public void append(LogEvent le) {
        // Basically make a clone of it, otherwise it gets nuked
        theEventBus.post(new LogDTO(le));
    }
}
