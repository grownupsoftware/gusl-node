package gusl.node.jersey;

import com.codahale.metrics.Gauge;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.MetricsFactory;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.IdGenerator;
import gusl.node.providers.GUSLGZipEncoder;
import gusl.node.transport.HttpUtils;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.Level;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.server.filter.EncodingFilter;
import org.glassfish.jersey.uri.internal.JerseyUriBuilder;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Base Jersey Client used in Integrations.
 *
 * @author dhudson
 */
public class JerseyNodeClient {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private final Client theHttpClient;
    private ObjectMapper theObjectMapper;
    private ExecutorService theExecutorService;

    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String HEAD = "HEAD";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String OPTIONS = "OPTIONS";

    public JerseyNodeClient(JerseyHttpConfig config) {

        ClientConfig clientConfig = createStandardConfig();

        if (config.getObjectMapper() != null) {
            theObjectMapper = config.getObjectMapper();
            JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
            jacksonProvider.setMapper(theObjectMapper);
            clientConfig.register(jacksonProvider);
        }

        if (config.getProxy() != null) {
            HttpProxy proxy = config.getProxy();
            clientConfig.connectorProvider(new ApacheConnectorProvider());
            clientConfig.property(ClientProperties.PROXY_URI, proxy.getUrl());
            clientConfig.property(ClientProperties.PROXY_USERNAME, proxy.getUsername());
            clientConfig.property(ClientProperties.PROXY_PASSWORD, proxy.getPassword());
        }

        if (config.getProperties() != null) {
            config.getProperties().forEach((s, o) -> clientConfig.property(s, o));
        }

        clientConfig.property(ClientProperties.CONNECT_TIMEOUT, config.getConnectionTimeout());
        clientConfig.property(ClientProperties.READ_TIMEOUT, config.getReadTimeout());

        if (config.getExecutorService() != null) {
            theExecutorService = config.getExecutorService();
            clientConfig.executorService(theExecutorService);
        }

        if (config.isDebugging()) {
            setDebugLogging(true);
        }

        if (config.isRequiresConnectionPool()) {

            logger.info(" .......   Setting Polling Http Client Config to ....  {}", config);
            String metrixPrefix = config.getName();
            if (metrixPrefix == null) {
                metrixPrefix = "JerseyNodeClient." + IdGenerator.generateUniqueNodeIdAsString();
            }

            metrixPrefix += ".connection";

            final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
            connectionManager.setMaxTotal(config.getMaxConnections());
            connectionManager.setDefaultMaxPerRoute(config.getMaxConnections());

            if (config.getSocketConfig() == null) {
                connectionManager.setDefaultSocketConfig(JerseyHttpConfig.defaultSocketConfig());
            } else {
                connectionManager.setDefaultSocketConfig(config.getSocketConfig());
            }

            clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, connectionManager);
            clientConfig.connectorProvider(new ApacheConnectorProvider());

            MetricsFactory.registerWithoutClass(metrixPrefix + ".max", (Gauge<Integer>) () -> connectionManager.getMaxTotal());
            MetricsFactory.registerWithoutClass(metrixPrefix + ".per route", (Gauge<Integer>) () -> connectionManager.getDefaultMaxPerRoute());
            MetricsFactory.registerWithoutClass(metrixPrefix + ".available", (Gauge<Integer>) () -> connectionManager.getTotalStats().getAvailable());
            MetricsFactory.registerWithoutClass(metrixPrefix + ".leased", (Gauge<Integer>) () -> connectionManager.getTotalStats().getLeased());
            MetricsFactory.registerWithoutClass(metrixPrefix + ".pending", (Gauge<Integer>) () -> connectionManager.getTotalStats().getPending());

            BackgroundThreadPoolExecutor.scheduleWithFixedDelay(() -> {
                connectionManager.closeExpiredConnections();
                connectionManager.closeIdleConnections(30, TimeUnit.SECONDS);
            }, 30, 30, TimeUnit.SECONDS);
        }

        theHttpClient = ClientBuilder.newClient(clientConfig);
    }

    public void close() {
        if (theHttpClient != null) {
            theHttpClient.close();
        }
        if (theExecutorService != null) {
            theExecutorService.shutdown();
        }
    }

    private ClientConfig createStandardConfig() {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.register(EncodingFilter.class);
        clientConfig.register(GUSLGZipEncoder.class);
        clientConfig.register(new GUSLLoggingFeature(logger));
        return clientConfig;
    }

    public final void setDebugLogging(boolean debug) {
        if (debug) {
            ((org.apache.logging.log4j.core.Logger) logger.getRawLogger()).setLevel(Level.DEBUG);
            logger.debug("Now set to debug level");
        } else {
            ((org.apache.logging.log4j.core.Logger) logger.getRawLogger()).setLevel(Level.INFO);
        }
    }

    public String encode(String username, String password) {
        return Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));
    }

    public Client getClient() {
        return theHttpClient;
    }

    public <RESPONSE> RESPONSE parseResponse(String url, Response response, Class<RESPONSE> responseClass) {
        try {
            return HttpUtils.parseResponse(getObjectMapper(), response, responseClass);
        } catch (IOException ex) {
            logger.warn("Request [{}] - unable to parse message {} ", url, response, ex);
        }
        return null;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T parseResponseAsType(String url, Response response, TypeReference valueTypeRef) {
        String payload;
        try {
            payload = getPayloadAsString(response);
        } catch (IOException ex) {
            logger.error("Request [{}] - Unable to convert response into a string", url, ex);
            return null;
        }
        return parseResponseAsType(url, payload, valueTypeRef);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> T parseResponseAsType(String url, String payload, TypeReference valueTypeRef) {
        try {
            return (T) getObjectMapper().readValue(payload, valueTypeRef);
        } catch (IOException ex) {
            logger.warn("Request [{}] - Unable to parse payload [{}]", url, payload, ex);
        }

        return null;
    }

    public String getPayloadAsString(Response response) throws IOException {
        return HttpUtils.getPayloadAsString(response);
    }

    public String getPossiblePayloadFromException(WebApplicationException ex) {
        String result = "";

        if (ex.getResponse() != null) {
            try {
                result = getPayloadAsString(ex.getResponse());
            } catch (IOException ignore) {
            }
        }
        return result;
    }

    public Invocation.Builder addBasicAuthorisation(Invocation.Builder builder, String creds) {
        return builder.header(HttpUtils.AUTHORISATION_HEADER_NAME, "Basic " + creds);
    }

    public Invocation.Builder addBearerAuthorisation(Invocation.Builder builder, String creds) {
        return builder.header(HttpUtils.AUTHORISATION_HEADER_NAME, "Bearer " + creds);
    }

    public ObjectMapper getObjectMapper() {
        return theObjectMapper;
    }

    public String buildURI(String path, Object params) throws URISyntaxException, IllegalAccessException {
        JerseyUriBuilder builder = new JerseyUriBuilder();
        builder.path(path);

        for (Field field : ClassUtils.getFieldsFor(params.getClass(), true)) {
            if (field.get(params) != null) {
                builder.queryParam(field.getName(), field.get(params).toString());
            }
        }

        return builder.build().toString();
    }

    public Invocation.Builder getJsonBuilder(String path) {
        WebTarget webTarget = getClient().target(path);
        return webTarget.request(MediaType.APPLICATION_JSON);
    }

    public Entity<String> getEmptyJsonEntity() {
        return Entity.json("");
    }

    public <T> Entity<String> buildJsonEntity(T payload) throws JsonProcessingException {
        return Entity.json(theObjectMapper.writeValueAsString(payload));
    }

    public Invocation.Builder getXmlBuilder(String path) {
        WebTarget webTarget = getClient().target(path);
        return webTarget.request(MediaType.APPLICATION_XML);
    }

    public <T> Entity<String> buildXmlEntity(T payload) throws JsonProcessingException {
        return Entity.xml(theObjectMapper.writeValueAsString(payload));
    }

    public Invocation.Builder getFormBuilder(String path) {
        WebTarget webTarget = getClient().target(path);
        return webTarget.request(MediaType.APPLICATION_FORM_URLENCODED);
    }

    public Entity<Form> buildFormEntity(Form form) {
        return Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);
    }

    public String urlEncode(String path) {
        try {
            return URLEncoder.encode(path, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
        }
        return null;
    }
}
