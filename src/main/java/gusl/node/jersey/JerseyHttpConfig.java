package gusl.node.jersey;

import com.fasterxml.jackson.databind.ObjectMapper;
import gusl.core.tostring.ToString;
import gusl.core.utils.Platform;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.http.config.SocketConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.RequestEntityProcessing;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * @author dhudson
 * @since 04/04/2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
public class JerseyHttpConfig {

    public static final int SOCKET_IO_SIZE = 16 * 1024;
    public static final int IO_THREADS_DEFAULT = Platform.getNumberOfCores();
    public static final int CONNECT_TIMEOUT_DEFAULT = 10_000;
    public static final int READ_TIMEOUT_DEFAULT = 120_000;
    public static final int MAX_CONNECTIONS_DEFAULT = 100;

    private HttpProxy proxy;
    private Map<String, Object> properties;
    private ExecutorService executorService;
    private ObjectMapper objectMapper;
    private boolean debugging;
    private SocketConfig socketConfig;
    private int maxConnections;
    private int connectionTimeout;
    private int readTimeout;
    private String name;
    private boolean requiresConnectionPool;

    public static SocketConfig defaultSocketConfig() {
        SocketConfig.Builder builder = SocketConfig.custom();
        builder.setRcvBufSize(SOCKET_IO_SIZE);
        builder.setSndBufSize(SOCKET_IO_SIZE);
        builder.setSoKeepAlive(true);
        builder.setSoReuseAddress(true);
        builder.setTcpNoDelay(true);
        builder.setSoLinger(-1);
        return builder.build();
    }

    public static Map<String, Object> defaultClientProperties() {
        Map<String, Object> clientProperties = new HashMap<>(3);
        clientProperties.put(ClientProperties.METAINF_SERVICES_LOOKUP_DISABLE, Boolean.TRUE);
        clientProperties.put(ClientProperties.JSON_PROCESSING_FEATURE_DISABLE, Boolean.TRUE);
        clientProperties.put(ClientProperties.MOXY_JSON_FEATURE_DISABLE, Boolean.TRUE);
        clientProperties.put(ClientProperties.REQUEST_ENTITY_PROCESSING, RequestEntityProcessing.BUFFERED);
        return clientProperties;
    }

    public static JerseyHttpConfig of(ObjectMapper mapper) {
        return JerseyHttpConfig.builder().objectMapper(mapper)
                .socketConfig(defaultSocketConfig())
                .properties(defaultClientProperties())
                .requiresConnectionPool(false)
                .readTimeout(READ_TIMEOUT_DEFAULT)
                .connectionTimeout(CONNECT_TIMEOUT_DEFAULT)
                .maxConnections(MAX_CONNECTIONS_DEFAULT)
                .build();
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
