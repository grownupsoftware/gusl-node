package gusl.node.jersey;

import gusl.core.logging.GUSLLogger;
import gusl.node.filters.JerseyClientLoggingFilter;

import javax.ws.rs.RuntimeType;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;

/**
 * @author dhudson on 19/09/2019
 */
public class GUSLLoggingFeature implements Feature {

    private final GUSLLogger theLogger;

    public GUSLLoggingFeature(GUSLLogger logger) {
        theLogger = logger;
    }

    @Override
    public boolean configure(FeatureContext context) {
        boolean enabled = false;

        if (context.getConfiguration().getRuntimeType() == RuntimeType.CLIENT) {
            JerseyClientLoggingFilter filter = new JerseyClientLoggingFilter(theLogger);
            context.register(filter);
            enabled = true;
        }
//        if (context.getConfiguration().getRuntimeType() == RuntimeType.SERVER) {
//            ServerLoggingFilter serverClientFilter = (ServerLoggingFilter) createLoggingFilter(context, RuntimeType.SERVER);
//            context.register(serverClientFilter);
//            enabled = true;
//        }
        return enabled;
    }
}
