package gusl.node.jersey;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.ext.ContextResolver;

/**
 * @author dhudson
 * @since 27/10/2021
 */
public class CustomerMapperProvider implements ContextResolver<ObjectMapper> {

    private final ObjectMapper theMapper;

    public CustomerMapperProvider(ObjectMapper mapper) {
        theMapper = mapper;
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
        return theMapper;
    }
}
