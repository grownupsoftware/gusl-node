package gusl.node.jersey;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson on 09/06/2020
 */
@NoArgsConstructor
@Getter
@Setter
public class HttpProxy {

    private String url;
    private String username;

    @JsonDeserialize(using = ObfuscationDeserializer.class)
    @ToStringMask
    private String password;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
