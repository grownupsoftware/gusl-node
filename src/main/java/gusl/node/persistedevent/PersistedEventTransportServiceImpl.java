package gusl.node.persistedevent;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.inject.Inject;
import gusl.core.eventbus.OnEvent;
import gusl.core.utils.ClassUtils;
import gusl.model.persistedevent.PersistedEvent;
import gusl.model.persistedevent.PersistedEventDO;
import gusl.model.persistedevent.PersistedEventTransportEvent;
import gusl.node.service.AbstractNodeService;
import org.jvnet.hk2.annotations.Service;

/**
 *
 * @author grantwallace
 */
@Service
public class PersistedEventTransportServiceImpl extends AbstractNodeService implements PersistedEventTransportService {

    @Inject
    private ObjectMapper theObjectMapper;

    @Override
    public void unpackPersistedEventAndResend(PersistedEventDO persistedEvent) {
        Class<?> eventClass;
        try {
            eventClass = Class.forName(persistedEvent.getEventClassName());
        } catch (ClassNotFoundException ex) {
            logger.error("Failed to find class for [{}] - is it on class path",
                    persistedEvent.getEventClassName(),
                    ex);
            return;
        }

        if (persistedEvent.getEventBody() != null && !persistedEvent.getEventBody().isEmpty()) {
            try {
                Object convertedEventObj = theObjectMapper.readValue(persistedEvent.getEventBody(), eventClass);

                try {
                    PersistedEvent convertedEvent = (PersistedEvent) convertedEventObj;
                    convertedEvent.setPersistedEventId(persistedEvent.getId());
                    logger.debug("Posting: [{}] of type {} ", persistedEvent.getId(), eventClass);
                    postEvent(convertedEvent);
                } catch (Throwable t) {
                    logger.debug("Not a persisted event - still posting: {} ", eventClass);
                    postEvent(convertedEventObj);
                }
            } catch (IOException ex) {
                logger.error("Failed to convert message [{}] for event class [{}]",
                        persistedEvent.getEventBody(),
                        persistedEvent.getEventClassName(),
                        ex);
            }
        } else {
            try {
                // event has no body so fire empty class
                postEvent(ClassUtils.getNewInstance(eventClass));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException ex) {
                logger.error("Failed to create instance for  event class [{}]",
                        persistedEvent.getEventClassName(),
                        ex);
            }

        }

    }

    @OnEvent
    public void handleEvent(PersistedEventTransportEvent event) {
        if (event == null || event.getPersistedEvent() == null) {
            return;
        }
        unpackPersistedEventAndResend(event.getPersistedEvent());
    }

}
