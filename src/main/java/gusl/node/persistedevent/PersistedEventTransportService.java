package gusl.node.persistedevent;

import gusl.model.persistedevent.PersistedEventDO;
import org.jvnet.hk2.annotations.Contract;

/**
 *
 * @author grantwallace
 */
@Contract
public interface PersistedEventTransportService {

    public void unpackPersistedEventAndResend(PersistedEventDO persistedEvent);
}
