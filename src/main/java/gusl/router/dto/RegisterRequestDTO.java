package gusl.router.dto;

import gusl.core.tostring.ToString;
import gusl.model.cache.CacheGroup;
import gusl.model.nodeconfig.NodeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * I can work out where the request came from, just not the port.
 *
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class RegisterRequestDTO {

    private NodeType type;
    private int port;
    // Whats required for this node to start
    private List<NodeType> requiredNodes;
    // What cache events to subscribe to
    private List<CacheGroup> cacheGroups;
    private String version;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
