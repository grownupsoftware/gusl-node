package gusl.router.dto;

import gusl.core.tostring.ToString;
import gusl.model.systemconfig.SystemConfigDO;
import lombok.*;

import java.util.Map;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterResponseDTO {

    private Integer nodeId;
    private boolean master;
    private Integer port;
    private Map<String, SystemConfigDO> configs;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
