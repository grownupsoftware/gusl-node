package gusl.router.dto;

import gusl.core.tostring.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QuiesceRequestDTO {

    private boolean quiesceToggle;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
