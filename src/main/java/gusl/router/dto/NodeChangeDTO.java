package gusl.router.dto;

import gusl.core.tostring.ToString;
import gusl.model.routerconfig.ResourceNode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class NodeChangeDTO {
    private List<ResourceNode> nodes;

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
