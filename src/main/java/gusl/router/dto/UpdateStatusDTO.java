package gusl.router.dto;

import gusl.core.tostring.ToString;
import gusl.model.nodeconfig.NodeStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class UpdateStatusDTO {

    private int nodeId;
    private NodeStatus status;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
