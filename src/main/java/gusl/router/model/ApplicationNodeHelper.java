package gusl.router.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import gusl.model.routerconfig.ResourceNode;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;

/**
 *
 * @author dhudson
 */
public class ApplicationNodeHelper {

    private ApplicationNodeHelper() {
    }

    public static List<ResourceNode> getActiveNodes(List<ResourceNode> nodes) {
        if (nodes == null) {
            return Collections.<ResourceNode>emptyList();
        }
        return nodes.stream().filter(node -> node.getStatus() == NodeStatus.ACTIVE).collect(Collectors.toList());
    }

    public static Map<NodeType, List<ResourceNode>> groupByNodeType(List<ResourceNode> nodes) {
        return nodes.stream().collect(Collectors.groupingBy(ResourceNode::getType));
    }

    public static Map<NodeType, List<ResourceNode>> groupByActiveNodesByType(List<ResourceNode> nodes) {
        return groupByNodeType(getActiveNodes(nodes));
    }

    public static String getURL(ResourceNode node, String path) {
        StringBuilder builder = new StringBuilder(40);
        builder.append(node.getApplicationUrl());
        if (!path.startsWith("/")) {
            builder.append("/");
        }

        builder.append(path);
        return builder.toString();
    }
}
