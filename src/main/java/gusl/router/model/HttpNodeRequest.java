package gusl.router.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gusl.core.tostring.ToString;
import gusl.model.nodeconfig.NodeType;
import gusl.node.async.AsyncTimeoutConstants;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dhudson
 * @since 05/03/2021
 */
public class HttpNodeRequest<RESPONSE> {

    private int timeoutInSeconds;
    private long loadBalanceHash;
    private NodeType nodeType;
    private String path;
    private Map<String, String> headers;
    private int nodeId;
    private Object payload;
    private Class<RESPONSE> responseClass;
    private String params;
    // Populated by the processors
    private String url;
    private Class<?> genericHint;

    public HttpNodeRequest() {
        loadBalanceHash = 0;
        timeoutInSeconds = AsyncTimeoutConstants.DEFAULT_TIMEOUT;
    }

    public int getTimeoutInSeconds() {
        return timeoutInSeconds;
    }

    public HttpNodeRequest setTimeoutInSeconds(int timeoutInSeconds) {
        this.timeoutInSeconds = timeoutInSeconds;
        return this;
    }

    public long getLoadBalanceHash() {
        return loadBalanceHash;
    }

    public HttpNodeRequest setLoadBalanceHash(long loadBalanceHash) {
        this.loadBalanceHash = loadBalanceHash;
        return this;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public HttpNodeRequest setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
        return this;
    }

    public String getPath() {
        return path;
    }

    public HttpNodeRequest setPath(String path) {
        this.path = path;
        return this;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public HttpNodeRequest setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public int getNodeId() {
        return nodeId;
    }

    public HttpNodeRequest setNodeId(int nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public Object getPayload() {
        return payload;
    }

    public HttpNodeRequest setPayload(Object payload) {
        this.payload = payload;
        return this;
    }

    public Class<RESPONSE> getResponseClass() {
        return responseClass;
    }

    public HttpNodeRequest setResponseClass(Class<RESPONSE> responseClass) {
        this.responseClass = responseClass;
        return this;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean hasPayload() {
        return (payload != null);
    }

    public boolean hasResponse() {
        return !(responseClass == null || responseClass == Void.class);
    }

    public HttpNodeRequest setHashFrom(@NonNull String key) {
        loadBalanceHash = (long) key.hashCode();
        return this;
    }

    public HttpNodeRequest setGenericHint(Class<?> clazz) {
        genericHint = clazz;
        return this;
    }

    public Class<?> getGenericHint() {
        return genericHint;
    }

    @JsonIgnore
    public String getNodeRequestDetails() {
        if (nodeId != 0) {
            return Integer.toString(nodeId);
        }

        StringBuilder builder = new StringBuilder(32);
        builder.append("Node Type ").append(nodeType).append(" balance key ").append(loadBalanceHash);
        return builder.toString();
    }

    public void addHeader(String key, String value) {
        if (headers == null) {
            headers = new HashMap<>(3);
        }
        headers.put(key, value);
    }

    public void addHeaders(Map<String, String> headers) {
        headers.forEach(this::addHeader);
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
