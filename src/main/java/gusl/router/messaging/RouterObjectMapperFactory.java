package gusl.router.messaging;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import gusl.core.json.ObjectMapperFactory;

/**
 * Extra features than the normal ObjectMapper.
 *
 * @author dhudson
 */
public class RouterObjectMapperFactory {

    private static final ObjectMapper theObjectMapper = createObjectMapper();

    private RouterObjectMapperFactory() {
    }

    /**
     * Return the standard configured object mapper.
     * <p>
     * Object Mappers are thread safe, so there on no need to create more than
     * one unless you would like different features.
     *
     * @return the router object mapper.
     */
    public static ObjectMapper getObjectMapper() {
        return theObjectMapper;
    }

    private static ObjectMapper createObjectMapper() {
        ObjectMapper mapper = ObjectMapperFactory.createDefaultObjectMapper();
        //mapper.activateDefaultTyping(BasicPolymorphicTypeValidator.builder().build()); //enableDefaultTyping();
        mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        return mapper;
    }
}
