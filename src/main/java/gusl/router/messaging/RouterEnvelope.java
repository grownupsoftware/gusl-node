package gusl.router.messaging;

import gusl.core.tostring.ToString;

/**
 * This is the message structure (on the wire) for the Router.
 *
 * JSON will be used to transport the data.
 *
 * @author dhudson
 */
public class RouterEnvelope {

    private long sendTimestamp;
    private long receiveTimestamp;
    private RouterMessageType messageType;
    private long correlationId;
    private Object payload;

    public RouterEnvelope() {
    }

    public long getSendTimestamp() {
        return sendTimestamp;
    }

    public void setSendTimestamp(long sendTimestamp) {
        this.sendTimestamp = sendTimestamp;
    }

    public void setSendTimestamp() {
        sendTimestamp = System.currentTimeMillis();
    }

    public long getReceiveTimestamp() {
        return receiveTimestamp;
    }

    public void setReceiveTimestamp(long receiveTimestamp) {
        this.receiveTimestamp = receiveTimestamp;
    }

    public void setReceiveTimestamp() {
        receiveTimestamp = System.currentTimeMillis();
    }

    public RouterMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(RouterMessageType messageType) {
        this.messageType = messageType;
    }

    public long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(long correlationId) {
        this.correlationId = correlationId;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
