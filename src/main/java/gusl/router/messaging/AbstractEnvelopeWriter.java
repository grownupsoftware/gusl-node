package gusl.router.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Base EnvelopeWriter
 *
 * @author dhudson
 */
public abstract class AbstractEnvelopeWriter {

    private final ReentrantLock theLock = new ReentrantLock();
    private final AtomicLong theCorrelationId = new AtomicLong(0);
    private final ObjectMapper theObjectMapper = RouterObjectMapperFactory.getObjectMapper();

    AbstractEnvelopeWriter() {
    }

    private long getCorrelationId() {
        return theCorrelationId.getAndIncrement();
    }

    ReentrantLock getLock() {
        return theLock;
    }

    ObjectMapper getObjectMapper() {
        return theObjectMapper;
    }

    /**
     * Create an Envelope with a given type.
     *
     * @param type of message
     * @return Envelope
     */
    public RouterEnvelope createEnvelopeFor(RouterMessageType type) {
        RouterEnvelope envelope = new RouterEnvelope();
        envelope.setMessageType(type);
        return envelope;
    }

    /**
     * Create an Envelope with a given type and correlation id.
     *
     * @param type of message
     * @return new envelope with correlation id set.
     */
    public RouterEnvelope createCorrelatedEnvelopeFor(RouterMessageType type) {
        RouterEnvelope envelope = createEnvelopeFor(type);
        envelope.setCorrelationId(getCorrelationId());
        return envelope;
    }
}
