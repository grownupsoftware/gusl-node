package gusl.router.messaging;

/**
 * The Router can only understand well qualified message types and they must be
 * defined here.
 *
 * @author dhudson
 */
public enum RouterMessageType {

    // Client to server
    REGISTRATION_REQUEST,
    // Server to client
    REGISTRATION_RESPONSE,
    // Client to server
    STATUS_UPDATE_REQUEST,
    // Server to client
    STARTUP_REQUEST,
    // Server to client
    SHUTDOWN_REQUEST,
    // Server to client
    SUBSCRIBE_REQUEST,
    //Client to server
    SUBSCRIBE_RESPONSE,
    // Server to client
    UNSUBSCRIBE_REQUEST,
    //Client to server
    UNSUBSCRIBE_RESPONSE,
    // Server to client
    SERVER_PING_CLIENT_REQUEST,
    //Client to server
    CLIENT_PING_SERVER_REQUEST,
    // Client to server
    DEREGISTER_REQUEST,
    // Client and Server
    PING_RESPONSE,
    // Sever to client
    SECONDARY_NOTIFICATION,
    // Server to Client
    KILL,
    // Server to Client
    PROMOTE_TO_PRIMARY_REQUEST,
    // Client to Server
    PROMOTE_TO_PRIMARY_RESPONSE,
    // Client to server
    ACQUIRE_LOCK_REQUEST,
    GET_LOCK_REQUEST,
    RELEASE_LOCK_REQUEST,
    TRY_LOCK_REQUEST,
    // server to client
    ACQUIRE_LOCK_RESPONSE,
    GET_LOCK_RESPONSE,
    RELEASE_LOCK_RESPONSE,
    TRY_LOCK_RESPONSE,
    // Server to client
    QUIESCE_REQUEST,
    // Client to server
    QUIESCE_RESPONSE,
    NODE_CHANGE
}
