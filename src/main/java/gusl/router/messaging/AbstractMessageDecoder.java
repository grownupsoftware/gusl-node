package gusl.router.messaging;

import java.nio.ByteBuffer;

/**
 * Decoding of Byte Buffers.
 *
 * Byte Buffers must be framed with a SHORT of message length before the
 * payload.
 *
 * @author dhudson
 */
public abstract class AbstractMessageDecoder {

    private static final int SIZE_OF_SHORT = 2;
    private final ByteBuffer theBuffer;

    public AbstractMessageDecoder(int bufferSize) {
        theBuffer = ByteBuffer.allocateDirect(bufferSize);
    }

    public void decode(int count, byte[] bytes) {

        short messageSize;

        theBuffer.put(bytes, 0, count);
        theBuffer.flip();

        boolean hasMore = true;

        while (hasMore) {

            // We are at the start of the message
            if (theBuffer.remaining() < SIZE_OF_SHORT) {
                // Less than 2 bytes so can't read the message size,
                // wait for more data;
                theBuffer.position(theBuffer.limit());
                theBuffer.limit(theBuffer.capacity());
                return;
            }

            messageSize = theBuffer.getShort();

            // We have enough data in the buffer to process the message
            if (theBuffer.limit() >= messageSize) {

                processMessage(messageSize, theBuffer);

                if (!theBuffer.hasRemaining()) {
                    hasMore = false;
                    theBuffer.clear();
                } else {
                    // Move contents to the next record
                    theBuffer.compact();
                    theBuffer.flip();
                }
            } else {
                //  Its a fragmented message
                theBuffer.position(theBuffer.limit());
                theBuffer.limit(theBuffer.capacity());
                hasMore = false;
            }
        }
    }

    /**
     * process the message from the buffer.
     *
     * NB: The buffer must be positioned at the end of the message, ready for
     * the next message to read.
     *
     * The buffer is currently positioned at the beginning of the payload and
     * the message length has already been read.
     *
     * @param messageSize size of the message
     * @param buffer to process
     */
    public abstract void processMessage(int messageSize, ByteBuffer buffer);
}
