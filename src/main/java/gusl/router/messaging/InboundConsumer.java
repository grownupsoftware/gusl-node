package gusl.router.messaging;

import gusl.core.utils.IOUtils;
import gusl.router.client.RouterConstants;
import lombok.CustomLog;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Handle reading of the inbound message from server / client.
 *
 * @author dhudson
 */
@CustomLog
public class InboundConsumer extends Thread {

    private static int theWorkerCount = 0;
    private boolean isRunning;

    private final InputStream theInputStream;
    private final RouterMessageDecoder theDecoder;
    private final ConnectionLostHandler theConnectionLostHandler;

    public InboundConsumer(Socket socket, RouterMessageHandler messageHandler,
                           ConnectionLostHandler connectionLostHandler) throws IOException {
        theWorkerCount++;
        theConnectionLostHandler = connectionLostHandler;
        theInputStream = socket.getInputStream();
        theDecoder = new RouterMessageDecoder(messageHandler);
        isRunning = true;
    }

    @Override
    public void run() {

        byte[] ioBuf = new byte[RouterConstants.IO_BUFFER_SIZE];
        int bytesRead;
        while (isRunning) {
            try {

                bytesRead = theInputStream.read(ioBuf);

                if (bytesRead == 0) {
                    continue;
                }

                if (bytesRead == -1) {
                    // Any issue at all, need to notify
                    theConnectionLostHandler.connectionLost("EOF", null);
                    isRunning = false;
                    break;
                }

                theDecoder.decode(bytesRead, ioBuf);

            } catch (IOException ex) {
                // Any issue at all, need to notify
                if (isRunning) {
                    theConnectionLostHandler.connectionLost("IOException", ex);
                    logger.warn("Died ...", ex);
                }
                isRunning = false;
            }
        }
    }

    public void shutdown() {
        isRunning = false;
        IOUtils.closeQuietly(theInputStream);
    }

    public int getWorkerCount() {
        return theWorkerCount;
    }

    public void startup() {
        logger.debug("Starting");
        start();
    }
}
