package gusl.router.messaging;

import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

import static gusl.router.client.RouterConstants.DECODER_BUFFER_SIZE;

/**
 * Write the Envelope on the wire.
 * <p>
 * If unable to write for any reason, notify the connection lost handler.
 *
 * @author dhudson
 */
@CustomLog
public class SocketEnvelopeWriter extends AbstractEnvelopeWriter {

    private final OutputStream theOutputStream;
    private final ConnectionLostHandler theConnectionLostHandler;

    private boolean isClosing;

    private final ByteBuffer theBuffer = ByteBuffer.allocate(DECODER_BUFFER_SIZE);

    public SocketEnvelopeWriter(Socket socket, ConnectionLostHandler connectionLostHandler) throws IOException {
        theOutputStream = socket.getOutputStream();
        theConnectionLostHandler = connectionLostHandler;
    }

    /**
     * Write a Message to the Router.
     * <p>
     * This method is thread safe.
     * <p>
     * If unable to write the message, the ConnectionLost handler is notified.
     *
     * @param envelope to write
     * @return true if the message was written
     */
    public boolean write(RouterEnvelope envelope) {
        boolean written = true;
        getLock().lock();
        try {
            envelope.setSendTimestamp();
            theBuffer.clear();

            try {
                byte[] message = getObjectMapper().writeValueAsBytes(envelope);
                if (message.length < DECODER_BUFFER_SIZE) {

                    theBuffer.putShort((short) message.length);
                    theBuffer.put(message);
                    theOutputStream.write(theBuffer.array(), 0, theBuffer.position());
                } else {
                    logger.info("We cannot write message as it is greater than the buffer size, message length {}, type {}", message.length, envelope.getMessageType());
                }

            } catch (IOException ex) {
                if (!isClosing) {
                    theConnectionLostHandler.connectionLost("Unable to write message [" + envelope + "]", ex);
                }
                written = false;
            }

            return written;
        } finally {
            getLock().unlock();
        }
    }

    public boolean write(RouterMessageType type, Object payload) {
        RouterEnvelope env = createEnvelopeFor(type);
        env.setPayload(payload);
        return write(env);
    }

    /**
     * Write an empty payload message.
     *
     * @param type of message to write
     * @return true if written
     */
    public boolean write(RouterMessageType type) {
        return write(createEnvelopeFor(type));
    }

    /**
     * Notify the client that there has been a ping timeout
     */
    public void pingTimeout() {
        getLock().lock();
        try {
            IOUtils.closeQuietly(theOutputStream);
            theConnectionLostHandler.connectionLost("Ping Timeout", null);
        } finally {
            getLock().unlock();
        }
    }

    public void shutdown() {
        isClosing = true;
        IOUtils.closeQuietly(theOutputStream);
    }

}
