package gusl.router.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.CustomLog;

import java.io.IOException;
import java.nio.ByteBuffer;

import static gusl.router.client.RouterConstants.DECODER_BUFFER_SIZE;


/**
 * Decode Messages from the Router Client / Server.
 *
 * @author dhudson
 */
@CustomLog
public class RouterMessageDecoder extends AbstractMessageDecoder {

    private final RouterMessageHandler theCallback;

    private final ObjectMapper theObjectMapper = RouterObjectMapperFactory.getObjectMapper();

    public RouterMessageDecoder(RouterMessageHandler callBack) {
        super(DECODER_BUFFER_SIZE);
        theCallback = callBack;
    }

    @Override
    public void processMessage(int messageSize, ByteBuffer buffer) {

        byte[] message = new byte[messageSize];
        buffer.get(message);

        RouterEnvelope envelope;
        try {
            envelope = theObjectMapper.readValue(message, RouterEnvelope.class);
            envelope.setReceiveTimestamp();

            theCallback.messageCallback(envelope);
        } catch (IOException ex) {
            logger.warn("Unknown message format {}", new String(message), ex);
        }
    }

}
