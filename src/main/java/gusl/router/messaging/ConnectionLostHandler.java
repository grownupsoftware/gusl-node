package gusl.router.messaging;

/**
 * Connection Lost Handler
 * 
 * @author dhudson
 */
@FunctionalInterface
public interface ConnectionLostHandler {
   
    /**
     * Notify the handler that the connection has been lost
     * 
     * @param reason
     * @param cause 
     */
    public void connectionLost(String reason, Throwable cause);
}
