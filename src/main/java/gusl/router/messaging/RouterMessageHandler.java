package gusl.router.messaging;

/**
 * A complete message has been received, now handle it.
 *
 * @author dhudson
 */
public interface RouterMessageHandler {

    /**
     * Once a complete message has been decoded, this callback will be
     * triggered.
     *
     * @param envelope wire format of the message
     */
    public void messageCallback(RouterEnvelope envelope);

}
