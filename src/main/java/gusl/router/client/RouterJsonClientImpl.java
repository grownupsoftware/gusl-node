package gusl.router.client;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLException;
import gusl.core.utils.IOUtils;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.Utils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.RouterClientConfig;
import gusl.node.application.events.NodeRegisteredEvent;
import gusl.node.application.events.SetupCompleteEvent;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.events.QuiesceRequestEvent;
import gusl.router.dto.*;
import gusl.router.messaging.*;
import gusl.router.route.RequestRouter;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import static gusl.router.messaging.RouterMessageType.REGISTRATION_REQUEST;

/**
 * @author dhudson
 */
@Service
@CustomLog
public class RouterJsonClientImpl implements RouterJsonClient, RouterMessageHandler, ConnectionLostHandler {

    private static final int SOCKET_RETRY_TIME = 2000;
    private boolean isConnected;
    private boolean isClosing;
    private Socket theSocket;
    private SocketEnvelopeWriter theWriter;
    private InboundConsumer theReader;

    private String theRegistraHost;
    private int theRegistraPort;

    // Lets keep this if we need to re-connect and reregister
    private RegisterRequestDTO theRegistrationRequest;

    @Inject
    private NodeEventBus theEventBus;

    @Inject
    private NodeDetails theNodeDetails;

    @Inject
    private RequestRouter theRequestRouter;

    public RouterJsonClientImpl() {
    }

    @Override
    public void register(RegisterRequestDTO dto) {
        if (!isConnected) {
            connect();
        }

        theRegistrationRequest = dto;
        register();
    }

    private void register() {
        RouterEnvelope registration = theWriter.createEnvelopeFor(REGISTRATION_REQUEST);
        registration.setPayload(theRegistrationRequest);
        theNodeDetails.setCacheGroups(theRegistrationRequest.getCacheGroups());
        theWriter.write(registration);
    }

    // NB: This is blocking until connected.
    private void connect() {
        if (isConnected) {
            return;
        }

        while (!isConnected) {
            try {
                theSocket = new Socket();
                InetSocketAddress socketAddress = new InetSocketAddress(theRegistraHost, theRegistraPort);
                theSocket.connect(socketAddress, SOCKET_RETRY_TIME);
                RouterConstants.conditionSocket(theSocket);

                theWriter = new SocketEnvelopeWriter(theSocket, this);
                theReader = new InboundConsumer(theSocket, this, this);
                theReader.startup();
                isConnected = true;
                logger.info("Connected to registra {}", socketAddress);
            } catch (IOException e) {
                logger.info("Could not connect to registra [{}:{}] reason {}", theRegistraHost, theRegistraPort, e.getMessage());
                Utils.sleep(SOCKET_RETRY_TIME);
            }
        }
    }

    @Override
    public void connectionLost(String reason, Throwable cause) {
        if (!isClosing) {
            logger.warn("Lost connection to router {}", reason, cause);
            isConnected = false;
            connect();
            if (theRegistrationRequest != null) {
                register();
            }
        }
    }

    @Override
    public void messageCallback(RouterEnvelope envelope) {
        switch (envelope.getMessageType()) {
            case REGISTRATION_RESPONSE:
                RegisterResponseDTO response = (RegisterResponseDTO) envelope.getPayload();

                theNodeDetails.setNodeId(response.getNodeId());
                theNodeDetails.setMaster(response.isMaster());

                // Lets set the IdGenerator here
                IdGenerator.setNodeUniqueId(theNodeDetails.getNodeId());

                GUSLServiceLocator.injectSystemConfig(response.getConfigs());

//                logger.info("Registered [{}] .. with a Node ID of [{}] and is master {}",
//                        theNodeDetails.getNodeType(), theNodeDetails.getNodeId(), theNodeDetails.isMaster());

                theEventBus.post(new NodeRegisteredEvent(theNodeDetails));
                break;

            case STARTUP_REQUEST:
                NodeChangeDTO nodeChange = (NodeChangeDTO) envelope.getPayload();
                updateNodes(nodeChange);

                // Let the application startup now
                theEventBus.post(new SetupCompleteEvent(theNodeDetails));
                break;

            case NODE_CHANGE:
                NodeChangeDTO nodeChanges = (NodeChangeDTO) envelope.getPayload();
                updateNodes(nodeChanges);
                break;

            case SHUTDOWN_REQUEST:
                logger.warn("Node has been requested to stop");
                theNodeDetails.setStatus(NodeStatus.STOPPED);
                isClosing = true;
                theWriter.shutdown();
                theReader.shutdown();
                IOUtils.closeQuietly(theSocket);
                System.exit(0);
                break;

            case QUIESCE_REQUEST:
                QuiesceRequestDTO request = (QuiesceRequestDTO) envelope.getPayload();
                logger.warn("Quiesce Request {}", request.isQuiesceToggle());
                QuiesceRequestEvent event = new QuiesceRequestEvent();
                event.setQuiesceRequested(request.isQuiesceToggle());

                // Let the application now, N.B: This listens to this
                theEventBus.post(event);
                break;

            default:
                logger.warn("Don't know how to handle a router message of {}", envelope);
        }
    }

    private void updateNodes(NodeChangeDTO nodeChange) {
        theNodeDetails.setNodes(nodeChange.getNodes());
        theRequestRouter.setNodes(nodeChange.getNodes());
    }

    @Override
    public void nodeStatusChange(UpdateStatusDTO status) {
        theWriter.write(RouterMessageType.STATUS_UPDATE_REQUEST, status);
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        //logger.temp("NodeConfig: {}", config);
        RouterClientConfig routerConfig = config.getRouterClientConfig();
        // Lets get the Registra Address
        String[] address = routerConfig.getRegistraAddress().split(":");
        theRegistraHost = address[0];
        if (address.length == 1) {
            // No port so use default
            theRegistraPort = RouterConstants.ROUTER_SERVER_PORT;
        } else {
            theRegistraPort = Integer.parseInt(address[1]);
        }
    }

    /**
     * Other event handlers need to listen to this event, and fail fast if this
     * action is not required.
     *
     * @param event
     */
    @OnEvent(order = 100)
    public void handleEvent(QuiesceRequestEvent event) {
        if (event.isQuiesceRequested()) {
            theNodeDetails.setStatus(NodeStatus.QUIESCED);
        } else {
            theNodeDetails.setStatus(NodeStatus.ACTIVE);
        }
        UpdateStatusDTO status = new UpdateStatusDTO();
        status.setNodeId(theNodeDetails.getNodeId());
        status.setStatus(theNodeDetails.getStatus());
        nodeStatusChange(status);
    }
}
