package gusl.router.client;

import gusl.node.properties.GUSLConfigurable;
import gusl.router.dto.RegisterRequestDTO;
import gusl.router.dto.UpdateStatusDTO;
import org.jvnet.hk2.annotations.Contract;

/**
 *
 * @author dhudson
 */
@Contract
public interface RouterJsonClient extends GUSLConfigurable {

    public void register(RegisterRequestDTO dto);

    public void nodeStatusChange(UpdateStatusDTO status);
}
