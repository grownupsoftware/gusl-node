package gusl.router.client;

import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author dhudson
 */
public class RouterConstants {

    public static final int ROUTER_SERVER_PORT = 40905;

    // Router Messages are small, so lets up the buffer to 4K
    public static final int IO_BUFFER_SIZE = 1024 * 4;

    // This has to be bigger than the Socket IO buffer as it may be partially full
    // when the next decode comes in
    public static final int DECODER_BUFFER_SIZE = IO_BUFFER_SIZE * 2;

    /**
     * Used to condition the client and server side of the socket.
     *
     * @param socket
     * @throws SocketException
     */
    public static void conditionSocket(Socket socket) throws SocketException {
        socket.setSendBufferSize(IO_BUFFER_SIZE);
        socket.setReceiveBufferSize(IO_BUFFER_SIZE);
        socket.setTcpNoDelay(true);
        // So we can set this, but I doubt if it works
        socket.setPerformancePreferences(0, 2, 1);
        // TCP Ping
        socket.setKeepAlive(true);
    }
}
