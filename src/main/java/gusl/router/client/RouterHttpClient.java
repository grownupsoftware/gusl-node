package gusl.router.client;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.nodeconfig.NodeType;
import gusl.node.properties.GUSLConfigurable;
import gusl.router.dto.NodesDTO;
import gusl.router.model.HttpNodeRequest;
import org.jvnet.hk2.annotations.Contract;

import javax.ws.rs.core.Response;
import java.io.Closeable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

/**
 * Router Node Client Contract.
 *
 * @author dhudson
 */
@Contract
public interface RouterHttpClient extends GUSLConfigurable, Closeable {

    String CONTEXT_PATH = "/router/rest/";

    <RESPONSE> CompletableFuture<RESPONSE> routeRequest(HttpNodeRequest request);

    <RESPONSE> CompletableFuture<RESPONSE> sendRouterRequest(String path, Object request, Class<RESPONSE> responseClass);

    Future<Response> execute(String url, Object entity);

    /* ---  Publish Events ---= */

    void publishEvent(Object event) throws GUSLErrorException;

    void publishAndWait(Object event) throws GUSLErrorException;

    void publishOnlyTo(Integer to, Object event) throws GUSLErrorException;

    void publishToAllNodesOfType(NodeType type, Object event) throws GUSLErrorException;

    void publishToAllNodesOfTypeExcept(NodeType type, Object event, int nodeId) throws GUSLErrorException;

    void publishToOnlyMasterOfType(NodeType type, Object event) throws GUSLErrorException;

    void publishToOneNodeOfType(NodeType type, Object event) throws GUSLErrorException;

    void publishToBalancedNodeOfType(NodeType type, Object event, long hash) throws GUSLErrorException;

    void publishToAllNodesInList(Object event, NodeType... types) throws GUSLErrorException;

    void publishToAllNodesExcludeList(Object event, NodeType... types) throws GUSLErrorException;

    CompletableFuture<NodesDTO> getActiveNodes();

    <RESPONSE> HttpNodeRequest<RESPONSE> createHttpNodeRequest(String path, Object request, Class<RESPONSE> response);

    <RESPONSE> CompletableFuture<RESPONSE> sendRequest(HttpNodeRequest<RESPONSE> nodeRequest);
}
