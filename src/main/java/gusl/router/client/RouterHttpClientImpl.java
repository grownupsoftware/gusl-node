package gusl.router.client;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeType;
import gusl.model.nodeconfig.RouterClientConfig;
import gusl.model.routerconfig.ResourceNode;
import gusl.node.application.events.NodeRegisteredEvent;
import gusl.node.transport.HttpRequestClientImpl;
import gusl.node.transport.HttpUtils;
import gusl.router.dto.NodesDTO;
import gusl.router.model.ApplicationNodeHelper;
import gusl.router.model.HttpNodeRequest;
import gusl.router.publish.PublishRequest;
import gusl.router.publish.PublishRequestCommands;
import gusl.router.route.RequestRouter;
import lombok.NoArgsConstructor;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

/**
 * @author dhudson
 */
@Service
@NoArgsConstructor
public class RouterHttpClientImpl extends HttpRequestClientImpl implements RouterHttpClient {

    private RouterClientConfig theClientConfig;
    private final Map<String, String> thePublishHeader = new HashMap<>(1);
    private int theCurrentNodeId;

    @Inject
    private RequestRouter theRequestRouter;

    public <RESPONSE> CompletableFuture<RESPONSE> sendRouterRequest(String path, Object request, Class<RESPONSE> responseClass) {
        HttpNodeRequest<RESPONSE> nodeRequest = new HttpNodeRequest<>();
        nodeRequest.setNodeType(NodeType.ROUTER);
        nodeRequest.setResponseClass(responseClass);
        nodeRequest.setPath(path);
        nodeRequest.setPayload(request);
        return routeRequest(nodeRequest);
    }

    // I think that this is on the method that we need
    @Override
    public <RESPONSE> CompletableFuture<RESPONSE> routeRequest(HttpNodeRequest request) {
        ResourceNode node = theRequestRouter.getFor(request);

        if (node != null) {
            request.setUrl(ApplicationNodeHelper.getURL(node, request.getPath()));
        } else {
            request.setUrl(buildRouterRouteRequest(request.getNodeType(), request.getPath()));
            request.setHeaders(createRoutingHeaderMap(request.getNodeType()));
        }

        return sendRequest(request);
    }

    private String buildRouterRouteRequest(NodeType type, String path) {
        StringBuilder builder = new StringBuilder(100);
        builder.append(getServerURL());
        builder.append(CONTEXT_PATH);
        builder.append("route/");
        builder.append(type.name().toLowerCase());
        builder.append("/rest");
        if (!path.startsWith("/")) {
            builder.append("/");
        }
        builder.append(path);

        //logger.warn("Routing request {}", builder.toString());
        return builder.toString();
    }

    private Map<String, String> createRoutingHeaderMap(NodeType type) {
        Map<String, String> headers = new HashMap<>(1);
        headers.put(HttpUtils.NODE_ROUTE_HEADER_NAME, type.toString());
        return headers;
    }

    private Map<String, String> createRoutingHeaderMap(NodeType type, Long id) {
        Map<String, String> headers = new HashMap<>(2);
        headers.put(HttpUtils.NODE_ROUTE_HEADER_NAME, type.toString());
        headers.put(HttpUtils.NODE_ROUTER_ID_HEADER_NAME, id.toString());
        return headers;
    }

    private HttpNodeRequest<Void> createPublishRequest(String path, Object event) {
        HttpNodeRequest<Void> request = new HttpNodeRequest();
        request.setPath(path);
        request.setUrl(getServerURL() + CONTEXT_PATH + path);
        request.addHeaders(thePublishHeader);
        request.setPayload(event);
        request.setNodeId(theCurrentNodeId);
        request.setNodeType(NodeType.ROUTER);

        return request;
    }

    @Override
    public void publishEvent(Object event) throws GUSLErrorException {
        if (isConfigured()) {
            sendRequest(createPublishRequest("publish", event));
        }
    }

    @Override
    public void publishAndWait(Object event) throws GUSLErrorException {
        HttpUtils.waitForFuture(sendRequest(createPublishRequest("publish/wait", event)));
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theClientConfig = config.getRouterClientConfig();

        if (theClientConfig != null) {
            configure(config.getNodeType(), theClientConfig.getUrl(), theClientConfig.getHttpClientConfig());
        }
    }

    @OnEvent
    public void handleSetup(NodeRegisteredEvent event) {
        // Listen for the Node Registered Event, as it has the Node ID
        thePublishHeader.put(HttpUtils.NODE_PUBLISH_NODE_ID, Integer.toString(event.getNodeDetails().getNodeId()));
        theCurrentNodeId = event.getNodeDetails().getNodeId();
        //logger.info("...................................   NodeId is .....  {}", theCurrentNodeId);
    }

    @Override
    public void publishOnlyTo(Integer to, Object event) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_ONLY_TO, event);
        request.setToNode(to);
        sendPublishRequest(request);
    }

    @Override
    public void publishToAllNodesOfType(NodeType type, Object event) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_ALL_NODES_OF_TYPE, event);
        request.setType(type);
        sendPublishRequest(request);
    }

    @Override
    public void publishToAllNodesOfTypeExcept(NodeType type, Object event, int nodeId) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_ALL_NODES_OF_TYPE_EXCEPT, event);
        request.setType(type);
        request.setExclude(nodeId);
        sendPublishRequest(request);
    }

    @Override
    public void publishToOnlyMasterOfType(NodeType type, Object event) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_TO_MASTER_NODE, event);
        request.setType(type);
        sendPublishRequest(request);
    }

    @Override
    public void publishToOneNodeOfType(NodeType type, Object event) {
        ResourceNode node = theRequestRouter.getFor(type, 0);
        if (node != null) {
            sendRequest(createEventBusRequest(node, event));
        } else {
            PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_TO_ONE_NODE_OF_TYPE, event);
            request.setType(type);
            sendPublishRequest(request);
        }
    }

    @Override
    public void publishToBalancedNodeOfType(NodeType type, Object event, long hash) {
        ResourceNode node = theRequestRouter.getFor(type, hash);
        if (node != null) {
            HttpNodeRequest<Void> request = createEventBusRequest(node, event);
            request.setLoadBalanceHash(hash);
            sendRequest(request);
        } else {
            PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_TO_BALANCED_NODE_OF_TYPE, event);
            request.setType(type);
            request.setHash(hash);
            sendPublishRequest(request);
        }
    }

    private HttpNodeRequest<Void> createEventBusRequest(ResourceNode node, Object event) {
        HttpNodeRequest<Void> request = new HttpNodeRequest<>();
        request.setNodeId(node.getNodeId());
        request.setNodeType(node.getType());
        request.setPath("eventbus");
        request.setUrl(ApplicationNodeHelper.getURL(node, request.getPath()));
        request.setPayload(event);
        return request;
    }

    @Override
    public void publishToAllNodesInList(Object event, NodeType... types) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_TO_ALL_GIVEN_NODES, event);
        request.setTypes(Arrays.asList(types));
        sendPublishRequest(request);
    }

    @Override
    public void publishToAllNodesExcludeList(Object event, NodeType... types) throws GUSLErrorException {
        PublishRequest request = createRequest(PublishRequestCommands.PUBLISH_TO_NODES_EXCEPT_GIVEN, event);
        request.setTypes(Arrays.asList(types));
        sendPublishRequest(request);
    }

    private PublishRequest createRequest(int command, Object event) {
        PublishRequest request = new PublishRequest(command);
        request.setFromNode(theCurrentNodeId);
        request.setEvent(event);

        return request;
    }

    private void sendPublishRequest(PublishRequest request) {
        sendRequest(createPublishRequest("publish/command", request));
    }

    @Override
    public CompletableFuture<NodesDTO> getActiveNodes() {
        HttpNodeRequest<NodesDTO> request = createHttpNodeRequest("registra/active", null, NodesDTO.class);
        return sendRequest(request);
    }

    @Override
    public Future<Response> execute(String url, Object entity) {
        Invocation.Builder builder = getHttpClient().getJsonBuilder(url);
        return builder.async().post(Entity.json(entity));
    }

    @Override
    public <RESPONSE> HttpNodeRequest<RESPONSE> createHttpNodeRequest(String path, Object request, Class<RESPONSE> response) {
        HttpNodeRequest<RESPONSE> nodeRequest = new HttpNodeRequest<>();
        nodeRequest.setPath(path);
        nodeRequest.setPayload(request);
        nodeRequest.setResponseClass(response);
        nodeRequest.setNodeType(NodeType.ROUTER);
        nodeRequest.setUrl(getServerURL() + CONTEXT_PATH + path);
        return nodeRequest;
    }

}
