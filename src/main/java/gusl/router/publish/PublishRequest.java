package gusl.router.publish;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.List;
import gusl.core.tostring.ToString;
import gusl.model.nodeconfig.NodeType;

/**
 *
 * @author dhudson
 */
public class PublishRequest {

    // Where did we come from
    private int fromNode;
    // The event
    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
    private Object event;
    // What are doing PublishRequestCommands
    private int operation;
    // Only to this node
    private int toNode;
    private NodeType type;
    // Load balance ID
    private long hash;
    // Exclude this node
    private int exclude;
    // Include of exclude these types
    private List<NodeType> types;

    public PublishRequest() {
    }

    public PublishRequest(int command) {
        operation = command;
    }

    public int getFromNode() {
        return fromNode;
    }

    public void setFromNode(int fromNode) {
        this.fromNode = fromNode;
    }

    public Object getEvent() {
        return event;
    }

    public void setEvent(Object event) {
        this.event = event;
    }

    public int getOperation() {
        return operation;
    }

    public void setOperation(int operation) {
        this.operation = operation;
    }

    public int getToNode() {
        return toNode;
    }

    public void setToNode(int toNode) {
        this.toNode = toNode;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public long getHash() {
        return hash;
    }

    public void setHash(long hash) {
        this.hash = hash;
    }

    public int getExclude() {
        return exclude;
    }

    public void setExclude(int exclude) {
        this.exclude = exclude;
    }

    public List<NodeType> getTypes() {
        return types;
    }

    public void setTypes(List<NodeType> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
