package gusl.router.publish;

/**
 * Why not an Enum I hear you cry.
 *
 * Ints are lighter and faster on the wire.
 *
 * @author dhudson
 */
public interface PublishRequestCommands {

    public static final int PUBLISH_ONLY_TO = 1;
    public static final int PUBLISH_ALL_NODES_OF_TYPE = 2;
    public static final int PUBLISH_ALL_NODES_OF_TYPE_EXCEPT = 3;
    public static final int PUBLISH_TO_MASTER_NODE = 4;
    public static final int PUBLISH_TO_ONE_NODE_OF_TYPE = 5;
    public static final int PUBLISH_TO_BALANCED_NODE_OF_TYPE = 6;
    public static final int PUBLISH_TO_ALL_GIVEN_NODES = 7;
    public static final int PUBLISH_TO_NODES_EXCEPT_GIVEN = 8;
}
