package gusl.router.route;

import java.util.Collections;
import java.util.List;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

/**
 *
 * Instead of trying to be clever with adding a removing nodes with locks, just
 * have an immutable one instead.
 *
 * @author dhudson
 */
public abstract class AbstractImmutableLoadBalancer implements NodeLoadBalancer {

    private final NodeType theType;
    private final List<ResourceNode> theNodes;

    public AbstractImmutableLoadBalancer(NodeType type, List<ResourceNode> nodes) {
        theType = type;
        theNodes = Collections.unmodifiableList(nodes);
    }

    @Override
    public List<ResourceNode> getNodes() {
        return theNodes;
    }

    @Override
    public NodeType getType() {
        return theType;
    }
}
