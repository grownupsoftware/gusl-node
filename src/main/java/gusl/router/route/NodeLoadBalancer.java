package gusl.router.route;

import java.util.List;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

/**
 * Contract for load balancers.
 *
 * @author dhudson
 */
public interface NodeLoadBalancer {

    /**
     * Return the load balanced node.
     *
     * Given an ID, return the load balanced node.
     *
     * @param id
     * @return load balanced Casanova Node
     */
    public ResourceNode getNodeFor(long id);

    /**
     * Get the node type.
     *
     * @return the node type
     */
    public NodeType getType();

    /**
     * Return the list of nodes.
     *
     * @return the list of nodes
     */
    public List<ResourceNode> getNodes();

}
