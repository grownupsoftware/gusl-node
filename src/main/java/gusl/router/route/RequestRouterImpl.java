package gusl.router.route;

import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.router.model.ApplicationNodeHelper;
import gusl.router.model.HttpNodeRequest;
import org.jvnet.hk2.annotations.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This contains the Active nodes in the construct.
 * <p>
 * There is a chance that getFor and set nodes could be called at the same time,
 * but if the node returned is null, it will route the request to router. This
 * then allows us to have a wait free lock free implementation of a router for
 * performance reasons.
 * </p>
 *
 * @author dhudson
 */
@Service
public class RequestRouterImpl implements RequestRouter {

    private final HashMap<NodeType, NodeLoadBalancer> theLoadBalancers;
    private List<ResourceNode> theNodes;

    public RequestRouterImpl() {
        theLoadBalancers = new HashMap<>();
        theNodes = new ArrayList<>(0);
    }

    @Override
    public void setNodes(List<ResourceNode> nodes) {
        theNodes = ApplicationNodeHelper.getActiveNodes(nodes);
        Map<NodeType, List<ResourceNode>> activeNodesByType = ApplicationNodeHelper.groupByActiveNodesByType(nodes);
        theLoadBalancers.clear();
        for (Entry<NodeType, List<ResourceNode>> entry : activeNodesByType.entrySet()) {
            theLoadBalancers.put(entry.getKey(), LoadBalanceFactory.getLoadBalancerFor(entry.getKey(), entry.getValue()));
        }
    }

    @Override
    public ResourceNode getFor(HttpNodeRequest request) {
        if (request.getNodeId() != 0) {
            return get(request.getNodeId());
        }

        return getFor(request.getNodeType(), request.getLoadBalanceHash());
    }

    @Override
    public ResourceNode getFor(NodeType type, long hash) {
        NodeLoadBalancer lb = theLoadBalancers.get(type);
        if (lb == null) {
            return null;
        }
        return lb.getNodeFor(hash);
    }

    @Override
    public ResourceNode get(int nodeId) {
        for (ResourceNode node : theNodes) {
            if (node.getNodeId() == nodeId) {
                return node;
            }
        }

        return null;
    }
}
