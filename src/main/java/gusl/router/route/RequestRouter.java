package gusl.router.route;

import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.router.model.HttpNodeRequest;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 * Instead of trying to keep track of nodes and their state, just keep a list of
 * active nodes at all time.
 *
 * @author dhudson
 */
@Contract
public interface RequestRouter {

    void setNodes(List<ResourceNode> nodes);

    ResourceNode getFor(NodeType type, long hash);

    ResourceNode get(int nodeId);

    ResourceNode getFor(HttpNodeRequest request);
}
