package gusl.router.route;

import java.util.List;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

/**
 * Route a requests, consistently given an unique ID of something, like account
 * number.
 *
 * @author dhudson
 */
public class IdCentricLoadBalancer extends AbstractImmutableLoadBalancer {

    // No other reason than its close to Casanovas birthday
    private static final long PRIME_NUMBER = 1723;

    public IdCentricLoadBalancer(NodeType type, List<ResourceNode> nodes) {
        super(type, nodes);
    }

    /**
     * Return the clustered node for a given key.
     *
     * Using the same id will ensure that the same clusterable is always
     * returned.
     *
     * This is true if the cluster is not changed in any way, but if the cluster
     * id added to or a node removed, then the clusterable will change.
     *
     * @param id
     * @return
     */
    @Override
    public ResourceNode getNodeFor(long id) {

        int size = getNodes().size();
        if (size == 0) {
            return null;
        } else if (size == 1) {
            return getNodes().get(0);
        }

        int index = (int) ((id % PRIME_NUMBER) % size);
        return getNodes().get(index);

    }

}
