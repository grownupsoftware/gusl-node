package gusl.router.route;

import gusl.node.async.AsyncTimeoutConstants;
import gusl.model.routerconfig.ResourceNode;

/**
 *
 * @author dhudson
 */
class NodeRequest {

    private long balanceHash;
    private ResourceNode node;
    // Payload
    private Object request;
    // Playload
    private Class<?> response;
    private String params;
    private String path;
    private int timeoutSeconds = AsyncTimeoutConstants.DEFAULT_TIMEOUT;

    public NodeRequest() {
    }

    public long getBalanceHash() {
        return balanceHash;
    }

    public void setBalanceHash(long balanceHash) {
        this.balanceHash = balanceHash;
    }

    public ResourceNode getNode() {
        return node;
    }

    public void setNode(ResourceNode node) {
        this.node = node;
    }

    public Object getRequest() {
        return request;
    }

    public void setRequest(Object request) {
        this.request = request;
    }

    public Class<?> getResponse() {
        return response;
    }

    public void setResponse(Class<?> response) {
        this.response = response;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public void setTimeoutSeconds(int timeoutSeconds) {
        this.timeoutSeconds = timeoutSeconds;
    }

}
