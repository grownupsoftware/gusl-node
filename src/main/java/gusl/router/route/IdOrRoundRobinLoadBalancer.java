package gusl.router.route;

import java.util.List;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

/**
 * If the ID is zero, then round robin will be used, otherwise Id Centric
 * Balancing will happen.
 *
 * @author dhudson
 */
public class IdOrRoundRobinLoadBalancer extends IdCentricLoadBalancer {

    // Start off at an odd number for the requests, otherwise all of the cache load events hit the same DS
    private long theRequestCount = System.currentTimeMillis();

    public IdOrRoundRobinLoadBalancer(NodeType type, List<ResourceNode> nodes) {
        super(type, nodes);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public ResourceNode getNodeFor(long id) {
        if (id == 0) {
            return super.getNodeFor(theRequestCount++);
        }

        return super.getNodeFor(id);
    }
}
