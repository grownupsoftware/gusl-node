package gusl.router.route;

import java.util.List;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

/**
 * Load Balancer just to share the requests.
 *
 * The id is ignored.
 *
 * @author dhudson
 */
public class RoundRobinLoadBalancer extends IdCentricLoadBalancer {

    private long theRequestCount = 0;

    public RoundRobinLoadBalancer(NodeType type, List<ResourceNode> nodes) {
        super(type, nodes);
    }

    /**
     * Ignore the ID, just cycle through the available nodes.
     *
     * @param id
     * @return
     */
    @Override
    public ResourceNode getNodeFor(long id) {
        return super.getNodeFor(theRequestCount++);
    }

}
