package gusl.router.route;

import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;

import java.util.List;

/**
 * @author dhudson
 */
public class LoadBalanceFactory {

    private LoadBalanceFactory() {
    }

    public static NodeLoadBalancer getLoadBalancerFor(NodeType type, List<ResourceNode> activeNodes) {
        switch (type) {
            case DATASERVICE:
            case LEDGER:
                return new IdOrRoundRobinLoadBalancer(type, activeNodes);
            default:
                return new RoundRobinLoadBalancer(type, activeNodes);
        }
    }

}
