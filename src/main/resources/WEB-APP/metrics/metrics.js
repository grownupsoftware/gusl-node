var app = angular.module('metricsApp', [])

    .controller('mainController', function ($scope, $http) {
        $scope.sortType = 'name'; // set the default sort type
        $scope.sortReverse = false; // set the default sort order
        $scope.searchValue = ''; // set the default search/filter term

        $scope.constructType = '';
        $scope.constructName = '';
        $scope.constructColour = ''; // Construct colour

        var firstTime = true;
        var graphData = [], chart = [], options = [];
        var gaugeData = [], gaugeChart = [], gaugeOptions = [];
        var next = 1;
        $scope.metrics = [];

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/info'
        }).then(function successCallback(response) {
            //console.log('response', response);
            var details = response.data.nodeDetails;
            $scope.constructType = details['operationalMode'] + '-' + details['instanceType'];
            $scope.constructName = details['constructName'];
            $scope.constructColour = constructToColour($scope.constructName);
        });

        function processMetrics(metric, type) {
            for (var name in metric) {
                if (metric.hasOwnProperty(name)) {
                    //console.log('name: ' + name + ' data: ', metric[name]);
                    if (firstTime) {
                        drawGraphics(metric[name], name, type);
                    }

                    if (type !== "gauges") {
                        graphData[name].addRows([[next, metric[name]["count"]]]);
                        chart[name].draw(graphData[name], options[name]);
                    }

                    if (type === "gauges") {
                        $('div[id="total_' + name + '"] input').val(metric[name]["value"]);
                    } else {
                        $('div[id="total_' + name + '"] input').val(metric[name]["count"]);
                    }

                    if (type === "timers") {
                        //counters do not have mean values
                        gaugeData[name].setValue(0, 1, millisecond(metric[name]["snapshot"]["min"]));
                        gaugeData[name].setValue(1, 1, millisecond(metric[name]["snapshot"]["mean"]));
                        gaugeData[name].setValue(2, 1, millisecond(metric[name]["snapshot"]["max"]));

                        if (gaugeOptions[name].max < metric[name]["snapshot"]["max"]) {
                            gaugeOptions[name].max = Math.round(metric[name]["snapshot"]["max"]);
                        }

                        gaugeChart[name].draw(gaugeData[name], gaugeOptions[name]);
                    }
                    if (type !== "counters" && type !== "timers" && type !== "gauges") {
                        //counters do not have mean values
                        gaugeData[name].setValue(0, 1, roundThree(metric[name]["mean-rate"]));
                        gaugeData[name].setValue(1, 1, roundThree(metric[name]["one-minute-rate"]));
                        gaugeData[name].setValue(2, 1, roundThree(metric[name]["five-minute-rate"]));

                        if (gaugeOptions[name].max < metric[name]["one-minute-rate"]) {
                            gaugeOptions[name].max = Math.round(metric[name]["one-minute-rate"]);
                        }

                        gaugeChart[name].draw(gaugeData[name], gaugeOptions[name]);
                    }
                    next++;
                }
            }
            if (firstTime) {
                $('.wrapper h4').unbind().click(function () {
                    var div = $(this).parent().parent();
                    if (div.hasClass("hidden")) {
                        div.removeClass("hidden");
                        if ($('div.hidden').length > 0) {
                            div.insertBefore($('div.hidden:first'));
                        }
                    } else {
                        div.addClass("hidden");
                        $('#charts').append(div);
                    }
                });
            }
        }

        function firstTimeDraw(data) {
            for (var index = 0; index < data.length; index++) {
                drawGraphics(data[index]);
            }
        }

        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }

        function constructToColour(constructName) {
            if (constructName) {
                var i, l, hval = 0x811c9dc5;
                for (i = 0, l = constructName.length; i < l; i++) {
                    hval ^= constructName.charCodeAt(i);
                    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
                }
                return '#' + ("00000" + (hval >>> 0).toString(16)).substr(-6);
            }
            return '#00000';
        }

        function displayHeadings() {
            // Need to set the heading by host name - not ideal!
            var name = '';
            // console.log('Host: ' + window.location.host);
            // console.log('Path: ' + window.location.pathname);
            var path = window.location.pathname.split("/")[1];
            switch (path) {
                case "edge":
                    name = 'Edge';
                    break;
                case "router":
                    name = 'Router';
                    break;
                case "ordermanager":
                    name = 'Order Manager';
                    break;
                case "dataservice":
                    name = 'Data Service';
                    break;
            }
            console.log('Name: ' + name);
            document.title = name + ' Metrics';
            var h1 = $('<h1>' + name + ' Metrics</h1>');
            $('#heading').append(h1);
        }

        function drawGraphics(metric, name, type, id) {
            var e;
            if (type === "gauges") {
                e = $('<div class="wrapper">' +
                    '<div><h3>' + name + '</h3><h4 class="close">[X]</h4></div>' +
                    '<div id="total_' + name + '" class="total">' +
                    '<p>Total:</p><input type="text" value="' + metric["value"] + '" readOnly="true"/>' +
                    '</div>' +
                    '</div>');

            } else {
                e = $('<div class="wrapper">' +
                    '<div><h3>' + name + '</h3><h4 class="close">[X]</h4></div>' +
                    '<div id="chart_' + name + '" class="first"></div>' +
                    '<div id="total_' + name + '" class="total">' +
                    '<p>Total:</p><input type="text" value="' + metric["count"] + '" readOnly="true"/>' +
                    '</div>' +
                    '<div id="gauge_' + name + '" class="second"></div>' +
                    '</div>');

            }

            if (id !== undefined) {
                console.log('------------------------------------->', id, e);
                //$('#'+id).append(e);
            } else {
                //$('#charts').append(e);
            }

            if (type !== "gauges") {
                drawTrendlines(metric, name);
            }
            if (type !== "counters" && type !== "gauges") {
                drawGauges(metric, name, type);
            }

        }

        function drawTrendlines(metric, name) {

            graphData[name] = new google.visualization.DataTable();
            graphData[name].addColumn('number', 'X');
            graphData[name].addColumn('number', 'Total');

            graphData[name].addRows([
                [0, metric["count"]]
            ]);

            options[name] = {
                chart: {
                    title: 'total'
                },
                hAxis: {
                    title: 'Time'
                },
                vAxis: {
                    title: 'Total'
                },
                legend: {
                    position: 'none'
                }
            };

            chart[name] = new google.visualization.LineChart(document.getElementById('chart_' + name));
            chart[name].draw(graphData[name], options[name]);
        }

        function drawGauges(metric, name, type) {

            var dataArray;
            var max = 1;
            if (type === "timers") {
                dataArray = [['Label', 'Value'],
                    ["Min", millisecond(metric["snapshot"]["min"])],
                    ["Mean", millisecond(metric["snapshot"]["mean"])],
                    ["Max", millisecond(metric["snapshot"]["max"])]
                ];
                if (metric["snapshot"]["max"] > 0) {
                    max = Math.round(metric["snapshot"]["max"] * 5);
                }
            } else {
                dataArray = [['Label', 'Value'],
                    ["Mean Rate", roundThree(metric["mean-rate"])],
                    ["1 Min Rate", roundThree(metric["one-minute-rate"])],
                    ["5 Min Rate", roundThree(metric["five-minute-rate"])]
                ];
                if (metric["one-minute-rate"] > 0) {
                    max = Math.round(metric["one-minute-rate"] * 5);
                }
            }

            gaugeData[name] = google.visualization.arrayToDataTable(dataArray);
            gaugeChart[name] = new google.visualization.Gauge(document.getElementById('gauge_' + name));
            gaugeOptions[name] = {
                width: 400, height: 120,
                minorTicks: 10, max: max
            };

            gaugeChart[name].draw(gaugeData[name], gaugeOptions[name]);
        }

        function roundThree(value) {
            return Math.round(value * 1000) / 1000;
        }

        function millisecond(value) {
            return Math.round(value / 1000000);
        }

        function startWebSockets() {
            console.log('starting...');

            // establish the communication channel over a websocket
            var ws = new WebSocket("ws://" + window.location.host + getContextPath() + "/wsmetrics");

            // called when socket connection established
            ws.onopen = function () {
                console.log("Connected to metrics service!");
                // setting headings by host
                displayHeadings();
            };

            // called when a message received from server
            ws.onmessage = function (evt) {
                var data = JSON.parse(evt.data);

                console.log('Gauges: ', data.gauges);
                console.log('Counters: ', data.counters);
                console.log('Histograms: ', data.histograms);
                console.log('Meters: ', data.meters);
                console.log('Timers: ', data.timers);


                updateMetricsTable(data.meters, "meters");
                //timers have the same data
                updateMetricsTable(data.timers, "timers");
                updateMetricsTable(data.counters, "counters");
                updateMetricsTable(data.gauges, "gauges");

                // process each group - could be a loop if the data is all the same
                processMetrics(data.meters, "meters");
                //timers have the same data
                processMetrics(data.timers, "timers");
                processMetrics(data.counters, "counters");
                processMetrics(data.gauges, "gauges");

                firstTime = false;
            };

            // called when socket connection closed
            ws.onclose = function () {
                console.log("Disconnected from metrics service!");
            };

            // called in case of an error
            ws.onerror = function (err) {
                console.log("ERROR!", err);
            };

            // sends msg to the server over websocket
            function sendToServer(msg) {
                ws.send(msg);
            }
        }

        function updateMetricsTable(metric, type) {
            for (var name in metric) {
                if (metric.hasOwnProperty(name)) {
                    var metricRow;
                    if (type === 'gauges') {
                        metricRow = {
                            name: name,
                            counter: metric[name]["value"],
                            data: metric,
                            type: type,
                            filter: true
                        };

                    } else {
                        metricRow = {
                            name: name,
                            counter: metric[name]["count"],
                            data: metric,
                            type: type,
                            filter: true
                        };

                    }
                    updateMetricsObject(metricRow, metric, type);
                }
            }
        }

        google.setOnLoadCallback(startWebSockets);

        function updateMetricsObject(metricRow, metric, type) {
            var found = false;
            for (var i = 0; i < $scope.metrics.length; i++) {
                if ($scope.metrics[i].name === metricRow.name) {
                    $scope.metrics[i].counter = metricRow.counter;
                    $scope.metrics[i].data = metricRow;
                    $scope.metrics[i].type = type;
                    found = true;
                    break;
                }
            }
            if (!found) {
                metricRow.show = false;
                $scope.metrics.push(metricRow);
            }
            $scope.$apply();
        }

        $scope.toggle = function (name) {
            for (var i = 0; i < $scope.metrics.length; i++) {
                if ($scope.metrics[i].name === name) {
                    $scope.metrics[i].show = !$scope.metrics[i].show;
                }
            }
        }
        $scope.change = function () {
            for (var i = 0; i < $scope.metrics.length; i++) {
                if ($scope.metrics[i].name.indexOf($scope.searchValue) < 0) {
                    $scope.metrics[i].filter = false;
                } else {
                    $scope.metrics[i].filter = true;
                }
            }
        }
    });

google.load('visualization', '1', {packages: ['corechart', 'line', 'gauge']});

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                $window.location.href = getContextPath() + path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
