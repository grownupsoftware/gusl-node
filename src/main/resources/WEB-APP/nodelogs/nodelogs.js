var app = angular.module('sortApp', [])

    .controller('mainController', function ($scope, $http) {
        $scope.sortType = 'time'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.searchValue = '';     // set the default search/filter term

        $scope.listOfOptions = ['DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'];
        $scope.nodeLogs = [];
        $scope.image = "pin-up";
        $scope.isPaused = false;

        $scope.constructType = '';
        $scope.constructName = '';
        $scope.constructColour = ''; // Construct colour

        $scope.selectedItemChanged = function () {
            console.log('Log level selected ' + $scope.selectedItem);
            $scope.ws.send($scope.selectedItem);
        };

        $scope.pause = function () {
            $scope.isPaused = !$scope.isPaused;
            if ($scope.isPaused) {
                $scope.image = "pin-down";
            } else {
                $scope.image = "pin-up";
                $scope.$apply();
            }
        };

        $scope.changeLogger = function (nodeLog) {
            let dto = {};
            dto['name'] = nodeLog['loggerName']

            $http({
                method: 'POST',
                url: getContextPath() + '/rest/logControl/toggle',
                data: dto
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        }

        $scope.copyToClipboard = function(nodeLog) {
            var textArea = document.createElement("textarea");
            textArea.value = nodeLog.message;
            let node = document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            document.execCommand('copy');
            document.body.removeChild(node);
        }

        function startWebSockets() {
            console.log('starting...');

            // establish the communication channel over a websocket
            var ws = new WebSocket("ws://" + window.location.host + getContextPath() + "/wslogs");

            // called when socket connection established
            ws.onopen = function () {
                console.log("Connected to Node Log Listener!");
                $scope.ws = this;
            };

            // called when a message received from server
            ws.onmessage = function (evt) {
                var index = $scope.nodeLogs.push(JSON.parse(evt.data));
                if (index > 20) {
                    // Remove the last one
                    $scope.nodeLogs.shift();
                }

                if (!$scope.isPaused) {
                    $scope.$apply();
                }
            };

            // called when socket connection closed
            ws.onclose = function () {
                console.log("Disconnected from Node");
                //alert("Lost connection to Node");
            };

            // called in case of an error
            ws.onerror = function (err) {
                console.log("ERROR!", err);
            };

            // sends msg to the server over websocket
            function sendToServer(msg) {
                ws.send(msg);
            }
        }

        function displayHeadings() {
            var context = window.location.pathname.split("/")[1];
            // Capitalise
            var name = context.charAt(0).toUpperCase() + context.slice(1) + ' Logs';
            document.title = name;
            var h1 = $('<h1>' + name + '</h1>');
            $('#heading').append(h1);
        }

        displayHeadings();

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/info'
        }).then(function successCallback(response) {
            //console.log('response', response);
            var details = response.data.nodeDetails;
            $scope.constructType = details['operationalMode'] + '-' + details['instanceType'];
            $scope.constructName = details['constructName'];
            $scope.constructColour = constructToColour($scope.constructName);
        });

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/logs'
        }).then(function successCallback(response) {
            // console.log('response', response);

            response.data.logs.forEach(function (logDTO) {
                $scope.nodeLogs.push(logDTO);
            });

            $scope.selectedItem = response.data.level;

        }, function errorCallback(response) {
        });

        startWebSockets();

        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }

        function constructToColour(constructName) {
            if (constructName) {
                let i, l, hval = 0x811c9dc5;
                for (i = 0, l = constructName.length; i < l; i++) {
                    hval ^= constructName.charCodeAt(i);
                    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
                }
                return '#' + ("00000" + (hval >>> 0).toString(16)).substr(-6);
            }
            return '#00000';
        }
    });

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                $window.location.href = getContextPath() + path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
