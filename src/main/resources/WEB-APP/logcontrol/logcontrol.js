var app = angular.module('sortApp', [])

    .controller('mainController', function ($scope, $http) {
        $scope.sortType = 'time'; // set the default sort type
        $scope.sortReverse = true;  // set the default sort order
        $scope.searchValue = '';     // set the default search/filter term

        $scope.listOfOptions = ['DEBUG', 'INFO', 'WARN', 'ERROR', 'FATAL'];
        $scope.nodeLoggers = [];

        $scope.constructType = '';
        $scope.constructName = '';
        $scope.constructColour = ''; // Construct colour

        function displayHeadings() {
            var context = window.location.pathname.split("/")[1];
            // Capitalise
            var name = context.charAt(0).toUpperCase() + context.slice(1) + ' Control';
            document.title = name;
            var h1 = $('<h1>' + name + '</h1>');
            $('#heading').append(h1);
        }

        displayHeadings();

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/info'
        }).then(function successCallback(response) {
            //console.log('response', response);
            var details = response.data.nodeDetails;
            $scope.constructType = details['operationalMode'] + '-' + details['instanceType'];
            $scope.constructName = details['constructName'];
            $scope.constructColour = constructToColour($scope.constructName);
        });

        $http({
            method: 'POST',
            url: getContextPath() + '/rest/logControl/getAll'
        }).then(function successCallback(response) {
            console.log('response', response);

            response.data.loggers.forEach(function (loggerDTO) {
                $scope.nodeLoggers.push(loggerDTO);
            });

        }, function errorCallback(response) {
        });

        $scope.revert = function (loggerDTO) {
            loggerDTO.level = null;

            $http({
                method: 'POST',
                url: getContextPath() + '/rest/logControl/change',
                data: loggerDTO
            }).then(function successCallback(response) {
                loggerDTO.level = response.data.level;
            }, function errorCallback(response) {
            });
        };

        $scope.revertAll = function (loggerDTO) {
            loggerDTO.level = null;

            $http({
                method: 'POST',
                url: getContextPath() + '/rest/logControl/changeAll',
                data: loggerDTO
            }).then(function successCallback(response) {
                loggerDTO.level = response.data.level;
            }, function errorCallback(response) {
            });
        };

        $scope.changeLogger = function (loggerDTO) {
            loggerDTO.level = "DEBUG";

            $http({
                method: 'POST',
                url: getContextPath() + '/rest/logControl/change',
                data: loggerDTO
            }).then(function successCallback(response) {
                loggerDTO.level = response.data.level;
            }, function errorCallback(response) {
            });
        };

        $scope.changeAll = function (loggerDTO) {
            loggerDTO.level = "DEBUG";

            $http({
                method: 'POST',
                url: getContextPath() + '/rest/logControl/changeAll',
                data: loggerDTO
            }).then(function successCallback(response) {
                loggerDTO.level = response.data.level;
            }, function errorCallback(response) {
            });
        };

        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }

        function constructToColour(constructName) {
            if (constructName) {
                var i, l, hval = 0x811c9dc5;
                for (i = 0, l = constructName.length; i < l; i++) {
                    hval ^= constructName.charCodeAt(i);
                    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
                }
                return '#' + ("00000" + (hval >>> 0).toString(16)).substr(-6);
            }
            return '#00000';
        }
    });

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                $window.location.href = getContextPath() + path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
