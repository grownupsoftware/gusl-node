var app = angular.module('latencyApp', [])

    .controller('mainController', function ($scope, $http) {
        $scope.sortType = 'name'; // set the default sort type
        $scope.sortReverse = false; // set the default sort order
        $scope.searchValue = ''; // set the default search/filter term


        $scope.hasChildren = function (metric) {
            var show = false;
            metric['metrics'].forEach(function (bucket) {
                if (bucket.total > 0) {
                    show = true;
                }
            });
            return show;
        }

        function load() {
            var path = window.location.pathname.split("/")[1];
            $http({
                method: 'GET',
                url: '/' + path + '/rest/latency'
            }).then(function successCallback(response) {
                console.log('response', response);
                var metrics = [];
                response.data.forEach(function (metric) {
                    metrics.push(metric);
                });
                $scope.metrics = metrics;
            }, function errorCallback(response) {
            });

        }

        function resetMeters() {
            var path = window.location.pathname.split("/")[1];

            $http({
                method: 'POST',
                url: '/' + path + '/rest/latency/reset'
            }).then(function successCallback(response) {
                load();
            }, function errorCallback(response) {
            });
        }

        $scope.toggle = function (name) {
            for (var i = 0; i < $scope.metrics.length; i++) {
                if ($scope.metrics[i].name === name) {
                    $scope.metrics[i].show = !$scope.metrics[i].show;
                }
            }
        }
        $scope.change = function () {
            for (var i = 0; i < $scope.metrics.length; i++) {
                if ($scope.metrics[i].name.indexOf($scope.searchValue) < 0) {
                    $scope.metrics[i].filter = false;
                } else {
                    $scope.metrics[i].filter = true;
                }
            }
        }

        function displayHeadings() {
            // Need to set the heading by host name - not ideal!
            var name = '';
            console.log('Host: ' + window.location.host);
            console.log('Path: ' + window.location.pathname);
            var path = window.location.pathname.split("/")[1];
            switch (path) {
                case "edge":
                    name = 'Edge';
                    break;
                case "admin":
                    name = 'Admin';
                    break;
                case "ordermanager":
                    name = 'Order Manager';
                    break;
                case "dataservice":
                    name = 'Data Service';
                    break;
            }
            console.log('Name: ' + name);
            document.title = name + ' Latency';
            var h1 = $('<h1>' + name + ' Latency</h1>');
            $('#heading').append(h1);
        }


        $scope.reset = function () {
            console.log('resetting');
            resetMeters();
            load();
        }
        $scope.refresh = function () {
            console.log('refresh');
            load();
        }
        $scope.download = function () {
            console.log('download');
        }

        displayHeadings();
        load();
    });

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                $window.location.href = getContextPath() + path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
